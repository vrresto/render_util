/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 430

#include atmosphere_definitions.glsl
#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl
#include util_definitions.glsl


vec3 blend_rnm(vec3 n1, vec3 n2);

layout(location = 0) out vec4 out_color_lit;
#if @enable_unlit_output@
layout(location = 1) out vec4 out_color_unlit;
#endif

in vec3 pass_pos_view;
in vec3 pass_pos_flat;
in vec3 pass_pos;
in float pass_vertex_horizontal_dist;
in TerrainVertexResult pass_vertex_result;
in float pass_terrain_lod;

#if @terrain_enable_vertex_color@
in vec3 pass_vertex_color;
#endif

uniform float curvature_map_max_distance;
uniform Terrain terrain;

uniform vec2 viewport_size;
uniform vec3 camera_pos;
uniform vec3 sun_dir;

#if @terrain_enable_water_map_pass@
uniform sampler2D water_map_pass_fb;
#endif


void main(void)
{
  if (pass_vertex_horizontal_dist + 20000.0 > curvature_map_max_distance)
    discard;

  vec3 x_tangent = dFdx( pass_pos);
  vec3 y_tangent = dFdy( pass_pos);
  vec3 face_normal = normalize(cross(x_tangent, y_tangent));

  float detail_blend = getDetailMapBlend(pass_pos_flat.xy);

#if @terrain_enable_texture@
  TerrainTextureFragmentData texture_fragment_data;
  fetchImplicitDerivatives(texture_fragment_data);
  texture_fragment_data.terrain_height =
      getTerrainHeight(pass_vertex_result, detail_blend);
  texture_fragment_data.detail_map_blend = detail_blend;
#endif

  out_color_lit = vec4(0.5, 0.5, 0.5, 1.0);
#if @enable_unlit_output@
  out_color_unlit = vec4(0.5, 0.5, 0.5, 1.0);
#endif

  vec3 normal = getTerrainNormal(pass_vertex_result, detail_blend);

  vec3 color = vec3(0.5);

#if @terrain_enable_texture@
  #if @far_noise_texture_is_normal_map@
    vec3 far_noise_normal = getTerrainFarNoiseNormal(pass_vertex_result, detail_blend);
    normal = blend_rnm(far_noise_normal, normal);
  #endif
  color = getTerrainTextureColor(texture_fragment_data);
#endif

//   vec2 base_coords = getHeightMapTextureCoords(terrain.base_layer, pass_pos_flat.xy);
//   vec4 base_type = texture(terrain.base_layer.type_map.sampler, base_coords);

//   float land_blend = base_type[2];
//   land_blend = smoothstep(0.8, 1.0, land_blend);

//   normal.xy *= land_blend;
//   normal = normalize(normal);

//   vec3 rock = vec3(95) / 255.0;
//   color = mix(rock, color, dot(normal, vec3(0,0,1)));

  LightParams light;
  getIncomingLight(pass_pos, light);

  vec3 light_direct;
  vec3 light_ambient;
  getReflectedLightTerrain(light, normal, light_direct, light_ambient);
  vec3 light_combined = clamp(light_direct + light_ambient, vec3(0), vec3(1));

  out_color_lit.rgb = color * light_combined;
  out_color_lit.rgb = clamp(out_color_lit.rgb * 1.1, vec3(0), vec3(1));

  float dist = length(pass_pos_view);

#if @terrain_enable_water@
  vec3 view_dir = -normalize(camera_pos - pass_pos);

  TerrainWaterFragmentParameters water;

  #if @terrain_enable_land@
    float water_map_value = sampleWaterMap(detail_blend);
    water.water_map_value = water_map_value;
    water.water_depth = clamp(water_map_value - 0.5, 0.0, 0.5) * 2.0;
    water.ground_color = out_color_lit.rgb;
    #if @terrain_enable_shore_waves@
      getShoreWaveAlpha(water);
    #endif

    #if @terrain_enable_water_map_pass@
      vec2 water_map_pass_fb_coords = gl_FragCoord.xy / viewport_size;
      float water_alpha = textureLod(water_map_pass_fb, water_map_pass_fb_coords, 1.0).r;
    #elif @terrain_enable_water_map@
      float water_alpha = getWaterAlpha(water_map_value);
    #endif
  #endif

  #if @terrain_water_enable_ice_cover@
    vec3 water_color = getWaterIceCoverColor(view_dir, light);
  #else
    getWaterNormalAndFoam(water, view_dir, dist);
    vec3 water_color = getWaterColor(water, light, view_dir, dist);
  #endif

  #if @terrain_enable_land@ && @terrain_enable_shore_waves@
    water_color = mix(water_color, vec3(1), water.shore_wave_alpha);
  #endif

  #if @terrain_enable_land@
    out_color_lit.rgb = mix(out_color_lit.rgb, water_color, water_alpha);
  #else
    out_color_lit.rgb = water_color;
  #endif

#if @debug_small_water_map@
  out_color_lit.r = sampleSmallWaterMap();
#endif
#endif // terrain_enable_water

#if @terrain_enable_forest@
  out_color_lit.rgb = applyForest(out_color_lit.rgb, light_combined, face_normal, dist);
#endif

#if @terrain_enable_lod_debug@
  float hue = pass_terrain_lod / float(@terrain_lod_count@);
  vec3 hsv = vec3(hue, 1.0, 1.0);
  out_color_lit.rgb *= hsv2rgb(hsv) * 1.5;
#endif

#if @terrain_enable_material_map_debug@
  out_color_lit.rgb *= getMaterialMapDebugColor();
#endif

#if @terrain_enable_vertex_color@
  out_color_lit.rgb *= pass_vertex_color;
#endif

#if @enable_unlit_output@
  #error unimplemented
//   out_color_unlit.rgb = color * light_ambient;
#endif

#if !@debug_small_water_map@
#if @enable_unlit_output@
  #error unimplemented
//   fogAndToneMap(out_color_lit.rgb, out_color_unlit.rgb, out_color_lit.rgb, out_color_unlit.rgb);
#else
#if @enable_atmosphere@
  out_color_lit.rgb = fogAndToneMap(out_color_lit.rgb, camera_pos, pass_pos);
#endif
#endif
#endif
}

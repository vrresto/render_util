#version 330

#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl

layout(location = 0) out vec4 out_color0;

varying vec3 pass_pos_flat;


void main(void)
{
  out_color0 = vec4(0,0,0,1);

#if @terrain_enable_water_map@
  float detail_blend = getDetailMapBlend(pass_pos_flat.xy);
  out_color0.r = getWaterAlpha(sampleWaterMap(detail_blend));
//   out_color0.r = (sampleWaterMap(detail_blend));
#endif
}

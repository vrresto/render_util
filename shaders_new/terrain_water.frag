/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *    Used techiques
 *
 *    Schlick's approximation:
 *    http://www.cs.virginia.edu/~jdl/bib/appearance/analytic%20models/schlick94b.pdf
 */

#version 330

#if !@terrain_enable_water@
  #error Water is not enabled
#endif

#include light_definitions.glsl
#include terrain_water_definitions.glsl
#include terrain_definitions.glsl

// #define DEBUG_GEOMETRY 1

#define LOW_DETAIL !@terrain_enable_detailed_water@
#define ENABLE_WAVES !LOW_DETAIL


const float SPEC_HARDNESS = 256.0;
const float ior_water = 1.333;
const float schlick_r0_water = pow(ior_water - 1, 2) / pow(ior_water + 1, 2);


uniform sampler2D sampler_water;
uniform sampler2D sampler_water_noise;

uniform vec3 water_color;
uniform vec3 sun_dir;


varying TerrainVertexResult pass_vertex_result;


vec3 calcWaterEnvColor(in LightParams light)
{
  vec3 color = vec3(0.65, 0.85, 1.0);
  color = mix(color, vec3(0.7), 0.6);
  vec3 color_low = color * vec3(0.8, 0.7, 0.5) * 0.5;
  color =  mix(color_low, color, smoothstep(0.0, 0.4, sun_dir.z));
  color *= smoothstep(-0.4, 0.2, sun_dir.z);

//   color *= 0.3;

  return color;
}


float fresnelSchlick(vec3 incoming, vec3 normal, float r0)
{
  float cos_angle = dot(normal, incoming);
  float r = r0 + (1 - r0) * pow(1 - cos_angle, 5);
  return clamp(r, 0, 1);
}


float calcSpecular(vec3 view_dir, vec3 normal, float hardness)
{
    vec3 R = reflect(view_dir, normal);
    vec3 lVec = -sun_dir;

    return pow(max(dot(R, lVec), 0.0), hardness);
}


#if !@terrain_water_enable_ice_cover@
vec3 getWaterColor(inout TerrainWaterFragmentParameters params,
                   in LightParams light, vec3 view_dir, float dist)
{
#if DEBUG_GEOMETRY
  return light.ambient + params.foam.rgb * light.sun * 0.5 * max(dot(params.normal, sun_dir), 0.0);
#endif

#if @terrain_enable_land@
  float water_depth = 1 - pow(1 - params.water_depth, 2);

  vec3 extincion = vec3(2.0, 0.6, 0.5);

  vec3 ground_color = params.ground_color *= 0.95;
  ground_color *= exp(-extincion * water_depth);
#endif

  float fresnel = fresnelSchlick(-view_dir, params.normal, schlick_r0_water);

  float specularDetailFactor = exp(-3 * (dist/30000));
  float specHardness = mix(SPEC_HARDNESS * 0.4, SPEC_HARDNESS, specularDetailFactor);
  float specular = calcSpecular(-view_dir, params.normal, specHardness);
  specular = mix(specular * 0.5, specular, specularDetailFactor);

#if ENABLE_SKY_REFLECTION
//   vec3 env_color = calcWaterEnvColor(pos, params.normal, view_dir);
#else
  vec3 env_color = calcWaterEnvColor(light);
#endif

  vec3 refraction_color = water_color;
  refraction_color *= light.ambient + light.sun * 0.5 * max(dot(params.normal, sun_dir), 0.0);
#if @terrain_enable_land@
  refraction_color = mix(ground_color, refraction_color, water_depth);
#endif

  vec3 color = mix(refraction_color, env_color, fresnel);
  color += specular * light.sun;

  vec3 foam_color = params.foam.rgb *
                      light.ambient
                      + light.sun * 0.8
                      + light.sun * 0.1 * max(dot(params.normal, sun_dir), 0.0);

  return mix(color, foam_color.rgb, params.foam.a);
}
#endif


#if !@terrain_water_enable_animation@ || !@terrain_enable_detailed_water@
void getWaterNormalAndFoam(inout TerrainWaterFragmentParameters params,
                           vec3 view_dir, float dist)
{
  params.normal = vec3(0,0,1);
  params.foam = vec4(0);
}
#endif


#if @terrain_water_enable_ice_cover@
vec3 getWaterIceCoverColor(vec3 view_dir, in LightParams light)
{
  vec3 water_color = texture(sampler_water, pass_vertex_result.water_texcoords).rgb;

  vec3 noise_color_0 = texture(sampler_water_noise,
                               pass_vertex_result.water_noise0_texcoords).rgb;
  vec3 noise_color_1 = texture(sampler_water_noise,
                               pass_vertex_result.water_noise1_texcoords).rgb;

  vec3 color = water_color;

  color += noise_color_0 - 0.5;
  color += noise_color_1 - 0.5;

  vec3 normal = vec3(0,0,1);

  vec3 light_ambient = light.ambient;
  vec3 light_direct = light.sun * max(dot(normal, sun_dir), 0.0);
  vec3 light_combined = clamp(light_direct + light_ambient, vec3(0), vec3(1));

  color *= light_combined;

  return color;
}
#endif

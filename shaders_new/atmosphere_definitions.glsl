#if @is_vertex@
void atmosphereVertexMain();
#endif

#if @is_fragment@
vec3 fogAndToneMap(vec3 color, vec3 camera_pos, vec3 frag_pos);
#endif

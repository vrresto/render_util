/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


// #if @terrain_enable_altitudinal_zones@
// struct AltitudinalZone
// {
//   float transition_start_height;
//   float transition_end_height;
//   vec3 color;
//   vec4 type;
// };
// #endif


struct TerrainAttributeMap
{
  sampler2D sampler;
  int resolution_m;
  ivec2 size_px;
  vec2 size_m;
};


struct TerrainWaterMap
{
  sampler2D sampler_table;
  sampler2DArray sampler_chunk_blocks;

  ivec2 table_size;

  int chunks_per_block;

  float chunk_sample_offset;
  float chunk_sample_scale;

  //FIXME - rename to chunks_size?
  int chunks_num_cols;
  int chunks_num_rows;

//   vec2 table_size_m;
//   vec2 table_shift_m;
  float chunk_size_m;
//   vec2 chunk_scale;
//   vec2 chunk_shift_m;

  bool y_flip;
};


struct TerrainLayer
{
  vec2 size_m;
  vec3 origin_m;
  vec2 origin_terrain_grid;
  vec2 map_texture_offset_px;
  TerrainAttributeMap height_map;
  TerrainAttributeMap normal_map;

#if @terrain_enable_texture@ && @terrain_enable_far_texture@
  TerrainAttributeMap far_texture;
#endif

#if @terrain_enable_water_map@
  TerrainAttributeMap small_water_map;
#endif

#if @terrain_enable_type_map@
  TerrainAttributeMap type_map;
#endif

#if @enable_terrain_layer_scale@
  float scale;
#endif

#if @terrain_enable_forest@
  TerrainAttributeMap forest_map;
#endif

#if @terrain_enable_material_map_debug@
  TerrainAttributeMap material_map_debug;
#endif
};


struct Terrain
{
//   float dummy;
  float max_elevation;
  int mesh_resolution_m;
  int tile_size_m;
  int tile_size_grid; // tile size in terrain grid space

  TerrainLayer detail_layer;

#if @terrain_enable_base_layer@
  TerrainLayer base_layer;
#endif

#if @terrain_enable_water_map@ && @terrain_enable_detail_water_map@
  TerrainWaterMap detail_layer_water_map;
#endif

#if @terrain_enable_type_map@
  float land_texture_scale_levels[@terrain_num_land_texture_scale_levels@];
#endif
};


struct TerrainVertexResultLayer
{
  vec2 height_map_coords;
  vec2 normal_map_coords;

#if @terrain_enable_far_texture@
  vec2 far_texture_coords;
#endif

#if @terrain_enable_forest@
  vec2 forest_alpha_map_coords;
#if @terrain_forest_enable_detail@ && @terrain_forest_enable_alpha_map_precision_workaround@
  vec2 forest_alpha_map_texel_coords;
#endif
#endif

#if @terrain_enable_type_map@
  vec2 type_map_coords;
#endif
};


struct TerrainVertexResult
{
#if @terrain_enable_texture@ && @terrain_enable_type_map@
  vec2 texcoord[@terrain_num_land_texture_scale_levels@];
#endif

#if @terrain_enable_water@ && @terrain_water_enable_animation@
  vec2 water_normal_map_coords[@terrain_water_animation_num_layers@];
#endif

#if @terrain_enable_water@ && @terrain_water_enable_texture@
  vec2 water_texcoords;
#endif

#if @terrain_enable_water@ && @terrain_water_enable_noise_texture@
  vec2 water_noise0_texcoords;
  vec2 water_noise1_texcoords;
#endif

#if @terrain_enable_texture@
  vec2 noise0_texcoord;
  vec2 noise1_texcoord;
  vec2 far_noise_texcoord;
#endif

#if @terrain_enable_forest@
  vec2 forest_texcoords;
  vec2 forest_alpha_noise_texcoords;
  vec2 forest_alpha_noise_detail_texcoords;
#endif

  TerrainVertexResultLayer detail_layer;
#if @terrain_enable_base_layer@
  TerrainVertexResultLayer base_layer;
#endif
};


vec2 terrainMapTexelCoordsFromGrid(in TerrainLayer layer,
                                  in TerrainAttributeMap map,
                                  vec2 pos_grid);

vec2 terrainMapUVFromGrid(in TerrainLayer layer,
                          in TerrainAttributeMap map,
                          vec2 pos_grid);

vec3 sampleTerrainNormalMap(in TerrainLayer layer,
                            in TerrainVertexResultLayer vertex_result);
float sampleAlphaMap(in TerrainAttributeMap map, vec2 texel_coords);
float getDetailMapBlend(vec2 pos_m);
float getTerrainHeight(in TerrainVertexResult vertex_result, float detail_blend);
vec3 getTerrainNormal(in TerrainVertexResult vertex_result, float detail_blend);
vec3 getTerrainFarNoiseNormal(in TerrainVertexResult vertex_result, float detail_blend);
// float sampleWaterMap(vec2 pos, in TerrainLayer layer);
vec2 getTerrainTexCoords(vec2 pos_terrain_grid, float tile_size_grid);


#if @is_vertex@ && @terrain_enable_texture@
void terrainTextureVertexMain(vec2 pos_terrain_grid);
#endif

#if @is_vertex@ && @terrain_enable_water@
void terrainWaterVertexMain(vec2 pos_terrain_grid);
#endif

#if @is_vertex@ && @terrain_enable_forest@
void terrainForestVertexMain(vec2 pos_terrain_grid);
#endif

#if @is_fragment@ && (@terrain_enable_texture@ || @terrain_enable_water@)
struct ImplicitDerivatives
{
  vec2 dx;
  vec2 dy;
};
#endif

#if @is_fragment@ && @terrain_enable_texture@
struct TerrainTextureFragmentData
{
#if @terrain_enable_type_map@
  ImplicitDerivatives d[@terrain_num_land_texture_scale_levels@];
#endif
  float terrain_height;
  float detail_map_blend;
};

void fetchImplicitDerivatives(out TerrainTextureFragmentData data);
vec3 getTerrainTextureColor(in TerrainTextureFragmentData data);
vec3 sampleBaseLayerTextures(float terrain_height);
#endif

#if @is_fragment@ && @terrain_enable_forest@
float applyForestAlphaNoise(float alpha, float dist, float add_threshold,
                            vec2 texcoords_offset);
float getForestAlpha(float dist, float add_threshold, vec2 offset_m);
vec3 applyForest(vec3 color, vec3 light, vec3 normal, float dist);
#endif

#if @is_fragment@ && @terrain_enable_material_map_debug@
vec3 getMaterialMapDebugColor();
#endif

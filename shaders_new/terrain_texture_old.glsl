/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#extension GL_ARB_texture_query_lod : require

#include terrain_definitions.glsl

uniform Terrain terrain;

#if @terrain_enable_type_map@
uniform sampler2DArray sampler_terrain[@num_terrain_samplers@];
#if @enable_base_map@
uniform sampler2D sampler_terrain_base_layer_types;
#endif
#endif

uniform sampler2D sampler_terrain_noise;

uniform vec3 camera_pos;
uniform vec3 camera_pos_terrain_integer_part;


#if @enable_altitudinal_zones@
uniform AltitudinalZone altitudinal_zones[@num_altitudinal_zones@];
#endif


varying vec3 pass_pos;
varying vec3 pass_pos_flat;
varying TerrainVertexResult pass_vertex_result;


// vertex
/////////////////////////////////////////////////////////////////////////////////////

#if @is_vertex@

vec2 getTexCoord(vec2 pos_terrain_grid, float tile_size_grid)
{
  #if ! @terrain_use_detail_layer_coordinate_system@
    #if @enable_terrain_layer_scale@
      tile_size_grid *= terrain.detail_layer.scale;
    #endif

    pos_terrain_grid -=
      (terrain.detail_layer.origin_m.xy  / terrain.mesh_resolution_m);
  #endif

  vec2 origin_tile = floor(camera_pos_terrain_integer_part.xy / tile_size_grid);

  vec2 texcoord = (pos_terrain_grid + vec2(0,1)) / tile_size_grid;
  texcoord -= origin_tile;

  return texcoord;
}


void terrainTextureVertexMain(vec2 pos_terrain_grid)
{
  pass_vertex_result.noise0_texcoord =
    getTexCoord(pos_terrain_grid, terrain.tile_size_grid / 32.0);

  pass_vertex_result.noise1_texcoord =
    getTexCoord(pos_terrain_grid, terrain.tile_size_grid / 128.0);

  vec2 pos_m = pos_terrain_grid * terrain.mesh_resolution_m;

  #if @terrain_enable_far_texture@
    pass_vertex_result.detail_layer.far_texture_coords =
      getMapTextureCoords(terrain.detail_layer, terrain.detail_layer.far_texture, pos_m);
    #if @enable_base_map@
      pass_vertex_result.base_layer.far_texture_coords =
        getMapTextureCoords(terrain.base_layer, terrain.base_layer.far_texture, pos_m);
   #endif
  #endif

  #if @terrain_enable_type_map@
    for (int i = 0; i < @num_land_texture_scale_levels@; i++)
    {
      float tile_size_grid = terrain.tile_size_grid * terrain.land_texture_scale_levels[i];

      pass_vertex_result.texcoord[i] =
        getTexCoord(pos_terrain_grid, tile_size_grid);
    }
    #if @terrain_use_detail_layer_coordinate_system@
      //FIXME this assumes mesh resolution equals type map resolution
      //FIXME getMapTextureCoords() adds an offset - this should too?
      pass_vertex_result.detail_layer.type_map_coords = pos_terrain_grid;
    #else
      pass_vertex_result.detail_layer.type_map_coords =
        getMapTextureCoords(terrain.detail_layer, terrain.detail_layer.type_map, pos_m)
        * terrain.detail_layer.type_map.size_px;
    #endif
  #endif

  #if @terrain_enable_type_map@ && @enable_base_map@
    pass_vertex_result.base_layer.type_map_coords =
      getMapTextureCoords(terrain.base_layer, terrain.base_layer.type_map, pos_m)
      * terrain.base_layer.type_map.size_px;
  #endif
}

#endif // is_vertex


// fragment
/////////////////////////////////////////////////////////////////////////////////////


#if @is_fragment@ && @terrain_enable_type_map@

struct TypeMapSample
{
  vec4 bottom_left;
  vec4 bottom_right;
  vec4 top_left;
  vec4 top_right;
  vec2 weight;
};


vec4 fetchTypeMapTexel(in TerrainGenericMap map, int x, int y)
{
  ivec2 coords = clamp(ivec2(x,y), ivec2(0), map.size_px - ivec2(1));
  return texelFetch(map.sampler, coords, 0);
}


void sampleTypeMap(in TerrainGenericMap map,
                   vec2 coords,
                   out TypeMapSample sample)
{
  coords -= 0.5;

  sample.weight = fract(coords);

  ivec2 min_coords = ivec2(floor(coords));
  ivec2 max_coords = min_coords + 1;

  sample.bottom_left = fetchTypeMapTexel(map, min_coords.x, min_coords.y);
  sample.bottom_right = fetchTypeMapTexel(map, max_coords.x, min_coords.y);
  sample.top_left = fetchTypeMapTexel(map, min_coords.x, max_coords.y);
  sample.top_right = fetchTypeMapTexel(map, max_coords.x, max_coords.y);
}


vec4 sampleTerrainType(vec4 type, in TerrainTextureFragmentData fragment)
{
  int sampler_nr = int(type.x * 255);
  uint index = uint(type.y * 255);
  uint scale_level = uint(type.z * 255);

  vec2 dx = fragment.d[scale_level].dx;
  vec2 dy = fragment.d[scale_level].dy;

  vec2 texcoord = pass_vertex_result.texcoord[scale_level];
  vec3 coords = vec3(texcoord, index);

#if 1
  switch (sampler_nr)
  {
    case 0:
      return textureGrad(sampler_terrain[0], coords, dx, dy);
  #if @num_terrain_samplers@ > 1
    case 1:
      return textureGrad(sampler_terrain[1], coords, dx, dy);
  #endif
  #if @num_terrain_samplers@ > 2
    case 2:
      return textureGrad(sampler_terrain[2], coords, dx, dy);
  #endif
  #if @num_terrain_samplers@ > 3
    case 3:
      return textureGrad(sampler_terrain[3], coords, dx, dy);
  #endif
    default:
      return vec4(0);
  }
#else
  float lod = 7;

  switch (sampler_nr)
  {
    case 0:
      return textureLod(sampler_terrain[0], coords, lod);
  #if @num_terrain_samplers@ > 1
    case 1:
      return textureLod(sampler_terrain[1], coords, lod);
  #endif
  #if @num_terrain_samplers@ > 2
    case 2:
      return textureLod(sampler_terrain[2], coords, lod);
  #endif
  #if @num_terrain_samplers@ > 3
    case 3:
      return textureLod(sampler_terrain[3], coords, lod);
  #endif
    default:
      return vec4(0);
  }
#endif

  return vec4(0);
}


vec4 sampleTerrainTextures(in TypeMapSample sample, in TerrainTextureFragmentData fragment)
{
  vec4 bottom_left = sampleTerrainType(sample.bottom_left, fragment);
  vec4 bottom_right = sampleTerrainType(sample.bottom_right, fragment);
  vec4 top_left = sampleTerrainType(sample.top_left, fragment);
  vec4 top_right = sampleTerrainType(sample.top_right, fragment);

  vec4 bottom = mix(bottom_left, bottom_right, sample.weight.x);
  vec4 top = mix(top_left, top_right, sample.weight.x);

  return mix(bottom, top, sample.weight.y);
}


#if @enable_base_map@

vec4 sampleBaseLayerTerrainType(vec4 type_texel,
                                in TerrainTextureFragmentData fragment,
                                vec4 default_color)
{
  int type_index = int(type_texel.r * 255);

  vec4 type = texelFetch(sampler_terrain_base_layer_types, ivec2(type_index, 0), 0);

  vec4 color = sampleTerrainType(type, fragment);

  if (type_index == 0)
    color = default_color;

  return color;
}


vec4 sampleBaseLayerTerrainTextures(in TypeMapSample sample,
                                    in TerrainTextureFragmentData fragment,
                                    vec4 default_color)
{
  vec4 bottom_left = sampleBaseLayerTerrainType(sample.bottom_left, fragment, default_color);
  vec4 bottom_right = sampleBaseLayerTerrainType(sample.bottom_right, fragment, default_color);
  vec4 top_left = sampleBaseLayerTerrainType(sample.top_left, fragment, default_color);
  vec4 top_right = sampleBaseLayerTerrainType(sample.top_right, fragment, default_color);

  vec4 bottom = mix(bottom_left, bottom_right, sample.weight.x);
  vec4 top = mix(top_left, top_right, sample.weight.x);

  return mix(bottom, top, sample.weight.y);
}

#endif


vec4 sampleTerrainTextures(in TerrainTextureFragmentData fragment)
{
  TypeMapSample sample;
  sampleTypeMap(terrain.detail_layer.type_map,
                pass_vertex_result.detail_layer.type_map_coords, sample);

  return sampleTerrainTextures(sample, fragment);
}

#endif // is_fragment && terrain_enable_type_map


#if @is_fragment@


vec3 getDefaultColor(in TerrainTextureFragmentData fragment)
{
  vec3 color = vec3(63, 112, 23) / 255.0;

  #if @enable_altitudinal_zones@
    for (int i = 0; i < @num_altitudinal_zones@; i++)
    {
      #if @enable_altitudinal_zone_texture@ && @terrain_enable_type_map@
        vec3 zone_color = sampleTerrainType(altitudinal_zones[i].type, fragment).rgb;
      #else
        vec3 zone_color = altitudinal_zones[i].color;
      #endif

      color = mix(color, zone_color,
                  smoothstep(altitudinal_zones[i].transition_start_height,
                             altitudinal_zones[i].transition_end_height,
                             fragment.terrain_height));
    }
  #endif

  return color;
}


#if @enable_base_map@

vec3 mixBaseTextures(vec3 color1, vec3 color2, float blend)
{
  blend = smoothstep(0.3, 0.7, blend);

  return mix(color1, color2, blend);
}


// #if @terrain_enable_far_texture@
// vec3 sampleBaseLayerTextures(float terrain_height, in TerrainTextureFragmentData fragment)
// {
//   float height = terrain_height;
//   vec3 color = vec3(63, 112, 23) / 255.0;
//
//   #if @enable_altitudinal_zones@
//     for (int i = 0; i < @num_altitudinal_zones@; i++)
//     {
//       #if @enable_altitudinal_zone_texture@ && @terrain_enable_type_map@
//         vec3 zone_color = sampleTerrainType(altitudinal_zones[i].type, fragment).rgb;
//       #else
//         vec3 zone_color = altitudinal_zones[i].color;
//       #endif
//
//       color = mix(color, zone_color,
//                   smoothstep(altitudinal_zones[i].transition_start_height,
//                              altitudinal_zones[i].transition_end_height,
//                              height));
//     }
//   #endif

//   #if @terrain_enable_type_map@ && @terrain_enable_base_type_map@
//     vec2 coords = pass_vertex_result.base_layer.type_map_coords;
//     TypeMapSample sample;
//     sampleTypeMap(terrain.base_layer.type_map, coords, sample);
//
//     vec4 base_color = sampleBaseLayerTerrainTextures(sample, fragment, vec4(color, 1));
//     color = base_color.rgb;
//   #endif
//
//   return color;
// }
// #endif

#endif


// #define ENABLE_DETAIL_LAYER_TEXTURE @terrain_enable_type_map@ && @terrain_enable_detail_layer_textures@
// #define ENABLE_NEAR_TEXTURE

#if @terrain_enable_far_texture@
vec3 getFarTextureColor(in TerrainTextureFragmentData fragment)
{
  vec3 color = vec3(0);

  #if @terrain_enable_detail_layer@
  {
    vec2 coords = pass_vertex_result.detail_layer.far_texture_coords;
    color = texture(terrain.detail_layer.far_texture.sampler, coords).rgb;
  }
  #endif

  #if @enable_base_map@
  {
    vec2 coords = pass_vertex_result.base_layer.far_texture_coords;
    vec3 base_color = texture(terrain.base_layer.far_texture.sampler, coords).rgb;

    #if @terrain_enable_detail_layer@
    {
      color = mix(base_color, color, fragment.detail_map_blend);
    }
    #else
    {
      color = base_color;
    }
    #endif
  }
  #endif

  return color;
}
#endif

#if @terrain_enable_far_texture@

float getFarTextureBlend(float dist)
{
  const float fade_start = 20000.0;
  const float fade_end = 40000.0;

  return smoothstep(fade_start, fade_end, dist);
}


vec3 applyFarTexture(vec3 color_in, vec2 pos, float dist)
{
#if @terrain_enable_detail_layer@
#if 0
  ivec2 nearest = ivec2(round(pass_type_map_coords));
  vec3 color = texelFetch(terrain.detail_layer.far_texture.sampler, nearest, 0).rgb;
#else
  vec2 coords = pass_vertex_result.detail_layer.far_texture_coords;
  vec3 color = texture(terrain.detail_layer.far_texture.sampler, coords).rgb;
#endif
#endif
//   return color;

  float lod = textureQueryLOD(terrain.detail_layer.far_texture.sampler, coords).x;

  const float fade_start = 20000.0;
  const float fade_end = 40000.0;

  float blend = clamp(lod, 0, 1);
  blend = mix(blend, 1.0, smoothstep(fade_start, fade_end, dist));

#if @terrain_enable_type_map@ && @terrain_enable_detail_layer_textures@ && @terrain_enable_detail_layer@
  return mix(color_in, color, blend);
#else
  return color;
#endif
}
#endif

float sampleNoise(vec2 coord)
{
//   const float noise_strength = 2.0;
//   const float noise_strength = 4;

  float noise = texture(sampler_terrain_noise, coord).x;

  noise *= 2;

  return noise;
}


vec3 applyTerrainNoise(vec3 color, float dist)
{
  vec2 coord = pass_vertex_result.noise0_texcoord;

  color = mix(color, color * sampleNoise(coord), 0.5 * (1 - smoothstep(500, 1000, dist)));

  coord = pass_vertex_result.noise1_texcoord;

  color = mix(color, color * sampleNoise(coord), 1.0 * (1 - smoothstep(100, 200, dist)));

  return color;
}

// #define ENABLE_DETAIL_LAYER_TEXTURES

vec3 getTerrainTextureColor(in TerrainTextureFragmentData fragment)
{
  float dist = distance(camera_pos, pass_pos);
  vec3 color = vec3(0);

  #if @terrain_enable_far_texture@
    color = getFarTextureColor(fragment);
  #endif

  #if @terrain_enable_near_texture@
    vec3 near_color = getDefaultColor(fragment);

//     #if @terrain_enable_detail_layer@
//       #if @terrain_enable_detail_layer_textures@ && @terrain_enable_type_map@
//         near_color = sampleTerrainTextures(fragment).rgb;
//       #endif
//     #endif

    #if @enable_base_map@ && 0
      vec3 base_color = sampleBaseLayerTextures(fragment.terrain_height, fragment);
//       #if @terrain_enable_detail_layer@ && @terrain_enable_detail_layer_textures@
//         texture_color = mix(base_color, texture_color, fragment.detail_map_blend);
//       #else
        near_color = base_color;
//       #endif
    #endif


    #if @terrain_enable_far_texture@
      color = mix(near_color, color, getFarTextureBlend(dist));
    #else
      color = near_color;
    #endif
  #endif


// #if @terrain_enable_far_texture@
//   color = applyFarTexture(color, pass_pos_flat.xy, dist);
// #endif

  color = applyTerrainNoise(color, dist);


  return color;
}


void fetchImplicitDerivatives(out TerrainTextureFragmentData fragment)
{
#if @terrain_enable_type_map@
  for (int i = 0; i < @num_land_texture_scale_levels@; i++)
  {
    vec2 texcoord = pass_vertex_result.texcoord[i];
    fragment.d[i].dx = dFdx(texcoord);
    fragment.d[i].dy = dFdy(texcoord);
  }
#endif
}


#endif // is_fragment

#if @terrain_enable_water@

struct TerrainWaterAnimationLayer
{
  float tile_size_grid;
  float tile_size_m;
};


struct TerrainWaterParameters
{
  TerrainWaterAnimationLayer animation_layers[@terrain_water_animation_num_layers@];
};


struct TerrainWaterFragmentParameters
{
#if @terrain_enable_land@
  float water_depth;
  float water_map_value;
  vec3 ground_color;
#endif
#if @terrain_enable_land@ && @terrain_enable_shore_waves@
  float shore_wave_alpha;
#endif
  vec3 normal;
  vec4 foam;
};

#endif


#if @terrain_enable_water_map@
float sampleWaterMap(float detail_layer_blend);
float getWaterAlpha(float water_map_value);
#endif

#if @is_fragment@ && @terrain_enable_water@
#if @terrain_enable_land@
float sampleSmallWaterMap();
void getShoreWaveAlpha(inout TerrainWaterFragmentParameters);
#else
#endif

void getWaterNormalAndFoam(inout TerrainWaterFragmentParameters params,
                           vec3 view_dir, float dist);

vec3 getWaterColor(inout TerrainWaterFragmentParameters params, in LightParams light,
                   vec3 view_dir, float dist);

vec3 getWaterIceCoverColor(vec3 view_dir, in LightParams light);
#endif

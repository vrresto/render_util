/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#extension GL_ARB_texture_query_lod : require

#include terrain_definitions.glsl

uniform Terrain terrain;

#if @terrain_enable_type_map@
uniform sampler2DArray sampler_terrain[@terrain_num_land_texture_samplers@];
#if @terrain_enable_base_layer@ && @terrain_enable_base_layer_type_map@
uniform sampler2D sampler_terrain_base_layer_types;
#endif
#endif

uniform sampler2D sampler_terrain_noise;
uniform sampler2D sampler_terrain_far_noise;

uniform vec3 camera_pos;


#if @terrain_enable_altitudinal_zones@
uniform AltitudinalZone altitudinal_zones[@terrain_num_altitudinal_zones@];
#endif


uniform float near_texture_fade_start_dist;
uniform float near_texture_fade_end_dist;


varying vec3 pass_pos;
varying vec3 pass_pos_flat;
varying vec3 pass_pos_view;
varying TerrainVertexResult pass_vertex_result;


struct TypeMapSample
{
  vec4 bottom_left;
  vec4 bottom_right;
  vec4 top_left;
  vec4 top_right;
  vec2 weight;
};


// vertex
/////////////////////////////////////////////////////////////////////////////////////

#if @is_vertex@


uniform vec2 terrain_land_texture_offset;


vec2 getTexCoord(vec2 pos_terrain_grid, float tile_size_grid)
{
  return getTerrainTexCoords(pos_terrain_grid, tile_size_grid);
}


void terrainTextureVertexMain(vec2 pos_terrain_grid)
{
  pass_vertex_result.noise0_texcoord =
    getTexCoord(pos_terrain_grid, terrain.tile_size_grid / 32.0);

  pass_vertex_result.noise1_texcoord =
    getTexCoord(pos_terrain_grid, terrain.tile_size_grid / 128.0);

  pass_vertex_result.far_noise_texcoord =
    getTexCoord(pos_terrain_grid, 128);

  vec2 pos_m = pos_terrain_grid * terrain.mesh_resolution_m;

  #if @terrain_enable_far_texture@
    pass_vertex_result.detail_layer.far_texture_coords =
      terrainMapUVFromGrid(terrain.detail_layer, terrain.detail_layer.far_texture,
                                  pos_terrain_grid);
    #if @terrain_enable_base_layer@
      pass_vertex_result.base_layer.far_texture_coords =
        terrainMapUVFromGrid(terrain.base_layer, terrain.base_layer.far_texture,
                                    pos_terrain_grid);
   #endif
  #endif

  #if @terrain_enable_type_map@
    for (int i = 0; i < @terrain_num_land_texture_scale_levels@; i++)
    {
      float tile_size_grid = terrain.tile_size_grid * terrain.land_texture_scale_levels[i];

      pass_vertex_result.texcoord[i] =
        getTexCoord(pos_terrain_grid, tile_size_grid);
    }
  #endif

  #if @terrain_enable_type_map@
    pass_vertex_result.detail_layer.type_map_coords =
      terrainMapTexelCoordsFromGrid(terrain.detail_layer, terrain.detail_layer.type_map,
                                    pos_terrain_grid);
  #endif

  #if @terrain_enable_type_map@ && @terrain_enable_base_layer@
    pass_vertex_result.base_layer.type_map_coords =
      terrainMapTexelCoordsFromGrid(terrain.base_layer, terrain.base_layer.type_map,
                                    pos_terrain_grid);
  #endif
}


#endif // is_vertex


// fragment
/////////////////////////////////////////////////////////////////////////////////////

#if @is_fragment@


#if @far_noise_texture_is_normal_map@
vec3 getTerrainFarNoiseNormal(in TerrainVertexResult vertex_result, float detail_blend)
{
  vec2 coord = pass_vertex_result.far_noise_texcoord;
  vec3 normal =  texture(sampler_terrain_far_noise, coord).xyz;
  normal = (normal * 2) - 1;
  return normal;
}
#endif


#if @terrain_enable_type_map@
vec4 fetchTypeMapTexel(in TerrainAttributeMap map, int x, int y)
{
  ivec2 coords = clamp(ivec2(x,y), ivec2(0), map.size_px - ivec2(1));
  return texelFetch(map.sampler, coords, 0);
}
#endif


#if @terrain_enable_type_map@
void sampleTypeMap(in TerrainAttributeMap map,
                   vec2 texel_coords,
                   out TypeMapSample sample)
{
  sample.weight = fract(texel_coords);

  ivec2 min_coords = ivec2(floor(texel_coords));
  ivec2 max_coords = min_coords + 1;

  sample.bottom_left = fetchTypeMapTexel(map, min_coords.x, min_coords.y);
  sample.bottom_right = fetchTypeMapTexel(map, max_coords.x, min_coords.y);
  sample.top_left = fetchTypeMapTexel(map, min_coords.x, max_coords.y);
  sample.top_right = fetchTypeMapTexel(map, max_coords.x, max_coords.y);
}
#endif


#if @terrain_enable_type_map@
vec4 sampleTerrainType(vec4 type, in TerrainTextureFragmentData fragment)
{
  int sampler_nr = int(type.x * 255);
  uint index = uint(type.y * 255);
  uint scale_level = uint(type.z * 255);

  vec2 dx = fragment.d[scale_level].dx;
  vec2 dy = fragment.d[scale_level].dy;

  vec2 texcoord = pass_vertex_result.texcoord[scale_level];
  vec3 coords = vec3(texcoord, index);

#if 1
  switch (sampler_nr)
  {
    case 0:
      return textureGrad(sampler_terrain[0], coords, dx, dy);
  #if @terrain_num_land_texture_samplers@ > 1
    case 1:
      return textureGrad(sampler_terrain[1], coords, dx, dy);
  #endif
  #if @terrain_num_land_texture_samplers@ > 2
    case 2:
      return textureGrad(sampler_terrain[2], coords, dx, dy);
  #endif
  #if @terrain_num_land_texture_samplers@ > 3
    case 3:
      return textureGrad(sampler_terrain[3], coords, dx, dy);
  #endif
    default:
      return vec4(0);
  }
#else
  float lod = 7;

  switch (sampler_nr)
  {
    case 0:
      return textureLod(sampler_terrain[0], coords, lod);
  #if @terrain_num_land_texture_samplers@ > 1
    case 1:
      return textureLod(sampler_terrain[1], coords, lod);
  #endif
  #if @terrain_num_land_texture_samplers@ > 2
    case 2:
      return textureLod(sampler_terrain[2], coords, lod);
  #endif
  #if @terrain_num_land_texture_samplers@ > 3
    case 3:
      return textureLod(sampler_terrain[3], coords, lod);
  #endif
    default:
      return vec4(0);
  }
#endif

  return vec4(0);
}
#endif


vec3 getDefaultColor(in TerrainTextureFragmentData fragment)
{
  vec3 color = vec3(63, 112, 23) / 255.0;

  #if @terrain_enable_altitudinal_zones@
    for (int i = 0; i < @terrain_num_altitudinal_zones@; i++)
    {
      #if @terrain_enable_altitudinal_zone_texture@ && @terrain_enable_type_map@
        vec3 zone_color = sampleTerrainType(altitudinal_zones[i].type, fragment).rgb;
      #else
        vec3 zone_color = altitudinal_zones[i].color;
      #endif

      color = mix(color, zone_color,
                  smoothstep(altitudinal_zones[i].transition_start_height,
                             altitudinal_zones[i].transition_end_height,
                             fragment.terrain_height));
    }
  #endif

  return color;
}


#if @terrain_enable_far_texture@
float getFarTextureBlend(float dist)
{
  // FIXME use a different texture's LOD as reference?
  float lod = textureQueryLOD(terrain.detail_layer.far_texture.sampler,
                              pass_vertex_result.detail_layer.far_texture_coords).x;

  float blend = clamp(lod, 0, 1);

  return mix(blend, 1.0, smoothstep(near_texture_fade_start_dist,
                                    near_texture_fade_end_dist, dist));
//   return smoothstep(fade_start, fade_end, dist);
}
#endif


#if @terrain_enable_far_texture@
vec3 getFarTextureColor(in TerrainTextureFragmentData fragment)
{
  #if @terrain_enable_detail_layer@
    vec2 detail_coords = pass_vertex_result.detail_layer.far_texture_coords;
    vec3 detail_color = texture(terrain.detail_layer.far_texture.sampler, detail_coords).rgb;
  #endif

  #if @terrain_enable_base_layer@
    vec2 base_coords = pass_vertex_result.base_layer.far_texture_coords;
    vec3 base_color = texture(terrain.base_layer.far_texture.sampler, base_coords).rgb;
  #endif

  #if @terrain_enable_base_layer@ && @terrain_enable_detail_layer@
    return mix(base_color, detail_color, fragment.detail_map_blend);
  #elif @terrain_enable_base_layer@
    return base_color;
  #elif @terrain_enable_detail_layer@
    return detail_color;
  #endif

  return vec3(1,0,0);
}
#endif


#if @terrain_enable_base_layer@ && @terrain_enable_base_layer_type_map@
vec4 sampleBaseLayerTerrainType(vec4 type_texel,
                                in TerrainTextureFragmentData fragment,
                                vec4 default_color)
{
  int type_index = int(type_texel.r * 255);

  vec4 type = texelFetch(sampler_terrain_base_layer_types, ivec2(type_index, 0), 0);

  vec4 color = sampleTerrainType(type, fragment);

  if (type_index == 0)
    color = default_color;

  return color;
}
#endif


#if @terrain_enable_base_layer@ && @terrain_enable_base_layer_type_map@
vec4 sampleBaseLayerTextures(in TypeMapSample sample,
                             in TerrainTextureFragmentData fragment,
                             vec4 default_color)
{
  vec4 bottom_left = sampleBaseLayerTerrainType(sample.bottom_left, fragment, default_color);
  vec4 bottom_right = sampleBaseLayerTerrainType(sample.bottom_right, fragment, default_color);
  vec4 top_left = sampleBaseLayerTerrainType(sample.top_left, fragment, default_color);
  vec4 top_right = sampleBaseLayerTerrainType(sample.top_right, fragment, default_color);

  vec4 bottom = mix(bottom_left, bottom_right, sample.weight.x);
  vec4 top = mix(top_left, top_right, sample.weight.x);

  return mix(bottom, top, sample.weight.y);
}
#endif


#if @terrain_enable_base_layer@
vec3 getBaseLayerTextureColor(in TerrainTextureFragmentData fragment,
                              vec3 default_color)
{
  #if @terrain_enable_base_layer_type_map@
    vec2 coords = pass_vertex_result.base_layer.type_map_coords;
    TypeMapSample sample;
    sampleTypeMap(terrain.base_layer.type_map, coords, sample);

    return sampleBaseLayerTextures(sample, fragment, vec4(default_color, 1)).rgb;
  #else
    return default_color;
  #endif
}
#endif


#if @terrain_enable_detail_layer@ && @terrain_enable_type_map@
vec4 sampleTerrainTextures(in TypeMapSample sample, in TerrainTextureFragmentData fragment)
{
  vec4 bottom_left = sampleTerrainType(sample.bottom_left, fragment);
  vec4 bottom_right = sampleTerrainType(sample.bottom_right, fragment);
  vec4 top_left = sampleTerrainType(sample.top_left, fragment);
  vec4 top_right = sampleTerrainType(sample.top_right, fragment);

  vec4 bottom = mix(bottom_left, bottom_right, sample.weight.x);
  vec4 top = mix(top_left, top_right, sample.weight.x);

  return mix(bottom, top, sample.weight.y);
}
#endif


#if @terrain_enable_detail_layer@ && @terrain_enable_type_map@
vec3 getDetailLayerTextureColor(in TerrainTextureFragmentData fragment)
{
  TypeMapSample sample;
  sampleTypeMap(terrain.detail_layer.type_map,
                pass_vertex_result.detail_layer.type_map_coords, sample);

  return sampleTerrainTextures(sample, fragment).rgb;
}
#endif


#if @terrain_enable_near_texture@
vec3 getNearTextureColor(in TerrainTextureFragmentData fragment)
{
//   return vec3(0,1,0);


  vec3 default_color = getDefaultColor(fragment);

  #if @terrain_enable_base_layer@
    vec3 base_color = getBaseLayerTextureColor(fragment, default_color);
  #endif

  #if @terrain_enable_detail_layer@
    vec3 detail_color = getDetailLayerTextureColor(fragment);
  #endif

  #if @terrain_enable_base_layer@ && @terrain_enable_detail_layer@
    return mix(base_color, detail_color, fragment.detail_map_blend);
  #elif @terrain_enable_base_layer@
    return base_color;
  #elif @terrain_enable_detail_layer@
    return detail_color;
  #else
    return default_color;
  #endif
}
#endif


float sampleTerrainNoise(vec2 coord)
{
  float noise = texture(sampler_terrain_noise, coord).x;

  noise *= 2;

  return noise;
}


vec3 applyTerrainNoise(vec3 color, float dist)
{
  vec2 coord = pass_vertex_result.noise0_texcoord;
  color = mix(color, color * sampleTerrainNoise(coord), 0.5 * (1 - smoothstep(500, 1000, dist)));

  coord = pass_vertex_result.noise1_texcoord;
  color = mix(color, color * sampleTerrainNoise(coord), 1.0 * (1 - smoothstep(100, 200, dist)));

  #if ! @far_noise_texture_is_normal_map@
    coord = pass_vertex_result.far_noise_texcoord;
    float far_noise = texture(sampler_terrain_far_noise, coord).x;
    color = mix(color, color * far_noise * 2, 0.5);
  #endif

  return color;
}


vec3 getTerrainTextureColor(in TerrainTextureFragmentData fragment)
{
  float dist = length(pass_pos_view);

  #if @terrain_enable_far_texture@
    vec3 far_color = getFarTextureColor(fragment);
  #endif

  #if @terrain_enable_near_texture@
    vec3 near_color = getNearTextureColor(fragment);
  #endif

  vec3 color = vec3(0.5);

  #if @terrain_enable_far_texture@ && @terrain_enable_near_texture@
    color = mix(near_color, far_color, getFarTextureBlend(dist));
  #elif @terrain_enable_far_texture@
    color = far_color;
  #elif @terrain_enable_near_texture@
    color = near_color;
  #endif

  color = applyTerrainNoise(color, dist);

  return color;
}


void fetchImplicitDerivatives(out TerrainTextureFragmentData fragment)
{
#if @terrain_enable_type_map@
  for (int i = 0; i < @terrain_num_land_texture_scale_levels@; i++)
  {
    vec2 texcoord = pass_vertex_result.texcoord[i];
    fragment.d[i].dx = dFdx(texcoord);
    fragment.d[i].dy = dFdy(texcoord);
  }
#endif
}


#endif // is_fragment

/**
 *    Rendering utilities
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This terrain implementation makes use of the technique described in the paper
 * "Continuous Distance-Dependent Level of Detail for Rendering Heightmaps (CDLOD)"
 * by Filip Strugar <http://www.vertexasylum.com/downloads/cdlod/cdlod_latest.pdf>.
 */

#version 430

#extension GL_ARB_shader_draw_parameters : require

#define ENABLE_CURVATURE @enable_curvature@

#include terrain_definitions.glsl
#include atmosphere_definitions.glsl


struct InstanceData
{
  vec2 grid;
  float lod;
  float unused;
};


layout(location = 0) in vec4 vertex;

layout(std430, binding = 0) readonly buffer shader_storage
{
  InstanceData instances[];
};


uniform sampler2D sampler_curvature_map;
uniform float curvature_map_max_distance;

uniform float cdlod_min_dist;

uniform vec3 camera_pos;
// integer part of camera position in terrain grid space
uniform vec3 camera_pos_terrain_integer_part;
// fraction part of camera position in terrain grid space
uniform vec3 camera_pos_terrain_fraction_part;

uniform mat4 world_to_view_rotation;
uniform mat4 projection;

uniform Terrain terrain;

#if @terrain_enable_height_offset@
uniform float terrain_height_offset;
#endif

out vec3 pass_pos_flat;
out vec3 pass_pos;
out vec3 pass_pos_view;
out vec3 pass_normal;
out float pass_vertex_horizontal_dist;
out float pass_terrain_lod;
out TerrainVertexResult pass_vertex_result;
#if @terrain_enable_vertex_color@
out vec3 pass_vertex_color;
#endif
out vec2 pass_pos_terrain_grid;


const float lod_transition_start = 0.82;
const float lod_transition_end = 0.99;


vec2 getCurvatureDiff(float dist, float height)
{
  float u = dist / curvature_map_max_distance;
  float v = height / terrain.max_elevation;

  return texture(sampler_curvature_map, vec2(u,v)).xy;
}


vec3 getNormalFromGrid(vec2 grid)
{
  vec2 coords =
    terrainMapUVFromGrid(terrain.detail_layer,
                         terrain.detail_layer.normal_map,
                         grid);

  return texture(terrain.detail_layer.normal_map.sampler, coords).xyz;
}


float getHeightFromGrid(vec2 grid, vec2 world_coord, float lod)
{
  float height = 0;

#if @terrain_enable_detail_layer@
  vec2 height_map_texture_coord =
    terrainMapUVFromGrid(terrain.detail_layer,
                         terrain.detail_layer.height_map,
                         grid);

  float hm_smoothed = textureLod(terrain.detail_layer.height_map.sampler,
                                 height_map_texture_coord, lod).x;

  float detail = hm_smoothed;

// for debugging - hm_raw should equal hm_smoothed
#if @terrain_texelfetch_heightmap@
//   ivec2 height_map_texture_coord_px =
//     ivec2(terrain.detail_layer.height_map.size_px * height_map_texture_coord);
//
//   float hm_raw = texelFetch(terrain.detail_layer.height_map.sampler,
//                             height_map_texture_coord_px, 0).x;
//
//   detail = mix(hm_raw, hm_smoothed,
//     smoothstep(cdlod_min_dist * lod_transition_start * 0.7,
//                cdlod_min_dist * lod_transition_start * 0.9, approx_dist));
#endif

  height = detail;
#endif

#if @terrain_enable_base_layer@
  vec2 base_height_map_texture_coord =
      terrainMapUVFromGrid(terrain.base_layer,
                           terrain.base_layer.height_map,
                           grid);

  float base = textureLod(terrain.base_layer.height_map.sampler,
                          base_height_map_texture_coord,
                          lod).x;

  base += terrain.base_layer.origin_m.z;

  #if @terrain_enable_detail_layer@
    float detail_blend = getDetailMapBlend(world_coord);
    height = mix(base, detail, detail_blend);
  #else
    height = base;
  #endif
#endif

  return height;
}


vec2 morphVertex(vec2 grid_coords, float lod_morph)
{
#if @terrain_draw_node_border@
  return grid_coords;
#endif

  vec2 fp = fract(grid_coords / 2) * 2;;

  return grid_coords - fp * lod_morph;
}

uniform float lod_distances[@terrain_lod_count@];
uniform float node_scale_for_lod[@terrain_lod_count@];


float getLodDist(int lod)
{
  return lod_distances[lod];
}


float getNodeScale(int lod)
{
  return node_scale_for_lod[lod];
}


void main(void)
{
  InstanceData instance = instances[gl_InstanceID + gl_BaseInstanceARB];

  int lod = int(instance.lod);
  pass_terrain_lod = lod;

//   float grids_per_tile = terrain.tile_size_grid;
  float cdlod_lod_distance = getLodDist(lod);

  float node_scale = getNodeScale(lod);

  vec3 pos = vertex.xyz;

#if @terrain_draw_node_border@
  if (pos.x == 0 || pos.y == 0)
    pass_vertex_color = vec3(1,0,0);
  else
    pass_vertex_color = vec3(1);
#endif

  pos.xy *= node_scale;
  pos.xy += instance.grid;

  vec2 pos2d_m = pos.xy * terrain.mesh_resolution_m;

  float lod_morph = smoothstep(
            cdlod_lod_distance * lod_transition_start,
            cdlod_lod_distance * lod_transition_end,
            distance(vec3(pos2d_m, getHeightFromGrid(pos.xy, pos2d_m, float(lod))),
                     camera_pos));

  pos.xy = morphVertex(vertex.xy, lod_morph);
  pos.xy *= node_scale;
  pos.xy += instance.grid;

  vec2 pos_terrain_grid = pos.xy;
  pass_pos_terrain_grid = pos_terrain_grid;

  pos2d_m = pos.xy * terrain.mesh_resolution_m;

  pos.z = getHeightFromGrid(pos_terrain_grid, pos2d_m, float(lod) + lod_morph);

  pass_normal = getNormalFromGrid(pos_terrain_grid);

#if @terrain_enable_height_offset@
  pos.z += terrain_height_offset;
#endif

  pass_pos_flat = vec3(pos2d_m, pos.z);
  pass_vertex_horizontal_dist = distance(pos2d_m, camera_pos.xy);

  float height_diff = 0;

  // curvature
  #if ENABLE_CURVATURE
  {
    vec2 pos_xy_camera_relative = pos.xy - camera_pos_terrain_integer_part.xy;
    pos_xy_camera_relative *= terrain.mesh_resolution_m;
    pos_xy_camera_relative -= camera_pos_terrain_fraction_part.xy;

    float horizontalDist = length(pos_xy_camera_relative);

    vec2 view_dir_horizontal = normalize(pos_xy_camera_relative);

    vec2 diff = getCurvatureDiff(horizontalDist, pos.z);

    float horizontalDistNew = horizontalDist + diff.x;

    vec2 pos_xy_camera_relative_new = view_dir_horizontal * horizontalDistNew;

    pos_xy_camera_relative_new += camera_pos_terrain_fraction_part.xy;
    pos_xy_camera_relative_new /= terrain.mesh_resolution_m;

    vec2 pos_xy_new = pos_xy_camera_relative_new + camera_pos_terrain_integer_part.xy;

    pos.xy = pos_xy_new;
    height_diff = diff.y;
  }
  #endif

  pass_pos = (pos * vec3(vec2(terrain.mesh_resolution_m), 1)) + vec3(0, 0, height_diff);

  pos.xyz -= camera_pos_terrain_integer_part;
  pos.xy *= terrain.mesh_resolution_m;
  pos.z += height_diff;
  pos.xyz -= camera_pos_terrain_fraction_part;

  gl_Position = world_to_view_rotation * vec4(pos,1);
  pass_pos_view = gl_Position.xyz;
  gl_Position = projection * gl_Position;

  pass_vertex_result.detail_layer.height_map_coords =
    terrainMapUVFromGrid(terrain.detail_layer,
                         terrain.detail_layer.height_map, pos_terrain_grid);

  pass_vertex_result.detail_layer.normal_map_coords =
    terrainMapUVFromGrid(terrain.detail_layer, terrain.detail_layer.normal_map,
                         pos_terrain_grid);

#if @terrain_enable_base_layer@
  pass_vertex_result.base_layer.height_map_coords =
    terrainMapUVFromGrid(terrain.base_layer, terrain.base_layer.height_map,
                         pos_terrain_grid);
  pass_vertex_result.base_layer.normal_map_coords =
    terrainMapUVFromGrid(terrain.base_layer, terrain.base_layer.normal_map,
                         pos_terrain_grid);
#endif

#if @terrain_enable_texture@
  terrainTextureVertexMain(pos_terrain_grid);
#endif

#if @terrain_enable_water@
  terrainWaterVertexMain(pos_terrain_grid);
#endif

#if @terrain_enable_forest@
  terrainForestVertexMain(pos_terrain_grid);
#endif

#if @enable_atmosphere@
  atmosphereVertexMain();
#endif
}

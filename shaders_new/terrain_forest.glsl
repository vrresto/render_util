/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include terrain_definitions.glsl


uniform sampler2D sampler_generic_noise;
uniform sampler2D sampler_forest_far;
uniform sampler2DArray sampler_forest_layers;

uniform Terrain terrain;

uniform mat4 world_to_view;
uniform vec3 camera_pos;
uniform vec3 sun_dir;
uniform int forest_layer;


varying float pass_vertex_horizontal_dist;
varying vec3 pass_pos_flat;
varying vec3 pass_pos;
varying vec3 pass_pos_view;
varying vec3 pass_normal;
varying TerrainVertexResult pass_vertex_result;


in vec2 pass_pos_terrain_grid;

const float noise_coords_scale = 0.004;
const float noise_detail_coords_scale = 0.016;


vec2 metersToForestLayerUVRelative(vec2 m)
{
  return m / terrain.mesh_resolution_m;
}


vec2 metersToForestAlphaMapTexelRelative(vec2 m)
{
  //FIXME
  return m / terrain.detail_layer.forest_map.resolution_m;
}


vec2 metersToForestAlphaMapUVRelative(vec2 m)
{
  //FIXME
  return metersToForestAlphaMapTexelRelative(m) /
         vec2(terrain.detail_layer.forest_map.size_px);
}


#if @is_vertex@


void terrainForestVertexMain(vec2 pos_terrain_grid)
{
  vec2 texcoords = pos_terrain_grid;

  pass_vertex_result.forest_texcoords = texcoords;

  pass_vertex_result.forest_alpha_noise_texcoords =
    texcoords * noise_coords_scale;
  pass_vertex_result.forest_alpha_noise_detail_texcoords =
    texcoords * noise_detail_coords_scale;

  pass_vertex_result.detail_layer.forest_alpha_map_coords =
    terrainMapUVFromGrid(terrain.detail_layer,
                         terrain.detail_layer.forest_map,
                         pos_terrain_grid);

#if @terrain_forest_enable_detail@ && @terrain_forest_enable_alpha_map_precision_workaround@
  pass_vertex_result.detail_layer.forest_alpha_map_texel_coords =
    terrainMapTexelCoordsFromGrid(terrain.detail_layer,
                                  terrain.detail_layer.forest_map,
                                  pos_terrain_grid);
#endif
}


void terrainForestVertexLayerMain(in TerrainVertexResultLayer layer, vec2 pos_terrain_grid)
{
}


#endif // is_vertex


#if @is_fragment@


float sampleForestAlphaNoise(vec2 coords)
{
  return texture(sampler_generic_noise, coords).x;
}


float applyForestAlphaNoise(float alpha, float dist, float add_threshold,
                            vec2 texcoords_offset)
{
  vec2 noise_texcoords = pass_vertex_result.forest_alpha_noise_texcoords +
    noise_coords_scale * texcoords_offset;

  vec2 noise_detail_texcoords = pass_vertex_result.forest_alpha_noise_detail_texcoords +
    noise_detail_coords_scale * texcoords_offset;

  float forest_noise =
    sampleForestAlphaNoise(noise_texcoords);
  float forest_noise_detail =
    sampleForestAlphaNoise(noise_detail_texcoords);

  float forest_noise_combined = mix(forest_noise, forest_noise_detail, 0.3);

  forest_noise_combined *= 0.9;

  float alpha_threshold = 0.1;

  alpha_threshold += forest_noise_combined;
  alpha_threshold += add_threshold;

  alpha_threshold = clamp(alpha_threshold, 0.0, 1.0);

  float tranistion_min = 0.01;

  tranistion_min += add_threshold * 2;

  float transition = mix(tranistion_min, 1.0, 1-exp(-dist/50000)); //FIXME de-hardcode

  return smoothstep(max(alpha_threshold - transition/2, 0),
                    min(alpha_threshold + transition/2, 1),
                    alpha);
}


float getForestAlpha(float dist, float add_threshold, vec2 offset_m)
{
#if @terrain_forest_enable_detail@ && @terrain_forest_enable_alpha_map_precision_workaround@
  vec2 texel_coords = pass_vertex_result.detail_layer.forest_alpha_map_texel_coords
                      + metersToForestAlphaMapTexelRelative(offset_m);

  return sampleAlphaMap(terrain.detail_layer.forest_map, texel_coords);
#else
  vec2 alpha_map_texcoords = pass_vertex_result.detail_layer.forest_alpha_map_coords;
                             + metersToForestAlphaMapUVRelative(offset_m);

  return texture(terrain.detail_layer.forest_map.sampler, alpha_map_texcoords).r;
#endif
}


//FIXME - review / move
float intersectionDist(vec3 plane_normal,
               vec3 plane_point,
               vec3 ray_origin,
               vec3 ray_dir)
{

  float d = dot(plane_point, -plane_normal);
  float v = dot(ray_dir, plane_normal);

  v = clamp(v, -1, -0.001);

  float dist = -(d + dot(ray_origin, plane_normal)) / v;

  return dist;
}


vec3 applyForest(vec3 color, vec3 light, vec3 face_normal, float dist)
{
  vec3 view_dir = normalize(pass_pos - camera_pos);
  vec3 view_dir_view = normalize(pass_pos_view);

  float alpha = getForestAlpha(dist, 0, vec2(0));
  alpha = applyForestAlphaNoise(alpha, dist, 0, vec2(0));

  if (dist > 15000) //FIXME de-hardcode
  {
    vec4 far_color = texture(sampler_forest_far, pass_vertex_result.forest_texcoords);
    far_color.rgb *= light;

    vec3 combined = color * (1.0 - far_color.a) + far_color.rgb;

    color = mix(color, combined, alpha);

    return color;
  }

  const float layer_height = 3; //FIXME de-hardcode

  float dist_in_layer =
    intersectionDist(-face_normal,
                     pass_pos + vec3(0, 0, layer_height),
                     pass_pos,
                     -view_dir);

  // FIXME HACK to avoid artifacts at mountain ridges
  // caused by filtered normals and/or not considering the triangle borders
  dist_in_layer = clamp(dist_in_layer, 0, 20);

  vec2 layer_delta = (-view_dir * dist_in_layer).xy;

  vec2 layer_texcoord_offset = metersToForestLayerUVRelative(layer_delta);

  for (int i = 0; i < 5; i++) // FIXME de-hardcode
  {
    #if @terrain_enable_forest_pass@
      // FIXME - do this outside the loop
      if (i > 0 && dist <= 2000) // FIXME de-hardcode
        break;
    #endif

    vec2 texcoords_offset = layer_texcoord_offset * i;
    vec2 texcoords = pass_vertex_result.forest_texcoords + texcoords_offset;

#if @terrain_forest_layers_separate_alpha@
    float add_threshold = 0;
    if (i == 0)
      add_threshold = 0.02;

    alpha = getForestAlpha(dist, add_threshold, layer_delta * i);
    alpha = applyForestAlphaNoise(alpha, dist, add_threshold, texcoords_offset);
#endif

    vec4 layer_color = texture(sampler_forest_layers, vec3(texcoords, i));

    layer_color.rgb *= light;

    layer_color.a *= alpha;

//     layer_color.a *=  step(2000, dist); //FIXME de-hardcode

    color = mix(color, layer_color.rgb, layer_color.a);
  }

  return color;
}


#endif // is_fragment

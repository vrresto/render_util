/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include terrain_definitions.glsl

//FIXME - forest is missing
//FIXME: it is assumed that far texture, heightmap and terrain grid resolutions are equal


layout(location = 0) out vec4 out_color;

uniform Terrain terrain;

uniform sampler2DArray sampler_terrain[@terrain_num_land_texture_samplers@];
uniform sampler2D sampler_terrain_base_layer_types;

#if @terrain_enable_altitudinal_zones@
uniform AltitudinalZone altitudinal_zones[@terrain_num_altitudinal_zones@];
#endif


const float type_map_resolution_m = 200; //FIXME make uniform


float getHeight()
{
  ivec2 coords = ivec2(gl_FragCoord.xy);

  #if @is_base_layer@
    return texelFetch(terrain.base_layer.height_map.sampler, coords, 0).r;
  #else
    return texelFetch(terrain.detail_layer.height_map.sampler, coords, 0).r;
  #endif
}


vec4 sampleTerrainTexture(in sampler2DArray sampler, vec3 coord, float tile_size_m)
{
  const float lod_bias = 0.5;

  float size_px = float(textureSize(sampler, 0).x);
  float texel_size_m = tile_size_m / size_px;
  float texels_per_type_map_pixel = type_map_resolution_m  / texel_size_m;
  float lod = (log(texels_per_type_map_pixel) / log(2.0)) + lod_bias;

  return textureLod(sampler, coord, lod);
}


vec4 sampleTerrainTexture(int sampler_nr, vec3 coord, float tile_size_m)
{
  switch (sampler_nr)
  {
    case 0:
      return sampleTerrainTexture(sampler_terrain[0], coord, tile_size_m);
  #if @terrain_num_land_texture_samplers@ > 1
    case 1:
      return sampleTerrainTexture(sampler_terrain[1], coord, tile_size_m);
  #endif
  #if @terrain_num_land_texture_samplers@ > 2
    case 2:
      return sampleTerrainTexture(sampler_terrain[2], coord, tile_size_m);
  #endif
  #if @terrain_num_land_texture_samplers@ > 3
    case 3:
      return sampleTerrainTexture(sampler_terrain[3], coord, tile_size_m);
  #endif
    default:
      return vec4(0);
  }
}


vec3 getColor(vec3 type, vec2 coords_terrain_grid)
{
  int sampler_nr = int(type.x * 255);
  uint index = uint(type.y * 255);
  uint scale_level = uint(type.z * 255);

  float scale = terrain.land_texture_scale_levels[scale_level];
//   float tile_size_terrain_grid_scaled = terrain.tile_size_grid * scale;
  vec2 coord_m = coords_terrain_grid * type_map_resolution_m;

  float tile_size_m = 1600 * scale; //FIXME

  vec2 texcoord = coord_m / tile_size_m;

//   float tile_size_texels = tile_size_m / type_map_resolution_m;

  return sampleTerrainTexture(sampler_nr, vec3(texcoord, float(index)), tile_size_m).rgb;
}


vec3 getDefaultColor(vec2 coords_terrain_grid)
{
  float height = getHeight();

  vec3 color = vec3(1,0,0);

  #if @terrain_enable_altitudinal_zones@
    for (int i = 0; i < @terrain_num_altitudinal_zones@; i++)
    {
      #if @terrain_enable_altitudinal_zone_texture@
        vec3 zone_color = getColor(altitudinal_zones[i].type.xyz, coords_terrain_grid);
      #else
        vec3 zone_color = altitudinal_zones[i].color;
      #endif

      color = mix(color, zone_color,
                  smoothstep(altitudinal_zones[i].transition_start_height,
                             altitudinal_zones[i].transition_end_height,
                             height));
    }
  #endif

  return color;
}


vec3 getBaseLayerTypeColor(vec4 type_texel, vec3 default_color, vec2 coords_terrain_grid)
{
  int type_index = int(type_texel.r * 255);
  vec4 type = texelFetch(sampler_terrain_base_layer_types, ivec2(type_index, 0), 0);

  vec3 color = getColor(type.xyz, coords_terrain_grid);

  if (type_index == 0)
    color = default_color;

  return color;
}


vec3 getTypeColor(vec3 default_color, vec2 coords_terrain_grid)
{
  vec3 color = vec3(0);

  #if @is_base_layer@
    vec4 type = texelFetch(terrain.base_layer.type_map.sampler,
                           ivec2(gl_FragCoord.xy), 0);
  #else
    vec4 type = texelFetch(terrain.detail_layer.type_map.sampler,
                           ivec2(gl_FragCoord.xy), 0);
  #endif

  #if @is_base_layer@
    color = getBaseLayerTypeColor(type, default_color, coords_terrain_grid);
  #else
    color = getColor(type.xyz, coords_terrain_grid);
  #endif

  return color;
}


void main(void)
{
  vec2 coords_terrain_grid = gl_FragCoord.xy;

  //FIXME this assumes grid resolution equals type map resolution
  #if @is_base_layer@
    coords_terrain_grid -= terrain.base_layer.map_texture_offset_px;
  #else
    coords_terrain_grid -= terrain.detail_layer.map_texture_offset_px;
  #endif

  #if @is_base_layer@
    vec3 default_color = getDefaultColor(coords_terrain_grid);
    vec3 color = getTypeColor(default_color, coords_terrain_grid);
  #else
    vec3 color = getTypeColor(vec3(0), coords_terrain_grid);
  #endif

  out_color = vec4(color, 1);
}

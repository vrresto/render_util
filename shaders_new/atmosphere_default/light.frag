#include light_definitions.glsl


uniform vec3 sun_dir;


vec3 calcIncomingDirectLight()
{
  vec3 directLightColor = vec3(1.0, 1.0, 0.95);
  vec3 directLightColorLow = directLightColor * vec3(1.0, 0.9, 0.6);

  directLightColorLow *= 0.8;

  vec3 directLightColorVeryLow = directLightColorLow * vec3(1.0, 0.67, 0.37);

  directLightColor = mix(directLightColorLow, directLightColor, smoothstep(0.0, 0.3, sun_dir.z));
  directLightColor = mix(directLightColorVeryLow, directLightColor, smoothstep(-0.02, 0.1, sun_dir.z));
  directLightColor *= smoothstep(-0.02, 0.02, sun_dir.z);

  return directLightColor;
}


void getReflectedLightTerrain(in LightParams light, vec3 normal, out vec3 direct, out vec3 ambient)
{
  ambient = light.ambient;
  direct = light.sun * max(dot(normal, sun_dir), 0.0);
  //TODO
//   direct += light.moon * max(dot(normal, moon_dir), 0.0);
}


void getIncomingLight(vec3 pos, out LightParams light)
{
  light.ambient = vec3(0.95, 0.98, 1.0);
  light.ambient *= 0.6;
  vec3 ambientLightColorLow = light.ambient * 0.6;
  light.ambient = mix(ambientLightColorLow, light.ambient, smoothstep(0.0, 0.3, sun_dir.z));
  light.ambient *= smoothstep(-0.4, 0.0, sun_dir.z);

  light.sun = calcIncomingDirectLight();
  //TODO
  light.moon = vec3(0);
}

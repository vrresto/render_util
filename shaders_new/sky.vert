/**
 *    Rendering utilities
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 430

#include atmosphere_definitions.glsl

uniform mat4 projection;
uniform mat4 world_to_view;
uniform vec3 camera_pos;

layout(location = 0) in vec4 vertex;

out vec3 pass_pos;

void main(void)
{
  vec4 pos = vertex;

  pos.xyz *= 100;

  pos.xyz += camera_pos;

  pass_pos = pos.xyz;

  gl_Position = projection * world_to_view * pos;

#if @enable_atmosphere@
  atmosphereVertexMain();
#endif
}

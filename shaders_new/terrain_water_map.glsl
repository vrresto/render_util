/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330


#if !@terrain_enable_water_map@
  #error Water map is not enabled
#endif


#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl
#include util_definitions.glsl


uniform Terrain terrain;


varying TerrainVertexResult pass_vertex_result;
varying vec3 pass_pos_flat; //FIXME


float sampleWaterMapChunk(vec2 table_coords, ivec2 chunk_coords, in TerrainWaterMap map)
{
  chunk_coords = clamp(chunk_coords, ivec2(0), map.table_size - ivec2(1));

  int chunk_index = int(texelFetch(map.sampler_table, chunk_coords, 0).x);

  int block_index = chunk_index / map.chunks_per_block;
  int block_subindex = chunk_index % map.chunks_per_block;

  // chunk position in block
  int chunk_pos_x = block_subindex % map.chunks_num_cols;
  int chunk_pos_y = block_subindex / map.chunks_num_cols;

  vec2 chunk_relative_coords = table_coords - vec2(chunk_coords);

  // clamp to edge / avoid bleeding over of adjacent chunks
  chunk_relative_coords = clamp(chunk_relative_coords, vec2(0), vec2(1));

  chunk_relative_coords *= map.chunk_sample_scale;
  chunk_relative_coords += map.chunk_sample_offset;

  vec2 coords_offset =
    vec2(chunk_pos_x, chunk_pos_y) /
    vec2(map.chunks_num_cols, map.chunks_num_rows);

  vec3 coords;
  coords.xy = coords_offset +
              (chunk_relative_coords / vec2(map.chunks_num_cols, map.chunks_num_rows));
  coords.z = block_index;

#if @terrain_water_map_smooth_sampling@
  return sampleAlphaMap(map.sampler_chunk_blocks, coords);
#else
  return texture(map.sampler_chunk_blocks, coords).x;
#endif
}


float sampleWaterMapAtTableCoords(vec2 coords, in TerrainWaterMap map)
{
  float border = map.chunk_sample_offset;
//   border = water_map_chunk_sample_offset * 4;

  ivec2 chunk_coords = ivec2(floor(coords));
  vec2 chunk_relative_coords = fract(coords);

  ivec2 adjacent_chunk_offset_sign;
  adjacent_chunk_offset_sign.x = chunk_relative_coords.x < 0.5 ? -1 : 1;
  adjacent_chunk_offset_sign.y = chunk_relative_coords.y < 0.5 ? -1 : 1;

//   vec2 nearest_grid_offset = round(coords) - coords;
//   vec2 nearest_grid_distance = abs(nearest_grid_offset);
//   ivec2 adjacent_chunk_offset_sign = ivec2(nearest_grid_offset / nearest_grid_distance);
  ivec2 adjacent_chunk1_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(1,0);
  ivec2 adjacent_chunk2_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(0,1);
  ivec2 adjacent_chunk3_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(1,1);

  float value = sampleWaterMapChunk(coords, chunk_coords, map);
  float value1 = sampleWaterMapChunk(coords, adjacent_chunk1_coords, map);
  float value2 = sampleWaterMapChunk(coords, adjacent_chunk2_coords, map);
  float value3 = sampleWaterMapChunk(coords, adjacent_chunk3_coords, map);

  vec2 adjacent_blend = vec2(1) - (abs(round(coords) - coords) / border);
//   vec2 adjacent_blend = vec2(1) - (nearest_grid_distance / border);
  adjacent_blend = clamp(adjacent_blend, vec2(0), vec2(1));

  vec4 weights = vec4(0);
  weights[3] = 0.25 * adjacent_blend.x * adjacent_blend.y;
  weights[1] = 0.5 * adjacent_blend.x - weights[3];
  weights[2] = 0.5 * adjacent_blend.y - weights[3];
  weights[0] = 1.0 - (weights[1] + weights[2] + weights[3]);

  return value * weights[0] +
         value1 * weights[1] +
         value2 * weights[2] +
         value3 * weights[3];
}


float sampleWaterMapAtPos(vec2 pos, in TerrainWaterMap map)
{
  //FIXME - pass layer

  vec2 waterMapTablePos = pos.xy;

  if (map.y_flip)
    waterMapTablePos.y = terrain.detail_layer.size_m.y - waterMapTablePos.y;

  vec2 waterMapTableCoords = waterMapTablePos / map.chunk_size_m;

  return 1.0 - sampleWaterMapAtTableCoords(waterMapTableCoords, map);
}


float sampleSmallWaterMap()
{
  return texture(terrain.detail_layer.small_water_map.sampler,
                 pass_vertex_result.detail_layer.normal_map_coords).r;
}


float sampleWaterMap(float detail_layer_blend)
{
  //FIXME add pass_vertex_result.*_layer.small_water_map_coords

  #if @terrain_enable_detail_layer@
    #if @terrain_enable_detail_water_map@
      float water = sampleWaterMapAtPos(pass_pos_flat.xy, terrain.detail_layer_water_map);
    #else
      float water = texture(terrain.detail_layer.small_water_map.sampler,
                            pass_vertex_result.detail_layer.normal_map_coords).r;
    #endif
  #endif

  #if @terrain_enable_base_layer@
    float water_base = texture(terrain.base_layer.small_water_map.sampler,
                          pass_vertex_result.base_layer.normal_map_coords).r;
  #endif

  #if @terrain_enable_detail_layer@ && @terrain_enable_base_layer@
    return mix(water_base, water, detail_layer_blend);
  #elif @terrain_enable_detail_layer@
    return water;
  #elif @terrain_enable_base_layer@
    return water_base;
  #else
    return 0.0;
  #endif
}


float getWaterAlpha(float water_map_value)
{
  float water_surface_depth = 0.5;
  float transition = 0.01;
  return smoothstep(water_surface_depth - transition/2.0,
                    water_surface_depth + transition/2.0,
                    water_map_value);
}

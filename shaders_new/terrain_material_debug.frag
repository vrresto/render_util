#version 430

#include terrain_definitions.glsl

in vec3 pass_pos_flat;


uniform Terrain terrain;;


vec3 getMaterialMapDebugColor()
{
  vec2 coords = terrainMapUVFromGrid(terrain.detail_layer,
                                     terrain.detail_layer.material_map_debug,
                                     pass_pos_flat.xy / terrain.mesh_resolution_m);

  vec4 material = texture(terrain.detail_layer.material_map_debug.sampler, coords);

  return vec3(material.w, material.x, material.y);
}

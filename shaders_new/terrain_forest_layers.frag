/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include light_definitions.glsl
#include atmosphere_definitions.glsl
#include terrain_definitions.glsl


layout(location = 0) out vec4 out_color0;

uniform sampler2DArray sampler_forest_layers;

uniform Terrain terrain;
uniform float curvature_map_max_distance;

uniform vec3 camera_pos;
uniform vec4 base_map_tint;
uniform bool blend_water_depth;
uniform vec3 sun_dir;
uniform int forest_layer;
uniform float terrain_height_offset;

varying float pass_vertex_horizontal_dist;
varying vec3 pass_pos_flat;
varying vec3 pass_pos;
varying vec3 pass_pos_view;
varying TerrainVertexResult pass_vertex_result;


void main(void)
{
//   if (forest_layer == 0)
//     discard;

#if @terrain_enable_forest@ && @terrain_enable_forest_pass@
  out_color0 = vec4(0,0,0,1);

  vec3 view_dir = normalize(camera_pos - pass_pos);

  float dist = length(pass_pos_view);

  if (dist > 2000) //FIXME de-hardcode
    discard;

  float detail_blend = getDetailMapBlend(pass_pos_flat.xy);

  vec3 normal = getTerrainNormal(pass_vertex_result, detail_blend);

  LightParams light;
  getIncomingLight(pass_pos, light);

  vec3 light_direct;
  vec3 light_ambient;
  getReflectedLightTerrain(light, normal, light_direct, light_ambient);
  vec3 light_combined = clamp(light_direct + light_ambient, vec3(0), vec3(1));

  vec2 texcoords = pass_vertex_result.forest_texcoords;

  vec4 color = texture(sampler_forest_layers, vec3(texcoords, forest_layer));

  out_color0 = color;

  out_color0.rgb *= light_combined;

  float add_threshold = 0;

//   if (forest_layer == 0)
    add_threshold = 0.02;

  float alpha = getForestAlpha(dist, add_threshold, vec2(0));
  alpha = applyForestAlphaNoise(alpha, dist, add_threshold, vec2(0));

  out_color0.a *= alpha;

//     out_color0.a /= 2;

  out_color0.a *= 1 - step(2000, dist); //FIXME de-hardcode

#if @enable_atmosphere@
  out_color0.rgb = fogAndToneMap(out_color0.rgb, camera_pos, pass_pos);
#endif

#else
  discard;
#endif
}

vec3 hsv2rgb(vec3 hsv);
vec2 rotate(vec2 v, float a);
float sampleAlphaMap(in sampler2DArray map, vec3 coords);
vec3 blend_rnm(vec3 n1, vec3 n2);

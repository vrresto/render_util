#version 330

#if @terrain_water_enable_animation@

#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl
#include util_definitions.glsl

#define PARALLAX_USE_TEXTURE_GRAD 1

uniform sampler2DArray sampler_water_normal_map_interpolated;
uniform sampler2D sampler_water_foam_texture;

uniform TerrainWaterParameters terrain_water_parameters;
uniform float sea_roughness;
uniform vec3 earth_center;


varying TerrainVertexResult pass_vertex_result;
varying vec3 pass_pos;
varying vec3 pass_pos_flat;


struct SamplingParameters
{
  ImplicitDerivatives implicit_derivatives[@terrain_water_animation_num_layers@];
};


vec3 blendNormal(vec3 n1, vec3 n2, float dist, float vis, float strength)
{
  float detailFactor = exp(-pow(3 * (dist/vis), 2));

//   float detailFactor = 1 - smoothstep(vis / 2, vis, dist);

  strength *= sea_roughness;

  n2.xy *= detailFactor * strength;

  return blend_rnm(n1, normalize(n2));
}


vec2 getWaterAnimationTexCoords(int layer, vec2 coords_offset)
{
  float tile_size_m = terrain_water_parameters.animation_layers[layer].tile_size_m;

  vec2 coords = pass_vertex_result.water_normal_map_coords[layer];
  coords += (coords_offset / tile_size_m);

  return coords;
}


vec4 sampleWaterAnimation(int layer, vec2 coords_offset)
{
  vec2 coords = getWaterAnimationTexCoords(layer, coords_offset);

  vec4 tex = texture(sampler_water_normal_map_interpolated, vec3(coords, layer));

  tex.xyz = tex.xyz * 2 - 1;
//   tex.y *= -1;

  return tex;
}


vec4 sampleWaterAnimationGrad(int layer, vec2 coords_offset,
                              in SamplingParameters params)
{
  vec2 coords = getWaterAnimationTexCoords(layer, coords_offset);

  ImplicitDerivatives d = params.implicit_derivatives[layer];

  vec4 tex = textureGrad(sampler_water_normal_map_interpolated, vec3(coords, layer),
                         d.dx, d.dy);

  tex.xyz = tex.xyz * 2 - 1;
//   tex.y *= -1;

  return tex;
}


vec4 sampleWaterAnimation(int layer)
{
  return sampleWaterAnimation(layer, vec2(0));
}


vec4 sampleFoamTexture(vec2 coords)
{
  return texture(sampler_water_foam_texture, coords);
}


vec4 getFoamTexture(vec2 coords_offset, vec2 pos_flat, float foam_alpha)
{
  vec2 coords = (pos_flat + coords_offset) / 30; //FIXME
  return vec4(vec3(1), sampleFoamTexture(coords).r * foam_alpha);
}


#if @terrain_water_enable_parallax@
float sampleWaterDepth(int layer, vec2 displacement
#if PARALLAX_USE_TEXTURE_GRAD
                      , in SamplingParameters params
#endif
                      )
{
#if PARALLAX_USE_TEXTURE_GRAD
//   float height0 = sampleWaterAnimationGrad(1, displacement, params).w;
  float height1 = sampleWaterAnimationGrad(2, displacement, params).w;
  float height2 = sampleWaterAnimationGrad(3, displacement, params).w;
#else
//   float height0 = sampleWaterAnimation(1, displacement).w;
  float height1 = sampleWaterAnimation(2, displacement).w;
  float height2 = sampleWaterAnimation(3, displacement).w;
#endif

  float height = mix(height1, height2, 0.5);

  return 1.0 - height;
}
#endif


#if @terrain_water_enable_parallax@
vec2 getWaterParallaxOffsetFast(vec3 view_dir, float dist, float wave_strength)
{
  float height = sampleWaterAnimation(2).w * sea_roughness * 4;

//   vec2 displacement = ((-view_dir) * height).xy * 2;
  vec2 displacement = (view_dir).xy  / (view_dir).z * height * 0.5;

  return displacement;
}
#endif


#if @terrain_water_enable_parallax@
// see parallax example on learnopengl.com
vec2 getWaterParallaxOffset(vec3 view_dir, float dist)
{
  const int layer = 2;
#if DEBUG_GEOMETRY
  const float layers = 20;
#else
  const float layers = 20;
#endif
  float layer_depth = 1.0 / layers;

  float current_layer_depth = 0;

#if DEBUG_GEOMETRY
  vec2 P_near = -view_dir.xy / -view_dir.z * 16;
#else
  vec2 P_near = -view_dir.xy / -view_dir.z * 16;
#endif
  vec2 P_far = -view_dir.xy * 8;

  vec2 P = mix(P_near, P_far, 1-exp(-1 * (dist / 400)));

  P *= sea_roughness;

  vec2 delta_texcoords = P / layers;

  vec2 current_offset = vec2(0);

  SamplingParameters params;

  for (int i = 0; i < @terrain_water_animation_num_layers@; i++)
  {
    vec2 coords = pass_vertex_result.water_normal_map_coords[i];
    params.implicit_derivatives[i].dx = dFdx(coords);
    params.implicit_derivatives[i].dy = dFdy(coords);
  }

#if PARALLAX_USE_TEXTURE_GRAD
  float current_depth_map_value = sampleWaterDepth(layer, current_offset, params);
#else
  float current_depth_map_value = sampleWaterDepth(layer, current_offset);
#endif

  while (current_layer_depth < current_depth_map_value)
  {
    current_offset -= delta_texcoords;

#if PARALLAX_USE_TEXTURE_GRAD
    current_depth_map_value = sampleWaterDepth(layer , current_offset, params);
#else
    current_depth_map_value = sampleWaterDepth(layer , current_offset);
#endif

    current_layer_depth += layer_depth;
  }

  vec2 prev_offset = current_offset + delta_texcoords;

  float after_depth = current_depth_map_value - current_layer_depth;

#if PARALLAX_USE_TEXTURE_GRAD
  float before_depth = sampleWaterDepth(layer, prev_offset, params)
                       - current_layer_depth + layer_depth;
#else
  float before_depth = sampleWaterDepth(layer, prev_offset)
                       - current_layer_depth + layer_depth;
#endif

  float weight = after_depth / (after_depth - before_depth);

  return prev_offset * weight + current_offset * (1.0 - weight);
}
#endif


void getWaterNormalAndFoam(inout TerrainWaterFragmentParameters params,
                           vec3 view_dir, float dist)
{
  vec3 pos = pass_pos;
  vec3 normal = normalize(pos - earth_center);
  vec4 foam = vec4(0);

  #if @terrain_enable_land@
    float water_depth = params.water_depth;
  #else
    const float water_depth = 1;
  #endif

  #if @terrain_water_enable_parallax@
    vec2 coords_offset = getWaterParallaxOffset(view_dir, dist);
//     vec2 coords_offset = getWaterParallaxOffsetFast(view_dir, dist);
  #else
    const vec2 coords_offset = vec2(0);
  #endif

  coords_offset *= water_depth;

  vec4 tex_0 = sampleWaterAnimation(0, coords_offset);
  vec4 tex_1 = sampleWaterAnimation(1, coords_offset);
  vec4 tex_2 = sampleWaterAnimation(2, coords_offset);
  vec4 tex_3 = sampleWaterAnimation(3, coords_offset);


  normal = blendNormal(normal, tex_0.xyz, dist, 15000, 0.3 * water_depth);
  normal = blendNormal(normal, tex_1.xyz, dist, 30000, 0.5 * water_depth);
  normal = blendNormal(normal, tex_2.xyz, dist, 30000, 0.5 * water_depth);
  normal = blendNormal(normal, tex_3.xyz, dist, 30000, 0.5 * water_depth);

  normal = normalize(normal);

  float foam_alpha = smoothstep(0.4, 0.6, tex_3.w * sea_roughness * water_depth * 1.5);
  foam_alpha = smoothstep(0.7, 0.95, tex_2.w * foam_alpha);

  foam = getFoamTexture(coords_offset, pass_pos_flat.xy, foam_alpha);

  params.normal = normal;
  params.foam = foam;
}

#else

void terrainWaterAnimationDummy() {}

#endif

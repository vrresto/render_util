#version 430

#include light_definitions.glsl


uniform vec3 sun_dir;


void getIncomingLight(vec3 pos, out LightParams light_params)
{
  light_params.ambient = vec3(0.1);
  light_params.moon = vec3(0);
  light_params.sun = vec3(1);
}

void getReflectedLightTerrain(in LightParams light, vec3 normal, out vec3 direct, out vec3 ambient)
{
  ambient = light.ambient;
  direct = light.sun * max(dot(normal, sun_dir), 0.0);
}

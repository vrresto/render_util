/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include terrain_definitions.glsl
#include terrain_water_definitions.glsl


uniform Terrain terrain;
uniform TerrainWaterParameters terrain_water_parameters;

varying TerrainVertexResult pass_vertex_result;


vec2 getWaterTexCoord(vec2 pos_terrain_grid, float tile_size_grid)
{
  return pos_terrain_grid / tile_size_grid;
}


void terrainWaterVertexMain(vec2 pos_terrain_grid)
{
#if @terrain_water_enable_animation@
  for (int i = 0; i < @terrain_water_animation_num_layers@; i++)
  {
    pass_vertex_result.water_normal_map_coords[i] =
      getWaterTexCoord(pos_terrain_grid,
                       terrain_water_parameters.animation_layers[i].tile_size_grid);
  }
#endif

#if @terrain_water_enable_texture@
  pass_vertex_result.water_texcoords =
    getTerrainTexCoords(pos_terrain_grid, terrain.tile_size_grid);
#endif
#if @terrain_water_enable_noise_texture@
  pass_vertex_result.water_noise0_texcoords =
    getTerrainTexCoords(pos_terrain_grid, terrain.tile_size_grid / 16.0);
  pass_vertex_result.water_noise1_texcoords =
    getTerrainTexCoords(pos_terrain_grid, terrain.tile_size_grid / 64.0);
#endif
}

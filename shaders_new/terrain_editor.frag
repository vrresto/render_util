/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl
#include util_definitions.glsl

#define ENABLE_TERRAIN_LAYER_SCALE @enable_terrain_layer_scale@


float getWaterFoam(vec3 pos, float dist, vec2 coord);


layout(location = 0) out vec4 out_color0;

uniform Terrain terrain;
uniform float curvature_map_max_distance;

uniform vec3 camera_pos;
uniform vec4 base_map_tint;
uniform bool blend_water_depth;
uniform vec3 sun_dir;

varying float pass_vertex_horizontal_dist;
varying vec3 pass_pos_flat;
varying vec3 pass_pos;
varying vec3 pass_pos_view;
varying TerrainVertexResult pass_vertex_result;
varying float pass_terrain_lod;


bool isClipped(in vec2 pos, in TerrainLayer layer)
{
#if ENABLE_TERRAIN_LAYER_SCALE
  vec2 layer_size = layer.size_m * layer.scale;
#else
  vec2 layer_size = layer.size_m;
#endif

  if (any(lessThan(pos, layer.origin_m.xy)) ||
      any(greaterThan(pos, layer.origin_m.xy + layer_size)))

  {
    return true;
  }
  else
  {
    return false;
  }
}


vec3 getColorAtHeight(float height)
{
  vec3 color = vec3(0.5, 0.5, 0.5);

  vec3 color_base = vec3(0.05, 0.3, 0.6) * 0.8;
  vec3 color0 = vec3(0.1, 0.5, 0.2) * 0.9;
  vec3 color1 = vec3(0.3, 0.5, 0);
  vec3 color2 = vec3(0.5, 0.5, 0.0);
  vec3 color3 = vec3(1,1,1);

//   color = mix(color_base, color0, clamp(height / 2, 0, 1));

//   color = color_base;

//   color = mix(color, color1, clamp(height / 10, 0, 1));

  color = color1;

  color = mix(color, color2, clamp(height / 300, 0, 1));
  color = mix(color, color3, clamp(height / 2000, 0, 1));

  return color;
}


void main(void)
{
#if @terrain_enable_lod_debug@
  float hue = pass_terrain_lod / float(@terrain_lod_count@);
  vec3 hsv = vec3(hue, 1.0, 0.9);
  out_color0 = vec4(hsv2rgb(hsv), 1);
  return;
#endif

#if @terrain_enable_base_layer@
  if (isClipped(pass_pos_flat.xy, terrain.base_layer) &&
      isClipped(pass_pos_flat.xy, terrain.detail_layer))
    discard;
#else
  if (isClipped(pass_pos_flat.xy, terrain.detail_layer))
    discard;
#endif

  out_color0 = vec4(0,0,0,1);

  float detail_blend = getDetailMapBlend(pass_pos_flat.xy);

  float height = getTerrainHeight(pass_vertex_result, detail_blend);

#if @terrain_enable_texture@
  TerrainTextureFragmentData texture_fragment_data;
  fetchImplicitDerivatives(texture_fragment_data);
  texture_fragment_data.terrain_height = height;
  texture_fragment_data.detail_map_blend = detail_blend;
#endif

//   float water_depth = getWaterDepth(pass_pos_flat.xy);
  float water_depth = 1;

  float tint_amount = 1 - detail_blend;
//
// #if @terrain_enable_base_layer@
//   float water_depth_base = getWaterDepthBase(pass_pos_flat.xy);
//
//   if (blend_water_depth)
//     water_depth = mix(water_depth_base, water_depth, detail_blend);
//   else
//     water_depth *= water_depth_base;
// #endif

  vec3 normal = getTerrainNormal(pass_vertex_result, detail_blend);

  LightParams light_params;
  getIncomingLight(pass_pos, light_params);

  vec3 light_direct;
  vec3 light_ambient;
  getReflectedLightTerrain(light_params, normal, light_direct, light_ambient);
  vec3 light_combined = clamp(light_direct + light_ambient, vec3(0), vec3(1));

//   float height_detail = getTerrainHeightDetail(pass_pos_flat.xy);
//   float height_base = getTerrainHeightBase(pass_pos_flat.xy);

//   vec3 color_base = getColorAtHeight(height_base);

  out_color0.rgb = getColorAtHeight(height);

#if @terrain_enable_texture@
  out_color0.rgb = getTerrainTextureColor(texture_fragment_data);
#endif

  out_color0.rgb *= light_combined;

  tint_amount *= base_map_tint.a;

  out_color0.rgb = mix(out_color0.rgb, base_map_tint.rgb, tint_amount);

  float dist = length(pass_pos_view);

#if @terrain_enable_water@
//   water = smoothstep(0.4, 0.6, water);
  vec3 view_dir = -normalize(camera_pos - pass_pos);

  float water = sampleWaterMap(detail_blend);

  vec4 foam = vec4(0);
  vec3 water_normal = vec3(1);
  getWaterNormalAndFoam(pass_pos, dist, pass_pos_flat.xy, view_dir, water_normal, foam);

  vec3 water_color = getWaterColor(water_normal, foam, view_dir, dist, light_params);

  out_color0.rgb = mix(out_color0.rgb, water_color, water);
#endif

#if @terrain_enable_forest@
  out_color0.rgb = applyForest(out_color0.rgb, light_combined, normal, dist);
#endif

//   out_color0.rgb = mix(out_color0.rgb, vec3(0.0, 0.2, 0.3), water_depth);

//   out_color0.rgb = getColorAtHeight(max(height_base, height_detail));

//   out_color0.rgb = color_base;

//   if (height_detail > 0.2)
//   {
//     out_color0.rgb = mix(out_color0.rgb, out_color0.rgb * vec3(2, 1, 1), detail_blend);
//   }

//   out_color0.rgb = mix(out_color0.rgb, vec3(0,0,1), water_depth);

//   out_color0.rgb = mix(out_color0.rgb, out_color0.rgb * vec3(2,1,1), detail_blend * (1 - water_depth));
//   out_color0.rgb = mix(out_color0.rgb, vec3(0.7, 0.5, 0.3), detail_blend * (1 - water_depth));

//   out_color0.rgb *= (light_direct * 1.5 + light_ambient * 0.5);

#if 0
#if @terrain_enable_base_layer@
  float height_base = getTerrainHeightBase(pass_pos_flat.xy);
#endif
  float height_detail = getTerrainHeightDetail(pass_pos_flat.xy);

  out_color0.rgb = vec3(0.0);
  float height_grid = 150;

//   float height = getTerrainHeight(pass_pos_flat.xy);

  float dist_to_height_line_detail = distance(height_detail / height_grid,
                                       round(height_detail / height_grid));

  if (pass_pos_flat.x > 0 && pass_pos_flat.x < terrain.detail_layer.size_m.x &&
      pass_pos_flat.y > 0 && pass_pos_flat.y < terrain.detail_layer.size_m.y)
  {
    int color = int(floor(height_detail / height_grid)) % 2;

//     if (dist_to_height_line_detail < 0.01)
//       out_color0.r = 1;
    out_color0.r = mix(out_color0.r, 1, color);
  }


  float dist_to_height_line_base = distance(height_base / height_grid,
                                       round(height_base / height_grid));

//   if (dist_to_height_line_base < 0.01)
//     out_color0.g = 1;

  int line_base = int(floor(height_base / height_grid)) % 2;
  out_color0.g = mix(out_color0.g, 1, line_base);
#endif
}

/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330


struct WaterAnimationParameters
{
  float frame_delta;
  int pos;
};


uniform sampler2DArray sampler_water_normal_map;
uniform int water_animation_num_frames = 0; // FIXME make shader parameter
uniform WaterAnimationParameters water_animation_params[@terrain_water_animation_num_layers@];
uniform int layer;

layout(location = 0) out vec4 out_color;


int getWaterAnimationPos(int frame_offset, int layer)
{
//   return 0;

  return (water_animation_params[layer].pos + frame_offset) % water_animation_num_frames;
}


vec4 interpolateWaterAnimationFrames(vec4 texA, vec4 texB, vec4 texC, int layer)
{
  vec4 phase1A = texA;
  vec4 phase1B = texB;
  vec4 phase2A = texA;
  vec4 phase2B = texB;

  float frame_delta = water_animation_params[layer].frame_delta;

  float frame_delta_2 = frame_delta + 0.5;
  if (frame_delta_2 > 1.0)
  {
    frame_delta_2 -= 1.0;
    phase2A = texB;
    phase2B = texC;
  }

  vec4 phase1 = mix(phase1A, phase1B, frame_delta);
  vec4 phase2 = mix(phase2A, phase2B, frame_delta_2);

  return mix(phase1, phase2, 0.5);
}


vec4 getTexel(int layer, ivec2 coords)
{
  vec4 texA = texelFetch(sampler_water_normal_map,
                      ivec3(coords, getWaterAnimationPos(0, layer)), 0);
  vec4 texB = texelFetch(sampler_water_normal_map,
                      ivec3(coords, getWaterAnimationPos(1, layer)), 0);
  vec4 texC = texelFetch(sampler_water_normal_map,
                      ivec3(coords, getWaterAnimationPos(2, layer)), 0);

  vec4 interpolated = interpolateWaterAnimationFrames(texA, texB, texC, layer);

  return interpolated;
}


void main(void)
{
  out_color = getTexel(layer, ivec2(gl_FragCoord.xy));
}

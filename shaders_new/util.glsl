/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *    Used techiques
 *
 *    Reoriented Normal Mapping:
 *    http://blog.selfshadow.com/publications/blending-in-detail/
 */

#version 430


// uniform ivec2 typeMapSize;


// vec2 cropCoords(vec2 coords, float crop_amount)
// {
//   float scale = 1 - 2  * crop_amount;
// 
//   coords = fract(coords);
//   coords *= scale;
//   coords += crop_amount;
// 
//   return coords;
// }

// vec3 intersect(vec3 lineP,
//                vec3 lineN,
//                vec3 planeN,
//                float  planeD)
// {
//   float distance = (planeD - dot(planeN, lineP)) / dot(lineN, planeN);
//   return lineP + lineN * distance;
// }


vec2 rotate(vec2 v, float a)
{
  float sn = sin(a);
  float cs = cos(a);

  float px = v.x * cs - v.y * sn; 
  float py = v.x * sn + v.y * cs;

  return vec2(px, py);
}


// see http://blog.selfshadow.com/publications/blending-in-detail/
vec3 blend_rnm(vec3 n1, vec3 n2)
{
  n1 = (n1 + vec3(1)) / 2;
  n2 = (n2 + vec3(1)) / 2;

  vec3 t = n1.xyz*vec3( 2,  2, 2) + vec3(-1, -1,  0);
  vec3 u = n2.xyz*vec3(-2, -2, 2) + vec3( 1,  1, -1);
  vec3 r = t*dot(t, u) - u*t.z;

  return normalize(r);
}


//http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


float fetchAlphaMapTexel(in sampler2DArray map, int x, int y, int idx)
{
  ivec2 coords = clamp(ivec2(x,y), ivec2(0),
                       textureSize(map, 0).xy - ivec2(1));
  return texelFetch(map, ivec3(coords, idx), 0).r;
}


float sampleAlphaMap(in sampler2DArray map, vec3 coords)
{
  vec2 texel_coords = coords.xy * textureSize(map, 0).xy;
  int idx = int(coords.z);

  texel_coords -= 0.5;

  vec2 weight = fract(texel_coords);

  ivec2 min_coords = ivec2(floor(texel_coords));
  ivec2 max_coords = min_coords + 1;

  float bottom_left = fetchAlphaMapTexel(map, min_coords.x, min_coords.y, idx);
  float bottom_right = fetchAlphaMapTexel(map, max_coords.x, min_coords.y, idx);
  float top_left = fetchAlphaMapTexel(map, min_coords.x, max_coords.y, idx);
  float top_right = fetchAlphaMapTexel(map, max_coords.x, max_coords.y, idx);

  float bottom = mix(bottom_left, bottom_right, weight.x);
  float top = mix(top_left, top_right, weight.x);

  return mix(bottom, top, weight.y);
}

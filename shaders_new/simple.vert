#version 130

uniform mat4 projection;
uniform mat4 world_to_view;

varying vec2 pass_texcoord;

void main(void)
{
  gl_Position = projection * world_to_view * gl_Vertex;
  pass_texcoord = gl_MultiTexCoord0.xy;
}

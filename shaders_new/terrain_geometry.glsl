/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 430

#include terrain_definitions.glsl

uniform Terrain terrain;
uniform vec3 camera_pos_terrain_integer_part;

in vec3 pass_normal;


vec2 getTerrainTexCoords(vec2 pos_terrain_grid, float tile_size_grid)
{
  //FIXME make this a uniform
  vec2 layer_origin_terrain_grid =
    terrain.detail_layer.origin_m.xy / terrain.mesh_resolution_m;

  #if ! @terrain_use_detail_layer_coordinate_system@
    #if @enable_terrain_layer_scale@
      tile_size_grid *= terrain.detail_layer.scale;
    #endif

    pos_terrain_grid -= layer_origin_terrain_grid;
  #endif

  vec2 origin_tile =
    floor((camera_pos_terrain_integer_part.xy - layer_origin_terrain_grid)
          / tile_size_grid);

#if @terrain_enable_land_texture_offset@
  pos_terrain_grid += terrain_land_texture_offset;
#endif

  vec2 texcoord = pos_terrain_grid  / tile_size_grid;
  texcoord -= origin_tile;

  return texcoord;
}


#if @enable_terrain_layer_scale@


vec2 terrainMapTexelCoordsFromGridWithOffset(in TerrainLayer layer,
                                            in TerrainAttributeMap map,
                                            vec2 pos_grid,
                                            vec2 offset_px)
{
  //FIXME this assumes mesh resolution equals type map resolution

  offset_px += layer.map_texture_offset_px;

  vec2 texel_coords = pos_grid - layer.origin_terrain_grid;
//   texel_coords /= map.resolution_terrain_grid; FIXME

  return (texel_coords + offset_px) / layer.scale;
}


#else


vec2 terrainMapTexelCoordsFromGridWithOffset(in TerrainLayer layer,
                                            in TerrainAttributeMap map,
                                            vec2 pos_grid,
                                            vec2 offset_px)
{
  //FIXME this assumes mesh resolution equals type map resolution

  offset_px += layer.map_texture_offset_px;

  vec2 texel_coords = pos_grid - layer.origin_terrain_grid;
//   texel_coords /= map.resolution_terrain_grid; FIXME

  return texel_coords + offset_px;
}


#endif


vec2 terrainMapTexelCoordsFromGrid(in TerrainLayer layer,
                                  in TerrainAttributeMap map,
                                  vec2 pos_grid)
{
  return terrainMapTexelCoordsFromGridWithOffset(layer, map, pos_grid, vec2(0));
}


vec2 terrainMapUVFromGrid(in TerrainLayer layer,
                          in TerrainAttributeMap map,
                          vec2 pos_grid)
{
  //FIXME this assumes mesh resolution equals map resolution
  return terrainMapTexelCoordsFromGridWithOffset(layer, map, pos_grid, vec2(0.5))
          / vec2(map.size_px);
}


vec3 sampleTerrainNormalMap(in TerrainLayer layer,
                            in TerrainVertexResultLayer vertex_result)
{
  vec3 normal = texture(layer.normal_map.sampler, vertex_result.normal_map_coords).xyz;
//   normal.y *= -1; //FIXME - if anywhere, this shouldn't be done in the shader
  return normal;
}


float fetchAlphaMapTexel(in TerrainAttributeMap map, int x, int y)
{
  ivec2 coords = clamp(ivec2(x,y), ivec2(0),
                       map.size_px - ivec2(1));
  return texelFetch(map.sampler, coords, 0).r;
}


float sampleAlphaMap(in TerrainAttributeMap map, vec2 texel_coords)
{
  vec2 weight = fract(texel_coords);

  ivec2 min_coords = ivec2(floor(texel_coords));
  ivec2 max_coords = min_coords + 1;

  float bottom_left = fetchAlphaMapTexel(map, min_coords.x, min_coords.y);
  float bottom_right = fetchAlphaMapTexel(map, max_coords.x, min_coords.y);
  float top_left = fetchAlphaMapTexel(map, min_coords.x, max_coords.y);
  float top_right = fetchAlphaMapTexel(map, max_coords.x, max_coords.y);

  float bottom = mix(bottom_left, bottom_right, weight.x);
  float top = mix(top_left, top_right, weight.x);

  return mix(bottom, top, weight.y);
}


vec3 getTerrainNormal(in TerrainVertexResult vertex_result, float detail_blend)
{
#if @terrain_use_pass_normal@
  return pass_normal;
#endif

  vec3 normal = vec3(0,0,1);

#if @terrain_enable_detail_layer@
  normal = sampleTerrainNormalMap(terrain.detail_layer, vertex_result.detail_layer);
#endif

#if @terrain_enable_base_layer@
  vec3 base_normal = sampleTerrainNormalMap(terrain.base_layer, vertex_result.base_layer);
  #if @terrain_enable_detail_layer@
    normal = mix(base_normal, normal, detail_blend);
  #else
    normal = base_normal;
  #endif
#endif

  return normal;
}


float getTerrainHeight(in TerrainVertexResult vertex_result, float detail_blend)
{
  float height = 0;

#if @terrain_enable_detail_layer@
  height = texture(terrain.detail_layer.height_map.sampler,
                           vertex_result.detail_layer.height_map_coords).x;
#endif

#if @terrain_enable_base_layer@
  float base = texture(terrain.base_layer.height_map.sampler,
                         vertex_result.base_layer.height_map_coords).x;
  base += terrain.base_layer.origin_m.z;

  #if @terrain_enable_detail_layer@
    height = mix(base, height, detail_blend);
  #else
    height = base;
  #endif
#endif

  return height;
}


float getDetailMapBlend(vec2 pos)
{
  const float blend_dist = 8000.0;

#if @enable_terrain_layer_scale@
  vec2 map_size = terrain.detail_layer.size_m * terrain.detail_layer.scale;
#else
  vec2 map_size = terrain.detail_layer.size_m;
#endif

  vec2 min_pos = terrain.detail_layer.origin_m.xy;
  vec2 max_pos = terrain.detail_layer.origin_m.xy + map_size;

#if @enable_detail_map_clip@
  vec2 clip_min = min_pos + detail_map_clip_min * map_size;
  vec2 clip_max = min_pos + detail_map_clip_max * map_size;

  if (any(lessThan(pos, clip_min)) || any(greaterThan(pos, clip_max)))
    return 0.0;
#endif

#if !@enable_terrain_layer_blend@ || @enable_detail_map_clip@
  if (all(greaterThan(pos, min_pos)) && all(lessThan(pos, max_pos)))
    return 1.0;
  else
    return 0.0;
#endif

  vec2 detail_blend =
    smoothstep(min_pos, min_pos + blend_dist, pos) -
    smoothstep(max_pos - blend_dist, max_pos, pos);

  return detail_blend.x * detail_blend.y;
}

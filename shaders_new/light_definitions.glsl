struct LightParams
{
  vec3 ambient;
  vec3 sun;
  vec3 moon;
};

void getIncomingLight(vec3 pos, out LightParams light);
void getReflectedLightTerrain(in LightParams light, vec3 normal, out vec3 direct, out vec3 ambient);

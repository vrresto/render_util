#version 430


uniform vec3 sun_dir;


#if @is_vertex@

out vec3 pass_sun_light;
out vec3 pass_sky_light;
out vec3 pass_ambient_light;


void atmosphereVertexMain()
{
  pass_sun_light =
    smoothstep(-0.05, 0.05, sun_dir.z) *
    mix(vec3(1.0, 0.3, 0.0),
        mix(vec3(1.0, 0.7, 0.2),
            vec3(1),
            smoothstep(0.05, 0.3, sun_dir.z)),
        smoothstep(0.0, 0.1, sun_dir.z));

  pass_sky_light =
    smoothstep(-0.2, 0.0, sun_dir.z) *
    mix(vec3(1.0, 0.5, 0.2), vec3(1), smoothstep(-0.1, 0.2, sun_dir.z));

  pass_ambient_light = vec3(0.95, 0.98, 1.0);
  pass_ambient_light *= 0.3;
  vec3 ambientLightColorLow = pass_ambient_light * 0.6;
  pass_ambient_light = mix(ambientLightColorLow,
                           pass_ambient_light,
                           smoothstep(0.0, 0.3, sun_dir.z));
  pass_ambient_light *= smoothstep(-0.2, 0.1, sun_dir.z);
}


#endif


#if @is_fragment@

in vec3 pass_sun_light;
in vec3 pass_sky_light;
in vec3 pass_ambient_light;

const float brightness_curve_exponent = 1.2;
const float gamma = 2.2;
const float exposure = 1.0;

const vec3 rgb_wavelengths = vec3(0.68, 0.55, 0.44);

const vec3 rayleigh_rgb_proportion =
  vec3((1.0/pow(rgb_wavelengths.r,4)) / (1.0/pow(rgb_wavelengths.b,4)),
       (1.0/pow(rgb_wavelengths.g,4)) / (1.0/pow(rgb_wavelengths.b,4)),
       1.0);

const vec3 rayleigh_scattering = rayleigh_rgb_proportion / 30000.0;
const vec3 mie_scattering = vec3(1) / 10000.0;

const float PI = acos(-1.0);
const float MIE_G = 0.8;


vec3 toneMap(vec3 color)
{
  color = pow(color, vec3(brightness_curve_exponent));
  color = pow(vec3(1.0)
          - exp(-color * pow(exposure, brightness_curve_exponent)), vec3(1.0 / gamma));
  return color;
}


float exponentialFogDensity(
    float scale_height,
    float height1, float height2)
{
  float minHeight = min(height1, height2);
  float maxHeight = max(height1, height2);
  float delta = distance(maxHeight, minHeight);

  if (delta < 1e-20)
    return exp(-(minHeight / scale_height));

  return (exp(-(minHeight / scale_height)) -
          exp(-(maxHeight / scale_height)))
         / (delta / scale_height);
}


vec3 avgTransmitance(vec3 extinction)
{
  return (exp(-extinction) - exp(vec3(0))) / (-extinction);
}


float miePhase(vec3 view_dir, vec3 light_dir)
{
  return (1.f - MIE_G*MIE_G) /
         (4*PI * pow(1.f + MIE_G*MIE_G - 2*MIE_G*dot(view_dir, light_dir), 1.5));
}


vec3 atmosphereColor(float air_dist, float haze_dist, vec3 view_dir)
{
  float mie_phase = miePhase(view_dir, sun_dir);

  vec3 avg_transmittance = avgTransmitance(rayleigh_scattering * air_dist);
  vec3 avg_transmittance_mie = avgTransmitance(mie_scattering * haze_dist);

  vec3 inscattering = vec3(0);

  inscattering +=
    pass_sky_light *
    avg_transmittance *
    rayleigh_scattering * air_dist;

  inscattering +=
    pass_sun_light *
    avg_transmittance_mie *
    mie_scattering * haze_dist * mie_phase;

  return toneMap(inscattering);
}


vec3 fogAndToneMap(vec3 color, vec3 camera_pos, vec3 frag_pos)
{
  float dist = distance(camera_pos, frag_pos);
  vec3 view_dir = -normalize(camera_pos - frag_pos);

  float optical_dist = dist * exponentialFogDensity(6000.0, camera_pos.z, frag_pos.z);
  float optical_dist_mie = dist * exponentialFogDensity(1200.0, camera_pos.z, frag_pos.z);

  vec3 transmittance = exp(-(rayleigh_scattering * optical_dist));

  color *= transmittance;

  vec3 atmosphere_color = atmosphereColor(optical_dist, optical_dist_mie, view_dir);

  color = mix(color, vec3(1), atmosphere_color);

  return color;
}


#endif

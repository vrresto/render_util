#version 430

#include light_definitions.glsl


uniform vec3 sun_dir;

#if @enable_atmosphere@
in vec3 pass_sun_light;
in vec3 pass_ambient_light;
#endif


void getReflectedLightTerrain(in LightParams light, vec3 normal, out vec3 direct, out vec3 ambient)
{
  ambient = light.ambient;
  direct = light.sun * max(dot(normal, sun_dir), 0.0);
  //TODO
//   direct += light.moon * max(dot(normal, moon_dir), 0.0);
}


void getIncomingLight(vec3 pos, out LightParams light)
{
#if @enable_atmosphere@
  light.sun = pass_sun_light;
  light.ambient = pass_ambient_light;
#else
  light.sun = vec3(1.0);
  light.ambient = vec3(0.3);
#endif

  //TODO
  light.moon = vec3(0);
}

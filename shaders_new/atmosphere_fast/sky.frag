/**
 *    Rendering utilities
 *    Copyright (C) 2022  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 430

uniform vec3 camera_pos;
uniform vec3 sun_dir;
uniform vec2 sun_size;

in vec3 pass_pos;

layout(location = 0) out vec4 out_color;

vec3 fogAndToneMap(vec3 color);
vec3 fogAndToneMap(vec3 color, vec3 view_dir, float dist);
vec3 fogAndToneMap(vec3 color, vec3 camera_pos, vec3 frag_pos);


vec3 applyFog( in vec3  rgb,      // original color of the pixel
               in float dist, // camera to point distance
               in vec3  rayOri,   // camera position
               in vec3  rayDir );  // camera to point vector


void main(void)
{
  out_color.w = 1.0;
  out_color.rgb = vec3(0.0, 0.0, 0.0);

//   out_color.rgb = fogAndToneMap(out_color.rgb,
//                                    -normalize(camera_pos - pass_pos),
//                                    500000);

//   out_color.rgb = fogAndToneMap(out_color.rgb,
//            -normalize(camera_pos - pass_pos),
//            500000);


  vec3 view_dir = -normalize(camera_pos - pass_pos);
  vec3 pos = camera_pos + view_dir * 2000000;

  float sun = smoothstep(sun_size.y - 0.00002, sun_size.y, dot(view_dir, sun_dir));

#if @enable_atmosphere@
  out_color.rgb = fogAndToneMap(out_color.rgb,
                                   camera_pos,
                                   pos);
#endif

  out_color.rgb += vec3(sun);
}

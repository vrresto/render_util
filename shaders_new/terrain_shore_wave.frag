#version 430

#if @terrain_enable_shore_waves@ && @terrain_enable_land@ && @terrain_enable_water@

#include util_definitions.glsl
#include light_definitions.glsl
#include terrain_definitions.glsl
#include terrain_water_definitions.glsl


const float PI = 3.14159265359;

uniform sampler2D sampler_shore_wave;
uniform vec4 shore_wave_scroll;

in vec3 pass_pos_flat;


float getShoreWaveNoise(float pos)
{
  return sin(pos * 5);
}


float sampleShoreWave(vec2 pos, float waterDepth, float offset)
{
  const float shore_wave_length = 600;
  const float rotation = 0.25 * PI;

  pos = (rotate(pos, rotation));

  float sampling_pos = (pos.y / shore_wave_length);
  float noise_sampling_pos = (pos.x / shore_wave_length) + sampling_pos;

  float noise = getShoreWaveNoise(noise_sampling_pos);

  sampling_pos += offset;

  sampling_pos += noise * 0.03;

//   float dist_from_coast = (waterDepth - 0.5) * 2;
  float dist_from_coast = clamp(waterDepth - 0.35, 0, 1) * (1/0.65);

//   sampling_pos.y -= pow(1 - clamp(clamp(waterDepth - 0.5, 0, 1) * 2, 0, 1), 4);

  sampling_pos -= 0.5 * pow(1 - dist_from_coast, 2);

//   sampling_pos -= 2 * exp(-0.6 * dist_from_coast);


//   sampling_pos -= 3.5 * (1 - smoothstep(dist_from_coast, 0.0, 1.0));


  float shore_wave_strength = texture(sampler_shore_wave, vec2(sampling_pos, 0)).x;
//   float shore_wave_strength = pow(1 - fract(sampling_pos), 4);

  return shore_wave_strength;

//   float shore_wave_strength2 = texture(sampler_shore_wave, vec2(0, shore_wave_scroll)).x;
//   float shore_wave_strength2_w = pow(1 - (clamp(waterDepth - 0.4, 0, 1) * 2), 2);
//   shore_wave_strength2_w *= 2;
//   return mix(shore_wave_strength, shore_wave_strength2, shore_wave_strength2_w);
}


void getShoreWaveAlpha(inout TerrainWaterFragmentParameters params)
{
  vec2 pos = pass_pos_flat.xy;
  float amount = 1;
//   float waterDepth = params.water_depth;
  float waterDepth = params.water_map_value;

  float shore_wave_strength = 0;

  shore_wave_strength += sampleShoreWave(pos.xy, waterDepth, shore_wave_scroll.x);
  shore_wave_strength += sampleShoreWave(pos.xy, waterDepth, shore_wave_scroll.x + 0.2);
  shore_wave_strength += sampleShoreWave(pos.xy, waterDepth, shore_wave_scroll.x + 0.7);

//   shore_wave_strength += sampleShoreWave(pos.xy, waterDepth, shore_wave_scroll.y);
  shore_wave_strength *= 0.6;
  shore_wave_strength = clamp(shore_wave_strength, 0, 1);

  shore_wave_strength = mix(shore_wave_strength * 0.3, shore_wave_strength, amount);

//   float shore_wave_strength2 = texture(sampler_shore_wave, vec2(0, shore_wave_scroll)).x;
//   float shore_wave_strength = texture(sampler_shore_wave, (pos.xy / 300.0) + vec2(0, shore_wave_scroll)).x;

// //   shore_wave_strength = mix(shore_wave_strength2, shore_wave_strength, smoothstep(0.5, 0.7, waterDepth));
//   shore_wave_strength = mix(shore_wave_strength, shore_wave_strength2, 0.7 * (1-smoothstep(0.5, 0.9, waterDepth)));


//   shore_wave_strength += 2 * shore_wave_strength * (1 - smoothstep(0.4, 0.5, waterDepth));
  shore_wave_strength += 3 * shore_wave_strength * (1 - smoothstep(0.4, 0.5, waterDepth));
  shore_wave_strength = clamp(shore_wave_strength, 0, 1);
  shore_wave_strength *= smoothstep(0.4, 0.5, waterDepth);
  shore_wave_strength *= pow(1 - ((clamp(waterDepth - 0.5, 0, 1)) * 2), 3);

  params.shore_wave_alpha = shore_wave_strength;
}

#else

void shoreWaveDummy() {}

#endif

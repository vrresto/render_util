#!/usr/bin/env python

import sys
import os
import parser
import gl_XML
import enabled_procs


template_void = """
inline void {ep}({args_decl})
{{
  auto iface = getCurrentInterface();
  iface->getFunctions().{ep}({args});
  if (iface->hasError())
    throw GLError();
}}
"""


template_non_void = """
inline {ret_type} {ep}({args_decl})
{{
  auto iface = getCurrentInterface();
  auto ret = iface->getFunctions().{ep}({args});
  if (iface->hasError())
    throw GLError();
  return ret;
}}
"""


def getArgs(func, ep):
  p_string = ""
  comma = ""

  for p in func.parameterIterator(ep):
    if p.is_padding:
      continue
    p_string = p_string + comma + p.name
    comma = ", "

  return p_string


for ep in enabled_procs.iterate():
  func = parser.functions_by_ep[ep]
  ep_params = func.entry_point_parameters[ep]

  if func.return_type != "void":
    template = template_non_void
  else:
    template = template_void

  print template.format(ret_type = func.return_type,
                        ep = ep,
                        args_decl = gl_XML.create_parameter_string(ep_params, 1),
                        args = getArgs(func, ep))

import sys
import os
import gl_XML


api = gl_XML.parse_GL_API(os.environ['GLAPI_PATH'] + "/gl_API.xml")
functions_by_ep = {}


for func in api.functionIterateByOffset():
  if func.desktop != True:
    continue

  for ep in func.entry_points:
    functions_by_ep[ep] = func

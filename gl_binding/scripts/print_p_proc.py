#!/usr/bin/env python

import sys
import os
import parser
import enabled_procs


template = "{ret_type} GLAPIENTRY (*{ep}) ({args_decl}) = nullptr;"


for ep in enabled_procs.iterate():
  func = parser.functions_by_ep[ep]
  print template.format(ret_type = func.return_type,
                        ep = ep,
                        args_decl = func.get_parameter_string(ep))

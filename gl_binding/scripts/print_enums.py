#!/usr/bin/env python

import sys
import os
import parser


enum_definition_line = "constexpr auto GL_{name} = {value};"


for enum in parser.api.enumIterateByName():
  print enum_definition_line.format(name = enum.name, value = enum.value)

import sys
import os


gl_procs = []


for line in sys.stdin:
    gl_procs.append(line.strip())


def iterate():
    return gl_procs.__iter__()

#!/usr/bin/env python

import sys
import os
import parser
import gl_XML
import enabled_procs


template = """
m_functions.{ep} = ({ret_type} GLAPIENTRY (*) ({arg_types}))
  get_proc_address(\"gl{ep}\");
"""


for ep in enabled_procs.iterate():
  func = parser.functions_by_ep[ep]
  ep_params = func.entry_point_parameters[ep]
  print template.format(ret_type = func.return_type,
                        ep = ep,
                        arg_types = gl_XML.create_parameter_string(ep_params, 0));

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/gl_binding/util.h>
#include <render_util/gl_binding/enums.h>

#include <set>
#include <string>
#include <cstdlib>
#include <cassert>
#include <GL/gl.h>


using namespace render_util::gl_binding::enums;


namespace
{


thread_local std::set<std::string> g_state_procs;


} // namespace



namespace render_util::gl_binding
{


const char *getGLErrorString(unsigned int code)
{
  switch (code)
  {
    case GL_NO_ERROR:
      return "GL_NO_ERROR";
    case GL_INVALID_ENUM:
      return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
      return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
      return "GL_INVALID_OPERATION";
    case GL_STACK_OVERFLOW:
      return "GL_STACK_OVERFLOW";
    case GL_STACK_UNDERFLOW:
      return "GL_STACK_UNDERFLOW";
    case GL_OUT_OF_MEMORY:
      return "GL_OUT_OF_MEMORY";
    default:
      return "unknown error";
  }
}


bool isStateProc(const char* name)
{
  if (g_state_procs.empty())
  {
    g_state_procs =
    {
      #include "gl_binding/_generated/state_procs_string_array_elements.inc.h"
    };
  }
  assert(!g_state_procs.empty());
  return g_state_procs.find(name) != g_state_procs.end();
}


} // namespace render_util::gl_binding

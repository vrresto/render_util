/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <render_util/gl_binding/util.h>
#include <render_util/gl_binding/interface.h>
#include <render_util/gl_binding/enums.h>
#include <log.h>

#include <stdexcept>


using namespace render_util::gl_binding::enums;


namespace render_util::gl_binding
{


thread_local GL_Interface* GL_Interface::s_current = nullptr;


GL_Interface::GL_Interface(GetProcAddressFunc *getProcAddress)
{
  auto get_proc_address = [getProcAddress] (const char *name)
  {
    auto addr = getProcAddress(name);

    if (!addr)
      throw std::runtime_error(std::string("GL procedure is missing: ") + name);

    return addr;
  };

  #include "gl_binding/_generated/gl_p_proc_init.inc.h"
  #include "gl_binding/_generated/state_p_proc_init.inc.h"

  checkError();
}


GL_Interface::~GL_Interface()
{
  assert(this != s_current);
}


void GL_Interface::checkError()
{
  m_functions.Finish();
  auto err = m_functions.GetError();
  if (err != GL_NO_ERROR)
  {
    LOG_ERROR << "GL error: " << getGLErrorString(err) << std::endl;
    abort();
  }
};


void GL_Interface::setCurrent(GL_Interface *iface)
{
  if (s_current)
  {
    #if ENABLE_GL_DEBUG_CALLBACK
    LOG_INFO << "Disabling GL debug message callback." << std::endl;
    s_current->m_functions.Disable(GL_DEBUG_OUTPUT);
    s_current->m_functions.DebugMessageCallback(nullptr, nullptr);
    s_current->checkError();
    #endif
  }

  s_current = iface;

  if (s_current)
  {
    #if ENABLE_GL_DEBUG_CALLBACK
    LOG_INFO << "Enabling GL debug message callback." << std::endl;
    s_current->m_functions.DebugMessageCallback(messageCallback, s_current);
    s_current->m_functions.DebugMessageControl(GL_DONT_CARE, GL_DONT_CARE,
                                                GL_DONT_CARE, 0, nullptr, false);
    s_current->m_functions.DebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_ERROR,
                                                GL_DONT_CARE, 0, nullptr, true);
    s_current->m_functions.DebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,
                                                GL_DONT_CARE, 0, nullptr, true);
    s_current->m_functions.Enable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    s_current->m_functions.Enable(GL_DEBUG_OUTPUT);
    s_current->checkError();
    #endif
  }
}


#if ENABLE_GL_DEBUG_CALLBACK
void GLAPIENTRY GL_Interface::messageCallback(GLenum source,
  GLenum type,
  GLuint id,
  GLenum severity,
  GLsizei length,
  const GLchar* message,
  const void* userParam)
{
  if (type == GL_DEBUG_TYPE_ERROR || type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR)
  {
    LOG_ERROR << message << std::endl;

    auto iface = (GL_Interface*)userParam;
    iface->m_has_error = true;
  }
  else
  {
    LOG_WARNING << message << std::endl;
  }
}
#endif


} // namespace render_util::gl_binding

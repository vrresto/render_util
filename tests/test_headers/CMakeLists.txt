set(headers
  render_util/state.h
  render_util/altitudinal_zone.h
  render_util/terrain_resources.h
  render_util/viewer/value_wrapper.h
  render_util/viewer/parameter_base.h
  render_util/viewer/numeric_parameter.h
  render_util/viewer/boolean_parameter.h
  render_util/viewer/vector_parameter.h
  render_util/viewer/simple_parameter.h
  render_util/viewer/multiple_choice_parameter.h
  render_util/viewer/parameters.h
  render_util/viewer/editor_parameters.h
  render_util/viewer/parameter.h
  render_util/terrain_base.h
  render_util/map_geography.h
)

if(render_util_build_viewer)
  set(headers argparse/argparse.hpp ${headers})
endif()


add_custom_target(generate_header_tests)


foreach(header ${headers})
  # MESSAGE("header: ${header}")
  string(REPLACE "/" "_" target_name "${header}")
  string(REPLACE "." "_" target_name "${target_name}")
  # MESSAGE("target_name: ${target_name}")
  set(output ${CMAKE_CURRENT_BINARY_DIR}/test_${target_name}.cpp)
  add_custom_command(
      OUTPUT ${output}
      COMMAND echo "output: ${output}"
      COMMAND echo test: "'#include'" '<'${header}'>'
      COMMAND echo "'#include'" '<'${header}'>' > "${output}"
  )
  add_custom_target("generate_test_${target_name}_src" DEPENDS "${output}")
  add_dependencies(generate_header_tests "generate_test_${target_name}_src")
  set(test_headers_srcs ${test_headers_srcs} ${output})
endforeach()


add_library(test_headers ${test_headers_srcs})


target_link_libraries(test_headers
  render_util_common
  gl_binding
)

if(render_util_build_viewer)
  target_link_libraries(test_headers viewer_includes)
endif()


add_dependencies(test_headers generate_header_tests)

#include <render_util/viewer/application.h>
#include <render_util/viewer/map_loader.h>
#include <render_util/viewer/viewer.h>
#include <render_util/terrain_resources.h>
#include <util.h>

#include <argparse/argparse.hpp>
#include <iostream>


using namespace render_util;


const std::string APP_NAME = "dummy_terrain_viewer";


namespace
{


ElevationMap::ConstPtr createDummyHeightMap(glm::ivec2 size)
{
  auto map = std::make_shared<ElevationMap>(size);
  return map;
}

TerrainResourcesBase::MaterialMap::ConstPtr createDummyMaterialMap(glm::ivec2 size)
{
  auto map = std::make_shared<TerrainResourcesBase::MaterialMap>(size);
  return map;
}


} // namespace


class TerrainResources : public TerrainResourcesBase
{
  struct Layer : public TerrainResourcesBase::Layer
  {
    ElevationMap::ConstPtr m_heightmap;
    MaterialMap::ConstPtr m_material_map;

    Layer() :
      m_heightmap(createDummyHeightMap(glm::ivec2(1024))),
      m_material_map(createDummyMaterialMap(m_heightmap->getSize()))
    {
    }

    virtual glm::ivec2 getSizePx() { return m_heightmap->getSize(); }
    virtual glm::vec3 getOriginM() { return { 0, 0, 0 }; }
    virtual float getScale() { return 1.f; }
    virtual unsigned int getResolutionM() { return 200; }
    virtual glm::vec2 getMapTextureOffset() { return { 0, 0 }; }
    virtual ElevationMap::ConstPtr getHeightMap() { return m_heightmap; }
    virtual MaterialMap::ConstPtr getMaterialMap() { return m_material_map; }
  };

  Layer m_layer;

public:
  virtual bool hasBaseLayer() { return false; }
  virtual TerrainResourcesBase::Layer& getLayer(int number)
  {
    assert(number == 0);
    return m_layer;
  }

  virtual ImageGreyScale::Ptr getNoiseTexture() { return {}; }
  virtual std::shared_ptr<GenericImage> getFarNoiseTexture() { return {}; }
  virtual bool farNoiseTextureIsNormalMap() { return false; }
};


class MapLoader : public viewer::MapLoaderBase
{
  TerrainResources m_res;

  virtual TerrainResourcesBase& getTerrainResources()
  {
    return m_res;
  }

  virtual const std::shared_ptr<GenericImage> getCirrusTexture() const
  {
    UNIMPLEMENTED
  }
};


int main(int argc, char** argv)
{
  viewer::initLog(APP_NAME);

  argparse::ArgumentParser arguments(APP_NAME, "git");
  render_util::viewer::addLogArguments(arguments);

  auto create_map_loader = [&] () ->
    std::shared_ptr<render_util::viewer::MapLoaderBase>
  {
    return std::make_shared<MapLoader>();
  };

  auto viewer = viewer::createTerrainViewer(APP_NAME, create_map_loader);
  viewer->addArguments(arguments);

  try
  {
    arguments.parse_args(argc, argv);
    render_util::viewer::processLogArguments(arguments);
    viewer->processArguments(arguments);
  }
  catch (const std::exception& err)
  {
    std::cerr << "Error: " << err.what() << std::endl;
    std::cerr << arguments;
    exit(1);
  }

  viewer->run();
}

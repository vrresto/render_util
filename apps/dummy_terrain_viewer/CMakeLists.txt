set(terrain_test_SRCS
  tiff.cpp
  map_loader.cpp
  terrain_test.cpp
)

add_executable(dummy_terrain_viewer
  main.cpp
)

target_link_libraries(dummy_terrain_viewer
  viewer
)

install(TARGETS dummy_terrain_viewer
  DESTINATION .
)

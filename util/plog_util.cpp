/*
  Based on plog/Util.h
  Copyright (c) 2016 Sergey Podobry (sergey.podobry at gmail.com).
  Documentation and sources: https://github.com/SergiusTheBest/plog
  License: MPL 2.0, http://mozilla.org/MPL/2.0/
*/

#include <plog/Util.h>
#include <util.h>

#include <time.h>


namespace plog
{
  namespace util
  {
    std::string processFuncName(const char* func)
    {
      const char* funcBegin = func;
      const char* funcEnd = ::strchr(funcBegin, '(');

      if (!funcEnd)
      {
        return std::string(func);
      }

      // search backwards for the first space char
      for (const char* i = funcEnd - 1; i >= funcBegin; --i)
      {
        if (*i == ' ')
        {
          funcBegin = i + 1;
          break;
        }
      }

      return std::string(funcBegin, funcEnd);
    }
  }
}

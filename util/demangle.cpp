#include <util/demangle.h>

#include <cxxabi.h>


std::string util::demangle(const char* mangled_name)
{
  std::string name = mangled_name;
  auto tmp = abi::__cxa_demangle(mangled_name, nullptr, nullptr, nullptr);
  if (tmp)
  {
    name = tmp;
    free(tmp);
    tmp = nullptr;
  }
  return name;
}

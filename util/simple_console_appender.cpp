/*
  Based on plog/Appenders/ConsoleAppender.h
  Copyright (c) 2016 Sergey Podobry (sergey.podobry at gmail.com).
  Documentation and sources: https://github.com/SergiusTheBest/plog
  License: MPL 2.0, http://mozilla.org/MPL/2.0/
*/

#include <log/simple_console_appender.h>

#include <unistd.h>

namespace util::log
{
  SimpleConsoleAppenderBase::SimpleConsoleAppenderBase()
    : m_isatty(!!isatty(fileno(stdout)))
  {
  }
}

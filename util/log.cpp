#include <log.h>

#include <array>


using namespace util::log;


namespace
{


std::array<const char*, Verbosity_COUNT> g_verbosity_string =
{
  "Error",
  "Warning",
  "Info",
  "Debug",
  "Trace",
};


} // namespace


namespace util::log
{


const char* verbosityToString(int verbosity)
{
  return g_verbosity_string.at(verbosity);
}


plog::Severity toPlogSeverity(Verbosity verbosity)
{
  switch (verbosity)
  {
    case ERROR:
      return plog::error;
    case WARNING:
      return plog::warning;
    case INFO:
      return plog::info;
    case DEBUG:
      return plog::debug;
    case TRACE:
      return plog::verbose;
    default:
      abort();
  }
}


Verbosity fromPlogSeverity(plog::Severity plog_severity)
{
  switch (plog_severity)
  {
    case plog::fatal:
    case plog::error:
      return ERROR;
    case plog::warning:
      return WARNING;
    case plog::info:
      return INFO;
    case plog::debug:
      return DEBUG;
    case plog::verbose:
    case plog::none:
      return TRACE;
    default:
      abort();
  }
}


} // util::log

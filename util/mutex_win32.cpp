#include <util/mutex.h>

#include <windows.h>


namespace util
{


struct Mutex::Impl
{
  CRITICAL_SECTION cs;
};


Mutex::Mutex() : m_impl(std::make_unique<Impl>())
{
  InitializeCriticalSection(&m_impl->cs);
}


Mutex::~Mutex()
{
  DeleteCriticalSection(&m_impl->cs);
}


void Mutex::lock()
{
  EnterCriticalSection(&m_impl->cs);
}


void Mutex::unlock()
{
  LeaveCriticalSection(&m_impl->cs);
}


} // namespace util

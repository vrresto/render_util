/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util/child_process_unix.h>
#include <log.h>

#include <stdexcept>
#include <cassert>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>


namespace util
{


ChildProcessUnix::ChildProcessUnix(const std::vector<std::string> &args) : m_args(args)
{
}


ChildProcessUnix::~ChildProcessUnix()
{
  assert(!m_running);
  close(m_pipefd[0]);
}


bool ChildProcessUnix::isFinished()
{
  return m_exited || m_signaled;
}


int ChildProcessUnix::exitStatus()
{
  assert(m_exited);
  return m_exit_status;
}


bool ChildProcessUnix::success()
{
  return m_exited && (m_exit_status == 0);
}


void ChildProcessUnix::updateStatus()
{
  assert(m_running);
  assert(!m_exited);

  int wstatus = 0;
  auto res = waitpid(m_pid, &wstatus, WNOHANG);
  if (res == -1)
  {
    perror("waitpid error: ");
  }

  assert(res != -1);
  if (res == m_pid)
  {
    if (WIFEXITED(wstatus))
    {
      m_running = false;
      m_exited = true;
      m_exit_status = WEXITSTATUS(wstatus);
    }
    else if (WIFSIGNALED(wstatus))
    {
      m_running = false;
      m_signaled = true;
      m_signal = WTERMSIG(wstatus);
    }
    else
    {
      throw std::runtime_error("Unexpected child process status: " + std::to_string(wstatus));
    }
  }
}


void ChildProcessUnix::start()
{
  assert(!m_running);

  auto res = pipe2(m_pipefd, O_NONBLOCK);
  if (res != 0)
  {
    perror("pipe failed: ");
    abort();
  }

  LOG_INFO << "Launching child process:";
  for (auto &arg : m_args)
    LOG_INFO << " " << arg;
  LOG_INFO << std::endl;

  auto pid = fork();

  if (pid != 0)
  {
    m_pid = pid;
    m_running = true;

    close(m_pipefd[1]);
  }
  else
  {
    dup2(m_pipefd[1], 1);
    dup2(m_pipefd[1], 2);
    close(m_pipefd[0]);
    close(m_pipefd[1]);

    auto filename = strdup(m_args.at(0).c_str());

    std::vector<char*> argv;
    for (auto &arg : m_args)
      argv.push_back(strdup(arg.c_str()));
    argv.push_back(nullptr);

    std::vector<char*> envp;
    envp.push_back(nullptr);

    execve(filename, argv.data(), envp.data());

    fprintf(stderr, "Failed to execute %s\n", filename);
    perror("execve failed");
    _Exit(1);
  }
}


int ChildProcessUnix::readOutput(char *buffer, size_t buffer_size)
{
  return read(m_pipefd[0], buffer, buffer_size);
}


void ChildProcessUnix::kill()
{
  ::kill(m_pid, SIGTERM);
}


} // namespace util

/**
 *    Utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <iostream>

#include <log.h>


namespace
{
  CustomAssertHander* g_handler = nullptr;
}


extern "C"
{


void register_custom_assert_handler(CustomAssertHander* handler)
{
  g_handler = handler;
}


void custom_assert(int line, const char* file, const char* cond)
{
  if (g_handler)
    g_handler(line, file, cond);

  thread_local int recursion_level = 0;
  thread_local int orig_line {};
  thread_local const char* orig_file {};
  thread_local const char* orig_cond {};

  recursion_level++;

  if (recursion_level <= 1)
  {
    orig_line = line;
    orig_file = file;
    orig_cond = cond;

    LOG_SEPARATOR;
    LOG_ERROR << "Assertion failed: " << cond << std::endl;
    LOG_ERROR << "File: " << file << ":" << line << std::endl;
    LOG_FLUSH;
  }
  else
  {
    std::cerr << "Recursion during assert!" << std::endl;
    std::cerr << "Current failed assertion: " << cond
              << ", File: " << file << ":" << line << std::endl;
    std::cerr << "Original failed assertion: " << orig_cond
              << ", File: " << orig_file << ":" << orig_line << std::endl;
  }

  abort();
}


} // extern "C"

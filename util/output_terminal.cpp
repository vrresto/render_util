/**
 *    Utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util/output_terminal.h>
#include <log.h>

#include <cassert>


namespace util
{


OutputTerminal::OutputTerminal()
{
  m_lines.push_back("");
}


void OutputTerminal::newLine()
{
  LOG_INFO << m_lines.back() << std::endl;
  m_lines.push_back("");
}


void OutputTerminal::write(const char* chars, size_t num_chars)
{
  for (int i = 0; i < num_chars; i++)
  {
    if (chars[i] == '\n')
    {
      newLine();
    }
    else if (chars[i] == '\r')
    {
      m_lines.back() = "";
    }
    else
    {
      m_lines.back().push_back(chars[i]);
    }
  }
}


void OutputTerminal::clear()
{
  m_lines.clear();
  m_lines.push_back("");
}


const std::list<std::string>& OutputTerminal::getLines() const
{
  return m_lines;
}


} // namespace util

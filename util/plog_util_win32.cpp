/*
  Based on plog/Util.h
  Copyright (c) 2016 Sergey Podobry (sergey.podobry at gmail.com).
  Documentation and sources: https://github.com/SergiusTheBest/plog
  License: MPL 2.0, http://mozilla.org/MPL/2.0/
*/

#include <plog/Util.h>
#include <util.h>

#include <windows.h>


namespace plog
{
  namespace util
  {
    void ftime(Time* time)
    {
      timeb t;
      ::ftime(&t);
      time->time = t.time;
      time->millitm = t.millitm;
    }


    void localtime_s(struct tm* t, const time_t* time)
    {
      ::localtime_s(t, time);
    }


    unsigned int gettid()
    {
      return GetCurrentThreadId();
    }


    void gmtime_s(struct tm* t, const time_t* time)
    {
      ::gmtime_s(t, time);
    }
  }
}

/*
  Based on plog/Util.h
  Copyright (c) 2016 Sergey Podobry (sergey.podobry at gmail.com).
  Documentation and sources: https://github.com/SergiusTheBest/plog
  License: MPL 2.0, http://mozilla.org/MPL/2.0/
*/

#include <plog/Util.h>
#include <util.h>

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>


namespace plog
{
  namespace util
  {
    void ftime(Time* t)
    {
      timeval tv;
      ::gettimeofday(&tv, NULL);

      t->time = tv.tv_sec;
      t->millitm = static_cast<unsigned short>(tv.tv_usec / 1000);
    }

    void localtime_s(struct tm* t, const time_t* time)
    {
      ::localtime_r(time, t);
    }

    unsigned int gettid()
    {
#if defined(__linux__)
      return static_cast<unsigned int>(::syscall(__NR_gettid));
#else
      #error not implemented
#endif
    }

    void gmtime_s(struct tm* t, const time_t* time)
    {
      ::gmtime_r(time, t);
    }
  }
}

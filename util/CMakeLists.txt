set(srcs
  demangle.cpp
  unimplemented.cpp
  normal_file.cpp
  output_terminal.cpp
  log.cpp
  simple_console_appender.cpp
  plog_util.cpp
)

if(ENABLE_CUSTOM_ASSERT)
  set(srcs ${srcs}
    custom_assert.cpp
  )
endif()

if(ENABLE_MAP_PROJECTION)
  set(srcs ${srcs}
    map_projection.cpp
  )
endif()

if(NOT platform_mingw)
  set(srcs ${srcs}
    child_process_unix.cpp
    plog_util_unix.cpp
  )
endif()

if(platform_mingw)
  set(srcs ${srcs}
    plog_util_win32.cpp
    mutex_win32.cpp
  )
endif()

add_library(render_util_util
  ${srcs}
)


target_link_libraries(render_util_util
  render_util_common
)


if(ENABLE_CUSTOM_ASSERT)
  target_compile_options(render_util_util PUBLIC
    -isystem ${PROJECT_SOURCE_DIR}/include/util/custom_assert
  )
endif()

if(ENABLE_MAP_PROJECTION)
  target_link_libraries(render_util_util GeographicLib)
endif()

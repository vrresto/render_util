/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util/map_projection.h>
#include <util.h>
#include <render_util/physics.h> //FIXME this should be part of util

#include <GeographicLib/LambertConformalConic.hpp>

#include <functional>
#include <map>


using namespace util;
using render_util::physics::EARTH_RADIUS;
using Factory = std::function<std::unique_ptr<MapProjection>()>;

using namespace GeographicLib;


namespace
{


struct Equirectangular : public MapProjection
{
  glm::dvec2 project(glm::dvec2 geodesic) override
  {
    return geodesic * getMetersPerDegreeLongitude();
  }

  glm::dvec2 unproject(glm::dvec2 projected) override
  {
    return projected / getMetersPerDegreeLongitude();
  }

  double getMetersPerDegreeLongitude() override // FIXME this doesn't really belong here
  {
    return render_util::physics::EARTH_CIRCUMFERENCE / 360.0;
  }
};


struct MercatorSpherical : public MapProjection
{
  glm::dvec2 project(glm::dvec2 geodesic) override
  {
    geodesic *= (double(PI) / 180.0);
    glm::dvec2 mercator(0);
    mercator.x = geodesic.x;
    mercator.y = glm::atanh(glm::sin(geodesic.y));
    return EARTH_RADIUS * mercator;
  }

  glm::dvec2 unproject(glm::dvec2 mercator) override
  {
    mercator /= EARTH_RADIUS;

    glm::dvec2 geodesic(0);
    geodesic.x = mercator.x;
    geodesic.y = glm::atan(glm::sinh(mercator.y));

    return geodesic / (double(PI) / 180.0);
  }

  double getMetersPerDegreeLongitude() override // FIXME this doesn't really belong here
  {
    return render_util::physics::EARTH_CIRCUMFERENCE / 360.0;
  }
};


struct MercatorWGS84 : public MapProjection
{
  const LambertConformalConic &m_projection;

  MercatorWGS84() : m_projection(LambertConformalConic::Mercator())
  {
  }

  glm::dvec2 toVec2(std::array<double, 2> coords)
  {
    return glm::dvec2(coords[1], coords[0]);
  }

  std::array<double, 2> fromVec2(glm::dvec2 coords)
  {
    return std::array<double, 2> { coords[1], coords[0] };
  }

  glm::dvec2 project(glm::dvec2 geodesic) override
  {
    glm::dvec2 projected;
    m_projection.Forward(0, geodesic.y, geodesic.x, projected.x, projected.y);
    return projected;
  }

  glm::dvec2 unproject(glm::dvec2 mercator) override
  {
    glm::dvec2 unprojected;
    m_projection.Reverse(0, mercator.x, mercator.y, unprojected.y, unprojected.x);
    return unprojected;
  }

  double getMetersPerDegreeLongitude() override // FIXME this doesn't really belong here
  {
    return render_util::physics::EARTH_CIRCUMFERENCE / 360.0;
  }
};


template <class T>
Factory makeFactory()
{
  return [] { return std::make_unique<T>(); };
}


const std::map<std::string, Factory>& getFactories()
{
  static std::map<std::string, Factory> factories =
  {
    { "equirectangular", makeFactory<Equirectangular>() },
    { "mercator-spherical", makeFactory<MercatorSpherical>() },
    { "mercator-wgs84", makeFactory<MercatorWGS84>() },
  };
  return factories;
}


} // namespace


namespace util
{


std::vector<std::string> getMapProjectionNames()
{
  std::vector<std::string> names;
  for (auto &it : getFactories())
    names.push_back(it.first);
  return names;
}


std::unique_ptr<MapProjection> createMapProjection(std::string name)
{
  try
  {
    return getFactories().at(name)();
  }
  catch (std::out_of_range&)
  {
    throw std::runtime_error("No projection with name \"" + name + "\" exists");
  }
}


} // namespace util

include(CMakeDependentOption)


option(USE_PLOG "" true)
option(USE_UNIX_CONSOLE "" false)
option(ENABLE_GL_DEBUG_CALLBACK "" false)
option(DEBUG_SHADERS "" false)
option(DEBUG_COASTLINE "" false)
option(DEBUG_SMALL_WATER_MAP "" false)
option(ENABLE_BASE_MAP "" false)
#option(ENABLE_VIEWER_BASE_MAP "" false)
option(ENABLE_MAP_PROJECTION "" false)
option(ENABLE_CUSTOM_ASSERT "" true)
option(auto_generate_gl_procs "" true)
option(render_util_build_viewer "" false)
option(ENABLE_WARNINGS "" true)
option(ENABLE_TESTS "" true)

if(platform_mingw)
  option(ENABLE_VIEWER_DBUS_INTERFACE "" false)
else()
  option(ENABLE_VIEWER_DBUS_INTERFACE "" true)
endif()

CMAKE_DEPENDENT_OPTION(ENABLE_VIEWER_BASE_MAP "" OFF "ENABLE_BASE_MAP" OFF)


set(gl_procs_file "" CACHE FILEPATH "")


set(ARGPARSE_DIR "${PROJECT_SOURCE_DIR}/_modules/argparse" CACHE PATH "")
set(MESA_DIR "$ENV{MESA_DIR}" CACHE PATH "")
set(GLM_DIR "$ENV{GLM_DIR}" CACHE PATH "")
set(HALF_DIR "$ENV{HALF_DIR}" CACHE PATH "")
set(GLFW_DIR "$ENV{GLFW_DIR}" CACHE PATH "")


if(NOT renderutil_base_dir)
  set(renderutil_base_dir "render_util")
endif()

if(NOT renderutil_shader_dir)
  set(renderutil_shader_dir "${renderutil_base_dir}/shaders")
endif()

if(NOT renderutil_cache_dir)
  set(renderutil_cache_dir "${renderutil_base_dir}/cache")
endif()

if(NOT DEBUG_DIR)
  set(DEBUG_DIR "${renderutil_base_dir}/debug")
endif()

if(NOT plog_dir)
  set(plog_dir ${PROJECT_SOURCE_DIR}/_modules/plog)
endif()

if (NOT DEFINED enable_atmosphere_precomputed_plot_parameterisation)
  set(enable_atmosphere_precomputed_plot_parameterisation 0)
endif()

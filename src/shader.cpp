/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cstdlib>
#include <glm/gtc/type_ptr.hpp>

#include <render_util/render_util.h>
#include <render_util/shader_debug_config.h>
#include <render_util/shader.h>
#include <util.h>
#include <render_util/gl_binding/gl_functions.h>
#include <log.h>

using namespace render_util::gl_binding;
using namespace std;
using namespace glm;
using namespace render_util;


namespace
{


class ErrorWithLog : public std::runtime_error
{
  std::string m_log;

public:
  ErrorWithLog(std::string message, const std::string& log) :
    std::runtime_error(message),
    m_log(log)
  {
  }

  const std::string& getLog()
  {
    return m_log;
  }
};


class ShaderPreProcessError : public ErrorWithLog
{
  std::string m_file_name;

public:
  ShaderPreProcessError(std::string file_name, std::string error) :
    ErrorWithLog("Preprocess error: " + file_name, error + '\n'),
    m_file_name(file_name)
  {
  }

  const std::string& getFileName()
  {
    return m_file_name;
  }
};


class ShaderCompileError : public ErrorWithLog
{
  std::string m_file_name;

public:
  ShaderCompileError(std::string file_name, const std::string& log) :
    ErrorWithLog("Compile error: " + file_name, log),
    m_file_name(file_name)
  {
  }

  const std::string& getFileName()
  {
    return m_file_name;
  }
};


class LinkError : public ErrorWithLog
{
public:
  LinkError(const std::string& log) : ErrorWithLog("Link error", log) {}
};


void logParameters(plog::Severity severity, const ShaderParameters& p)
{
  std::ostringstream s;
  p.dump(s);

  PLOG(severity) << "Set parameters are:" << std::endl
                 << std::endl << s.str() << std::endl;
}


string getParameterValue(const string &parameter, const ShaderParameters &params)
{
  auto pos = parameter.find(':');

  string parameter_name;
  string default_value;

  if (pos != string::npos && pos < parameter.size()-1)
  {
    parameter_name = parameter.substr(0, pos);
    default_value = parameter.substr(pos+1);
  }
  else
  {
    parameter_name = parameter;
  }

  assert(!parameter_name.empty());

  try
  {
    return params.toString(parameter_name);
  }
  catch (...)
  {
    if (!default_value.empty())
    {
      return default_value;
    }
    else
    {
      throw std::runtime_error("Unset parameter: " + parameter_name);
    }
  }
}


string resolveParameters(string source, const ShaderParameters &params)
{
  istringstream in(source);
  string out;

  while (in.good())
  {
    int line_num = 0;
    string line;
    getline(in, line);

    string trimmed = util::trim(line);
    if (trimmed.empty() || util::isPrefix("//", trimmed))
    {
      out += '\n';
      line_num++;
      continue;
    }

    enum State
    {
      NONE,
      PARSE
    };

    State state = NONE;

    string parsed;
    for (char c : line)
    {
      switch (state)
      {
        case NONE:
          assert(parsed.empty());
          if (c == '@')
          {
            state = PARSE;
          }
          else
          {
            out.push_back(c);
          }
          break;
        case PARSE:
          if (c == '@')
          {
            assert(!parsed.empty());

            out += getParameterValue(parsed, params);

            parsed.clear();

            state = NONE;
          }
          else
          {
            parsed.push_back(c);
          }
          break;
      }
    }

    if (state != NONE)
    {
      LOG_ERROR << "Parse error at line " << line_num <<":" << std::endl << line << std::endl;
      throw std::runtime_error("Parse error");
    }

    out += '\n';
    line_num++;
  }

  return out;
}


string readInclude(string include_file, vector<string> search_path, string &path_out)
{
  for (auto &dir : search_path)
  {
    vector<char> content;
    string path = dir + "/" + include_file;
    if (util::readFile(path, content, true))
    {
      path_out = path;
      return string(content.data(), content.size());
    }
  }

  LOG_ERROR << "failed to read include file: " << include_file << endl;
  throw std::runtime_error("failed to read include file");
}


std::string getShaderTypeString(unsigned type)
{
  switch (type)
  {
    case GL_FRAGMENT_SHADER:
      return "fragment";
    case GL_VERTEX_SHADER:
      return "vertex";
    case GL_GEOMETRY_SHADER:
      return "geometry";
    case GL_COMPUTE_SHADER:
      return "compute";
    default:
      throw std::runtime_error("Unknown shader type: " + std::to_string(type));
  }
}


std::string getShaderTypeExtension(unsigned type)
{
  switch (type)
  {
    case GL_FRAGMENT_SHADER:
      return ".frag";
    case GL_VERTEX_SHADER:
      return ".vert";
    case GL_GEOMETRY_SHADER:
      return ".geom";
    case GL_COMPUTE_SHADER:
      return ".compute";
    default:
      throw std::runtime_error("Unknown shader type: " + std::to_string(type));
  }
}


} // namespace


namespace render_util
{


Shader::Shader(const std::string &name,
                     const std::vector<std::string> &paths_,
                     GLenum type,
                     const ShaderParameters &params_) :
  m_type(type),
  m_name(name)
{
  auto params = params_;

  auto type_str = getShaderTypeString(type);
  auto ext = getShaderTypeExtension(type);

  params.set("is_vertex", (type == GL_VERTEX_SHADER));
  params.set("is_fragment", (type == GL_FRAGMENT_SHADER));
  params.set("is_geometry", (type == GL_GEOMETRY_SHADER));
  params.set("is_compute", (type == GL_COMPUTE_SHADER));

  vector<char> data;

  vector<string> paths;

  for (auto &path : paths_)
  {
    paths.push_back(path + '/' + name + ext);
    paths.push_back(path + '/' + name + ".glsl");
  }

  std::ostringstream error_log;

  for (auto p : paths)
  {
    if (util::readFile(p, data, true))
    {
      LOG_TRACE << "sucessully read shader: " << p << endl;
      m_filename = p;
      try
      {
        preProcess(data, params, paths_);
      }
      catch (std::exception& e)
      {
        LOG_ERROR << "Failed to preprocess shader: " << p << std::endl;
        LOG_ERROR << "Reason: " << e.what() << std::endl;
        m_preprocessed_source.clear();
        throw ShaderPreProcessError(m_filename, e.what());
      }
      return;
    }
    else
    {
      error_log << "Failed to read " << p << '\n';
    }
  }

  LOG_ERROR << "Failed to read " << type_str << " shader file: " << name << endl;

  throw ErrorWithLog("Failed to read " + type_str + " shader: " + name, error_log.str());
}


Shader::~Shader()
{
  if (m_id)
    gl::DeleteShader(m_id);
}


void Shader::compile()
{
  if (m_preprocessed_source.empty())
    return;

  GLuint id = gl::CreateShader(m_type);
  assert(id);

  m_id = id;

  const GLchar *sources[1] = { m_preprocessed_source.c_str() };

  gl::ShaderSource(id, 1, sources, 0);

  try
  {
    gl::CompileShader(id);
  }
  catch (GLError&)
  {
    // ignore the error as we check for successful compilation anyway
    GL_Interface::getCurrent()->clearError();
  }

  GLint success = 0;
  gl::GetShaderiv(id, GL_COMPILE_STATUS, &success);
  if (success !=  GL_TRUE)
  {
    GLint maxLength = 0;
    gl::GetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

    GLchar *infoLog = (GLchar*) malloc(maxLength);
    gl::GetShaderInfoLog(id, maxLength, &maxLength, infoLog);

    std::ostringstream error_log;

    error_log << infoLog;
    error_log << endl;

// #if ENABLE_GL_DEBUG_CALLBACK
//     {
//       LOG_ERROR << "Error compiling shader: " << m_filename << endl;
//     }
// #else
    {
      LOG_ERROR << "Error compiling shader: " << m_filename << endl << infoLog << endl;
    }
// #endif

    for (int i = 0; i < m_includes.size(); i++)
    {
      LOG_ERROR << "include " << i+1 << ": " << m_includes[i] << endl;
      error_log << "include " << i+1 << ": " << m_includes[i] << endl;
    }

    LOG_ERROR << endl;

    free(infoLog);

    throw ShaderCompileError(m_filename, error_log.str());
  }
}


void Shader::preProcess(const vector<char> &data_in, const ShaderParameters &params,
                        const std::vector<std::string> &paths_)
{
  string source(data_in.data(), data_in.size());


  source = resolveParameters(source, params);

  istringstream in(source);
  string out;

  int line_num = 1;

  while (in.good())
  {
    string line;
    getline(in, line);

    string trimmed = util::trim(line);
    if (trimmed.empty())
    {
      out += '\n';
      line_num++;
      continue;
    }

    if (util::isPrefix("#include", trimmed))
    {
      auto tokens = util::tokenize(trimmed);
      assert(tokens.size() == 2);
      assert(tokens.at(0) == "#include");

      auto include_file = tokens.at(1);

      string include_file_path;
      auto include_file_content = readInclude(include_file, paths_, include_file_path);
      m_includes.push_back(include_file_path);

      include_file_content = resolveParameters(include_file_content, params);
      include_file_content = string("#line 1 ") + to_string(m_includes.size()) + "\n" + include_file_content;

      out += include_file_content;
      out += '\n';
      out += "#line " + to_string(line_num + 1) + " 0 \n";

      line_num++;
      continue;
    }

    out += line;
    out += '\n';
    line_num++;
  }

  m_preprocessed_source = std::move(out);
}


void ShaderParameters::add(const ShaderParameters &other)
{
  for (auto &entry : other.m_map)
  {
    m_map[entry.first] = entry.second;
  }
}


void ShaderParameters::dump(std::ostream& out) const
{
  std::map<std::string, ShaderParameter> ordered(m_map.begin(), m_map.end());

  for (auto &it : ordered)
  {
    out << it.first << " = " << util::toString(it.second) << std::endl;
  }
}


ShaderProgram::ShaderProgram(const std::string &name,
      const std::vector<std::string> &vertex_shaders,
      const std::vector<std::string> &fragment_shaders,
      const std::vector<std::string> &geometry_shaders,
      const std::vector<std::string> &compute_shaders,
      const std::vector<std::string> &paths,
      bool must_be_valid,
      const std::map<unsigned int, std::string> &attribute_locations,
      const ShaderParameters &parameters)
  : m_parameters(parameters),
    name(name),
    vertex_shaders(vertex_shaders),
    fragment_shaders(fragment_shaders),
    geometry_shaders(geometry_shaders),
    compute_shaders(compute_shaders),
    paths(paths),
    must_be_valid(must_be_valid),
    attribute_locations(attribute_locations)
{
  try
  {
    create();
    assertIsValid();
  }
  catch (ErrorWithLog& e)
  {
    LOG_ERROR << "Failed to create shader program: " << name << endl;
    logParameters(plog::warning, parameters);

    std::ostringstream log;
    log << e.getLog();
    log << '\n';
    parameters.dump(log);

    if (id)
      gl::DeleteProgram(id);

    throw ShaderCreationError(name, e.what(), log.str());
  }
  catch (std::exception& e)
  {
    LOG_ERROR << "Failed to create shader program: " << name << endl;
    logParameters(plog::warning, parameters);

    std::ostringstream log;
    log << e.what();
    log << '\n';
    parameters.dump(log);

    if (id)
      gl::DeleteProgram(id);

    throw ShaderCreationError(name, e.what(), log.str());
  }
}


ShaderProgram::~ShaderProgram()
{
  LOG_TRACE<<"~ShaderProgram()"<<endl;
  CHECK_GL_ERROR();

  if (id)
    gl::DeleteProgram(id);

  CHECK_GL_ERROR();
}

bool ShaderProgram::link()
{
  gl::LinkProgram(id);
  CHECK_GL_ERROR();

  GLint is_linked = 0;
  gl::GetProgramiv(id, GL_LINK_STATUS, &is_linked);
  if (!is_linked)
  {
    GLint maxLength = 0;
    gl::GetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);

    GLchar *infoLog = (GLchar*) malloc(maxLength);
    gl::GetProgramInfoLog(id, maxLength, &maxLength, infoLog);

    std::ostringstream error_log;

    error_log << infoLog;
    error_log << endl;

    LOG_ERROR << endl << "Error linking program: " << name << endl << infoLog << endl;

    free(infoLog);

    LOG_ERROR << "Shaders:" << std::endl;
    for (auto& shader : shaders)
    {
      LOG_ERROR << getShaderTypeString(shader->getType())
                << ": " << shader->getFileName()
                << std::endl;
      error_log << getShaderTypeString(shader->getType())
                << ": " << shader->getFileName()
                << std::endl;
    }

    throw LinkError(error_log.str());
  }

  return is_linked;
}

void ShaderProgram::create()
{
  FORCE_CHECK_GL_ERROR();

  GLint current_program_save;
  gl::GetIntegerv(GL_CURRENT_PROGRAM, &current_program_save);

  FORCE_CHECK_GL_ERROR();

  is_valid = false;

  id = gl::CreateProgram();
  assert(id != 0);

  LOG_TRACE<<name<<": num fragment shaders: "<<fragment_shaders.size()<<endl;
  for (auto name : fragment_shaders)
  {
    shaders.push_back(std::make_unique<Shader>(name, paths, GL_FRAGMENT_SHADER,
                                                         m_parameters));
  }

  LOG_TRACE<<name<<": num vertex shaders: "<<vertex_shaders.size()<<endl;
  for (auto name : vertex_shaders)
  {
    shaders.push_back(std::make_unique<Shader>(name, paths, GL_VERTEX_SHADER,
                                                         m_parameters));
  }

  LOG_TRACE<<name<<": num geometry shaders: "<<geometry_shaders.size()<<endl;
  for (auto name : geometry_shaders)
  {
    shaders.push_back(std::make_unique<Shader>(name, paths, GL_GEOMETRY_SHADER,
                                                         m_parameters));
  }

  LOG_TRACE<<name<<": num compute shaders: "<<compute_shaders.size()<<endl;
  for (auto name : compute_shaders)
  {
    shaders.push_back(std::make_unique<Shader>(name, paths, GL_COMPUTE_SHADER,
                                                         m_parameters));
  }

  FORCE_CHECK_GL_ERROR();

  for (auto &shader : shaders)
  {
    shader->compile();
  }

  int num_attached = 0;

  for (auto &shader : shaders)
  {
    if (!shader->getID())
      continue;

    gl::AttachShader(id, shader->getID());
    num_attached++;

    gl::Finish();
    GLenum error = gl::GetError();
    if (error != GL_NO_ERROR)
    {
      LOG_ERROR<<"glAttachShader() failed for program "<<name<<", shader: "<<shader->getFileName()<<endl;
      LOG_ERROR<<"gl error: "<<gl_binding::getGLErrorString(error)<<endl;
      throw ShaderCreationError(name, "glAttachShader failed for " + shader->getFileName());
    }
  }

  for (auto it : attribute_locations)
  {
    gl::BindAttribLocation(id, it.first, it.second.c_str());

    gl::Finish();
    GLenum error = gl::GetError();
    if (error != GL_NO_ERROR)
    {
      LOG_ERROR<<"gl::BindAttribLocation() failed for program "<<name<<endl;
      LOG_ERROR<<"index: "<<it.first<<", name: "<<it.second<<endl;
      LOG_ERROR<<"gl error: "<<gl_binding::getGLErrorString(error)<<endl;
      throw ShaderCreationError(name, "glBindAttribLocation error");
    }
  }

  bool is_linked = false;
  if (num_attached)
  {
    is_linked = link();
  }

  gl::Finish();
  auto error = gl::GetError();
  assert(error == GL_NO_ERROR || error == GL_INVALID_VALUE);
  assert(gl::GetError() == GL_NO_ERROR);

  is_valid = is_linked && (error == GL_NO_ERROR);

  if (isValid())
  {
    getAllUniforms();
  }

  FORCE_CHECK_GL_ERROR();
}

GLuint ShaderProgram::getId()
{
  return id;
}

void ShaderProgram::assertIsValid()
{
  if (must_be_valid && !is_valid)
    throw ShaderCreationError(name, "Validation failed");
}


bool ShaderProgram::hasUnsetUniforms()
{
  assert(set_uniforms_count <= uniforms.size());
  return set_uniforms_count != uniforms.size();
}


void ShaderProgram::logUnsetUniforms()
{
  if (hasUnsetUniforms())
  {
    for (auto& it : uniforms)
    {
      if (!it.second.is_set)
      {
        LOG_ERROR << this->name << ": unset uniform: " << it.second.name << endl;
      }
    }

    LOG_WARNING << "Set uniforms for program " << this->name << ":" << std::endl;
    for (auto& it : uniforms)
    {
      if (it.second.is_set)
      {
        LOG_WARNING << it.second.name << endl;
      }
    }
  }
}


void ShaderProgram::logErrors()
{
  if (!isUsable())
  {
    LOG_ERROR << "Program: " << name << std::endl;
    logUnsetUniforms();
    logParameters(plog::warning, m_parameters);
    were_errors_logged = true;
  }
}


void ShaderProgram::logErrorsOnce()
{
  if (!were_errors_logged)
    logErrors();
}


void ShaderProgram::assertUniformsAreSet()
{
#if DEBUG_SHADERS
  if (!isUsable())
  {
    logErrors();
    abort();
  }
#endif
}


void ShaderProgram::getAllUniforms()
{
  auto add_uniform = [this] (std::string& name)
  {
    LOG_DEBUG << "program: " << this->name << " uniform: " << name << std::endl;

    // skip built-in uniforms
    if (util::isPrefix("gl_", name))
      return;

    //FIXME TODO skip uniform blocks

    auto location = gl::GetUniformLocation(getId(), name.c_str());
    if (location == -1)
      return; //FIXME

    auto& uniform = uniforms[name];
    uniform.name = name;
    uniform.location = location;
    // assert(uniform.location != -1); FIXME
  };

  int num_active = 0;
  gl::GetProgramiv(id, GL_ACTIVE_UNIFORMS, &num_active);
  for (int i = 0; i < num_active; i++)
  {
    char name_buffer[1024];
    int name_length = 0;
    int size = 0;
    GLenum type = 0;
    gl::GetActiveUniform(id, i, sizeof(name_buffer),
                         &name_length, &size, &type, name_buffer);
    string name(name_buffer, name_length);

    if (size > 1)
    {
      const std::string suffix = "[0]";
      assert(name.size() > suffix.size());
      assert(util::isSuffix(suffix, name));

      auto base = name.substr(0, name.size() - suffix.size());

      for (int i = 0; i < size; i++)
      {
        auto name = base + '[' + std::to_string(i) + ']';
        add_uniform(name);
      }
    }
    else
    {
      add_uniform(name);
    }
  }
}


ShaderProgram::Uniform* ShaderProgram::getUniform(const string& name)
{
  auto it = uniforms.find(name);
  if (it != uniforms.end())
    return &it->second;
  else
    return nullptr;
}


template <>
void ShaderProgram::setUniform<int>(int location, const int &value)
{
  gl::ProgramUniform1i(id, location, value);
}


template <>
void ShaderProgram::setUniform<unsigned int>(GLint location, const unsigned &value)
{
  gl::ProgramUniform1ui(id, location, value);
}


template <>
void ShaderProgram::setUniform(int location, const bool &value)
{
  gl::ProgramUniform1i(id, location, value);
}


template <>
void ShaderProgram::setUniform<float>(GLint location, const float &value)
{
  gl::ProgramUniform1f(id, location, value);
}


template <>
void ShaderProgram::setUniform(GLint location, const glm::vec2 &value)
{
  gl::ProgramUniform2fv(id, location, 1, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::vec3>(GLint location, const glm::vec3 &value)
{
  gl::ProgramUniform3fv(id, location, 1, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::vec4>(GLint location, const glm::vec4 &value)
{
  gl::ProgramUniform4fv(id, location, 1, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::ivec2>(GLint location, const glm::ivec2 &value)
{
  gl::ProgramUniform2iv(id, location, 1, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::ivec3>(GLint location, const glm::ivec3 &value)
{
  gl::ProgramUniform3iv(id, location, 1, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::mat3>(int location, const glm::mat3 &value)
{
  gl::ProgramUniformMatrix3fv(id, location, 1, false, value_ptr(value));
}


template <>
void ShaderProgram::setUniform<glm::mat4>(GLint location, const glm::mat4 &value)
{
  gl::ProgramUniformMatrix4fv(id, location, 1, false, value_ptr(value));
}


} // namespace render_util

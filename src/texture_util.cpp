/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/image_resample.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <render_util/elevation_map.h>
#include <render_util/earth_curvature_map.h>
#include <render_util/state.h>

#include <fstream>
#include <memory>
#include <iostream>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/gl.h>

#include <render_util/gl_binding/gl_functions.h>
#include <atmosphere_map.h>
#include <log.h>

using namespace render_util::gl_binding;
using namespace render_util;
using namespace glm;
using namespace std;

namespace
{


void checkTextureSize(int w, int h)
{
  assert(isTextureSizeValid(w, h));
}


float mapFloatToUnsignedChar(float value)
{
  return ((value + 1.0) / 2.0) * 255;
}


} // namespace

namespace render_util
{


void setTextureImage(TexturePtr texture,
                     const unsigned char *data,
                     int w,
                     int h,
                     int bytes_per_pixel,
                     bool mipmaps)
{
  assert(data);
  assert(w);
  assert(h);
  CHECK_GL_ERROR();

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  checkTextureSize(w, h);

  TemporaryTextureBinding binding(texture);
  CHECK_GL_ERROR();

  GLint internal_format = -1;
  GLint format = -1;

  switch (bytes_per_pixel)
  {
    case 1:
      internal_format = GL_R8;
      format = GL_RED;
      break;
    case 2:
      internal_format = GL_RG8;
      format = GL_RG;
      break;
    case 3:
      internal_format = GL_RGB8;
      format = GL_RGB;
      break;
    case 4:
      internal_format = GL_RGBA8;
      format = GL_RGBA;
      break;
    default:
      LOG_TRACE<<bytes_per_pixel<<endl;
      assert(0);
      abort();
  }

  gl::TexImage2D(GL_TEXTURE_2D, 0,
                internal_format,
                w,
                h,
                0,
                format,
                GL_UNSIGNED_BYTE,
                data);

  CHECK_GL_ERROR();

  if (mipmaps)
  {
    gl::GenerateMipmap(GL_TEXTURE_2D);
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  }
//   else
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  CHECK_GL_ERROR();
}


TexturePtr createTexture(const unsigned char *data, int w, int h, int bytes_per_pixel, bool mipmaps)
{
  assert(data);
  assert(w);
  assert(h);
  CHECK_GL_ERROR();

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  checkTextureSize(w, h);

  TexturePtr texture = Texture::create(GL_TEXTURE_2D);
  CHECK_GL_ERROR();

  TemporaryTextureBinding binding(texture);
  CHECK_GL_ERROR();

  GLint internal_format = -1;
  GLint format = -1;

  switch (bytes_per_pixel)
  {
    case 1:
      internal_format = GL_R8;
      format = GL_RED;
      break;
    case 2:
      internal_format = GL_RG8;
      format = GL_RG;
      break;
    case 3:
      internal_format = GL_RGB8;
      format = GL_RGB;
      break;
    case 4:
      internal_format = GL_RGBA8;
      format = GL_RGBA;
      break;
    default:
      LOG_TRACE<<bytes_per_pixel<<endl;
      assert(0);
      abort();
  }


  gl::TexImage2D(GL_TEXTURE_2D, 0,
                internal_format,
                w,
                h,
                0,
                format,
                GL_UNSIGNED_BYTE,
                data);

  CHECK_GL_ERROR();

  if (mipmaps)
  {
    gl::GenerateMipmap(GL_TEXTURE_2D);
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  }
  else
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  
  CHECK_GL_ERROR();

  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  return texture;
}


TexturePtr createTextureExt(const unsigned char *data,
                          size_t data_size,
                          int w, int h,
                          int num_components,
                          size_t bytes_per_component,
                          int type,
                          int format,
                          int internal_format,
                          bool mipmaps)
{
  assert(data);
  assert(w);
  assert(h);
  assert(data_size == (w * h * num_components * bytes_per_component));
  FORCE_CHECK_GL_ERROR();

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  checkTextureSize(w, h);

  TexturePtr texture = Texture::create(GL_TEXTURE_2D);
  CHECK_GL_ERROR();

  TemporaryTextureBinding binding(texture);
  CHECK_GL_ERROR();

  LOG_TRACE << "reserving texture memory: " << w << "x" << h
    << " (" << num_components << " components, " << bytes_per_component << " bytes per component, "
    << data_size/1024.0/1024.0 << " mb)"
    << endl;

  gl::TexImage2D(GL_TEXTURE_2D, 0,
                internal_format,
                w,
                h,
                0,
                format,
                type,
                data);

  FORCE_CHECK_GL_ERROR();

  if (mipmaps)
  {
    gl::GenerateMipmap(GL_TEXTURE_2D);
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  }
  else
  {
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  }

  FORCE_CHECK_GL_ERROR();

  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  return texture;
}


TexturePtr createFloatTexture1D(const float *data, size_t size, int num_components)
{
  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  checkTextureSize(size, size);

  TexturePtr texture = Texture::create(GL_TEXTURE_1D);
  TemporaryTextureBinding binding(texture);

  gl::TexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  gl::TexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  gl::TexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  gl::TexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  GLint internal_format = -1;
  GLint format = -1;

  switch (num_components)
  {
    case 1:
      internal_format = GL_R32F;
      format = GL_RED;
      break;
    case 2:
      internal_format = GL_RG32F;
      format = GL_RG;
      break;
    case 3:
      internal_format = GL_RGB32F;
      format = GL_RGB;
      break;
    default:
      assert(0);
      abort();
  }
  
  gl::TexImage1D(GL_TEXTURE_1D,
                0,
                internal_format,
                size,
                0,
                format,
                GL_FLOAT,
                data);

  CHECK_GL_ERROR();

  return texture;
}

TexturePtr createFloatTexture(const float *data, int w, int h, int num_components, bool mipmaps)
{
  CHECK_GL_ERROR();

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  checkTextureSize(w, h);

  TexturePtr texture = Texture::create(GL_TEXTURE_2D);
  TemporaryTextureBinding binding(texture);

  CHECK_GL_ERROR();

  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  GLint internal_format = -1;
  GLint format = -1;

  switch (num_components)
  {
    case 1:
      internal_format = GL_R32F;
      format = GL_RED;
      break;
    case 2:
      internal_format = GL_RG32F;
      format = GL_RG;
      break;
    case 3:
      internal_format = GL_RGB32F;
      format = GL_RGB;
      break;
    default:
      assert(0);
      abort();
  }

  gl::TexImage2D(GL_TEXTURE_2D, 0,
                internal_format,
                w,
                h,
                0,
                format,
                GL_FLOAT,
                data);

  if (mipmaps)
    gl::GenerateMipmap(texture->getTarget());

  CHECK_GL_ERROR();

  return texture;
}


#if 0
unsigned int createUnsignedIntTexture(const unsigned int *data, int w, int h)
{
  GLuint current_texture_save;
  gl::GetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*) &current_texture_save);

  CHECK_GL_ERROR();

  GLuint texture = 0;
  gl::GenTextures(1, &texture);

  assert(texture);

  gl::BindTexture(GL_TEXTURE_2D, texture);    

  CHECK_GL_ERROR();

  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//     gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  GLint internal_format = GL_R32UI;
  GLint format = GL_RED;

  CHECK_GL_ERROR();

  gl::TexImage2D(GL_TEXTURE_2D,
              0,
              internal_format,
              w,
              h,
              0,
              format,
              GL_UNSIGNED_INT,
              data);

  CHECK_GL_ERROR();
  exit(1);
  CHECK_GL_ERROR();

  gl::BindTexture(GL_TEXTURE_2D, current_texture_save);

  return texture;
}
#endif

// namespace
// {
//   using namespace glm;
// 
//   glm::vec3 getNormal(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
//   {
// //       vec3 u = p2 - p1;
// //       vec3 v = p3 - p1;
// // 
// //       vec3 normal;
// // 
// //       normal.x = (u.y * v.z) - (u.z * v.y);
// //       normal.y = (u.z * v.x) - (u.x * v.z);
// //       normal.z = (u.x * v.y) - (u.y * v.x);
// //       return normal;
// 
//     vec3 a,b;
//     a = p1 - p2;
//     b = p1 - p3;
// 
//     return normalize(cross(a,b));
// 
// 
//   }
// 
//   vec3 getTerrainVertex(const render_util::ElevationMap &elevation_map, int x, int y)
//   {
//     const float grid_resolution = 200.0;
//     
//     return vec3(x * grid_resolution, y * grid_resolution, elevation_map->get(x, y));
//   }
// }

// ImageGreyScale::Ptr createTerrainLightMap(const ElevationMap &elevation_map)
// {
//   using namespace glm;
//   
//   vec3 sun_direction(normalize(vec3(0.5, 0.5, 0.7)));
// //     vec3 sun_direction(normalize(vec3(0.0, 0.0, 1.0)));
// 
//   ImageGreyScale *image = new ImageGreyScale(ivec2(elevation_map.getWidth(), elevation_map.getHeight()));
// 
//   for (int y = 0; y < elevation_map.getHeight(); y++)
//   {
//     for (int x = 0; x < elevation_map.getWidth(); x++)
//     {
//       vec3 v00 = getTerrainVertex(elevation_map, x+1, y+1);
//       vec3 v10 = getTerrainVertex(elevation_map, x+0, y+0);
//       vec3 v20 = getTerrainVertex(elevation_map, x+1, y+0);
// 
//       vec3 n0 = getNormal(v00, v10, v20);
// 
//       vec3 v01 = getTerrainVertex(elevation_map, x+1, y+1);
//       vec3 v11 = getTerrainVertex(elevation_map, x+0, y+1);
//       vec3 v21 = getTerrainVertex(elevation_map, x+0, y+0);
// 
//       vec3 n1 = getNormal(v01, v11, v21);
//       
// //         LOG_TRACE<<n1.x<<" "<<n1.y<<" "<<n1.z<<endl;
// 
//       vec3 n = n0 + n1;
//       n *= 0.5;
// 
//       float light = glm::clamp(dot(n, sun_direction), 0.0f, 1.0f);
// //         dot(n, sun_direction);
//       
// //         light = !(x % 2) && (y % 2) ? 1.0 : 0.0;
// 
//       image->setPixel(x, y, light * 255);
//     }
//   }
// 
//   return ImageGreyScale::Ptr(image);
// }


//FIXME doesn't belong here
TexturePtr createAmosphereThicknessTexture(std::string resource_path)
{
  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  AtmosphereMapElementType *atmosphere_map =
    new AtmosphereMapElementType[atmosphere_map_num_elements];

  ifstream in(resource_path + "/atmosphere_map", ios_base::binary);
  assert(in.good());

  in.read((char*) atmosphere_map, atmosphere_map_size_bytes);
  assert(in.good());

  const glm::ivec2 dimensions = atmosphere_map_dimensions;

  TexturePtr texture = createFloatTexture((float*)atmosphere_map, dimensions.x, dimensions.y, 2);

  TextureParameters<int> params;
  params.set(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  params.set(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  params.set(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  params.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  params.apply(texture);

  // texture_manager.bind(TEXUNIT_ATMOSPHERE_THICKNESS_MAP, texture); FIXME

  CHECK_GL_ERROR();

  delete[] atmosphere_map;

  return texture;
}


//FIXME doesn't belong here
TexturePtr createCurvatureTexture(std::string resource_path)
{
  using namespace render_util::earth_curvature_map;

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  vector<char> curvature_map;
  if (!util::readFile(resource_path + "/curvature_map", curvature_map))
  {
    assert(0);
  }
  assert(curvature_map.size() == curvature_map_size_bytes);

  TexturePtr texture = createFloatTexture((const float*)curvature_map.data(),
                                          curvature_map_width,
                                          curvature_map_height,
                                          2);

  TextureParameters<int> params;
  params.set(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  params.set(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  params.set(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  params.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  params.apply(texture);

  // texture_manager.bind(TEXUNIT_CURVATURE_MAP, texture); FIXME

  CHECK_GL_ERROR();

  return texture;
}


//FIXME obsolete
ImageRGBA::Ptr createMapFarTexture(ImageGreyScale::ConstPtr type_map,
                            const vector<ImageRGBA::ConstPtr> &textures,
                            int type_map_meters_per_pixel,
                            int meters_per_tile)

{
  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  //FIXME take texture scale into account

  typedef Sampler<ImageRGBA> SamplerRGBA;
  typedef Surface<ImageRGBA> SurfaceRGBA;

  LOG_DEBUG<<"generating far texture..."<<endl;

  int tile_size_pixels = meters_per_tile / type_map_meters_per_pixel;
  assert(tile_size_pixels * type_map_meters_per_pixel == meters_per_tile);

  ImageRGBA::Ptr texture = make_shared<ImageRGBA>(type_map->size());

  vector<ImageRGBA::Ptr> mipmaps(textures.size());
  for (size_t i = 0; i < mipmaps.size(); i++)
  {
    if (textures[i])
    {
      mipmaps[i] = downSample(textures[i], textures[i]->w() / tile_size_pixels);
      //     stringstream path;
      //     path << "dump/" << i << ".tga";
      //     saveImageToFile(path.str(), mipmaps[i].get());
    }
  }

  for (int y = 0; y < texture->h(); y++)
  {
    for (int x = 0; x < texture->w(); x++)
    {
      for (int i = 0; i < texture->numComponents(); i++)
      {
        int color = 0;

        unsigned type = type_map->get(x,y);

        assert(type < mipmaps.size());

        if (type >= mipmaps.size())
          type = 0;

        auto mipmap = mipmaps[type];
        if (mipmap)
        {
          ivec2 tile_coords = ivec2(x,y);
          tile_coords %= tile_size_pixels;
          color = mipmap->get(tile_coords, i);
        }

        texture->at(x,y,i) = color;
      }
    }
  }

//   exit(0);
  LOG_DEBUG<<"generating map texture finished."<<endl;

  return texture;
}



bool isTextureSizeValid(int w, int h)
{
  GLint max_size = 0;
  gl::GetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);
  assert(max_size > 0);

  if (w <= max_size && h <= max_size)
  {
    return true;
  }
  else
  {
    LOG_DEBUG << "max size: " << max_size << ", requested size: " << w << "x" << h << std::endl;
    return false;
  }
}


void getTextureImage(unsigned int texture_id,
                     int format,
                     int type,
                     size_t bytes_per_pixel,
                     std::vector<uint8_t> &data, glm::ivec2 &size)
{
  StateModifier state;
  state.setPixelStorePackAlignment(1);

  gl::GetTextureLevelParameteriv(texture_id, 0, GL_TEXTURE_WIDTH, &size.x);
  gl::GetTextureLevelParameteriv(texture_id, 0, GL_TEXTURE_HEIGHT, &size.y);
  data.resize(size.x * size.y * bytes_per_pixel);
  gl::GetTextureImage(texture_id, 0, format, type, data.size(), data.data());
}


} // namespace render_util


namespace render_util::texture_format
{


int TextureFormatParametersBase::getFormat(unsigned int num)
{
  switch (num)
  {
    case 1:
      return GL_RED;
    case 2:
      return GL_RG;
    case 3:
      return GL_RGB;
    case 4:
      return GL_RGBA;
    default:
      assert(0);
      abort();
  }
}


int TextureFormatParameters<unsigned char>::getType()
{
   return GL_UNSIGNED_BYTE;
}


int TextureFormatParameters<unsigned char>::getInternalFormat(unsigned int num)
{
  switch (num)
  {
    case 1:
      return GL_R8;
    case 2:
      return GL_RG8;
    case 3:
      return GL_RGB8;
    case 4:
      return GL_RGBA8;
    default:
      assert(0);
      abort();
  }
}


int TextureFormatParameters<float>::getType()
{
  return GL_FLOAT;
}


int TextureFormatParameters<float>::getInternalFormat(unsigned int num)
{
  switch (num)
  {
    case 1:
      return GL_R32F;
    case 2:
      return GL_RG32F;
    case 3:
      return GL_RGB32F;
    case 4:
      return GL_RGBA32F;
    default:
      assert(0);
      abort();
  }
};


int TextureFormatParameters<half_float::half>::getType()
{
  return GL_HALF_FLOAT;
}


int TextureFormatParameters<half_float::half>::getInternalFormat(unsigned int num)
{
  switch (num)
  {
    case 1:
      return GL_R16F;
    case 2:
      return GL_RG16F;
    case 3:
      return GL_RGB16F;
    case 4:
      return GL_RGBA16F;
    default:
      assert(0);
      abort();
  }
};


} // namespace render_util::texture_format

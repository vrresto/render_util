/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/state.h>
#include <render_util/context.h>


using namespace render_util;
using namespace render_util::gl_binding;


namespace
{


struct SimpleStateModifier
{
  void setPixelStorePackAlignment(GLint param)
  {
    state::PixelStorei(GL_PACK_ALIGNMENT, param);
  }

  void setPixelStoreUnpackAlignment(GLint param)
  {
    state::PixelStorei(GL_UNPACK_ALIGNMENT, param);
  }

  void setPolygonMode(GLenum mode)
  {
    state::PolygonMode(GL_FRONT_AND_BACK, mode);
  }

  void setPolygonOffset(GLfloat factor, GLfloat units)
  {
    state::PolygonOffset(factor, units);
  }

  void setBlendFunc(GLenum sfactor, GLenum dfactor)
  {
    state::BlendFunc(sfactor, dfactor);
  }

  void setAlphaFunc(GLenum func, GLclampf ref)
  {
    state::AlphaFunc(func, ref);
  }

  void setProgram(unsigned program)
  {
    state::UseProgram(program);
  }

  void setFrontFace(GLenum value)
  {
    state::FrontFace(value);
  }

  void setCullFace(GLenum value)
  {
    state::CullFace(value);
  }

  void setDepthFunc(GLenum value)
  {
    state::DepthFunc(value);
  }

  void setDepthMask(bool value)
  {
    state::DepthMask(value);
  }

  void enable(size_t index, bool value)
  {
    auto cap = State::getCapFromIndex(index);
    if (value)
      state::Enable(cap);
    else
      state::Disable(cap);
  }
};


template <class Modifier>
void applyState(const State& state, Modifier& m)
{
  m.setPixelStorePackAlignment(state.pixel_store_pack_alignment);
  m.setPixelStoreUnpackAlignment(state.pixel_store_unpack_alignment);
  m.setPolygonMode(state.polygon_mode);
  m.setProgram(state.program);
  m.setFrontFace(state.front_face);
  m.setCullFace(state.cull_face);
  m.setDepthFunc(state.depth_func);
  m.setDepthMask(state.depth_mask);
  m.setBlendFunc(std::get<0>(state.blend_func),
                 std::get<1>(state.blend_func));
  m.setAlphaFunc(std::get<0>(state.alpha_func),
                 std::get<1>(state.alpha_func));
  m.setPolygonOffset(std::get<0>(state.polygon_offset),
                     std::get<1>(state.polygon_offset));

  for (int i = 0; i < State::CapIndex::ENUM_SIZE; i++)
    m.enable(i, state.caps.test(i));
}


StateStack &getStack()
{
  return Context::getCurrent().getStateStack();
}


} // namespace


namespace render_util
{


bool StateModifier::isCurrent()
{
  return getStack().current() == m_current_state;
}


void StateModifier::init()
{
  auto &stack = getStack();
  assert(stack.empty());

  auto initial_state = std::make_unique<State>();

  SimpleStateModifier modifier;
  applyState(*initial_state, modifier);

  stack.push(std::move(initial_state));
}


StateModifier::StateModifier()
{
  auto &stack = getStack();
  assert(!stack.empty());

  m_prev_state = stack.current();

  auto new_state = std::make_unique<State>();
  *new_state = *m_prev_state;
  m_current_state = new_state.get();

  stack.push(std::move(new_state));
}


StateModifier::~StateModifier()
{
  assert(isCurrent());

  applyState(*m_prev_state, *this);

  auto &stack = getStack();

  m_current_state = nullptr;
  stack.pop();

  assert(stack.current() == m_prev_state);
  assert(stack.size() >= 1);
}


void StateModifier::setDefaults()
{
  State default_state;
  applyState(default_state, *this);
}


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _RENDER_UTIL_TERRAIN_FOREST_H
#define _RENDER_UTIL_TERRAIN_FOREST_H

#include "module.h"
#include "layer.h"

#include <render_util/texunits.h>
#include <render_util/shader.h>
#include <render_util/image.h>
#include <render_util/terrain_base.h>

#include <glm/glm.hpp>

namespace render_util::terrain
{


class Forest : public ModuleSubClass<TerrainBase::ModuleIndex::FOREST>,
               public IMaterialModule
{
  ShaderParameters m_shader_params;
  TexturePtr m_layers_texture;
  TexturePtr m_forest_far_texture;

public:
  Forest(TerrainBase::BuildParameters&);

//   const ShaderParameters &getShaderParameters() const override { return m_shader_params; }
  void bindTextures(TextureState&) const override;
  void setUniforms(ShaderProgramPtr program) const override;
  void loadLayer(Layer&,
                 TerrainResourcesBase::Layer&,
                 bool is_base_layer/*FIXME HACK*/,
                 const ShaderSearchPath&) override;

  void setShaderParameters(unsigned int material,
                           unsigned int  detail_options,
                           ShaderParameters&) const override;

  void addShaders(unsigned int material,
                  int lod_options,
                  std::vector<std::string>& vertex_shaders,
                  std::vector<std::string>& fragment_shaders) const override;
};


} // namespace render_util::terrain

#endif

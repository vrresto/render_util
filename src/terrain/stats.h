#pragma once

#include <render_util/stats.h>
#include "terrain_config.h"


namespace render_util::terrain
{


template <class RenderList, class VAOTrianglesCount>
void getStats(RenderList& list,
              const std::vector<std::string>& material_names,
              const VAOTrianglesCount& triangles_per_vao,
              Stats& stats)
{
  constexpr auto VAO_COUNT = RenderList::MESH_COUNT;

  int groups = 0;
  int total_batches = 0;
  int total_instances = 0;
  int total_instances_full = 0;
  int total_instances_partial = 0;

  std::array<int, VAO_COUNT> total_instances_per_vao {};

  for (auto& group: list.getActiveGroups())
  {
    groups++;

    int group_batches = 0;
    int group_instances = 0;

    std::array<int, VAO_COUNT> instances_per_vao {};

    auto& group_stats = stats.addChild("group " + std::to_string(groups));

    int items = 0;
    auto item_visitor = [&] (auto& item)
    {
      if (!item.program || !item.program->isUsable())
        return;

      items++;

      {
        int i = 0;
        for (auto& batch : item.batches)
        {
          if (!batch.empty())
          {
            group_batches++;
            group_instances += batch.size();
          }
          instances_per_vao.at(i) += batch.size();
          i++;
        }
      }

#if TERRAIN_EXTENDED_STATS
      auto& item_stats =
        group_stats.addChild("material " + std::to_string(item.material_index) +
                            " (lod " + std::to_string(item.material_lod) + ")");

      item_stats.add("material", material_names.at(item.material_index));

      int item_instances = 0;
      for (int i = 0; i < VAO_COUNT; i++)
      {
        item_stats.add("instances vao " + std::to_string(i), item.batches.at(i).size());
        item_instances += item.batches.at(i).size();
      }
      item_stats.add("instances", item_instances);

      //FIXME this doesn't look right for forest render group
      // lod_stats.add("modules", getModuleNames(item->modules));
#endif
    };

    group->visitItems(item_visitor);
    group_stats.add("items", items);

    total_batches += group_batches;
    total_instances += group_instances;

    group_stats.add("batches", group_batches);

    group_stats.add("instances", group_instances);

    for (int i = 0; i < VAO_COUNT; i++)
    {
      total_instances_per_vao.at(i) += instances_per_vao.at(i);
      group_stats.add("instances vao " + std::to_string(i), instances_per_vao.at(i));
    }

    int triangles = 0;
    for (int i = 0; i < VAO_COUNT; i++)
      triangles += instances_per_vao.at(i) * triangles_per_vao.at(i);

    group_stats.add("triangles", triangles);
  }

  stats.add("total batches", total_batches);
  stats.add("total instances", total_instances);

  int total_triangles = 0;
  for (int i = 0; i < VAO_COUNT; i++)
  {
    total_triangles += total_instances_per_vao.at(i) * triangles_per_vao.at(i);
    stats.add("total instances vao " + std::to_string(i), total_instances_per_vao.at(i));
  }

  stats.add("total triangles", total_triangles);

  stats.add("groups", groups);
}


} //namespace render_util::terrain

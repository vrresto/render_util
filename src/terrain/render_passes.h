#pragma once

#include "render_list_base.h"


namespace render_util::terrain
{
  std::unique_ptr<RenderGroupParameters> createRenderGroupParameters(int group_id,
      TexturePtr& water_map_pass_fb_texture);
}

#include "geometry.h"
#include <render_util/normal_map.h>
#include <render_util/gl_binding/gl_functions.h>

#include <glm/gtc/round.hpp>


using namespace render_util;
using namespace render_util::terrain;

using namespace glm;

namespace
{


constexpr auto HEIGHT_MAP_METERS_PER_GRID = 200;


TexturePtr createNormalMapTexture(render_util::ElevationMap::ConstPtr map,
                                                    int meters_per_grid)
{
  using namespace render_util;

  LOG_INFO<<"TerrainCDLOD: creating normal map ..."<<std::endl;
  auto normal_map = std::make_unique<Image<float,3>>(map->size()); //FIXME - use half
  createNormalMapFromFloat(*map, *normal_map, meters_per_grid, false);
  LOG_INFO<<"TerrainCDLOD: creating normal map done."<<std::endl;

  auto normal_map_texture =
    createFloatTexture(reinterpret_cast<const float*>(normal_map->getData()),
                      map->getWidth(),
                      map->getHeight(),
                      3,
                      true);
  LOG_INFO<<"TerrainCDLOD: creating normal map  texture done."<<std::endl;

  normal_map_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  normal_map_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  normal_map_texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  normal_map_texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  normal_map_texture->setParameter(GL_TEXTURE_BORDER_COLOR, glm::vec4(0,0,1,0));

  return normal_map_texture;
}


TexturePtr createHeightMapTexture(render_util::ElevationMap::ConstPtr hm_image)
{
  using namespace render_util;

  LOG_INFO<<"TerrainCDLOD: creating height map texture ..."<<std::endl;

  auto height_map_texture = createTextureExt(image::convert<half_float::half>(hm_image), true);

  CHECK_GL_ERROR();

  height_map_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  height_map_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  height_map_texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  height_map_texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  CHECK_GL_ERROR();

  return height_map_texture;
}


auto createHeightMap(render_util::ElevationMap::ConstPtr image)
{
  auto hm = std::make_unique<AttributeMapTexture>("height_map");

  hm->resolution_m = HEIGHT_MAP_METERS_PER_GRID;
  hm->size_m = image->getSize() * (int)HEIGHT_MAP_METERS_PER_GRID;
  hm->size_px = image->getSize();
  hm->setTexture(createHeightMapTexture(image));

  return hm;
}


auto createNormalMap(render_util::ElevationMap::ConstPtr image)
{
  auto nm = std::make_unique<AttributeMapTexture>("normal_map");

  nm->resolution_m = HEIGHT_MAP_METERS_PER_GRID;
  nm->size_m = image->getSize() * (int)HEIGHT_MAP_METERS_PER_GRID;
  nm->size_px = image->getSize();
  nm->setTexture(createNormalMapTexture(image, HEIGHT_MAP_METERS_PER_GRID));

  return nm;
}


} // namespace


namespace render_util::terrain
{


HeightMap::HeightMap(const TerrainBase::BuildParameters& params)
{
  m_map = createLayerMap(params.resources.getDetailLayer());
  if (params.resources.hasBaseLayer())
    m_base_map = createLayerMap(params.resources.getBaseLayer());
}


std::unique_ptr<HeightMap::LayerMap> HeightMap::createLayerMap(TerrainResourcesBase::Layer& layer)
{
  auto map = std::make_unique<LayerMap>();
  map->map = layer.getHeightMap();
  assert(map->map);
  map->resolution_m = layer.getResolutionM();
  map->origin_m = layer.getOriginM();
  return map;
}


float HeightMap::getMaxHeight(const Rect& area) const
{
  float max_height = 0;

  auto visitor = [&] (float value)
  {
    max_height = max(max_height, value);
  };

  visitArea(area, visitor);

  return max_height;
}


float HeightMap::getMinHeight(const Rect& area) const
{
  float min_height = 10000.0;

  auto visitor = [&] (float value)
  {
    min_height = min(min_height, value);
  };

  visitArea(area, visitor);

  return min_height;
}


void loadGeometry(Layer& layer, int layer_number, TerrainBase::BuildParameters& params)
{
  auto hm_image = params.resources.getLayer(layer_number).getHeightMap();

  auto hm_image_resized = hm_image;
  auto new_size = glm::ceilPowerOfTwo(hm_image->size());
  if (new_size != hm_image->size())
  {
    hm_image_resized = image::extend(hm_image, new_size, 0.f,
                                     image::ExtendDirection::TOP_RIGHT,
                                     image::Origin::BOTTOM_LEFT);
  }


  layer.origin_m = params.resources.getLayer(layer_number).getOriginM();
  layer.origin_terrain_grid =
    glm::vec2(params.resources.getLayer(layer_number).getOriginM()) /
              glm::vec2(TerrainBase::GRID_RESOLUTION_M);

  layer.scale = params.resources.getLayer(layer_number).getScale();

  auto hm = ::createHeightMap(hm_image_resized);
  auto nm = ::createNormalMap(hm_image);

  layer.size_m = glm::vec2(hm_image->getSize() *
                 (int)HEIGHT_MAP_METERS_PER_GRID);

  layer.addMap(std::move(hm));
  layer.addMap(std::move(nm));
}


} // namespace render_util::terrain

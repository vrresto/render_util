#include "module.h"
#include <render_util/terrain_base.h>
#include <util/integer_sequence.h>
#include <log.h>

#include <optional>


namespace render_util
{


const std::string TerrainBase::getModuleName(ModuleIndex::Enum index)
{

#define X(value) case TerrainBase::ModuleIndex::value: \
  return util::makeLowercase(#value);

  switch (index)
  {
    X(NONE)
    X(LAND)
    X(FOREST)
    X(WATER)
    X(RENDERER)
    X(MATERIAL_MAP_DEBUG)
  }

#undef X

  throw std::invalid_argument("invalid module index");
}


std::vector<std::string> TerrainBase::getModuleNames()
{
  std::vector<std::string> names;

  util::visit(TerrainBase::ModuleIndex::AllValues(),
              [&] (auto index)
              {
                names.push_back(getModuleName(index));
              });

  return names;
}


TerrainBase::ModuleIndex::Enum TerrainBase::getModuleIndex(const std::string& name)
{
  std::optional<TerrainBase::ModuleIndex::Enum> index;

  util::visit(TerrainBase::ModuleIndex::AllValues(),
              [&] (TerrainBase::ModuleIndex::Enum value)
              {
                if (getModuleName(value) == name)
                  index = value;
              });

  if (index)
    return index.value();
  else
    throw std::runtime_error("No such module: " + name);
}


int TerrainBase::getModuleMask(ModuleIndex::Enum index)
{
  if (index == 0)
    return 0;

  assert(index > 0);
  assert(index < ModuleIndex::ENUM_SIZE);
  return 1 << (index-1);
}


} // namespace render_util

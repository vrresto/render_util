/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "water.h"
#include "material.h"
#include <render_util/terrain_base.h>
#include <render_util/texunits.h>
#include <render_util/texture_array.h>
#include <render_util/texture_util.h>
#include <render_util/framebuffer.h>
#include <render_util/state.h>
#include <render_util/quad_2d.h>
#include <render_util/shader_util.h>
#include <render_util/normal_map.h>
#include <render_util/config.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util;
using namespace render_util::gl_binding;


namespace
{


float getAnimationLayerTileSizeM(int layer)
{
  return 20.f * glm::pow(5, layer);
}


float sampleShoreWave(float pos)
{
  const float peak_pos = 0.05;

  if (pos < peak_pos)
  {
    return pow(pos / peak_pos, 2);
  }
  else
  {
    pos -= peak_pos;
    pos /= 1.0 - peak_pos;
    return pow(1 - pos, 8);
  }
}


ImageGreyScale::Ptr createShoreWaveTexture()
{
  LOG_INFO << "Creating shore wave texture ..." << std::endl;
  auto shore_wave = render_util::image::create<unsigned char>(0, glm::ivec2(4096, 1));
  assert(shore_wave);
  for (int i = 0; i < shore_wave->w(); i++)
  {
    float pos = 1.0 * i;
    pos /= shore_wave->w();
    float value = sampleShoreWave(pos);
    shore_wave->at(i,0) = value * 255;
  }
  LOG_INFO << "Creating shore wave texture ... done." << std::endl;
  return shore_wave;
}


struct WaterMap : public terrain::AttributeMapTextureBase
{
  glm::ivec2 table_size {};
  int chunk_block_count {};
  int chunks_per_block {};
  float chunk_sample_offset {};
  float chunk_sample_scale {};
  int chunks_num_cols {};
  int chunks_num_rows {};
  float chunk_size_m {};
  bool y_flip {};

  WaterMap() : AttributeMapTextureBase("water_map") {}

  void setUniforms(ShaderProgramPtr program) override
  {
    program->setUniform(getUniformName("y_flip"), y_flip);
    program->setUniform(getUniformName("table_size"), table_size);
    program->setUniform(getUniformName("chunks_per_block"), chunks_per_block);
    program->setUniformi(getUniformName("chunk_block_count"), chunk_block_count);
    program->setUniform(getUniformName("chunk_size_m"), chunk_size_m);
    program->setUniform(getUniformName("chunk_sample_offset"), chunk_sample_offset);
    program->setUniform(getUniformName("chunk_sample_scale"), chunk_sample_scale);
    program->setUniformi(getUniformName("chunks_num_cols"), chunks_num_cols);
    program->setUniformi(getUniformName("chunks_num_rows"), chunks_num_rows);
  }
};


constexpr int NUM_LAYERS = 4;


struct WaterAnimationLayer
{
  float current_frame = 0;
  float fps = 0;

  WaterAnimationLayer(float fps) : fps(fps) {}
};


} // namespace


namespace render_util::terrain
{


using FloatTextureArray = BasicTextureArray<float>;


struct WaterAnimationInterpolator
{
  glm::ivec2 m_texture_size {};
  ShaderProgramPtr m_program;
  std::array<std::unique_ptr<FrameBufferBase>, NUM_LAYERS> m_framebuffers;
  std::shared_ptr<FloatTextureArray> m_layers;
  bool m_shader_load_error = false;
  TextureState m_texture_state;

  WaterAnimationInterpolator(glm::ivec2 size) : m_texture_size(size)
  {
    m_layers = std::make_unique<FloatTextureArray>(NUM_LAYERS, size, 4);
    assert(m_layers);

    int i = 0;
    for (auto& fb : m_framebuffers)
    {
      fb = std::make_unique<FrameBuffer>(size, 1);

      gl::NamedFramebufferTextureLayer(fb->getID(), GL_COLOR_ATTACHMENT0,
                                       m_layers->getID(), 0, i);

      i++;
    }
  }

  void compute(std::function<void(ShaderProgramPtr)> set_uniforms)
  {
    if (!m_program)
      return;

    assert(m_layers);

    {
      TextureStateBinding texture_state(m_texture_state);

      StateModifier state;
      state.setDefaults();
      state.enableCullFace(false);
      state.enableDepthTest(false);
      state.enableBlend(false);

      for (int i = 0; i < m_framebuffers.size(); i++)
      {
        auto& fb = m_framebuffers.at(i);

        TemporaryFrameBufferBinding fbb(*fb, true);

        gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                  GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        m_program->setUniformi("layer", i);

        if (!m_program->isUsable())
        {
          m_program->logErrorsOnce();
          m_shader_load_error = true;
          return;
        }

        state.setProgram(m_program->getID());
        drawFullScreenQuad();
      }
    }

    m_layers->generateMipMap();
  }
};


struct WaterAnimation
{
  std::array<WaterAnimationLayer, NUM_LAYERS> m_layers { 20, 13 , 3, 0.4};
  int m_num_animation_steps = 0;

  void update(float);
  void setUniforms(ShaderProgramPtr program);
};


void WaterAnimation::update(float frame_delta)
{
  if (!m_num_animation_steps)
    return;

  for (auto &l : m_layers)
  {
    auto frame = l.current_frame + frame_delta * l.fps;
    int frame_floor = glm::floor(frame);
    auto frame_fract = glm::fract(frame);

    frame_floor %= m_num_animation_steps;

    l.current_frame = frame_floor + frame_fract;
  }
}


void WaterAnimation::setUniforms(ShaderProgramPtr program)
{
  assert(m_num_animation_steps > 0);

  program->setUniformi("water_animation_num_frames", m_num_animation_steps);
  // program->setUniformi("water_animation_num_layers", NUM_LAYERS);

  for (int i = 0; i < NUM_LAYERS; i++)
  {
    auto prefix = std::string("water_animation_params[") + std::to_string(i) + "].";
    auto &layer = m_layers.at(i);

    int frame = glm::floor(layer.current_frame);
    float delta = glm::fract(layer.current_frame);

    program->setUniformi(prefix + "pos", frame);
    program->setUniform<float>(prefix + "frame_delta", delta);
  }
}


Water::Water(render_util::TerrainBase::BuildParameters& params)
{
  auto& water_res = params.resources.to<TerrainWaterResources>();

  auto &height_maps = water_res.getWaterAnimationHeightMaps();
  auto &normal_maps = water_res.getWaterAnimationNormalMaps();
  // auto &foam_masks = water_res.getWaterAnimationFoamMasks();
  assert(normal_maps.size() == height_maps.size());
  // assert(normal_maps.size() == foam_masks.size());

  auto animation_steps = normal_maps.size();

  // m_animation_normal_maps = std::move(createTextureArray<uint8_t>(normal_maps));
  // m_animation_foam_masks = createTextureArray(foam_masks);

  glm::ivec2 animation_texture_size {};

  if (animation_steps > 0)
  {
    auto texture_size = normal_maps.at(0)->getSize();
    animation_texture_size = texture_size;

    // auto& first = normal_maps.at(0);
    // assert(first);
    // assert(first->getNumComponents() == 3);

    LOG_INFO << "Water animation texture size: " << texture_size << std::endl;

    auto texture = std::make_shared<TextureArray>(animation_steps, texture_size, 4);

    auto combined_image = std::make_unique<ImageRGBA>(texture_size);

    int i_array = 0;
    for (int i = 0; i < animation_steps; i++)
    // for (int i = animation_steps-1; i >= 0; i--)
    {
      LOG_INFO << "Loading water animation frame " << i << std::endl;

      auto height_map_image = height_maps.at(i)->load(0, 1);
      assert(height_map_image);
      assert(height_map_image->getSize() == texture_size);
      image::flipYInPlace(height_map_image);

#if 1
      auto normal_map_image = std::make_unique<ImageRGB>(height_map_image->size());
      createNormalMapFromUint8(*height_map_image, *normal_map_image, 2.0, 100.0, true);
#else
      auto normal_map_image = normal_maps.at(i)->load(0, 3);
      assert(normal_map_image);
      LOG_INFO << "normal_map_image: " << normal_map_image->getSize() << std::endl;
      assert(normal_map_image->getSize() == texture_size);
      image::flipYInPlace(normal_map_image);
#endif

      for (int y = 0; y < combined_image->h(); y++)
      {
        for (int x = 0; x < combined_image->w(); x++)
        {
          combined_image->at(x, y, 0) = normal_map_image->get(x, y, 0);
          combined_image->at(x, y, 1) = normal_map_image->get(x, y, 1);
          combined_image->at(x, y, 2) = normal_map_image->get(x, y, 2);
          combined_image->at(x, y, 3) = height_map_image->get(x, y, 0);

          // float height = height_map_image->get(x, y, 0) / 255.f;
          // height = glm::smoothstep(0.5f, 0.7f, height * 1.f);
          // combined_image->at(x, y, 3) = height * 255;
        }
      }

      assert(i_array < animation_steps);
      texture->setTextureImage(i_array, *combined_image);
      i_array++;
    }

    texture->generateMipMap();

    texture->setParameter(GL_TEXTURE_MAX_ANISOTROPY_EXT, 8);
    // texture->setParameter(GL_TEXTURE_LOD_BIAS, -2);

    m_animation_normal_maps = texture;

    m_animation = std::make_unique<WaterAnimation>();
    m_animation->m_num_animation_steps = animation_steps;

    m_animation_interpolator =
      std::make_unique<WaterAnimationInterpolator>(animation_texture_size);

    assert(m_animation_normal_maps);
    m_animation_interpolator->m_texture_state.bind("sampler_water_normal_map",
                                                  *m_animation_normal_maps);
  }
  else
  {
    m_texture = createTexture(water_res.getWaterTexture());
    m_noise_texture = createTexture(water_res.getWaterNoiseTexture());
  }

  auto foam_texture_image = water_res.getWaterFoamTexture();
  m_foam_texture = createTexture(foam_texture_image);

  m_water_color = water_res.getWaterColor();

  m_shore_wave_texture = createTexture(createShoreWaveTexture(), false);
}


Water::~Water()
{
}


void Water::reloadShaders(Layer& layer,
                          const ShaderSearchPath &shader_search_path,
                          bool enable_base_layer)
{
  m_shader_load_error = false;

  if (m_animation)
  {
    m_animation_interpolator->m_shader_load_error = false;

    ShaderParameters params;
    params.set("terrain_water_animation_num_layers", NUM_LAYERS);

    try
    {
      auto program = createShaderProgram("compute_water_animation",
                            shader_search_path, params);

      program->setUniformi("water_animation_num_frames", m_animation->m_num_animation_steps);
      m_animation_interpolator->m_texture_state.setSamplerUniforms(*program);
      m_animation_interpolator->m_program = program;
    }
    catch (std::exception& e)
    {
      LOG_ERROR << e.what() << std::endl;
      m_animation_interpolator->m_program.reset();
      m_shader_load_error = true;
    }
  }
}


void Water::bindTextures(TextureState& state) const
{
  assert(m_foam_texture);
  state.bind("sampler_water_foam_texture", *m_foam_texture);

  if (m_animation)
  {
    assert(m_animation_interpolator->m_layers);
    state.bind("sampler_water_normal_map_interpolated",
              *m_animation_interpolator->m_layers);
  }

  assert(m_shore_wave_texture);
  state.bind("sampler_shore_wave", *m_shore_wave_texture);

  if (m_texture)
    state.bind("sampler_water", *m_texture);

  if (m_noise_texture)
    state.bind("sampler_water_noise", *m_noise_texture);
}


void Water::updateAnimation(float frame_delta)
{
  m_shore_wave_pos += frame_delta * m_shore_wave_hz;
  if (m_animation)
    m_animation->update(frame_delta);
}


void Water::setUniforms(ShaderProgramPtr program) const
{
  program->setUniform("water_color", m_water_color);
  program->setUniform("shore_wave_scroll", m_shore_wave_pos);

  if (m_animation)
  {
    for (int i = 0; i < NUM_LAYERS; i++)
    {
      auto prefix = "terrain_water_parameters.animation_layers[" + std::to_string(i) + "].";

      float tile_size_m = getAnimationLayerTileSizeM(i);
      float tile_size_grid = tile_size_m / float(TerrainBase::GRID_RESOLUTION_M);

      program->setUniform(prefix + "tile_size_grid", tile_size_grid);
      program->setUniform(prefix + "tile_size_m", tile_size_m);
    }

    m_animation->setUniforms(program);
  }
}


void Water::setShaderParameters(unsigned int material,
                          unsigned int  detail_options,
                          ShaderParameters& params) const
{
  params.set<bool>("terrain_enable_water_map",
                   material & TerrainBase::MaterialID::WATER_MAP);

  bool enable_water = (material & TerrainBase::MaterialID::WATER);
  if (!enable_water)
    return;

  params.set("terrain_enable_water", enable_water);
  params.set<bool>("terrain_enable_detailed_water", detail_options & MaterialOption::WATER);

  params.set("terrain_water_animation_num_layers", NUM_LAYERS);

  params.set("terrain_water_enable_ice_cover", m_texture != nullptr && !m_animation);
  params.set("terrain_water_enable_texture", m_texture != nullptr);
  params.set("terrain_water_enable_noise_texture", m_noise_texture != nullptr);

  params.setIfNotDisabled("terrain_water_enable_animation",
                          m_animation && (detail_options & MaterialOption::WATER));
}


void Water::addShaders(unsigned int material,
                      int lod_options,
                      std::vector<std::string>& vertex_shaders,
                      std::vector<std::string>& fragment_shaders) const
{
  if (material & TerrainBase::MaterialID::WATER_MAP)
    fragment_shaders.push_back("terrain_water_map");

  if (material & TerrainBase::MaterialID::WATER)
  {
    vertex_shaders.push_back("terrain_water");
    fragment_shaders.push_back("terrain_water");

    if (m_animation && lod_options & MaterialOption::WATER)
      fragment_shaders.push_back("terrain_water_animation");
  }
}


void Water::loadLayer(Layer& layer,
                 TerrainResourcesBase::Layer& resources,
                 bool is_base_layer/*FIXME HACK*/,
                 const ShaderSearchPath&)
{
  auto& water_res = resources.to<TerrainWaterResources::Layer>();

  auto small_water_map = water_res.getSmallWaterMap();
  if (small_water_map)
  {
    auto size = small_water_map->size();

    auto map = std::make_unique<AttributeMapTexture>("small_water_map");
    auto texture = createTexture(small_water_map);

    texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

#if DEBUG_SMALL_WATER_MAP
    texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
#endif

    map->resolution_m = SMALL_WATER_MAP_RESOLUTION_M;
    map->size_m = size * SMALL_WATER_MAP_RESOLUTION_M;
    map->size_px = size;
    map->setTexture(texture);

    layer.addMap(std::move(map));
  }

  auto& map_in = water_res.getWaterMap();

  auto map = std::make_unique<WaterMap>();

  map->chunk_size_m = map_in.chunk_size_m;
  map->y_flip = map_in.y_flip;

  auto chunk_size_px = map_in.chunk_size_px;
  auto chunk_border_px = map_in.chunk_border_px;
  float chunk_scale = (chunk_size_px - (2 * chunk_border_px)) / chunk_size_px;

  map->chunk_sample_offset = (chunk_border_px * (1.0 / chunk_size_px));
  map->chunk_sample_scale = chunk_scale;

  assert(chunk_size_px > 0);
  assert(map_in.chunks_per_row > 0);

  map->chunks_num_rows = map_in.chunks_per_row;
  map->chunks_num_cols = map_in.chunks_per_row;

  map->table_size = map_in.table->size();

  map->chunks_per_block = map_in.chunks_per_row * map_in.chunks_per_row;
  map->chunk_block_count = map_in.chunk_blocks.size();

  auto chunk_block_texture_size = chunk_size_px * map_in.chunks_per_row;

  auto chunks_texture =
    std::make_shared<TextureArray>(map_in.chunk_blocks.size(),
                                   glm::ivec2(chunk_block_texture_size),
                                   1);

  for (int i = 0; i < map_in.chunk_blocks.size(); i++)
  {
    assert(map_in.chunk_blocks.at(i)->w() % chunk_size_px == 0);
    assert(map_in.chunk_blocks.at(i)->h() % chunk_size_px == 0);
    chunks_texture->setTextureImage(i, *map_in.chunk_blocks.at(i));
  }

  chunks_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  chunks_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  chunks_texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  chunks_texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  auto table_float = image::convert<float>(map_in.table);

  auto table_texture = createFloatTexture(
                                reinterpret_cast<const float*>(table_float->data()),
                                table_float->w(),
                                table_float->h(),
                                1,
                                false);

  table_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  table_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  table_texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  table_texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  {
    AttributeMapTextureBase::Texture texture;
    texture.texture = table_texture;
    texture.sampler_name = "sampler_table";
    map->textures.push_back(texture);
  }

  {
    AttributeMapTextureBase::Texture texture;
    texture.texture = chunks_texture;
    texture.sampler_name = "sampler_chunk_blocks";
    map->textures.push_back(texture);
  }

  layer.addMap(std::move(map), false);
}


void Water::compute(std::function<void(ShaderProgramPtr)> set_uniforms)
{
  if (m_animation)
  {
    if (m_animation_interpolator->m_program)
    {
      setUniforms(m_animation_interpolator->m_program);
      m_animation_interpolator->compute(set_uniforms);
      if (m_animation_interpolator->m_shader_load_error)
        m_shader_load_error = true;
    }
  }
}


template <>
std::unique_ptr<ModuleSubClass<TerrainBase::ModuleIndex::WATER>>
createModule<TerrainBase::ModuleIndex::WATER>(TerrainBase::BuildParameters& params)
{
  return std::make_unique<Water>(params);
}


} // namespace render_util::terrain

#pragma once

#include <render_util/image.h>
#include <render_util/geometry.h>

#include <optional>
#include <memory>
#include <glm/glm.hpp>


namespace render_util::terrain
{


template <class T>
struct AttributeMap
{
  using ImageType = Image<T>;

  std::shared_ptr<const ImageType> map;
  glm::vec2 origin_m = glm::vec2(0);
  unsigned int resolution_m = 0;

  template <class Visitor>
  void visitAreaClamped(glm::ivec2 area_origin,
                        glm::ivec2 area_size,
                        const Visitor& visitor)
  {
    using namespace glm;

    auto begin = area_origin;
    auto end = area_origin + area_size;

    for (int y = begin.y; y < end.y; y++)
    {
      for (int x = begin.x; x < end.x; x++)
      {
        glm::ivec2 clamped = glm::clamp(glm::ivec2(x,y),
                                        glm::ivec2(0),
                                        map->size() - glm::ivec2(1));
        visitor(map->get(clamped));
      }
    }
  }

  template <class Visitor>
  void visitArea(glm::ivec2 area_origin,
                        glm::ivec2 area_size,
                        const Visitor& visitor,
                        T default_value)
  {
    using namespace glm;

    auto begin = area_origin;
    auto end = area_origin + area_size;

    for (int y = begin.y; y < end.y; y++)
    {
      for (int x = begin.x; x < end.x; x++)
      {
        if (x < 0 || y < 0 || x >= map->w() || y >= map->h())
        {
          visitor(default_value);
        }
        else
        {
          visitor(map->get(x,y));
        }
      }
    }
  }

  template <class Visitor>
  void visitArea(const Rect& area, const Visitor& visitor,
                 std::optional<T> default_value, int oversample_pixels = 1)
  {
    assert(resolution_m);

    auto area_origin = (area.getOrigin() - origin_m);

//     assert(fract(area_origin / glm::vec2(resolution_m)) == glm::vec2(0));

    auto area_origin_px = glm::ivec2(area_origin / glm::vec2(resolution_m));

//     LOG_INFO << "area.extent: " << area.extent << endl;
//     LOG_INFO << "resolution_m: " << resolution_m << endl;
//     LOG_INFO << "fract(area.extent / resolution_m): "
//              << fract(area.extent / glm::vec2(resolution_m)) << std::endl;

//     assert(fract(area.getExtent() / glm::vec2(resolution_m)) == glm::vec2(0));
    auto area_extent_px = area.getExtent() / glm::vec2(resolution_m);

    // include other areas attributes near the border
    area_origin_px -= glm::ivec2(oversample_pixels);
    area_extent_px += glm::ivec2(2 * oversample_pixels);

    if (default_value)
      visitArea(area_origin_px, area_extent_px, visitor, *default_value);
    else
      visitAreaClamped(area_origin_px, area_extent_px, visitor);
  }
};


} // namespace render_util::terrain

/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TERRAIN_MODULE_H
#define RENDER_UTIL_TERRAIN_MODULE_H

#include "layer.h"
#include "buffer_updater.h"
#include "render_list_base.h"
#include <render_util/terrain_resources.h>
#include <util/object.h>

#include <vector>

namespace render_util::terrain
{


using ModuleIndex = TerrainBase::ModuleIndex;


class Module : public util::Object
{
  std::string m_name;
  TerrainBase::ModuleIndex::Enum m_id = TerrainBase::ModuleIndex::NONE;

protected:
  bool m_shader_load_error = false;

public:
  bool m_shader_parameters_changed = false;

  Module(TerrainBase::ModuleIndex::Enum id) : m_id(id) {}
  virtual ~Module() {}

  const TerrainBase::ModuleIndex::Enum getIndex() { return m_id; }
  const std::string &getName() { return m_name; }
  bool hasShaderLoadError() { return m_shader_load_error; }

  int getMask() { return TerrainBase::getModuleMask(getIndex()); }
};


template <TerrainBase::ModuleIndex::Enum ID>
struct ModuleSubClass : public Module
{
  ModuleSubClass() : Module(ID) {}
};


struct IRenderModule
{
  virtual void setDrawDistance(float dist) {}

  virtual void drawDebug(const Camera&) const {}

  virtual void destroyRenderLists() = 0;
  virtual void initializeRenderList(int render_group, const CreateProgramParameters&) = 0;

  virtual void clearRenderLists() = 0;
  virtual void prepareRenderGroup(int id, RenderGroupDispatcher&) = 0;

  virtual void setShaderParameters(ShaderParameters&) const {}
  virtual void setUniforms(ShaderProgram& program) const {}

  virtual void setBufferUpdateMethod(BufferUpdateMethod) {}
  virtual void setBufferUsage(unsigned) {}

  virtual void getStats(Stats&, const std::vector<std::string>& material_names) const {}
};


struct IMaterialModule
{
  virtual void bindTextures(TextureState&) const {}
  virtual void updateAnimation(float frame_delta) {}
  virtual void loadLayer(Layer&,
                         TerrainResourcesBase::Layer&,
                         bool is_base_layer/*FIXME HACK*/,
                         const ShaderSearchPath&) {}
  virtual void onLoadLayerFinished(Layer& layer) {}
  virtual void setAnisotropy(int) {};
  virtual void setBaseLayerTypeMapping(const std::vector<int>&) {}
  virtual bool setAltitudinalZones(const std::vector<AltitudinalZone>&)
  {
    return false;
  }

  virtual void compute(std::function<void(ShaderProgramPtr)> set_uniforms) {}
  virtual void compute(Layer&, std::function<void(ShaderProgramPtr)> set_uniforms) {}

  virtual void reloadShaders(Layer& layer,
                             const ShaderSearchPath &shader_search_path,
                             bool enable_base_layer) {}

  virtual void addShaders(unsigned int material,
                          int lod_options,
                          std::vector<std::string>& vertex_shaders,
                          std::vector<std::string>& fragment_shaders) const {}

  virtual void setSamplerUniforms(const TextureState&) const {}
  virtual void setSamplerUniforms(Layer& layer, const TextureState&) const {}

  virtual void setShaderParameters(unsigned int material,
                                   unsigned int  detail_options,
                                   ShaderParameters&) const {}

  virtual void setUniforms(ShaderProgramPtr program) const {}

  virtual bool isDebugModule() const { return false; }
};


template <TerrainBase::ModuleIndex::Enum ID>
std::unique_ptr<ModuleSubClass<ID>> createModule(TerrainBase::BuildParameters&);


std::string getModuleNames(unsigned modules);


} // namespace render_util::terrain

#endif

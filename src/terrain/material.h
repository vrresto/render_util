#pragma once

#include "attribute_map.h"
#include <render_util/shader.h>
#include <render_util/terrain_resources.h>
#include <render_util/terrain_base.h>

#include <functional>


namespace render_util::terrain
{


using MaterialID = TerrainBase::MaterialID;


constexpr unsigned DEFAULT_MATERIAL = TerrainBase::MaterialID::WATER;


struct IMaterialModule;
struct IRenderModule;
class RenderGroupParameters;


class MaterialMap
{
  using LayerMap = AttributeMap<unsigned>;

  std::unique_ptr<LayerMap> createLayerMap(TerrainResourcesBase::Layer&);

  std::unique_ptr<LayerMap> m_map;
  std::unique_ptr<LayerMap> m_base_map;

public:
  MaterialMap(TerrainBase::BuildParameters &params);
  ~MaterialMap();
  unsigned int getMaterialID(const Rect &area) const;
  bool isUniform(const Rect &area, int mask) const;
};


struct MaterialOption
{
  using Type = unsigned int;

  constexpr static Type LAND = 1;
  constexpr static Type WATER = 1 << 1;
  constexpr static Type FOREST = 1 << 2;
  constexpr static Type COAST = 1 << 3;
  constexpr static Type ALL = LAND | WATER | FOREST | COAST;
};


struct MaterialLodParameters
{
  double distance = 0;
  unsigned int options = 0;
};


class MaterialTable
{
  std::map<int, int> m_indices;
  std::vector<int> m_materials;

public:
  int get(int index) const
  {
    return m_materials.at(index);
  }

  int getIndex(int material)
  {
    assert(m_indices.size() == m_materials.size());

    auto it = m_indices.find(material);
    if (it != m_indices.end())
    {
      assert(!m_indices.empty());
      return it->second;
    }
    else
    {
      m_materials.push_back(material);
      auto& index = m_indices[material];
      index = m_materials.size() - 1;
      assert(!m_indices.empty());
      return index;
    }
  }

  bool isEmpty() const
  {
    assert(m_indices.size() == m_materials.size());
    return m_materials.empty();
  }

  int getSize() const
  {
    assert(m_indices.size() == m_materials.size());
    return m_materials.size();
  }
};


struct CreateProgramParameters
{
  const std::vector<IMaterialModule*>& modules;
  const render_util::ShaderSearchPath& shader_search_path;
  const RenderGroupParameters& group_params;
  const MaterialTable& material_table;
  IRenderModule& renderer;
  const TextureState& texture_state;
  const ShaderParameters& params;
  int module_mask = 0;
  bool far_noise_texture_is_normal_map = false;
  bool is_base_layer_active = false;
};


render_util::ShaderProgramPtr createProgram(const CreateProgramParameters&,
                                             int material_index,
                                             int material_lod);


ShaderProgramPtr createProgram(std::string name,
    const render_util::ShaderSearchPath &shader_search_path,
    const render_util::ShaderParameters &params_,
    bool enable_base_map,
    bool enable_atmoshphere,
    const std::vector<std::string>& vertex_shaders,
    const std::vector<std::string>& fragment_shaders);


std::string getMaterialName(unsigned id);


inline int getMaterialLod(float dist, bool low_detail,
                          const std::vector<MaterialLodParameters>& lods)
{
  int lod = 0;
  if (!low_detail)
  {
    for (int i = 0; i < lods.size(); i++)
    {
      if (dist <= lods[i].distance)
        lod = i;
    }
  }
  return lod;
}


} // namespace render_util::terrain

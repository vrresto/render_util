#include "buffer_updater.h"
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace render_util::terrain
{


BufferUpdater::BufferUpdater(unsigned buffer_id, size_t element_size,
                             BufferSize elements, unsigned usage) :
  m_id(buffer_id), m_element_size(element_size), m_elements(elements),
  m_usage(usage)
{
  // allocate & orphan
  gl::NamedBufferData(m_id, m_element_size * m_elements, nullptr, m_usage);
}


void CopyingBufferUpdaterBase::writeImpl(const void* src, BufferSize count)
{
  assert((m_write_pos + count) <= m_elements);
  gl::NamedBufferSubData(m_id,
                         m_element_size * m_write_pos,
                         m_element_size * count,
                         src);
  m_write_pos += count;
}


void* MappingBufferUpdaterBase::map()
{
  return gl::MapNamedBuffer(m_id, GL_WRITE_ONLY);
}


void MappingBufferUpdaterBase::unmap()
{
  gl::UnmapNamedBuffer(m_id);
}


} // namespace render_util::terrain

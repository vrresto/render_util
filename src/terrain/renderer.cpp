/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This terrain implementation makes use of the technique described in the paper
 * "Continuous Distance-Dependent Level of Detail for Rendering Heightmaps (CDLOD)"
 * by Filip Strugar <http://www.vertexasylum.com/downloads/cdlod/cdlod_latest.pdf>.
 */

#include "mesh.h"
#include "renderer.h"
#include "geometry.h"
#include "instance_buffer.h"
#include "buffer_updater.h"
#include "render_list.h"
#include "module.h"
#include "grid_mesh.h"
#include "stats.h"
#include <render_util/vao.h>
#include <render_util/geometry.h>
#include <render_util/texture_state.h>
#include <render_util/state.h>
#include <render_util/stats.h>
#include <block_allocator.h>
#include <render_util/gl_binding/gl_functions.h>

#include "terrain_config.h"

#if TERRAIN_ENABLE_VIEWER_PARAMETERS
  #include <render_util/viewer/value_wrapper.h>
  #include <render_util/viewer/parameters_base.h>
  #include <render_util/viewer/multiple_choice_parameter.h>
  #include <render_util/viewer/boolean_parameter.h>
  #include <render_util/viewer/simple_parameter.h>
  #include <render_util/viewer/parameterizable.h>
#endif


using namespace render_util;
using namespace render_util::terrain;
using namespace render_util::gl_binding;

using namespace glm;


#include <render_util/draw_box.h>


namespace
{


constexpr int METERS_PER_GRID = render_util::TerrainBase::GRID_RESOLUTION_M; //FIXME remove
constexpr unsigned int MESH_GRID_SIZE = NODE_GRID_SIZE; // FIXME remove
constexpr auto MAX_LOD = 9;
constexpr auto LEAF_NODE_SIZE = MESH_GRID_SIZE * METERS_PER_GRID;
constexpr auto LOD_COUNT = MAX_LOD + 1;


constexpr int MESH_FULL = 0;
constexpr int MESH_PARTIAL = 1;
constexpr int MESH_COUNT = 5;


struct NodeInstanceData
{
  vec2 grid;
  float lod;
  float unused;
};


using Node = QuadTreeNode;


using NodeAllocator = util::BlockAllocator<Node, 50000>;
using NodeRenderList = MultiGroupMultiMaterialInstanceList<NodeInstanceData, MESH_COUNT,
  TerrainBase::ModuleIndex::RENDERER>;


class TerrainRenderer : public ModuleSubClass<TerrainBase::ModuleIndex::RENDERER>,
                        public IRenderModule,
                        public IQuadTree
{
protected:
  // the minimum LOD distance scale necessary to avoid popping artifacts
  static constexpr float MIN_LOD_DISTANCE_SCALE = 2.4;

  float m_lod_distance_scale = MIN_LOD_DISTANCE_SCALE;

  unsigned m_detail_material_mask = 0;

  NodeAllocator m_node_allocator;
  NodeRenderList m_render_list;
  Node* m_root_node {};
  int m_node_count = 0;

  std::unique_ptr<MultiMeshVAO> m_vao;

  double m_draw_distance = 0;
  int m_max_node_subdivision = 0;

  int m_processed_nodes = 0;
  int m_processed_detail_nodes = 0;

  std::vector<Node*> m_selected_nodes;
  std::vector<const Node*> m_selected_detail_nodes;

  bool m_draw_node_border = false;

  void selectLod(Node* node, int lod, float dist, const vec3& camera_pos);

  void selectNode(Node *node,
                  int lod,
                  int subdivision_level,
                  float dist,
                  bool enable_detail);

  void selectNode(Node *node,
                     bool low_detail,
                     const render_util::Camera &camera, float dist);

  bool cullNode(Node *node, float dist, const Camera& camera);
  void processNode(Node *node, int lod, const Camera& camera,
                   bool low_detail, float dist);

  void processNodeSubArea(Node* node,
                          Node* child,
                          int child_lod,
                          int mesh,
                          float dist,
                          bool low_detail,
                          bool enable_detail);

  void initialize(const TerrainBase::BuildParameters&,
                  const MaterialMap&, MaterialTable&, unsigned) override;

  void setUniforms(ShaderProgram& program) const override;
  void update(const Camera &camera, bool low_detail) override;

  void getStats(Stats&, const std::vector<std::string>& material_names) const override;

  Node* createNode(const TerrainBase::BuildParameters&, ivec2 pos_grid, int lod,
                   const MaterialMap&, MaterialTable&, const HeightMap&);

  void setLodDistanceScale(float scale) override
  {
    m_lod_distance_scale = scale;
  }

  float getLodDistanceScale() override
  {
    return m_lod_distance_scale;
  }

  float getLodDist(int lod) const
  {
    return m_lod_distance_scale * LEAF_NODE_SIZE * pow(2, lod);
  }

  bool isNodeSubDivisionEnabled()
  {
    return m_max_node_subdivision > 0;
  }

public:
  TerrainRenderer();
  ~TerrainRenderer();

  void setShaderParameters(ShaderParameters& params) const override
  {
    params.set("terrain_lod_count", LOD_COUNT);
    params.set("terrain_draw_node_border", m_draw_node_border);
    params.set("terrain_enable_vertex_color", m_draw_node_border);
  }

  void initializeRenderList(int render_group, const CreateProgramParameters&) override;


  void setDrawDistance(float dist) override
  {
    m_draw_distance = dist;
  }

  void drawDebug(const Camera&) const override;

  void clearRenderLists() override;
  void prepareRenderGroup(int, RenderGroupDispatcher&) override;

  void destroyRenderLists() override;

  const std::vector<const QuadTreeNode*>& getSelectedDetailNodes() override
  {
    return m_selected_detail_nodes;
  }
};


TerrainRenderer::TerrainRenderer()
{
  static_assert(NODE_GRID_SIZE % 2 == 0);

  std::vector<int> sizes;
  for (int size = NODE_GRID_SIZE; size >= 2; size /= 2)
    sizes.push_back(size);

  assert(sizes.size() == MESH_COUNT);

  m_vao = createMultiGridMeshVAO(sizes);
}


TerrainRenderer::~TerrainRenderer()
{
  m_root_node = nullptr;
}


void TerrainRenderer::initialize(const TerrainBase::BuildParameters& params,
                                 const MaterialMap& material_map,
                                 MaterialTable& material_table,
                                 unsigned detail_material_mask)
{
  m_detail_material_mask = detail_material_mask;

  HeightMap hm(params);
  assert(!m_root_node);

  // static_assert(getNodeSizeGrid(MAX_LOD) % 2 == 0);
  assert(fract(getNodeSizeTerrainGrid(MAX_LOD) / 2.f) == 0);

  m_root_node = createNode(params, -ivec2(getNodeSizeTerrainGrid(MAX_LOD) / 2), MAX_LOD,
                           material_map, material_table, hm);
}


void TerrainRenderer::destroyRenderLists()
{
  clearRenderLists();
  m_render_list.deinitialize();
}


void TerrainRenderer::initializeRenderList(int group,
                                           const CreateProgramParameters& create_params)
{
  assert(m_render_list.isEmpty());
  m_render_list.initialize(group, create_params, *m_vao);
}


Node* TerrainRenderer::createNode(const TerrainBase::BuildParameters &params,
                                  ivec2 pos_grid,
                                  int lod,
                                  const MaterialMap &material_map,
                                  MaterialTable& material_table,
                                  const HeightMap& height_map)
{
  m_node_count++;

  auto node_size_grid = getNodeSizeTerrainGrid(lod);
  auto node_size = node_size_grid * TerrainBase::GRID_RESOLUTION_M;
  auto pos = pos_grid * TerrainBase::GRID_RESOLUTION_M;

  assert(node_size_grid >= 1);

  //FIXME
  // assert(node_size_grid >= 2);
  // assert(node_size_grid % 2 == 0);

  auto child_node_size_grid = node_size_grid / 2;
  auto child_node_size = child_node_size_grid * TerrainBase::GRID_RESOLUTION_M;
  auto child_lod = lod-1;

  Node *node = m_node_allocator.alloc();
  node->pos_grid = pos_grid;

  //FIXME
  int uniformity_test_material =
    TerrainBase::MaterialID::WATER_MAP | TerrainBase::MaterialID::WATER;
  // int uniformity_test_material = TerrainBase::MaterialID::ALL;

  if (lod <= 0)
  {
    auto area = Rect(vec2(pos), vec2(node_size));
    node->max_height = height_map.getMaxHeight(area);

    assert(params.use_material_map);

    node->material_id = material_map.getMaterialID(area);
    node->material_is_uniform = material_map.isUniform(area, uniformity_test_material);

    if (child_node_size_grid >= 1)
    {
      bool subdivide = false;

      // always subdivide nodes containing water when there is height variation
      if (node->material_id & TerrainBase::MaterialID::WATER)
      {
        auto min_height = height_map.getMinHeight(area);
        auto max_height = height_map.getMaxHeight(area);
        assert(min_height <= max_height);

        subdivide = subdivide || (min_height != max_height);

        for (int i = 0; i < 4; i++)
        {
          auto child_pos = pos_grid + getChildCoords(i) * child_node_size_grid;
          auto child_area = Rect(vec2(child_pos), vec2(child_node_size));

          auto child_min_height = height_map.getMinHeight(child_area);
          auto child_max_height = height_map.getMaxHeight(child_area);

          subdivide = subdivide || (child_min_height != child_max_height);
          subdivide = subdivide || (max_height != child_max_height);
        }
      }

      subdivide = subdivide || !node->material_is_uniform;

      if (subdivide)
      {
        for (int i = 0; i < 4; i++)
        {
          node->children.at(i) =
            createNode(params, pos_grid + getChildCoords(i) * child_node_size_grid,
                      child_lod, material_map, material_table, height_map);
        }
      }
    }
  }
  else
  {
    for (int i = 0; i < 4; i++)
    {
      node->children.at(i) =
        createNode(params, pos_grid + getChildCoords(i) * child_node_size_grid,
                   child_lod, material_map, material_table, height_map);
    }

    for (Node *child : node->children)
    {
      node->max_height = max(node->max_height, child->max_height);
      node->material_id |= child->material_id;
    }

    for (Node *child : node->children)
    {
      if ((node->material_id != child->material_id) || !child->material_is_uniform)
        node->material_is_uniform = false;
    }
  }

  auto bb_origin = vec3(pos, 0);
  auto bb_extent = vec3(node_size, node_size, node->max_height);
  node->bounding_box.set(bb_origin, bb_extent);

  assert(node->material_id);
  node->material_index = material_table.getIndex(node->material_id);
  node->material_index_detail = m_detail_material_mask ?
    material_table.getIndex(node->material_id & m_detail_material_mask) :
    -1;

  node->lod = lod;

  return node;
}


void TerrainRenderer::selectNode(Node *node,
                                 bool low_detail,
                                 const render_util::Camera &camera,
                                 float dist)
{
  selectNode(node, node->lod, 0, dist, false);
}



void TerrainRenderer::selectNode(Node* node, int lod,
                                 int subdivision_level,
                                 float dist,
                                 bool enable_detail)
{
  auto material_id = node->material_id;
  auto material_index = node->material_index;

  if (enable_detail)
  {
    material_id &= m_detail_material_mask;
    material_index = node->material_index_detail;
  }

  assert(material_index >= 0);

  NodeInstanceData instance_data {};
  instance_data.grid = node->pos_grid;
  instance_data.lod = lod;

  m_render_list.addInstance(material_id,
                            material_index,
                            dist,
                            false,
                            instance_data,
                            subdivision_level);

  assert(lod >= 0);
  assert((lod == 0) || !enable_detail);

  if (enable_detail)
    m_selected_detail_nodes.emplace_back(node);

  // m_selected_nodes.push_back(node->children[child]);
}


bool TerrainRenderer::cullNode(Node* node, float dist, const Camera& camera)
{
  // FIXME this doesn't take earth curvature into account

  if (m_draw_distance && dist > m_draw_distance)
    return true;

  if (camera.cull(node->bounding_box))
    return true;

  return false;
}


void TerrainRenderer::selectLod(Node* node, int lod, float dist, const vec3& camera_pos)
{
  auto subdivision_level = lod - node->lod;

  assert(lod >= 0);

  assert(subdivision_level >= 0);
  assert(subdivision_level < m_vao->getMeshCount());
  assert(m_max_node_subdivision < m_vao->getMeshCount());

  auto child_lod = lod - 1;
  float child_lod_dist = getLodDist(child_lod);

  if ((subdivision_level >= m_max_node_subdivision) ||
      node->material_is_uniform || !isNodeSubDivisionEnabled())
  {
    bool enable_detail = false;

    if (m_enable_detail && (lod == 0) && (dist <= child_lod_dist) && (node->material_id & ~m_detail_material_mask))
    {
      enable_detail = true;
    }

    selectNode(node, lod, subdivision_level, dist, enable_detail);
  }
  else
  {
    assert(subdivision_level < m_max_node_subdivision);

    for (int i = 0; i < 4; i++)
    {
      assert(node->children[i]);

      float child_dist =
        node->children[i]->bounding_box.getShortestDistance(camera_pos);

      //FIXME culling

      selectLod(node->children[i], lod, child_dist, camera_pos);
    }
  }
}


void TerrainRenderer::processNode(Node* node,
                               int lod, // FIXME this doesn't need to be passed
                               const Camera& camera,
                               bool low_detail,
                               float dist)
{
  m_processed_nodes++;

//   assert(lod == MAX_LOD || !cullNode(node, pass, camera));

  auto& camera_pos = camera.getPos();

  auto child_lod = lod - 1;
  float child_lod_dist = getLodDist(child_lod);

  assert(lod >= 0);

  if (lod == 0)
  {
    selectLod(node, lod, dist, camera_pos);
  }
  else
  {
    if (dist <= child_lod_dist)
    {
      for (int i = 0; i < 4; i++)
      {
        float child_dist =
          node->children[i]->bounding_box.getShortestDistance(camera_pos);

        if (cullNode(node->children[i], child_dist, camera))
          continue;

        if (child_dist <= child_lod_dist)
        {
          processNode(node->children[i], child_lod, camera, low_detail, child_dist);
        }
        else
        {
          selectLod(node->children[i], lod, child_dist, camera_pos);
        }
      }
    }
    else
    {
      selectLod(node, lod, dist, camera_pos);
    }
  }
}


void TerrainRenderer::update(const Camera &camera, bool low_detail)
{
  assert(m_render_list.isEmpty());

  clearRenderLists();

  assert(m_root_node);

  if (camera.is2D())
  {
    selectNode(m_root_node, true, camera, 0);
  }
  else
  {
    processNode(m_root_node, MAX_LOD, camera, low_detail,
                m_root_node->bounding_box.getShortestDistance(camera.getPos()));
  }
}


void TerrainRenderer::clearRenderLists()
{
  m_selected_nodes.clear();
  m_selected_detail_nodes.clear();
  m_render_list.clear();
  m_processed_nodes = 0;
  m_processed_detail_nodes = 0;
}


void TerrainRenderer::prepareRenderGroup(int id,
                                         RenderGroupDispatcher& dispatcher)
{
  auto group = m_render_list.getGroup(id);
  if (group && group->getInstanceCount())
    group->prepare(dispatcher);
}


void TerrainRenderer::setUniforms(ShaderProgram& program) const
{
  for (int i = 0; i < LOD_COUNT; i++)
  {
    program.setUniform<float>("lod_distances[" + std::to_string(i) + "]", getLodDist(i));
    program.setUniform<float>("node_scale_for_lod[" + std::to_string(i) + "]", getNodeScale(i));
  }

  program.setUniform("cdlod_min_dist", getLodDist(0));
}


void TerrainRenderer::drawDebug(const Camera& camera) const
{
  // for (auto node : m_selected_nodes)
  // {
  //   drawBox(node->bounding_box);
  // }
}


void TerrainRenderer::getStats(Stats& stats,
                               const std::vector<std::string>& material_names) const
{
  std::array<int, MESH_COUNT> triangles_per_mesh;
  triangles_per_mesh.at(MESH_FULL) = NODE_GRID_SIZE * NODE_GRID_SIZE * 2;
  triangles_per_mesh.at(MESH_PARTIAL) = (NODE_GRID_SIZE/2) * (NODE_GRID_SIZE/2) * 2;

  terrain::getStats(m_render_list, material_names, triangles_per_mesh, stats);

  stats.add("processed nodes", m_processed_nodes);
  stats.add("processed detail nodes", m_processed_detail_nodes);

  stats.add("total nodes", m_node_count);
  stats.add("allocated node blocks", m_node_allocator.getBlockCount());
  stats.add("node memory MB",
            util::limitDecimalPlaces(sizeof(Node) * m_node_count / 1024.f / 1024.f, 2));
}


#if TERRAIN_ENABLE_VIEWER_PARAMETERS
class ParameterizableTerrainRenderer : public TerrainRenderer,
                                       public viewer::IParameterizable
{
  void addParameters(viewer::ParametersBase& params) override
  {
    {
      viewer::ValueWrapper<int> wrapper =
      {
        .get = [this] () { return m_max_node_subdivision; },
        .set = [this] (auto value)
        {
          m_max_node_subdivision =
            clamp(value, 0, m_vao->getMeshCount()-1);
        },
      };
      using Param = viewer::SimpleParameter<int, viewer::ValueWrapper<int>>;
      params.add(std::make_unique<Param>("Max node subdivision",
                                         m_max_node_subdivision, wrapper));
    }

    {
      viewer::ValueWrapper<bool> wrapper =
      {
        .get = [this] () { return m_draw_node_border; },
        .set = [this] (bool value)
        {
          if (value != m_draw_node_border)
            m_shader_parameters_changed = true;
          m_draw_node_border = value;
        },
      };
      using Param = viewer::BooleanParameter<viewer::ValueWrapper<bool>>;
      params.add(std::make_unique<Param>("Draw node border", wrapper,
                                         m_draw_node_border));
    }
  }
};
#endif


} // namepsace


namespace render_util::terrain
{


template <>
std::unique_ptr<ModuleSubClass<TerrainBase::ModuleIndex::RENDERER>>
createModule<TerrainBase::ModuleIndex::RENDERER>(TerrainBase::BuildParameters&)
{
#if TERRAIN_ENABLE_VIEWER_PARAMETERS
  return std::make_unique<ParameterizableTerrainRenderer>();
#else
  return std::make_unique<TerrainRenderer>();
#endif
}


} // namespace render_util::terrain

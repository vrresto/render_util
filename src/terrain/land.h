/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _RENDER_UTIL_TERRAIN_LAND_H
#define _RENDER_UTIL_TERRAIN_LAND_H

#include "module.h"
#include "layer.h"
#include "material.h"

#include <render_util/texunits.h>
#include <render_util/shader.h>
#include <render_util/image.h>
#include <render_util/terrain_base.h>

#include <glm/glm.hpp>

namespace render_util::terrain
{


class Land : public ModuleSubClass<TerrainBase::ModuleIndex::LAND>,
             public IMaterialModule
{
  ShaderParameters m_shader_params;
  bool m_enable_normal_maps = false;
  std::vector<AltitudinalZone> m_altitudinal_zones;
  bool m_has_type_map = false;

  TexturePtr m_base_layer_types_texture;
  ImageRGBA::Ptr m_base_layer_types;

  std::map<unsigned, glm::uvec3> m_mapping;


// //   glm::ivec2 m_type_map_size = glm::ivec2(0);
//   TexturePtr m_type_map_texture;
//   TexturePtr m_type_map_texture_nm;
// //   glm::ivec2 m_base_type_map_size = glm::ivec2(0);
//   TexturePtr m_base_type_map_texture;
//   TexturePtr m_base_type_map_texture_nm;
  

  std::vector<TexturePtr> m_texture_arrays;
  std::vector<TexturePtr> m_texture_nm_arrays;
  std::vector<float> m_scale_levels;

  void createLandTextures(TerrainBase::BuildParameters&, TerrainLandResources&);
  glm::uvec3 getType(int index) const;
  void updateBaseLayerTypes();

public:
  static constexpr float MAX_TEXTURE_SCALE = 8;
  static constexpr int TYPE_MAP_RESOLUTION_M = TerrainBase::GRID_RESOLUTION_M;

  Land(TerrainBase::BuildParameters&);

  void setShaderParameters(unsigned int material,
                           unsigned int  detail_options,
                           ShaderParameters&) const;

  void bindTextures(TextureState&) const override;

  void onLoadLayerFinished(Layer&) override;

  void setUniforms(ShaderProgramPtr program) const override;

  void compute(Layer&, std::function<void(ShaderProgramPtr)> set_uniforms) override;

  void loadLayer(Layer&, TerrainResourcesBase::Layer&,
                 bool is_base_layer/*FIXME HACK*/,
                 const ShaderSearchPath&) override;

  void setAnisotropy(int) override;
  bool setAltitudinalZones(const std::vector<AltitudinalZone>&) override;
  void setBaseLayerTypeMapping(const std::vector<int>&) override;
  void reloadShaders(Layer& layer,
                     const ShaderSearchPath &shader_search_path,
                     bool enable_base_layer) override;
  void addShaders(unsigned int material,
                  int lod_options,
                  std::vector<std::string>& vertex_shaders,
                  std::vector<std::string>& fragment_shaders) const override;
};


} // namespace render_util::terrain

#endif

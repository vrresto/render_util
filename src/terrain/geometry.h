#pragma once

#include "layer.h"
#include "attribute_map.h"
#include <render_util/terrain_base.h>

namespace render_util::terrain
{


struct HeightMap
{
  using LayerMap = AttributeMap<float>;

  std::unique_ptr<LayerMap> m_map;
  std::unique_ptr<LayerMap> m_base_map;

  std::unique_ptr<LayerMap> createLayerMap(TerrainResourcesBase::Layer& layer);

public:
  HeightMap(const TerrainBase::BuildParameters& params);

  float getMaxHeight(const Rect& area) const;
  float getMinHeight(const Rect& area) const;

  template <class T>
  void visitArea(const Rect& area, const T& visitor) const
  {
    m_map->visitArea(area, visitor, 0, 0);
    if (m_base_map)
      m_base_map->visitArea(area, visitor, 0, 0);
  }
};


void loadGeometry(Layer&, int layer_number, TerrainBase::BuildParameters&);


} // namespace render_util::terrain

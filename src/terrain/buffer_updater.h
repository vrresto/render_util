#pragma once

#include <util/non_copyable.h>

#include <algorithm>
#include <limits>
#include <cstddef>
#include <cassert>


namespace render_util::terrain
{


enum class BufferUpdateMethod
{
  COPY, MAP
};


using BufferIndex = unsigned;
using BufferSize = unsigned;
constexpr BufferIndex INVALID_BUFFER_INDEX = std::numeric_limits<BufferIndex>::max();


class BufferUpdater : public util::NonCopyable
{
protected:
  const unsigned m_id {};
  const size_t m_element_size {};
  const BufferSize m_elements = {};
  BufferIndex m_write_pos = 0;
  unsigned m_usage {};

  BufferUpdater(unsigned buffer_id, size_t element_size, BufferSize elements,
                unsigned usage);

public:
  BufferIndex getPos() { return m_write_pos; }
};


class CopyingBufferUpdaterBase : public BufferUpdater
{
protected:
  void writeImpl(const void* src, BufferSize count);

public:
  CopyingBufferUpdaterBase(unsigned buffer_id, size_t element_size, BufferSize elements,
                           unsigned usage) :
    BufferUpdater(buffer_id, element_size, elements, usage) {}
};


template <class Element>
class CopyingBufferUpdater : public CopyingBufferUpdaterBase
{
public:
  CopyingBufferUpdater(unsigned buffer_id, BufferSize elements, unsigned usage) :
    CopyingBufferUpdaterBase(buffer_id, sizeof(Element), elements, usage) {}

  void write(const Element* src, BufferSize count)
  {
    writeImpl(src, count);
  }
};


class MappingBufferUpdaterBase : public BufferUpdater
{
protected:
  MappingBufferUpdaterBase(unsigned buffer_id, size_t element_size, BufferSize elements,
                           unsigned usage) :
    BufferUpdater(buffer_id, element_size, elements, usage) {}

  void* map();
  void unmap();
};


template <class Element>
class MappingBufferUpdater : public MappingBufferUpdaterBase
{
  Element* m_mapped_buffer {};

public:
  MappingBufferUpdater(unsigned buffer_id, BufferSize elements, unsigned usage) :
    MappingBufferUpdaterBase(buffer_id, sizeof(Element), elements, usage)
  {
    m_mapped_buffer = static_cast<Element*>(map());
    assert(m_mapped_buffer);
  }

  ~MappingBufferUpdater()
  {
    m_mapped_buffer = nullptr;
    unmap();
  }

  void write(const Element* src, BufferSize count)
  {
    assert((m_write_pos + count) <= m_elements);
    std::copy_n(src, count, &m_mapped_buffer[m_write_pos]);
    m_write_pos += count;
  }
};


} // namespace render_util::terrain

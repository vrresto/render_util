#pragma once

#include "render_list_base.h"
#include "instance_buffer.h"
#include "render_passes.h"
#include "terrain_config.h"
#include <render_util/state.h>


namespace render_util::terrain
{


template <class InstanceData, int MESH_COUNT>
class MultiMaterialInstanceList
{
public:
  using InstanceList = std::vector<InstanceData>;

  struct Item
  {
    bool is_active = false;
    render_util::ShaderProgramPtr program;
    std::array<InstanceList, MESH_COUNT> batches;
#if TERRAIN_EXTENDED_STATS
    int material_index {};
    int material_lod {};
    // int modules {};
#endif
  };

private:
  MultiMeshVAO* m_vao {};
  const RenderGroupParameters& m_params;
  std::vector<Item*> m_render_list;
  std::vector<Item> m_items;
  int m_instances = 0;

  Item& getItem(int material_index, int lod)
  {
    auto index = material_index * getMaterialLods().size() + lod;
    assert(index < m_items.size());
    return m_items.at(index);
  }

  Item& getOrAddItem(int material_index, int lod)
  {
    auto& item = getItem(material_index, lod);

    if (!item.is_active)
    {
      m_render_list.push_back(&item);
      item.is_active = true;
    }

    return item;
  }

public:
  MultiMaterialInstanceList(const RenderGroupParameters& params) : m_params(params) {}

  int isActiveForMaterials() { return m_params.m_active_for_materials; }
  double getDrawDistance() const { return m_params.m_draw_distance; }
  int getInstanceCount() const { return m_instances; }
  bool isEmpty() const { return m_render_list.empty(); }

  const std::vector<MaterialLodParameters>& getMaterialLods() const
  {
    return m_params.m_lod_params;
  }

  void addInstance(int material_index, float dist, const InstanceData& instance,
                   int vao_index, bool low_detail)
  {
    auto lod = getMaterialLod(dist, low_detail, getMaterialLods());
    auto& batch = getOrAddItem(material_index, lod).batches.at(vao_index);
    batch.push_back(instance);
    m_instances++;
  }

  void clear()
  {
    m_instances = 0;
    for (auto& item : m_render_list)
    {
      for (auto& batch : item->batches)
        batch.clear();
      item->is_active = false;
    }
    m_render_list.clear();
  }

  void initialize(const CreateProgramParameters& create_params,
                  MultiMeshVAO& vao,
                  ShaderErrors& shader_errors)
  {
    m_items.clear();
    m_vao = {};

    assert(vao.getMeshCount() == MESH_COUNT);

    m_vao = &vao;

    auto& group_params = create_params.group_params;

    auto material_indices_count = create_params.material_table.getSize();

    assert(material_indices_count);
    assert(!getMaterialLods().empty());
    m_items.resize(material_indices_count * getMaterialLods().size());
    assert(!m_items.empty());

    for (int i_mat = 0; i_mat < material_indices_count; i_mat++)
    {
      for (int lod = 0; lod < getMaterialLods().size(); lod++)
      {
        auto& item = getItem(i_mat, lod);

        try
        {
          item.program = createProgram(create_params, i_mat, lod);
        }
        catch (ShaderCreationError& e)
        {
          LOG_ERROR << e.what() << std::endl;
          shader_errors.push_back(e);
        }

#if TERRAIN_EXTENDED_STATS
        item.material_index = i_mat;
        item.material_lod = lod;
#endif
        item.batches = {};
      }
    }
  }

  template <class T>
  void visitItems(const T& visitor) const
  {
    for (auto& item : m_render_list)
    {
      visitor(std::as_const(*item));
    }
  }

  void prepare(RenderGroupDispatcher& dispatcher)
  {
    assert(!isEmpty());

    auto handler = [&] (auto&& cmds, auto&& buffer)
    {
      for (auto item : m_render_list)
      {
        if (!item->program)
          continue;

        assert(item->program->isValid());
        cmds.addProgramCommand(item->program);

        assert(m_vao);
        cmds.addVaoCommand(m_vao);

        for (int i = 0; i < MESH_COUNT; i++)
        {
          auto& batch = item->batches.at(i);

          if (batch.empty())
            continue;

          addInstances(batch, *m_vao, i, buffer, cmds);
        }
      }
    };

    dispatcher.prepare<InstanceData>(getInstanceCount(), handler);
  }
};


template <class InstanceData, int MESH_COUNT_T,
          TerrainBase::ModuleIndex::Enum RENDER_MODULE>
class MultiGroupMultiMaterialInstanceList
{
  using Group = MultiMaterialInstanceList<InstanceData, MESH_COUNT_T>;

  std::vector<std::unique_ptr<Group>> m_groups;
  int m_renderer_module = 0;

public:
  static constexpr auto MESH_COUNT = MESH_COUNT_T;

  ~MultiGroupMultiMaterialInstanceList()
  {
    clear();
  }

  auto getActiveGroups() const
  {
    std::vector<const Group*> groups;
    for (auto& group : m_groups)
    {
      if (group)
        groups.push_back(group.get());
    }
    return groups;
  }

  void clear()
  {
    for (auto &group : m_groups)
    {
      if (group)
        group->clear();
    }
  }

  bool isEmpty()
  {
    for (auto &group : m_groups)
    {
      if (group && !group->isEmpty())
        return false;
    }
    return true;
  }

  int getInstanceCount()
  {
    int count = 0;
    for (auto& group : m_groups)
    {
      if (group)
        count += group->getInstanceCount();
    }
    return count;
  }

  void addInstance(int material,
                   int material_index,
                   float dist,
                   bool low_detail,
                   const InstanceData& instance,
                   int vao_index)
  {
    for (auto& group : m_groups)
    {
      if (!group)
        continue;

      if (!(material & group->isActiveForMaterials()))
        continue;

      if (group->getDrawDistance() && dist > group->getDrawDistance())
        continue;

      group->addInstance(material_index, dist, instance, vao_index, low_detail);
    }
  }

  void initialize(int group_index, const CreateProgramParameters& create_params,
                  MultiMeshVAO& vao)
  {
    auto& group = m_groups.at(group_index);
    assert(!group);

    assert(create_params.group_params.m_render_module == RENDER_MODULE);

    group = std::make_unique<Group>(create_params.group_params);

    ShaderErrors shader_errors;

    group->initialize(create_params, vao, shader_errors);

    if (!shader_errors.empty())
      throw shader_errors;
  }

  void deinitialize()
  {
    m_groups.clear();
    m_groups.resize(RenderGroupID::ENUM_SIZE);
  }

  Group* getGroup(int index)
  {
    if (m_groups.empty())
      return nullptr;
    else
      return m_groups.at(index).get();
  }
};


} // namespace render_util::terrain

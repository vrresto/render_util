/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "layer.h"
#include "module.h"
#include "geometry.h"
#include "material.h"
#include "noise.h"
#include "render_list_base.h"
#include "renderer.h"
#include "instance_buffer.h"
#include "buffer_updater.h"
#include "render_passes.h"
#include <render_util/stats.h>
#include <render_util/state.h>
#include <render_util/terrain_util.h>
#include <render_util/texture_state.h>
#include <render_util/texunits.h>
#include <render_util/elevation_map.h>
#include <render_util/texture_util.h>
#include <render_util/image.h>
#include <render_util/image_util.h>
#include <render_util/image_resample.h>
#include <render_util/shader_util.h>
#include <render_util/render_util.h>
#include <render_util/earth_curvature_map.h>
#include <render_util/physics.h>
#include <util/integer_sequence.h>

#include <bitset>
#include <set>
#include <array>
#include <vector>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/round.hpp>

#include <GL/gl.h>
#include <render_util/gl_binding/gl_functions.h>
#include <log.h>

#include "terrain_config.h"

#if TERRAIN_ENABLE_VIEWER_PARAMETERS
  #include <render_util/viewer/value_wrapper.h>
  #include <render_util/viewer/parameters_base.h>
  #include <render_util/viewer/multiple_choice_parameter.h>
  #include <render_util/viewer/boolean_parameter.h>
  #include <render_util/viewer/parameterizable.h>
#endif


using namespace render_util::terrain;

using namespace render_util::gl_binding;
using namespace glm;

using render_util::Camera;
using render_util::TextureState;
using render_util::TexturePtr;
using render_util::TerrainBase;
using render_util::ImageRGB;
using render_util::ImageRGBA;
using render_util::ShaderProgramPtr;
using render_util::ShaderParameters;
using render_util::Rect;
using std::endl;
using std::vector;


namespace render_util
{


class Terrain : public TerrainBase
{
  ShaderSearchPath shader_search_path;

  bool m_enable_base_layer = true;

  std::string m_render_error;
  std::vector<ShaderCreationError> m_shader_errors;

  vec2 m_base_map_origin = vec2(0);
  float m_base_map_scale = 1;
  float m_base_map_height = 0;

  vec2 m_detail_map_origin = vec2(0);
  float m_detail_map_scale = 1;
//   float m_detail_map_height = 0;

  int m_enabled_modules = 0;
  bool m_has_type_map = false;

  int m_anisotropy = 1;

  MaterialTable m_material_table;

  std::vector<std::unique_ptr<Layer>> m_layers;

  std::vector<std::unique_ptr<Module>> m_modules;
  std::vector<IRenderModule*> m_render_modules;
  std::vector<IMaterialModule*> m_material_modules;
  std::vector<Module*> m_active_modules;
  std::vector<IRenderModule*> m_active_render_modules;
  std::vector<IMaterialModule*> m_active_material_modules;

  TextureState m_texture_state;

  render_util::ShaderParameters m_shader_params;
  std::string m_program_suffix;

  bool m_shader_load_error = false;

  IQuadTree* m_quad_tree {};

  bool m_enable_frame_buffer_mipmap = true;

  std::array<RenderGroupDispatcher, RenderGroupID::ENUM_SIZE>
    m_render_groups {};

  std::bitset<RenderGroupID::ENUM_SIZE> m_render_group_enable {};
  std::vector<int> m_active_render_groups;

  std::array<std::unique_ptr<RenderGroupParameters>,
    RenderGroupID::ENUM_SIZE> m_render_group_parameters;

  ShaderProgramPtr m_debug_draw_program;

  render_util::TexturePtr m_generic_noise_texture;
  render_util::TexturePtr m_noise_texture;
  render_util::TexturePtr m_far_noise_texture;
  bool m_far_noise_texture_is_normal_map = false;

  TextureStateObserver m_texture_state_create_observer;

  BufferUpdateMethod m_buffer_update_method = DEFAULT_BUFFER_UPDATE_METHOD;
  unsigned m_buffer_usage = DEFAULT_BUFFER_USAGE;

  void setUniforms(ShaderProgramPtr, const render_util::Camera&);
  bool isBaseLayerActive();
  void anisotropyChanged();
  void createLayer(int number, std::string name, BuildParameters &params);

  int getMaterialIndex(int material);

  bool hasShaderError();

  template <class T>
  T& getModule(ModuleIndex::Enum index)
  {
    for (auto& m : m_modules)
    {
      if (m->getIndex() == index)
        return m->to<T>();
    }
    throw std::runtime_error("Module not loaded: " + getModuleName(index));
  }

  template <class T>
  T& getActiveModule(ModuleIndex::Enum index)
  {
    for (auto& m : m_active_modules)
    {
      if (m->getIndex() == index)
        return m->to<T>();
    }
    throw std::runtime_error("Module not active: " + getModuleName(index));
  }

protected:
  bool m_wireframe = false;

  const auto& getModules() { return m_modules; }

public:
  Terrain(const ShaderSearchPath&);
  ~Terrain() override;

  void build(BuildParameters&) override;
  void draw(const Camera &camera, const UniformUpdater&) override;
  void update(const Camera &camera, bool low_detail) override;
  void setDrawDistance(float dist) override;
  render_util::TexturePtr getNormalMapTexture() override;
  void setProgramSuffix(std::string) override;

  void setLayerOrigin(int layer, glm::vec3 origin) override;
  void setLayerScale(int layer, float scale) override;

  void setMapTextureOffset(int layer, glm::vec2) override;
  glm::vec2 getMapTextureOffset(int layer) override;

  void reloadShaders() override;
  void updateAnimation(float frame_delta) override;
  void setAnisotropy(int) override;
  void setAltitudinalZones(const std::vector<AltitudinalZone>&) override;
  void setBaseLayerTypeMapping(const std::vector<int>&) override;
  void enableBaseLayer(bool) override;
  bool isBaseLayerEnabled() override;

  bool hasError() override;
  std::vector<std::string> getErrors() override;
  void getStats(Stats&) override;

  int getEnabledModules() override { return m_enabled_modules; }
  void setEnabledModules(int mask) override;

  void setShaderParameters(const ShaderParameters &p) override
  {
    m_shader_params = p;
  }

  int getRenderGroups() override
  {
    return RenderGroupID::ENUM_SIZE;
  }

  bool isRenderGroupEnabled(int group) override
  {
    return m_render_group_enable.test(group);
  }

  void enableRenderGroup(int group, bool enable) override
  {
    if (m_render_group_enable.test(group) != enable)
    {
      m_render_group_enable.set(group, enable);
      reloadShaders();
    }
  }

  void setTextureStateCreateObserver(TextureStateObserver observer) override
  {
    m_texture_state_create_observer = observer;
  }

  void setLodDistanceScale(float scale) override
  {
    m_quad_tree->setLodDistanceScale(scale);
  }

  float getLodDistanceScale() override
  {
    return m_quad_tree->getLodDistanceScale();
  }

  BufferUpdateMethod getBufferUpdateMethod()
  {
    return m_buffer_update_method;
  }

  void setBufferUpdateMethod(BufferUpdateMethod method)
  {
    m_buffer_update_method = method;

    for (auto& m : m_render_modules)
      m->setBufferUpdateMethod(method);

    for (auto& group : m_render_groups)
      group.setBufferUpdateMethod(method);
  }

  unsigned getBufferUsage()
  {
    return m_buffer_usage;
  }

  void setBufferUsage(unsigned usage)
  {
    m_buffer_usage = usage;

    for (auto& m : m_render_modules)
      m->setBufferUsage(usage);

    for (auto& group : m_render_groups)
      group.setBufferUsage(usage);
  }

  std::vector<ShaderCreationError> getShaderErrors() override
  {
    return m_shader_errors;
  }
};


Terrain::~Terrain()
{
}


Terrain::Terrain(const ShaderSearchPath &shader_search_path) :
  shader_search_path(shader_search_path)
{
  m_render_group_enable.set(RenderGroupID::TERRAIN, true);
  m_render_group_enable.set(RenderGroupID::FOREST, true);

  m_generic_noise_texture = createNoiseTexture();
}


void Terrain::updateAnimation(float frame_delta)
{
  for (auto& m : m_active_material_modules)
    m->updateAnimation(frame_delta);
}


void Terrain::setProgramSuffix(std::string suffix)
{
  m_program_suffix = suffix;
}


void Terrain::setUniforms(ShaderProgramPtr program, const render_util::Camera &camera)
{
  for (auto& m : m_active_material_modules)
    m->setUniforms(program);

  for (auto& layer : m_layers)
    layer->setUniforms(program);

  program->setUniform("viewport_size", glm::vec2(camera.getViewportSize()));

  program->setUniform<float>("terrain.max_elevation", render_util::physics::MAX_TERRAIN_ELEVATION);
  program->setUniform<float>("planet_radius", render_util::physics::EARTH_RADIUS);

  program->setUniformi("terrain.mesh_resolution_m", GRID_RESOLUTION_M);

  program->setUniformi("terrain.tile_size_m", TILE_SIZE_M);
  program->setUniformi("terrain.tile_size_grid", TILE_SIZE_GRID);

  program->setUniform("curvature_map_max_distance",
                      render_util::earth_curvature_map::curvature_map_max_distance);

  render_util::Camera::Vec3 terrain_scale(glm::ivec2(TerrainBase::GRID_RESOLUTION_M), 1);
  program->setUniform("camera_pos_terrain_integer_part",
                      glm::vec3(glm::floor(camera.getPosD() / terrain_scale)));
  program->setUniform("camera_pos_terrain_fraction_part",
                      glm::vec3(terrain_scale * glm::fract(camera.getPosD() / terrain_scale)));

  program->setUniform<float>("near_texture_fade_start_dist", NEAR_TEXTURE_FADE_START_DIST);
  program->setUniform<float>("near_texture_fade_end_dist", NEAR_TEXTURE_FADE_END_DIST);
}


void Terrain::setEnabledModules(int mask)
{
  m_active_modules.clear();
  m_active_render_modules.clear();
  m_active_material_modules.clear();
  m_enabled_modules = 0;

  for (auto& m : m_modules)
  {
    if (m->getMask() & mask)
    {
      m_active_modules.push_back(m.get());
      m_enabled_modules |= m->getMask();

      auto render = dynamic_cast<IRenderModule*>(m.get());
      if (render)
        m_active_render_modules.push_back(render);

      auto material = dynamic_cast<IMaterialModule*>(m.get());
      if (material)
        m_active_material_modules.push_back(material);
    }
  }

  reloadShaders();
}


void Terrain::build(BuildParameters &params)
{
  assert(m_layers.empty());

  m_has_type_map = true; //FIXME params.resources.hasTypeMap();
  m_shader_params = params.shader_parameters;

  auto noise_texture_image = params.resources.getNoiseTexture();
  if (noise_texture_image)
  {
    m_noise_texture = createTexture(noise_texture_image);
    m_noise_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    m_noise_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }

  m_far_noise_texture_is_normal_map =
    params.resources.farNoiseTextureIsNormalMap();
  auto far_noise_texture_image = params.resources.getFarNoiseTexture();
  if (far_noise_texture_image)
  {
    m_far_noise_texture = createTexture(far_noise_texture_image);
    m_far_noise_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    m_far_noise_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }

  auto load_module = [&] <ModuleIndex::Enum Index> ()
  {
    if (Index == ModuleIndex::NONE)
      return;

    constexpr auto MASK = MODULE_MASK<Index>;

    auto name = getModuleName(Index);

    bool enable = (params.load_modules & MASK);
    // enable = enable && (params.resources.getSupportedModules() & MASK);

    if (enable)
    {
      LOG_INFO << "Loading module: " << Index << " (" << name << ")" << std::endl;
      m_modules.push_back(createModule<Index>(params));
      assert(m_modules.back()->getIndex() == Index);

      auto material = dynamic_cast<IMaterialModule*>(m_modules.back().get());
      if (material)
        m_material_modules.push_back(material);

      auto render = dynamic_cast<IRenderModule*>(m_modules.back().get());
      if (render)
        m_render_modules.push_back(render);
    }
    else
    {
      LOG_INFO << "Not loading module: " << Index << " (" << name  << ")" << std::endl;
    }
  };

  util::visitWithTemplate(ModuleIndex::AllValues(), load_module);

  createLayer(LayerNumber::LAYER_DETAIL, "detail_layer", params);
  if (params.resources.hasBaseLayer())
    createLayer(LayerNumber::LAYER_BASE, "base_layer", params);

  for (auto& m : m_material_modules)
  {
    for (auto& layer : m_layers)
      m->onLoadLayerFinished(*layer);
  }

  LOG_INFO<<"Terrain: creating material map ..."<<endl;
  auto material_map = std::make_unique<MaterialMap>(params);
  LOG_INFO<<"Terrain: creating material map ... done."<<endl;

  m_quad_tree = &getModule<IQuadTree>(ModuleIndex::RENDERER);
  assert(m_quad_tree);
  LOG_INFO<<"Terrain: initializing quad tree ..."<<endl;
  m_quad_tree->initialize(params, *material_map, m_material_table,
                          ~TerrainBase::MaterialID::WATER);
  LOG_INFO<<"Terrain: initializing quad tree ... done."<<endl;

  TexturePtr water_map_pass_fb_texture;

  for (int i = 0; i < m_render_group_parameters.size(); i++)
  {
    m_render_group_parameters.at(i) =
      createRenderGroupParameters(i, water_map_pass_fb_texture);
  }

  for (auto& m : m_material_modules)
    m->bindTextures(m_texture_state);

  for (auto& layer : m_layers)
    layer->bindTextures(m_texture_state);

  assert(water_map_pass_fb_texture);
  m_texture_state.bind("water_map_pass_fb", *water_map_pass_fb_texture);

  assert(m_generic_noise_texture);
  m_texture_state.bind("sampler_generic_noise", *m_generic_noise_texture);

  if (m_noise_texture)
    m_texture_state.bind("sampler_terrain_noise", *m_noise_texture);
  if (m_far_noise_texture)
    m_texture_state.bind("sampler_terrain_far_noise", *m_far_noise_texture);

  if (m_texture_state_create_observer)
    m_texture_state_create_observer(m_texture_state);

  m_texture_state.dump();

  for (auto& m : m_render_modules)
  {
    m->setBufferUsage(m_buffer_usage);
    m->setBufferUpdateMethod(m_buffer_update_method);
  }

  setEnabledModules(params.enabled_modules);
  anisotropyChanged();

  LOG_INFO<<"Terrain: done building terrain."<<endl;

  m_debug_draw_program = createShaderProgram("terrain_debug",
                                             shader_search_path,
                                             m_shader_params);
}


bool Terrain::isBaseLayerActive()
{
  return m_enable_base_layer && m_layers.size() == 2;
}


void Terrain::setLayerOrigin(int layer, glm::vec3 origin)
{
  m_layers.at(layer)->origin_m = origin;

  m_layers.at(layer)->origin_terrain_grid =
    glm::vec2(origin) / glm::vec2(GRID_RESOLUTION_M);
}


void Terrain::setLayerScale(int layer, float scale)
{
  m_layers.at(layer)->scale = scale;
}


void Terrain::setMapTextureOffset(int layer, glm::vec2 offset)
{
  m_layers.at(layer)->map_texture_offset_px = offset;
}


glm::vec2 Terrain::getMapTextureOffset(int layer)
{
  return m_layers.at(layer)->map_texture_offset_px;
}


render_util::TexturePtr Terrain::getNormalMapTexture()
{
  //FIXME - layer number as parameter?
  assert(!m_layers.empty());
  auto map = m_layers.at(0)->getMap("normal_map");
  assert(map);
  return dynamic_cast<AttributeMapTexture*>(map)->getTexture();
}


void Terrain::draw(const render_util::Camera &camera,
                   const UniformUpdater& update_scene_uniforms)
{
  for (auto& group : m_render_groups)
    group.clear();

  auto set_uniforms = [&] (ShaderProgramPtr program)
  {
    setUniforms(program, camera);
  };

  for (auto& m : m_active_material_modules)
  {
    m->compute(set_uniforms);
    for (auto& l : m_layers)
      m->compute(*l, set_uniforms);
  }

  auto& groups = m_render_groups;

  for (auto group_index : m_active_render_groups)
  {
    assert(m_render_group_parameters.at(group_index));
    auto& params = *m_render_group_parameters.at(group_index);
    auto& group = groups.at(group_index);

    try
    {
      auto& m = getActiveModule<IRenderModule>(params.m_render_module);
      m.prepareRenderGroup(group_index, group);
    }
    catch (std::exception& e)
    {
      if (m_render_error.empty()) // avoid spamming the log
      {
        LOG_ERROR << e.what() << std::endl;
      }
      m_render_error = e.what();
    }

    for (auto& program : group.getPrograms())
    {
      assert(program);
      assert(program->isValid());

      update_scene_uniforms(*program);
      setUniforms(program, camera);
    }
  }

  StateModifier state;
  state.setDefaults();
  state.enableDepthTest(true);
  state.setFrontFace(GL_CCW);

  TextureStateBinding texture_state(m_texture_state);

  for (auto group_index : m_active_render_groups)
  {
    assert(m_render_group_parameters.at(group_index));
    auto& params = *m_render_group_parameters.at(group_index);
    auto& group = groups.at(group_index);

    StateModifier state;
    params.applyTo(state, m_wireframe);

    if (params.m_frame_buffer)
    {
      auto fb_size = params.m_double_size_frame_buffer ?
                      camera.getViewportSize() * 2 : camera.getViewportSize();
      params.m_frame_buffer->setSize(fb_size);

      {
        TemporaryFrameBufferBinding fbb(*params.m_frame_buffer, true);

        gl_binding::gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                  GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        group.render(params, camera);
      }

      if (params.m_double_size_frame_buffer &&
          m_enable_frame_buffer_mipmap)
      {
        auto& texture = params.m_frame_buffer->getTexture(0);
        texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        gl_binding::gl::GenerateTextureMipmap(texture->getID());
      }
    }
    else
    {
      group.render(params, camera);
    }

    if (group.getShaderErrorCount())
      m_shader_load_error = true;
  }

#if 0
  assert(m_debug_draw_program);
  render_util::updateUniforms(*m_debug_draw_program, camera);
  assert(m_debug_draw_program->isUsable());
  state.setProgram(m_debug_draw_program->getID());
  state.setPolygonMode(GL_LINE);
  m_renderer->drawDebug(camera);
#endif
}


void Terrain::setDrawDistance(float dist)
{
  for (auto& m : m_render_modules)
    m->setDrawDistance(dist);
}


void Terrain::setAnisotropy(int value)
{
  m_anisotropy = value;
  anisotropyChanged();
}


void Terrain::anisotropyChanged()
{
  //FIXME far texture

  for (auto& m : m_material_modules)
    m->setAnisotropy(m_anisotropy);

  if (m_noise_texture)
    m_noise_texture->setParameter(GL_TEXTURE_MAX_ANISOTROPY_EXT, m_anisotropy);
}


void Terrain::setBaseLayerTypeMapping(const std::vector<int>& mapping)
{
  for (auto& m : m_material_modules)
    m->setBaseLayerTypeMapping(mapping);
}


void Terrain::setAltitudinalZones(const std::vector<AltitudinalZone>& zones)
{
  bool shader_parameters_changed = false;

  for (auto& m : m_material_modules)
    shader_parameters_changed |= m->setAltitudinalZones(zones);

  if (shader_parameters_changed)
    reloadShaders();
}


void Terrain::reloadShaders()
{
  LOG_INFO << "Reloading shaders" << std::endl;

  m_render_error = {};
  m_shader_load_error = false;
  m_shader_errors.clear();

  for (auto& m : m_modules)
    m->m_shader_parameters_changed = false;

  for (auto& m : m_render_modules)
    m->destroyRenderLists();

  m_active_render_groups.clear();

  for (int i = 0; i < m_render_group_enable.size(); i++)
  {
    if (m_render_group_enable.test(i))
      m_active_render_groups.push_back(i);
  }

  for (auto index : m_active_render_groups)
  {
    auto& group_params = m_render_group_parameters.at(index);
    assert(group_params);

    try
    {
      auto& render_module = getActiveModule<IRenderModule>(group_params->m_render_module);

      CreateProgramParameters create_params
      {
        .modules = m_active_material_modules,
        .shader_search_path = shader_search_path,
        .group_params = *group_params,
        .material_table = m_material_table,
        .renderer = render_module,
        .texture_state = m_texture_state,
        .params = m_shader_params,
        .module_mask = m_enabled_modules,
        .far_noise_texture_is_normal_map = m_far_noise_texture_is_normal_map,
        .is_base_layer_active = isBaseLayerActive(),
      };

      render_module.initializeRenderList(index, create_params);
    }
    catch (ShaderErrors& errors)
    {
      std::set<std::string> names;

      for (auto& e : errors)
      {
        LOG_ERROR << e.what() << std::endl;

        if (!names.count(e.getProgramName())) // avoid duplicates
        {
          names.insert(e.getProgramName());
          m_shader_errors.push_back(e);
        }

        m_shader_load_error = true;
      }
    }
    catch (ShaderCreationError& e)
    {
      LOG_ERROR << e.what() << std::endl;
      m_shader_errors.push_back(e);
      m_shader_load_error = true;
    }
    catch (std::exception& e)
    {
      LOG_ERROR << e.what() << std::endl;
      m_render_error = e.what();
    }
  }

  for (auto& m : m_active_material_modules)
  {
    m->setSamplerUniforms(m_texture_state);

    for (auto& layer : m_layers)
    {
      m->reloadShaders(*layer, shader_search_path, isBaseLayerActive());
      m->setSamplerUniforms(*layer, m_texture_state);
    }
  }

  LOG_INFO << "Shaders reloaded" << std::endl;
}


bool Terrain::hasShaderError()
{
  if (m_shader_load_error)
  {
    return true;
  }
  else
  {
    for (auto& m : m_active_modules)
    {
      if (m->hasShaderLoadError())
        return true;
    }
  }

  return false;
}


bool Terrain::hasError()
{
  return !m_render_error.empty() || hasShaderError();
}


std::vector<std::string> Terrain::getErrors()
{
  std::vector<std::string> errors;

  if (hasShaderError())
  {
    errors.push_back("Shader error");
  }

  for (auto& e : m_shader_errors)
    errors.push_back(e.what());

  if (!m_render_error.empty())
    errors.push_back(m_render_error);

  return errors;
}


bool Terrain::isBaseLayerEnabled()
{
  return m_enable_base_layer;
}


void Terrain::enableBaseLayer(bool enable)
{
  bool changed = m_enable_base_layer != enable;
  m_enable_base_layer = enable;
  if (changed)
    reloadShaders();
}


void Terrain::createLayer(int number, std::string name, BuildParameters &params)
{
  LOG_INFO << "Creating layer " << number << " ..." <<  endl;

  auto layer = std::make_unique<Layer>(number);

  layer->uniform_prefix = "terrain." + name + ".";
  layer->name = "terrain." + name;

  LOG_INFO << "Initializing layer " << number << " ..." <<  endl;

  loadGeometry(*layer, number, params);

  for (auto& m : m_material_modules)
    m->loadLayer(*layer, params.resources.getLayer(number), false,
                 shader_search_path);

  LOG_INFO << "Initializing layer " << number << " ... done." <<  endl;

  m_layers.push_back(std::move(layer));

  setMapTextureOffset(number, params.resources.getLayer(number).getMapTextureOffset());

  LOG_INFO << "Creating layer " << number << " ... done." << endl;
};


void Terrain::update(const Camera &camera, bool low_detail)
{
  bool shader_parameters_changed = false;

  for (auto& m : m_active_modules)
  {
    if (m->m_shader_parameters_changed)
      shader_parameters_changed = true;
  }

  if (shader_parameters_changed)
    reloadShaders();

  for (auto& m : m_render_modules)
    m->clearRenderLists();

  m_quad_tree->update(camera, low_detail);
}


void Terrain::getStats(Stats& stats)
{
  std::vector<std::string> material_names;
  for (int i = 0; i < m_material_table.getSize(); i++)
    material_names.push_back(getMaterialName(m_material_table.get(i)));

  for (auto& m : m_active_render_modules)
    m->getStats(stats, material_names);

  stats.add<bool>("Land", m_enabled_modules & ModuleMask::LAND);
  stats.add<bool>("Water", m_enabled_modules & ModuleMask::WATER);
  stats.add<bool>("Forest", m_enabled_modules & ModuleMask::FOREST);

  stats.add("Active render groups", m_active_render_groups.size());
  for (auto index : m_active_render_groups)
  {
    auto& group = m_render_groups.at(index);
    auto& child = stats.addChild("Render group " + std::to_string(index)
                                 + ": " + getRenderGroupName(index));

    group.addStats(child);
  }
}


#if TERRAIN_ENABLE_VIEWER_PARAMETERS
class ParameterizableTerrain : public Terrain, public viewer::IParameterizable
{
public:
  ParameterizableTerrain(const ShaderSearchPath& p) : Terrain(p) {}

  void addParameters(viewer::ParametersBase& params) override
  {
    {
      viewer::ValueWrapper<bool> wrapper =
      {
        .get = [this] () { return m_wireframe; },
        .set = [this] (bool value)
          {
            m_wireframe = value;
          },
      };
      using Param = viewer::BooleanParameter<viewer::ValueWrapper<bool>>;
      params.add(std::make_unique<Param>("Wireframe", wrapper, m_wireframe));
    }

    {
      using Param = viewer::MultipleChoiceParameter<BufferUpdateMethod>;
      Param::Choices choices =
      {
        { "copy", BufferUpdateMethod::COPY },
        { "map", BufferUpdateMethod::MAP },
      };
      auto get = [this] () { return getBufferUpdateMethod(); };
      auto set = [this] (auto value) { setBufferUpdateMethod(value); };
      params.add(std::make_unique<Param>("Buffer update method", get, set, choices));
    }

    {
      using Param = viewer::MultipleChoiceParameter<unsigned>;
      Param::Choices choices =
      {
        { "dynamic", GL_DYNAMIC_DRAW },
        { "stream", GL_STREAM_DRAW },
        { "static", GL_STATIC_DRAW },
      };
      auto get = [this] () { return getBufferUsage(); };
      auto set = [this] (auto value) { setBufferUsage(value); };
      params.add(std::make_unique<Param>("Buffer usage", get, set, choices));
    }

    for (auto& m : getModules())
    {
      auto iface = dynamic_cast<viewer::IParameterizable*>(m.get());
      if (iface)
        iface->addParameters(params);
    }
  }
};
#endif


std::shared_ptr<TerrainBase> createTerrain(const ShaderSearchPath& shader_path)
{
#if TERRAIN_ENABLE_VIEWER_PARAMETERS
  return std::make_shared<ParameterizableTerrain>(shader_path);
#else
  return std::make_shared<Terrain>(shader_path);
#endif
}


} // namespace render_util

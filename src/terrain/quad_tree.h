#pragma once

#include <render_util/terrain_base.h>
#include <render_util/geometry.h>


namespace render_util::terrain
{


constexpr unsigned int NODE_GRID_SIZE = 32;


enum
{
  TOP_LEFT,
  TOP_RIGHT,
  BOTTOM_LEFT,
  BOTTOM_RIGHT
};


class HeightMap;
class MaterialMap;


struct QuadTreeNode
{
  std::array<QuadTreeNode*, 4> children {};
  int lod {};
  glm::ivec2 pos_grid {};
  float max_height = 0;
  render_util::Box bounding_box;
  unsigned int material_id = 0;
  int material_index {};
  int material_index_detail {};
  bool material_is_uniform = true;
};


inline glm::ivec2 getChildCoords(int child)
{
  switch (child)
  {
    case TOP_LEFT:
      return glm::ivec2(0,1);
    case TOP_RIGHT:
      return glm::ivec2(1,1);
    case BOTTOM_LEFT:
      return glm::ivec2(0,0);
    case BOTTOM_RIGHT:
      return glm::ivec2(1,0);
    default:
      abort();
  }
}


inline float getNodeScale(int lod)
{
  assert(lod >= 0);
  return std::pow(2, lod);
}


inline float getNodeScaleFloat(int lod)
{
  return std::pow(2, lod);
}


inline int getNodeSizeTerrainGrid(int lod)
{
  auto size = getNodeScaleFloat(lod) * NODE_GRID_SIZE;
  assert(glm::fract(size) == 0);
  return size;
}


inline float getNodeSizeTerrainGridFloat(int lod)
{
  return getNodeScaleFloat(lod) * NODE_GRID_SIZE;
}


inline int getNodeSizeMeters(int lod)
{
  return getNodeSizeTerrainGrid(lod) * TerrainBase::GRID_RESOLUTION_M;
}


inline float getNodeSizeMetersFloat(int lod)
{
  return getNodeSizeTerrainGridFloat(lod) * TerrainBase::GRID_RESOLUTION_M;
}


inline render_util::Box getChildBB(glm::vec2 parent_origin_m,
                            int parent_lod,
                            int child,
                            float max_height)
{
  auto node_size_m = getNodeSizeMetersFloat(parent_lod-1);
  auto origin_m = parent_origin_m + glm::vec2(getChildCoords(child)) * node_size_m;

  render_util::Box bb;
  bb.set(glm::vec3(origin_m, 0), glm::vec3(node_size_m, node_size_m, max_height));

  return bb;
}


} // namespace render_util::terrain

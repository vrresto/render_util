#pragma once

#include <grid_mesh.h>
#include <render_util/vao.h>

#include <memory>


namespace render_util::terrain
{


class MultiMeshVAO : public IndexedVAOBase
{
public:
  struct Mesh
  {
    unsigned element_count {};
    unsigned first_index {};
    int base_vertex {};
  };

  MultiMeshVAO(const std::vector<IndexedMesh>&);
  ~MultiMeshVAO();

  const Mesh& getMesh(int index) { return m_meshes.at(index); }
  int getMeshCount() { return m_meshes.size(); }

private:
  unsigned m_vertex_buffer_id {};
  std::vector<Mesh> m_meshes;
};


inline auto createGridMeshVAO(int size)
{
  auto mesh = createGridMesh(size + 1, size + 1);
  return std::make_unique<VertexArrayObject>(mesh, false);
};


inline auto createMultiGridMeshVAO(const std::vector<int>& sizes)
{
  std::vector<IndexedMesh> meshes;
  meshes.reserve(sizes.size());

  for (auto size : sizes)
    meshes.push_back(createGridMesh(size + 1, size + 1));

  return std::make_unique<MultiMeshVAO>(meshes);
};


} // namespace render_util::terrain

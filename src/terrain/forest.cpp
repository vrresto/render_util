/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "forest.h"
#include "material.h"
#include <render_util/image_resample.h>
#include <render_util/image_loader.h>
#include <render_util/texture_array.h>
#include <render_util/texture_2d.h>
#include <render_util/gl_binding/gl_functions.h>


namespace render_util::terrain
{


Forest::Forest(TerrainBase::BuildParameters &params)
{
  LOG_INFO << "Initializing forest ..." << std::endl;

  auto& resources = params.resources.to<TerrainForestResources>();

  std::vector<ImageRGBA::Ptr> far_texture_lods;

  {
    auto layers = resources.getForestLayers();

    LOG_INFO << "Creating " <<  layers.size() << " forest layers ..." << std::endl;

    auto texture = std::make_shared<TextureArray>(layers.size(),
                                                  layers.at(0)->size(), 4);

    auto lod_size = layers.at(0)->size().x;
    int lod = 0;

    while (lod_size > 0)
    {
      assert(lod_size > 0);

      ImageRGBA::Ptr image = downSample(*layers.at(0), lod);

      assert((*image).getSize() == glm::ivec2(lod_size));

      for (int y = 0; y < image->h(); y++)
      {
        for (int x = 0; x < image->w(); x++)
        {
          float alpha = image->get(x,y,3) / 255.f;

          for (int c = 0; c < 3; c++)
            image->at(x,y,c) *= alpha;
        }
      }

      saveImageToFile("forest_far_level_" + std::to_string(lod) +
                      "_layer_0.tga", image.get());

      for (int i = 1; i < layers.size(); i++)
      {
        auto layer = downSample(*layers.at(i), lod);
        assert(layer->size() == glm::ivec2(lod_size));

        saveImageToFile("forest_far_level_" + std::to_string(lod) +
                        "_layer_" + std::to_string(i) + ".tga", layer.get());

        for (int y = 0; y < image->h(); y++)
        {
          for (int x = 0; x < image->w(); x++)
          {
            float layer_alpha = layer->get(x,y,3) / 255.f;

            float orig_alpha = 1.0 - (image->get(x,y,3) / 255.f);

            orig_alpha *= 1.0 - layer_alpha;

            image->at(x,y,3) = (1.0 - orig_alpha) * 255;

            for (int c = 0; c < 3; c++)
            {
              float premultiplied = image->get(x,y,c) * (1.f - layer_alpha)
                                     + layer->get(x,y,c) * layer_alpha;

              image->at(x,y,c) = premultiplied;
            }
          }
        }
      }

      saveImageToFile("forest_far_level_" + std::to_string(lod) + ".tga", image.get());

      far_texture_lods.push_back(image);

      lod_size /= 2;
      lod++;
    }

    for (int i = 0; i < layers.size(); i++)
    {
      texture->setTextureImage(i, *layers.at(i));
    }

    texture->generateMipMap();

    m_layers_texture = texture;

    m_layers_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    m_layers_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // m_layers_texture->setParameter(GL_TEXTURE_LOD_BIAS, 2.0);
  }

  {
    auto far_texture = std::make_shared<Texture2D>(far_texture_lods.size(),
                                                   far_texture_lods.at(0)->size(),
                                                   4);

    for (int i = 0; i < far_texture_lods.size(); i++)
    {
      far_texture->setTextureImage(i, *far_texture_lods.at(i));
    }

    far_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    far_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    m_forest_far_texture = far_texture;
  }
}


void Forest::bindTextures(TextureState& state) const
{
  state.bind("sampler_forest_layers", *m_layers_texture);
  state.bind("sampler_forest_far", *m_forest_far_texture);
}


void Forest::setUniforms(ShaderProgramPtr program) const
{
}


void Forest::loadLayer(Layer& layer,
                         TerrainResourcesBase::Layer& resources,
                         bool is_base_layer/*FIXME HACK*/,
                         const ShaderSearchPath&)
{
  auto& forest_res = resources.to<TerrainForestResources::Layer>();
  auto forest_map = forest_res.getForestMap();
  auto texture = createTexture(forest_map, true);

  texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  // texture->setParameter(GL_TEXTURE_LOD_BIAS, 10.0);

  auto map = std::make_unique<AttributeMapTexture>("forest_map");
  map->resolution_m = TerrainBase::GRID_RESOLUTION_M;
  map->size_m = forest_map->getSize() * TerrainBase::GRID_RESOLUTION_M;
  map->size_px = forest_map->getSize();
  map->setTexture(texture),

  layer.addMap(std::move(map));
}


void Forest::setShaderParameters(unsigned int material,
                                 unsigned int  detail_options,
                                 ShaderParameters& params) const
{
  bool enable_forest = (material & TerrainBase::MaterialID::FOREST);
  if (enable_forest)
  {
    params.set("terrain_enable_forest", enable_forest);
    params.set<bool>("terrain_forest_enable_detail", detail_options & MaterialOption::FOREST);
  }
}


void Forest::addShaders(unsigned int material,
                        int lod_options,
                        std::vector<std::string>& vertex_shaders,
                        std::vector<std::string>& fragment_shaders) const
{
  bool enable_forest = (material & TerrainBase::MaterialID::FOREST);

  if (enable_forest)
  {
    vertex_shaders.push_back("terrain_forest");
    fragment_shaders.push_back("terrain_forest");
  }
}


template <>
std::unique_ptr<ModuleSubClass<TerrainBase::ModuleIndex::FOREST>>
createModule<TerrainBase::ModuleIndex::FOREST>(TerrainBase::BuildParameters& params)
{
  return std::make_unique<Forest>(params);
}


} // namespace render_util::terrain

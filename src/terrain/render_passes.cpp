#include "render_passes.h"


using namespace render_util;
using namespace render_util::terrain;


namespace
{


class RenderGroupForest : public RenderGroupParameters
{
public:
  RenderGroupForest()
  {
    m_iterations = 4;
    m_draw_distance = 2000;
    m_enable_blend = true;
//     m_enable_cull_face = false;
  }

  void setShaderParameters(ShaderParameters& p) const override
  {
    p.set("terrain_enable_height_offset", true);
  }

  void setUniforms(render_util::ShaderProgram& program,
                           const render_util::Camera&,
                           int iteration) const override
  {
    program.setUniformi("forest_layer", iteration + 1);
    program.setUniform<float>("terrain_height_offset", (iteration + 1) * 3);
  }
};


} // namespace


namespace render_util::terrain
{


std::unique_ptr<RenderGroupParameters> createRenderGroupParameters(int group_id,
    TexturePtr& water_map_pass_fb_texture //FIXME HACK
  )
{
  switch (group_id)
  {
    case RenderGroupID::WATER_MAP:
    {
      auto params = std::make_unique<RenderGroupParameters>();
      params->m_render_module = TerrainBase::ModuleIndex::RENDERER;
      params->m_program_name = "terrain_water_map_pass";
      params->m_material_mask = TerrainBase::MaterialID::WATER_MAP;
      params->m_active_for_materials = TerrainBase::MaterialID::ALL; //FIXME
      params->m_enabled_material_modules = TerrainBase::ModuleMask::WATER;
      params->m_lod_params =
      {
        { 0, 0 },
      };
      params->m_enable_atmosphere = false;
      params->m_frame_buffer = std::make_unique<FrameBuffer>(glm::ivec2(256), 1);
      params->m_double_size_frame_buffer = true;
      water_map_pass_fb_texture = params->m_frame_buffer->getTexture(0);
      return params;
    }
    case RenderGroupID::TERRAIN_WIREFRAME:
    {
      auto params = std::make_unique<RenderGroupParameters>();
      params->m_render_module = TerrainBase::ModuleIndex::RENDERER;
      params->m_program_name = "terrain_wireframe";
      params->m_material_mask = 0;
      params->m_active_for_materials = TerrainBase::MaterialID::ALL;
      params->m_enabled_material_modules = 0;
      params->m_lod_params =
      {
        { 0, 0 },
      };
      params->m_enable_wireframe = true;
      params->m_enable_polygon_offset = true;
      return params;
    }
    case RenderGroupID::TERRAIN:
    {
      auto params = std::make_unique<RenderGroupParameters>();
      params->m_render_module = TerrainBase::ModuleIndex::RENDERER;
      params->m_program_name = "terrain";
      params->m_material_mask = TerrainBase::MaterialID::ALL;
      params->m_active_for_materials = TerrainBase::MaterialID::ALL;
      params->m_enabled_material_modules = TerrainBase::ModuleMask::LAND |
                                           TerrainBase::ModuleMask::FOREST |
                                           TerrainBase::ModuleMask::WATER;
      params->m_lod_params =
      {
        { 0, 0 },
        { NEAR_TEXTURE_FADE_END_DIST, MaterialOption::LAND },
        { 15000, MaterialOption::LAND | MaterialOption::FOREST },
        { 5000, MaterialOption::LAND | MaterialOption::WATER | MaterialOption::FOREST },
        { 2000, MaterialOption::ALL },
      };
      return params;
    }
    case RenderGroupID::FOREST:
    {
      auto params = std::make_unique<RenderGroupForest>();
      params->m_render_module = TerrainBase::ModuleIndex::RENDERER;
      params->m_program_name = "terrain_forest_layers";
      params->m_active_for_materials = TerrainBase::MaterialID::FOREST;
      params->m_material_mask = TerrainBase::MaterialID::FOREST;
      params->m_enabled_material_modules = TerrainBase::ModuleMask::FOREST;
      params->m_lod_params =
      {
        { 2000, MaterialOption::FOREST },
      };
      return params;
    }
    default:
      return {};
  }
}


} // namespace render_util::terrain

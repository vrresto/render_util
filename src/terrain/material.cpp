#include "material.h"
#include "module.h"
#include "render_list_base.h"
#include <render_util/shader_util.h>
#include <render_util/texture_2d.h>
#include <render_util/config.h>
#include <render_util/gl_binding/gl_functions.h>


using render_util::TerrainBase;
using MaterialID = render_util::TerrainBase::MaterialID;

using namespace std;
using namespace render_util;
using namespace render_util::terrain;


namespace
{


class MaterialMapDebugModule : public ModuleSubClass<ModuleIndex::MATERIAL_MAP_DEBUG>,
                               public IMaterialModule
{
  void setShaderParameters(unsigned int material,
                                   unsigned int  detail_options,
                                   ShaderParameters& params) const override
  {
    params.set("terrain_enable_material_map_debug", true);
  }

  void addShaders(unsigned int material,
                        int lod_options,
                        std::vector<std::string>& vertex_shaders,
                        std::vector<std::string>& fragment_shaders) const override
  {
    fragment_shaders.push_back("terrain_material_debug");
  }

  void loadLayer(Layer& layer_out,
                 TerrainResourcesBase::Layer& layer_in,
                 bool is_base_layer/*FIXME HACK*/,
                 const ShaderSearchPath&) override

  {
    auto img_in = layer_in.getMaterialMap();

    ImageRGBA img_out(img_in->getSize());

    auto transform = [] (auto& in) -> RGBA
    {
      auto mat = in.components[0]; //FIXME ugly

      // FIXME duplicate in MaterialMap::createLayerMap
      if ((mat & MaterialID::WATER) && (mat & MaterialID::LAND))
        mat |= MaterialID::WATER_MAP;

      //FIXME ugly
      RGBA out;
      out[0] = bool(mat & MaterialID::LAND) * 255;
      out[1] = bool(mat & MaterialID::WATER) * 255;
      out[2] = bool(mat & MaterialID::FOREST) * 255 ,
      out[3] = bool(mat & MaterialID::WATER_MAP) * 255;

      return out;
    };

    image::transformPixels(*img_in, img_out, transform);

    auto texture = std::make_shared<Texture2D>(img_out);

    texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    auto map = std::make_unique<AttributeMapTexture>("material_map_debug");
    map->setTexture(texture);
    map->resolution_m = TerrainBase::GRID_RESOLUTION_M;
    map->size_px = img_out.getSize();
    map->size_m = map->size_px * map->resolution_m;

    layer_out.addMap(std::move(map));
  }

  bool isDebugModule() const { return true; }
};


} // namespace


namespace render_util::terrain
{


MaterialMap::MaterialMap(TerrainBase::BuildParameters &params)
{
  m_map = createLayerMap(params.resources.getDetailLayer());

  if (params.resources.hasBaseLayer())
    m_base_map = createLayerMap(params.resources.getBaseLayer());
}


MaterialMap::~MaterialMap()
{
}


std::unique_ptr<MaterialMap::LayerMap>
MaterialMap::createLayerMap(TerrainResourcesBase::Layer &layer)
{
  auto src = layer.getMaterialMap();
  assert(src);

  auto image = std::make_shared<LayerMap::ImageType>(src->getSize());
  image::visit(*src, *image,
    [] (auto& src, auto& dst)
    {
      if ((src & MaterialID::WATER) && (src & MaterialID::LAND))
        dst = src | MaterialID::WATER_MAP;
      else
        dst = src;
    });

  auto map = std::make_unique<LayerMap>();
  map->map = image;
  map->resolution_m = layer.getResolutionM();
  map->origin_m = layer.getOriginM();

  return map;
}


bool MaterialMap::isUniform(const Rect &area, int mask) const
{
  bool clamp_to_edge = !m_base_map;

  std::optional<unsigned> default_value;

  if (!clamp_to_edge)
    default_value = DEFAULT_MATERIAL;

  std::unordered_set<int> values;

  auto visitor = [&] (unsigned value)
  {
    values.insert(value & mask);
  };

  m_map->visitArea(area, visitor, default_value);

  if (m_base_map)
    m_base_map->visitArea(area, visitor, default_value);

  assert(values.size() >= 1);

  return values.size() == 1;
}


unsigned MaterialMap::getMaterialID(const Rect &area) const
{
  bool clamp_to_edge = !m_base_map;

  std::optional<unsigned> default_value;

  if (!clamp_to_edge)
    default_value = DEFAULT_MATERIAL;

  unsigned material = 0;

  auto visitor = [&] (unsigned value)
  {
    material |= value;
  };

  m_map->visitArea(area, visitor, default_value);

  if (m_base_map)
    m_base_map->visitArea(area, visitor, default_value);

  if (!material)
    material = DEFAULT_MATERIAL;

  return material;
}


render_util::ShaderProgramPtr createProgram(std::string name,
    const render_util::ShaderSearchPath &shader_search_path,
    const render_util::ShaderParameters &params_,
    bool enable_base_map,
    bool enable_atmosphere,
    const std::vector<std::string>& vertex_shaders,
    const std::vector<std::string>& fragment_shaders)
{

  bool enable_base_water_map = false;
  bool is_editor = false;

  assert(!name.empty());

  name += "_cdlod";

  ShaderParameters params = params_;

  params.set("terrain_enable_base_layer", enable_base_map);
  params.set("is_editor", is_editor);

  params.set<bool>("debug_coastline", DEBUG_COASTLINE);
  params.set<bool>("debug_small_water_map", DEBUG_SMALL_WATER_MAP);

  params.setIfNotDisabled("enable_atmosphere", enable_atmosphere);

  return createShaderProgram(name, shader_search_path, params, {},
                             vertex_shaders, fragment_shaders);
}


render_util::ShaderProgramPtr createProgram(const CreateProgramParameters& p,
                                             int material_index,
                                             int material_lod)
{
  auto material = p.material_table.get(material_index) &
                  p.group_params.m_material_mask;

  auto lod_options = p.group_params.m_lod_params.at(material_lod).options;

  auto module_mask = (p.module_mask & p.group_params.m_enabled_material_modules);

  std::vector<std::string> vertex_shaders;
  std::vector<std::string> fragment_shaders;

  auto params = p.params;

  params.set("far_noise_texture_is_normal_map", p.far_noise_texture_is_normal_map);

  for (auto& m : p.modules)
  {
    auto mod = dynamic_cast<Module*>(m); //FIXME ugly
    assert(mod);
    if ((mod->getMask() & module_mask) || m->isDebugModule())
    {
      m->addShaders(material, lod_options, vertex_shaders, fragment_shaders);
      m->setShaderParameters(material, lod_options, params);
    }
  }

  p.renderer.setShaderParameters(params);

  p.group_params.setShaderParameters(params);

  assert(!p.group_params.m_program_name.empty());

  auto program = createProgram(p.group_params.m_program_name,
                               p.shader_search_path,
                               params,
                               p.is_base_layer_active,
                               p.group_params.m_enable_atmosphere,
                               vertex_shaders,
                               fragment_shaders);

  assert(program);

  if (!program->isValid())
  {
    throw ShaderCreationError(p.group_params.m_program_name, "Validation failed");
  }

  p.texture_state.setSamplerUniforms(*program);
  p.renderer.setUniforms(*program);

  return program;
}


std::string getMaterialName(unsigned id)
{
  std::vector<std::string> components;

  if (id & MaterialID::LAND)
    components.push_back("land");
  if (id & MaterialID::WATER)
    components.push_back("water");
  if (id & MaterialID::FOREST)
    components.push_back("forest");

  std::string name;

//   assert(!components.empty());

  int i = 0;
  for (auto& c : components)
  {
    if (i > 0)
      name += "_";
    name += c;
    i++;
  }

  return name;
}


template <>
std::unique_ptr<ModuleSubClass<ModuleIndex::MATERIAL_MAP_DEBUG>>
createModule<ModuleIndex::MATERIAL_MAP_DEBUG>(TerrainBase::BuildParameters&)
{
  return std::make_unique<MaterialMapDebugModule>();
}


} // namespace render_util::terrain

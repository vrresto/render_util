#include "render_list_base.h"
#include <render_util/state.h>
#include <render_util/stats.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util;
using namespace render_util::terrain;
using namespace render_util::gl_binding;


namespace render_util::terrain
{


std::string getRenderGroupName(int index)
{

#define X(value) case RenderGroupID::value: \
  return util::makeLowercase(#value);

  switch (index)
  {
    X(WATER_MAP)
    X(TERRAIN)
    X(FOREST)
    X(TERRAIN_WIREFRAME)
  }

#undef X

  throw std::invalid_argument("invalid render group index");
}


void RenderGroupParameters::applyTo(StateModifier& state, bool wireframe) const
{
  state.enableBlend(m_enable_blend);
  state.enableCullFace(m_enable_cull_face);
  state.setPolygonMode((wireframe || m_enable_wireframe) ? GL_LINE : GL_FILL);
  state.enablePolygonOffsetFill(m_enable_polygon_offset);
  state.enablePolygonOffsetLine(m_enable_polygon_offset);

  if (m_enable_polygon_offset)
    state.setPolygonOffset(-0.5, -2.0);
  else
    state.setPolygonOffset(0.0, 0.0);
}


void RenderCommandList::render(const RenderGroupParameters& params, const Camera& camera)
{
  m_draw_cmd_buffer.prepare<DrawElementsIndirectCommand>(m_draw_cmds.size(),
    [&] (auto&& buffer)
    {
      buffer.write(m_draw_cmds.data(), m_draw_cmds.size());
    });

  StateModifier state;

  //FIXME - make part of State?
  gl::BindBuffer(GL_DRAW_INDIRECT_BUFFER, m_draw_cmd_buffer.getID());

  //FIXME do away with this loop
  //FIXME pass per-iteration data in SSBO
  //FIXME make iteration count a shader parameter
  //FIXME real_instance_index = gl_InstanceID % iteration_count
  //FIXME iteration_index = gl_InstanceID / iteration_count
  for (int i = 0; i < params.m_iterations; i++)
  {
    for (auto& p : m_program_cmds)
    {
      auto& program = p.program;
      assert(program);
      assert(program->isValid());

      params.setUniforms(*program, camera, i);

      if (!program->isUsable())
      {
        program->logErrorsOnce();
        m_shader_errors++;
        continue;
      }

      state.setProgram(program->getID());

      for (int i = 0; i < p.vao_cmd_count; i++)
      {
        auto& vao_cmd = m_vao_cmds.at(p.first_vao_cmd + i);

        assert(vao_cmd.vao);
        assert(vao_cmd.draw_cmd_count);

        VertexArrayObjectBinding vao_binding(*vao_cmd.vao);
        IndexBufferBinding index_buffer_binding(*vao_cmd.vao);

        assert(!GL_Interface::getCurrent()->hasError());

        try
        {
          auto offset = vao_cmd.first_draw_cmd * sizeof(DrawElementsIndirectCommand);
          gl::MultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT,
                                        reinterpret_cast<void*>(offset),
                                        vao_cmd.draw_cmd_count, 0);
        }
        catch (GLError&)
        {
          GL_Interface::getCurrent()->clearError();
          gl::GetError();
          m_draw_errors++;
        }

        m_actual_draw_calls++;
      }
    }
  }

  gl::BindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
}


void RenderCommandList::clear()
{
  m_programs.clear();
  m_program_cmds.clear();
  m_vao_cmds.clear();
  m_draw_cmds.clear();

  m_actual_draw_calls = 0;
  m_shader_errors = 0;
  m_draw_errors = 0;
}


void RenderGroupDispatcher::render(const RenderGroupParameters& params, const Camera& camera)
{
  //FIXME - make part of State?
  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_instance_buffer.getID());

  m_cmds.render(params, camera);

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
}


void RenderGroupDispatcher::clear()
{
  m_cmds.clear();
}


void RenderGroupDispatcher::addStats(Stats& stats)
{
  stats.add("Shader errors", m_cmds.getShaderErrorCount());
  stats.add("Draw commands", m_cmds.getDrawCommandCount());
  stats.add("Actual draw calls", m_cmds.getActualDrawCallCount());
  stats.add("Draw errors", m_cmds.getDrawErrorCount());

  auto buffer_elements = m_instance_buffer.getElementCount();
  auto buffer_size = m_instance_buffer.getElementSize() * buffer_elements;

  stats.add("instance buffer elements", buffer_elements);
  stats.add("instance buffer size KB", util::limitDecimalPlaces(buffer_size / 1024.f, 2));
}


} // namespace render_util::terrain

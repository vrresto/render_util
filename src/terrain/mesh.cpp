#include "mesh.h"
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace render_util::terrain
{


MultiMeshVAO::MultiMeshVAO(const std::vector<IndexedMesh>& meshes_in)
{
  size_t vertex_count = 0;
  size_t index_count = 0;

  for (auto& mesh : meshes_in)
  {
    vertex_count += mesh.vertices.size();
    index_count += mesh.getNumIndices();
  }

  std::vector<IndexedMesh::Vertex> vertices;
  std::vector<IndexedMesh::Index> indices;

  vertices.reserve(vertex_count);
  indices.reserve(index_count);

  m_meshes.reserve(meshes_in.size());

  for (auto& mesh_in : meshes_in)
  {
    auto& vertices_in = mesh_in.vertices;

    m_meshes.emplace_back();
    auto& mesh = m_meshes.back();

    mesh.element_count = mesh_in.getNumIndices();

    mesh.first_index = indices.size();
    mesh.base_vertex = vertices.size();

    vertices.insert(vertices.end(), vertices_in.begin(), vertices_in.end());

    for (auto& triangle : mesh_in.triangles)
    {
      indices.insert(indices.end(), triangle.begin(), triangle.end());
    }
  }

  gl::CreateBuffers(1, &m_vertex_buffer_id);
  assert(m_vertex_buffer_id > 0);

  gl::NamedBufferStorage(m_vertex_buffer_id,
                         vertices.size() * sizeof(IndexedMesh::Vertex),
                         vertices.data(), 0);

  constexpr auto BINDING_INDEX_VERTEX = 0;
  constexpr auto ATTRIB_INDEX_VERTEX = 0;

  constexpr auto VERTEX_COMPONENTS = 3;
  constexpr auto VERTEX_STRIDE = sizeof(IndexedMesh::Vertex);

  static_assert(VERTEX_STRIDE == VERTEX_COMPONENTS * sizeof(float));

  gl::VertexArrayVertexBuffer(getID(), BINDING_INDEX_VERTEX, m_vertex_buffer_id,
                              0, VERTEX_STRIDE);
  gl::VertexArrayAttribBinding(getID(), ATTRIB_INDEX_VERTEX, BINDING_INDEX_VERTEX);
  gl::VertexArrayAttribFormat(getID(), ATTRIB_INDEX_VERTEX, VERTEX_COMPONENTS,
                              GL_FLOAT, false, 0);
  gl::VertexArrayBindingDivisor(getID(), BINDING_INDEX_VERTEX, 0);
  gl::EnableVertexArrayAttrib(getID(), ATTRIB_INDEX_VERTEX);

  gl::NamedBufferStorage(getIndexBufferID(),
                         indices.size() * sizeof(IndexedMesh::Index),
                         indices.data(), 0);
}


MultiMeshVAO::~MultiMeshVAO()
{
  assert(m_vertex_buffer_id);
  gl::DeleteBuffers(1, &m_vertex_buffer_id);
}


} // namespace render_util::terrain

#pragma once

#include "instance_buffer.h"
#include "material.h"
#include "mesh.h"
#include <render_util/shader.h>
#include <render_util/camera.h>
#include <render_util/vao.h>
#include <render_util/framebuffer.h>
#include <util.h>

#include <vector>
#include <array>
#include <functional>


namespace render_util
{
  class StateModifier;
}


namespace render_util::terrain
{


//FIXME move
constexpr double NEAR_TEXTURE_FADE_START_DIST = 20000;
constexpr double NEAR_TEXTURE_FADE_END_DIST = 25000;


using ShaderErrors = std::vector<ShaderCreationError>;


struct RenderGroupID
{
  enum : int
  {
    WATER_MAP,
    TERRAIN,
    FOREST,
    TERRAIN_WIREFRAME,
    ENUM_SIZE
  };
};


struct DrawElementsIndirectCommand
{
  unsigned count;
  unsigned instanceCount;
  unsigned firstIndex;
  int baseVertex;
  unsigned baseInstance;
};


struct RenderGroupParameters
{
  TerrainBase::ModuleIndex::Enum m_render_module = TerrainBase::ModuleIndex::RENDERER;
  double m_draw_distance = 0;
  bool m_enable_blend = false;
  bool m_enable_cull_face = true;
  bool m_enable_wireframe = false;
  bool m_enable_polygon_offset = false;
  std::string m_program_name;
  int m_enabled_material_modules = 0;
  int m_iterations = 1;
  int m_material_mask = 0;
  int m_active_for_materials = 0;
  bool m_enable_atmosphere = true;
  bool m_double_size_frame_buffer = false;
  std::vector<MaterialLodParameters> m_lod_params; //FIXME
  std::unique_ptr<FrameBuffer> m_frame_buffer;

  virtual ~RenderGroupParameters() {}

  virtual void setShaderParameters(ShaderParameters&) const {}
  virtual void setUniforms(render_util::ShaderProgram&,
                           const render_util::Camera&,
                           int iteration) const {}

  void applyTo(StateModifier&, bool wireframe) const;
};


class RenderCommandList
{
  struct VaoCommand
  {
    IndexedVAOBase* vao {};
    int first_draw_cmd = 0;
    int draw_cmd_count = 0;
  };

  struct ProgramCommand
  {
    ShaderProgramPtr program;
    int first_vao_cmd = 0;
    int vao_cmd_count = 0;
  };

  std::vector<ProgramCommand> m_program_cmds;
  std::vector<ShaderProgramPtr> m_programs;

  std::vector<VaoCommand> m_vao_cmds;

  std::vector<DrawElementsIndirectCommand> m_draw_cmds;
  InstanceBuffer m_draw_cmd_buffer;

  int m_actual_draw_calls = 0;
  int m_shader_errors = 0;
  int m_draw_errors = 0;

public:
  void render(const RenderGroupParameters&, const Camera&);
  void clear();

  int getActualDrawCallCount() { return m_actual_draw_calls; }
  int getDrawErrorCount() { return m_draw_errors; }

  int getDrawCommandCount() { return m_draw_cmds.size(); }
  int getShaderErrorCount() { return m_shader_errors; }

  const std::vector<ShaderProgramPtr>& getPrograms() { return m_programs; }

  void addProgramCommand(ShaderProgramPtr program)
  {
    m_programs.push_back(program);

    m_program_cmds.emplace_back();
    auto& cmd = m_program_cmds.back();

    cmd.program = program;
    cmd.first_vao_cmd = m_vao_cmds.size();
  }

  void addVaoCommand(IndexedVAOBase* vao)
  {
    assert(!m_program_cmds.empty());
    auto& program_cmd = m_program_cmds.back();

    assert(program_cmd.first_vao_cmd <= m_vao_cmds.size());
    assert((program_cmd.first_vao_cmd + program_cmd.vao_cmd_count) == m_vao_cmds.size());

    if (program_cmd.vao_cmd_count)
    {
      assert(!m_vao_cmds.empty());
      if (m_vao_cmds.back().vao == vao) // no new vao cmd needed
        return;
    }

    program_cmd.vao_cmd_count++;

    m_vao_cmds.emplace_back();
    auto& cmd = m_vao_cmds.back();

    cmd.vao = vao;
    cmd.first_draw_cmd = m_draw_cmds.size();
  }

  DrawElementsIndirectCommand& addDrawCommand(IndexedVAOBase* vao)
  {
    assert(!m_vao_cmds.empty());
    auto& vao_cmd = m_vao_cmds.back();

    assert(vao_cmd.vao == vao);
    assert(vao_cmd.first_draw_cmd <= m_draw_cmds.size());
    assert((vao_cmd.first_draw_cmd + vao_cmd.draw_cmd_count) == m_draw_cmds.size());
    vao_cmd.draw_cmd_count++;

    m_draw_cmds.emplace_back();
    return m_draw_cmds.back();
  }

  void setBufferUpdateMethod(BufferUpdateMethod method)
  {
    m_draw_cmd_buffer.m_update_method = method;
  }

  void setBufferUsage(unsigned usage)
  {
    m_draw_cmd_buffer.m_usage = usage;
  }
};


class RenderGroupDispatcher
{
  RenderCommandList m_cmds;
  InstanceBuffer m_instance_buffer;

public:
  template <class InstanceData, class Handler>
  void prepare(int instances, const Handler& handler)
  {
    m_instance_buffer.prepare<InstanceData>(instances,
      [&] (auto&& buffer)
      {
        handler(m_cmds, buffer);
      });
  }

  void render(const RenderGroupParameters&, const Camera&);
  void clear();

  void setBufferUpdateMethod(BufferUpdateMethod method)
  {
    m_instance_buffer.m_update_method = method;
    m_cmds.setBufferUpdateMethod(method);
  }

  void setBufferUsage(unsigned usage)
  {
    m_instance_buffer.m_usage = usage;
    m_cmds.setBufferUsage(usage);
  }

  auto& getPrograms() { return m_cmds.getPrograms(); }
  auto getShaderErrorCount() { return m_cmds.getShaderErrorCount(); }

  void addStats(Stats&);
};


std::string getRenderGroupName(int);


template <class InstanceList, class InstanceBuffer>
void addInstances(const InstanceList& instances,
                  VertexArrayObject& vao,
                  InstanceBuffer& instance_buffer,
                  RenderCommandList& list)
{
  assert(!instances.empty());

  auto& cmd = list.addDrawCommand(&vao);

  cmd.count = vao.getNumIndices();
  cmd.firstIndex = 0;
  cmd.baseVertex = 0;
  cmd.baseInstance = instance_buffer.getPos();
  cmd.instanceCount = instances.size();

  instance_buffer.write(instances.data(), instances.size());
}


template <class InstanceList, class InstanceBuffer>
void addInstances(const InstanceList& instances,
                  MultiMeshVAO& vao, int mesh_index,
                  InstanceBuffer& instance_buffer,
                  RenderCommandList& list)
{
  assert(!instances.empty());

  auto& cmd = list.addDrawCommand(&vao);

  auto& mesh = vao.getMesh(mesh_index);

  cmd.count = mesh.element_count;
  cmd.firstIndex = mesh.first_index;
  cmd.baseVertex = mesh.base_vertex;

  cmd.baseInstance = instance_buffer.getPos();
  cmd.instanceCount = instances.size();

  instance_buffer.write(instances.data(), instances.size());
}


} // namespace render_util::terrain

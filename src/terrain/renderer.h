#pragma once

#include "quad_tree.h"
#include "buffer_updater.h"
#include <render_util/terrain_base.h>


namespace render_util
{
  class TextureState;
}


namespace render_util::terrain
{


class Module;
class ProgramFactory;
class RenderGroupBase;
class MaterialTable;
class MaterialMap;
class RenderGroupParameters;
class RenderListBase;


struct IQuadTree
{
  bool m_enable_detail = false;

  virtual void initialize(const TerrainBase::BuildParameters&,
                          const MaterialMap&, MaterialTable&,
                          unsigned detail_material_mask = 0) = 0;

  virtual void update(const Camera &camera, bool low_detail) = 0;

  virtual void setLodDistanceScale(float scale) = 0;
  virtual float getLodDistanceScale() = 0;

  virtual const std::vector<const QuadTreeNode*>& getSelectedDetailNodes() = 0;
};


} // namespace render_util::terrain

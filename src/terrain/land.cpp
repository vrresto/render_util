/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "land.h"
#include <log.h>
#include <render_util/texture_array.h>
#include <render_util/quad_2d.h>
#include <render_util/image_resample.h>
#include <render_util/texture_util.h>
#include <render_util/state.h>
#include <render_util/framebuffer.h>
#include <render_util/shader_util.h>
#include <render_util/image_loader.h>
#include <render_util/debug.h>
#include <render_util/gl_binding/gl_functions.h>

#include <set>
#include <deque>


// #define DEBUG_FAR_TEXTURE 1


using namespace render_util;
using namespace render_util::terrain;
using namespace render_util::gl_binding;
using std::endl;

using glm::vec2;
using glm::vec3;
using glm::vec4;

namespace
{


constexpr auto MAX_TERRAIN_TEXUNITS = 4;


struct FarTexture : public AttributeMapTexture
{
  bool& m_shader_load_error;
  bool m_is_base_layer = false;
  bool m_needs_refresh = true;
  ShaderProgramPtr m_program;
  std::unique_ptr<FrameBuffer> m_framebuffer;
  TextureState m_texture_state;

  FarTexture(glm::ivec2 size, bool& shader_load_error) :
    AttributeMapTexture("far_texture"),
    m_shader_load_error(shader_load_error)
  {
    m_framebuffer = std::make_unique<FrameBuffer>(size, 1);
    size_px = size;
    setTexture(m_framebuffer->getTexture(0));
  }

  void compute(std::function<void(ShaderProgramPtr)> set_uniforms);
};


void FarTexture::compute(std::function<void(ShaderProgramPtr)> set_uniforms)
{
  if (!m_needs_refresh || !m_program)
    return;

  set_uniforms(m_program);

  if (!m_program->isUsable())
  {
    m_program->logErrorsOnce();
    m_shader_load_error = true;
    return;
  }

  {
    TextureStateBinding texture_state(m_texture_state);

    {
      TemporaryFrameBufferBinding fb(*m_framebuffer, true);

      StateModifier state;
      state.setDefaults();
      state.enableCullFace(false);
      state.enableDepthTest(false);
      state.enableBlend(false);
      state.setProgram(m_program->getID());

      gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT |
                GL_STENCIL_BUFFER_BIT);

      drawFullScreenQuad();
    }
  }

  {
    TemporaryTextureBinding b(getTexture());

    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    gl::GenerateMipmap(GL_TEXTURE_2D);
  }

  m_needs_refresh = false;

#if DEBUG_FAR_TEXTURE
  {
    gl::Finish();

    LOG_INFO << "Saving image ..." << std::endl;

    int layer_number = m_is_base_layer ? 1 : 0;

    auto filename = getDebugDir() + "/far_texture" + std::to_string(layer_number) + ".tga";
    auto texture = m_framebuffer->getTexture(0);
    auto image = getTextureImage<uint8_t, 3>(texture->getID());
    render_util::saveImageToFile(filename, image.get(), render_util::ImageType::TGA);

    LOG_INFO << "Saving image ... done." << std::endl;
  }
#endif
}


std::unique_ptr<AttributeMapTexture>
createTypeMap(TerrainLandResources::TypeMap::ConstPtr type_map_in,
              const std::map<unsigned, glm::uvec3> &mapping)
{
  auto type_map = std::make_shared<ImageRGBA>(type_map_in->getSize());

  for (int y = 0; y < type_map->h(); y++)
  {
    for (int x = 0; x < type_map->w(); x++)
    {
      unsigned int orig_index = type_map_in->get(x,y) & 0x1F;

      glm::uvec3 new_index(0);

      auto it = mapping.find(orig_index);
      if (it != mapping.end())
      {
        new_index = it->second;
      }
      else
      {
        for (int i = 0; i < 4; i++)
        {
          auto it = mapping.find(orig_index - (orig_index % 4) + i);
          if (it != mapping.end())
          {
            new_index = it->second;
            break;
          }
          else
          {
            new_index.x = 0;
          }
        }
      }

      assert(new_index.y <= 0x1F+1);
      type_map->at(x,y,0) = new_index.x;
      type_map->at(x,y,1) = new_index.y;
      type_map->at(x,y,2) = new_index.z;
      type_map->at(x,y,3) = 255;
    }
  }

  TexturePtr texture;
  {
    texture = render_util::createTexture(type_map, false);
    texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }

  auto map = std::make_unique<AttributeMapTexture>("type_map");
  map->resolution_m = Land::TYPE_MAP_RESOLUTION_M;
  map->size_m = type_map->getSize() * Land::TYPE_MAP_RESOLUTION_M;
  map->size_px = type_map->getSize();
  map->setTexture(texture);

  return map;
}


void createTextureArrays(
    const std::vector<std::shared_ptr<render_util::ImageResource>> &textures_in,
    const std::vector<int> &scale_level_indices,
    std::vector<TexturePtr> &arrays_out,
    std::map<unsigned, glm::uvec3> &mapping_out)
{
  using namespace glm;
  using namespace std;
  using namespace render_util;

  std::vector<std::vector<std::shared_ptr<ImageResource>>> source_arrays;
  map<unsigned, glm::uvec3> mapping;

  std::set<int> all_texture_sizes;
  for (auto texture : textures_in)
  {
    if (!texture)
      continue;
    assert(texture->w() == texture->h());
    all_texture_sizes.insert(texture->w());
  }

  std::deque<int> texture_sizes;
  for (auto size : all_texture_sizes)
    texture_sizes.push_back(size);

  while (texture_sizes.size() > MAX_TERRAIN_TEXUNITS)
    texture_sizes.pop_front();

  auto smallest_size = texture_sizes.front();

  source_arrays.resize(texture_sizes.size());

  std::unordered_map<int, int> array_index_for_size;
  for (size_t i = 0; i < texture_sizes.size(); i++)
    array_index_for_size[texture_sizes.at(i)] = i;

  for (int i = 0; i < textures_in.size(); i++)
  {
    auto resource = textures_in.at(i);
    if (!resource)
      continue;

    auto size = glm::max(resource->getSize().x, smallest_size);

    auto index = array_index_for_size.at(size);
    auto scale_index = scale_level_indices.at(i);

    LOG_TRACE << "index: " << index << std::endl;

    auto& source_array = source_arrays.at(index);

    source_array.push_back(resource);

    glm::uvec3 type =
    {
      index,
      source_array.size()-1,
      scale_index
    };

    mapping.insert(make_pair(i, type));
  }

  mapping_out = mapping;

  for (int i = 0; i < source_arrays.size(); i++)
  {
    auto &source_array = source_arrays.at(i);
    assert(!source_array.empty());
    if (source_array.empty())
      continue;

    LOG_TRACE<<"array: "<<i<<endl;

    auto texture_size = texture_sizes.at(i);
    auto num_components = 3;

    auto texture_array = std::make_shared<TextureArray>(source_array.size(),
                                                        glm::ivec2(texture_size),
                                                        num_components);

    for (int i = 0; i < source_array.size(); i++)
    {
      auto& resource = source_array.at(i);

      assert(resource->getSize().x == resource->getSize().y);
      auto source_size = resource->getSize().x;

      LOG_INFO << "Loading texture: " << resource->getName()
               << " (" << i+1 << " of " << source_array.size() << ")"
               << std::endl;

      auto image = resource->load(0, num_components);
      assert(image->numComponents() == num_components);

      image::flipYInPlace(image);

      if (source_size != texture_size)
      {
        assert(source_size < texture_size);

        auto resampled = std::make_unique<GenericImage>(glm::ivec2(texture_size),
                                                        num_components);
        resampleImage(*image, *resampled, 1);
        image = std::move(resampled);
      }

      texture_array->setTextureImage(i, *image);
    }

    texture_array->generateMipMap();

    arrays_out.push_back(texture_array);
  }
}


} // namespace


namespace render_util::terrain
{


Land::Land(TerrainBase::BuildParameters &params)
{
  LOG_INFO << "Initializing land module ..." << std::endl;

  auto& land_res = params.resources.to<TerrainLandResources>();

  m_has_type_map = land_res.hasTypeMap();

  if (m_has_type_map)
  {
    createLandTextures(params, land_res);
  }

  m_altitudinal_zones = params.resources.getAltitudinalZones();

  m_base_layer_types_texture = Texture::create(GL_TEXTURE_2D);
  m_base_layer_types_texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  m_base_layer_types_texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  setBaseLayerTypeMapping(params.resources.getBaseLayerTypeMapping());

  LOG_INFO << "Initializing land module ... done." << std::endl;
}


void Land::createLandTextures(TerrainBase::BuildParameters &params,
                              TerrainLandResources& land_res)
{
  using namespace glm;
  using namespace std;
  using namespace render_util;
  using TextureArray = vector<ImageRGBA::ConstPtr>;

  auto &resources = params.resources;

  assert(!land_res.getLandTextures().empty());
  assert(land_res.getLandTextures().size() == land_res.getLandTexturesScale().size());

  std::vector<float> scales = land_res.getLandTexturesScale();
  std::set<float> distinct_scales;

  for (auto &scale : scales)
  {
    assert(scale != 0.0);
    assert(fract(scale) == 0.0);
    if (scale > 0.0)
      scale = 1.0 / scale;
    else
      scale = -scale;
    distinct_scales.insert(scale);
  }

  for (auto scale : distinct_scales)
    m_scale_levels.push_back(scale);

  std::vector<int> scale_level_indices;

  for (auto scale : scales)
  {
    int index = 0;
    for (auto s : m_scale_levels)
    {
      if (s == scale)
        break;
      index++;
    }
    assert(index < m_scale_levels.size());
    scale_level_indices.push_back(index);
  }

  LOG_INFO << "Creating land textures ..." << endl;

  createTextureArrays(land_res.getLandTextures(),
                      scale_level_indices,
                      m_texture_arrays,
                      m_mapping);

#if 0
  if (!textures_nm.empty())
  {
    m_enable_normal_maps = true;
    m_shader_params.set("enable_terrain_detail_nm", true);
    assert(textures.size() == textures_nm.size());

    createTextureArrays<ImageRGB>(textures_nm,
                                  texture_scale,
                                  type_map_,
                                  MAX_TEXTURE_SCALE,
                                  m_textures_nm,
                                  m_type_map_texture_nm,
                                  base_type_map_,
                                  m_base_type_map_texture_nm);
  }
#endif

//   for (int i = 0; i < m_texture_arrays.size(); i++)
//   {
//     CHECK_GL_ERROR();
//
//     if (!m_texture_arrays.at(i))
//       continue;
//
//     m_shader_params.set(string( "enable_terrain") + to_string(i), true);
//   }

  // assert(m_texture_arrays.size() <= MAX_TERRAIN_TEXUNITS);

//   for (int i = 0; i < m_textures_nm.size(); i++)
//   {
//     CHECK_GL_ERROR();
//
//     if (!m_textures_nm.at(i))
//       continue;
//
//     m_shader_params.set(string( "enable_terrain_detail_nm") + to_string(i), true);
//   }

  LOG_INFO << "Creating land textures ... done." << endl;
}


void Land::loadLayer(Layer &layer,
                                 TerrainResourcesBase::Layer &resources,
                                 bool is_base_layer,
                                 const ShaderSearchPath &shader_search_path)
{
  assert(m_has_type_map);

  if (!m_has_type_map)
    return;

  LOG_INFO << "Initializing layer land module ..." << std::endl;

  std::unique_ptr<AttributeMapTexture> type_map;

  auto& land_res = resources.to<TerrainLandResources::Layer>();

  if (resources.isBaseLayer())
  {
    auto base_layer = dynamic_cast<TerrainResourcesBase::BaseLayer*>(&resources);
    assert(base_layer);

    auto type_map_image = land_res.getTypeMap();

    type_map = std::make_unique<AttributeMapTexture>("type_map");

    auto size = type_map_image->getSize();

    auto texture = createTexture(type_map_image, false);

    texture->setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    texture->setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    texture->setParameter(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    texture->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    type_map->resolution_m = Land::TYPE_MAP_RESOLUTION_M;
    type_map->size_m = size * Land::TYPE_MAP_RESOLUTION_M;
    type_map->size_px = size;
    type_map->setTexture(texture);
  }
  else
  {
    type_map = createTypeMap(land_res.getTypeMap(), m_mapping);
  }

  auto size = land_res.getTypeMap()->getSize();

  auto map = std::make_unique<FarTexture>(size, m_shader_load_error);
  map->resolution_m = Land::TYPE_MAP_RESOLUTION_M;
  map->size_m = size * Land::TYPE_MAP_RESOLUTION_M;
  map->m_is_base_layer = resources.isBaseLayer();

  layer.addMap(std::move(map));
  layer.addMap(std::move(type_map));

  LOG_INFO << "Initializing layer land module ... done." << std::endl;
}


void Land::onLoadLayerFinished(Layer& layer)
{
  auto map = layer.getMap("far_texture");
  assert(map);
  auto far_texture = dynamic_cast<FarTexture*>(map);
  assert(far_texture);

  // if (!enable_base_layer && far_texture->m_is_base_layer)
    // return;

  layer.bindTextures(far_texture->m_texture_state);
  bindTextures(far_texture->m_texture_state);
}


void Land::bindTextures(TextureState& state) const
{
  using namespace render_util;

  if (m_has_type_map)
  {
    for (int i = 0; i < m_texture_arrays.size(); i++)
    {
      auto array = m_texture_arrays.at(i);
      if (array)
      {
        state.bind("sampler_terrain[" + std::to_string(i) + "]", *array);
      }
    }
  }

  if (m_enable_normal_maps)
  {
//     assert(m_type_map_texture_nm);
//     tm.bind(TEXUNIT_TYPE_MAP_NORMALS, m_type_map_texture_nm);
//
//     for (int i = 0; i < m_textures_nm.size(); i++)
//     {
//       CHECK_GL_ERROR();
//
//       auto textures = m_textures_nm.at(i);
//       if (!textures)
//         continue;
//
//       auto texunit = TEXUNIT_TERRAIN_DETAIL_NM0 + i;
//       assert(texunit < TEXUNIT_NUM);
//
//       tm.bind(texunit, textures);
//
//       CHECK_GL_ERROR();
//     }
  }

  //FIXME
  // assert(0);
  // tm.bind(TEXUNIT_TERRAIN_BASE_LAYER_TYPES, m_base_layer_types_texture);
}


#if 0
void Land::bindTextures(TextureManager &tm)
{
  using namespace render_util;

  for (int i = 0; i < m_texture_arrays.size(); i++)
  {
    CHECK_GL_ERROR();

    auto textures = m_texture_arrays.at(i);
    if (!textures)
      continue;

    auto texunit = TEXUNIT_TERRAIN + i;
    assert(texunit < TEXUNIT_NUM);

    tm.bind(texunit, textures);

    CHECK_GL_ERROR();
  }

  if (m_enable_normal_maps)
  {
//     assert(m_type_map_texture_nm);
//     tm.bind(TEXUNIT_TYPE_MAP_NORMALS, m_type_map_texture_nm);
// 
//     for (int i = 0; i < m_textures_nm.size(); i++)
//     {
//       CHECK_GL_ERROR();
// 
//       auto textures = m_textures_nm.at(i);
//       if (!textures)
//         continue;
// 
//       auto texunit = TEXUNIT_TERRAIN_DETAIL_NM0 + i;
//       assert(texunit < TEXUNIT_NUM);
// 
//       tm.bind(texunit, textures);
// 
//       CHECK_GL_ERROR();
//     }
  }

  tm.bind(TEXUNIT_TERRAIN_BASE_LAYER_TYPES, m_base_layer_types_texture);
}

void Land::unbindTextures(TextureManager &tm)
{
  LOG_TRACE<<endl;

  using namespace render_util;

  for (int i = 0; i < m_texture_arrays.size(); i++)
  {
    CHECK_GL_ERROR();

    auto texture = m_texture_arrays.at(i);
    if (!texture)
      continue;

    auto texunit = TEXUNIT_TERRAIN + i;
    assert(texunit < TEXUNIT_NUM);

    tm.unbind(texunit, texture->getTarget());

    CHECK_GL_ERROR();
  }

  if (m_enable_normal_maps)
  {
//     tm.unbind(TEXUNIT_TYPE_MAP_NORMALS, m_type_map_texture_nm->getTarget());
//
//     for (int i = 0; i < m_textures_nm.size(); i++)
//     {
//       CHECK_GL_ERROR();
//
//       auto texture = m_textures_nm.at(i);
//       if (!texture)
//         continue;
//
//       auto texunit = TEXUNIT_TERRAIN_DETAIL_NM0 + i;
//       assert(texunit < TEXUNIT_NUM);
//
//       tm.unbind(texunit, texture->getTarget());
//
//       CHECK_GL_ERROR();
//     }
  }
}
#endif


glm::uvec3 Land::getType(int index) const
{
  auto it = m_mapping.find(index);
  if (it != m_mapping.end())
  {
    return it->second;
  }
  else
    return glm::uvec3(0);
}


void Land::setUniforms(ShaderProgramPtr program) const
{
  if (m_has_type_map)
  {
    // for (int i = 0; i < m_texture_arrays.size(); i++)
    //   program->setUniformi("sampler_terrain[" + std::to_string(i) + "]", TEXUNIT_TERRAIN + i);

    assert(!m_scale_levels.empty());

    int i = 0;
    for (auto scale : m_scale_levels)
    {
      program->setUniform("terrain.land_texture_scale_levels[" + std::to_string(i) + "]", scale);
      i++;
    }
  }

  int i = 0;
  for (auto zone : m_altitudinal_zones)
  {
    auto prefix = "altitudinal_zones[" + std::to_string(i) + "].";
    program->setUniform(prefix + "transition_start_height", zone.transition_start_height);
    program->setUniform(prefix + "transition_end_height", zone.transition_end_height);
    program->setUniform(prefix + "color", zone.color);
    glm::vec4 type = glm::vec4(glm::vec3(getType(zone.type_index)) / 255.f, 1.f);
    program->setUniform<glm::vec4>(prefix + "type", type);
    i++;
  }
}


void Land::setShaderParameters(unsigned int material,
                                           unsigned int  detail_options,
                                           ShaderParameters &params) const
{
  bool enable_texture = (material & TerrainBase::MaterialID::LAND);
  if (!enable_texture)
    return;

  bool enable_near_texture = enable_texture && (detail_options & MaterialOption::LAND);
  bool enable_type_map = m_has_type_map && enable_near_texture;

  if (m_has_type_map)
  {
    assert(!m_scale_levels.empty());
  }
  params.set("terrain_num_land_texture_scale_levels", m_scale_levels.size());
  params.set("terrain_num_land_texture_samplers", m_texture_arrays.size());
  params.set("terrain_enable_altitudinal_zones", !m_altitudinal_zones.empty());
  params.set("terrain_num_altitudinal_zones", m_altitudinal_zones.size());

  params.set("terrain_enable_texture", enable_texture);
  params.set("terrain_enable_land", enable_texture);
  params.setIfNotDisabled("terrain_enable_near_texture", enable_near_texture);
  params.set("terrain_enable_type_map", enable_type_map);
  params.setIfNotDisabled("terrain_enable_base_layer_type_map", enable_type_map);
}


void Land::addShaders(unsigned int material,
                      int lod_options,
                      std::vector<std::string>& vertex_shaders,
                      std::vector<std::string>& fragment_shaders) const
{
  bool enable_texture = (material & TerrainBase::MaterialID::LAND);

  if (enable_texture)
  {
    vertex_shaders.push_back("terrain_texture");
    fragment_shaders.push_back("terrain_texture");
  }
}


void Land::setAnisotropy(int value)
{
  for (auto &array : m_texture_arrays)
    array->setParameter(GL_TEXTURE_MAX_ANISOTROPY_EXT, value);
}


void Land::setBaseLayerTypeMapping(const std::vector<int>& mapping)
{
  if (!m_base_layer_types || m_base_layer_types->w() != (mapping.size()+1))
  {
    m_base_layer_types =
      std::make_shared<ImageRGBA>(glm::ivec2(mapping.size() + 1, 1));
  }

  for (int i = 0; i < m_base_layer_types->w(); i++)
  {
    glm::uvec4 pixel(0);

    if (i > 0)
    {
      auto type = getType(mapping.at(i-1));
      pixel = glm::uvec4(type, 255);
    }

    for (int component = 0; component < m_base_layer_types->numComponents(); component++)
      m_base_layer_types->at(i, 0, component) = pixel[component];
  }

  setTextureImage(m_base_layer_types_texture, m_base_layer_types, false);
}


bool Land::setAltitudinalZones(const std::vector<AltitudinalZone>& zones)
{
  bool shader_parameters_changed = zones.size() != m_altitudinal_zones.size();
  m_altitudinal_zones = zones;
  return shader_parameters_changed;
}


void Land::compute(Layer& layer,
                               std::function<void(ShaderProgramPtr)> set_uniforms)
{
  auto map = layer.getMap("far_texture");
  assert(map);
  auto far_texture = dynamic_cast<FarTexture*>(map);
  assert(far_texture);

  far_texture->compute(set_uniforms);
}


void Land::reloadShaders(Layer& layer,
                                     const ShaderSearchPath &shader_search_path,
                                     bool enable_base_layer)
{
  m_shader_load_error = false;

  auto map = layer.getMap("far_texture");
  assert(map);
  auto far_texture = dynamic_cast<FarTexture*>(map);
  assert(far_texture);

  if (!enable_base_layer && far_texture->m_is_base_layer)
    return;

  ShaderParameters params;
  if (m_has_type_map)
  {
    assert(!m_scale_levels.empty());
  }
  params.set("terrain_num_land_texture_scale_levels", m_scale_levels.size());
  params.set("terrain_num_land_texture_samplers", m_texture_arrays.size());
  params.set("terrain_enable_altitudinal_zones", !m_altitudinal_zones.empty());
  params.set("terrain_num_altitudinal_zones", m_altitudinal_zones.size());
  params.set("is_base_layer", far_texture->m_is_base_layer);
  params.set("terrain_enable_far_texture", false);
  params.set("terrain_enable_type_map", true);

  params.set("terrain_enable_texture", true);
  params.set("terrain_enable_forest", false);
  params.set("terrain_enable_water", false);

  params.set("terrain_enable_base_layer", enable_base_layer);

  //FIXME
  params.set("enable_terrain_layer_scale", true);

  try
  {
    far_texture->m_program = createShaderProgram("compute_terrain_far_texture",
                                                 shader_search_path, params);
    far_texture->m_texture_state.setSamplerUniforms(*far_texture->m_program);
  }
  catch (std::exception& e)
  {
    LOG_ERROR << e.what() << std::endl;
    far_texture->m_program.reset();
    m_shader_load_error = true;
  }

  far_texture->m_needs_refresh = true;
}


template <>
std::unique_ptr<ModuleSubClass<TerrainBase::ModuleIndex::LAND>>
createModule<TerrainBase::ModuleIndex::LAND>(TerrainBase::BuildParameters& params)
{
  return std::make_unique<Land>(params);
}


} // namespace render_util::terrain

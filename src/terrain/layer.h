/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TERRAIN_LAYER_H
#define RENDER_UTIL_TERRAIN_LAYER_H

#include <render_util/texture_state.h>
#include <render_util/texture.h>
#include <render_util/shader.h>

#include <glm/glm.hpp>
#include <vector>
#include <optional>
#include <functional>


namespace render_util::terrain
{


struct AttributeMapTextureBase
{
  struct Texture
  {
    TexturePtr texture;
    std::string sampler_name;
  };

  std::vector<Texture> textures;

  AttributeMapTextureBase(std::string name) : m_name(name) {}

  virtual ~AttributeMapTextureBase() {}

  virtual void setUniforms(ShaderProgramPtr program) {}

  const std::string& getName() { return m_name; }

  std::string getUniformName(const std::string& property_name)
  {
    assert(!m_uniform_prefix.empty());
    return m_uniform_prefix + property_name;
  }

  void setUniformPrefix(std::string prefix)
  {
    m_uniform_prefix = prefix + m_name + '.';
  }

private:
  std::string m_name;
  std::string m_uniform_prefix;
};


struct AttributeMapTexture : public AttributeMapTextureBase
{
  int resolution_m;
  glm::vec2 size_m;
  glm::ivec2 size_px;

  AttributeMapTexture(std::string name) :
    AttributeMapTextureBase(name) {}

  void setTexture(TexturePtr texture)
  {
    textures.clear();
    textures.push_back({ texture, "sampler" });
  }

  TexturePtr getTexture()
  {
    return textures.at(0).texture;
  }

  void setUniforms(ShaderProgramPtr program) override
  {
    program->setUniformi(getUniformName("resolution_m"), resolution_m);
    program->setUniform(getUniformName("size_px"), size_px);
    program->setUniform(getUniformName("size_m"), size_m);
  }
};


struct Layer
{
  int layer_number = -1;
  glm::vec3 origin_m = glm::vec3(0);
  glm::vec2 origin_terrain_grid = glm::vec2(0);
  glm::vec2 size_m = glm::vec2(0);
  glm::vec2 map_texture_offset_px = glm::vec2(0);
  float scale = 1;
  std::string uniform_prefix;
  std::string name;

  Layer(int number) : layer_number(number) {}

  AttributeMapTextureBase* getMap(const std::string& name)
  {
    for (auto& map : m_maps)
    {
      if (map->getName() == name)
        return map.get();
    }
    return nullptr;
  }

  void bindTextures(TextureState& state) const
  {
    for (auto &map : m_maps)
    {
      for (int i = 0; i < map->textures.size(); i++)
      {
        auto& map_texture = map->textures.at(i);
        state.bind(map->getUniformName(map_texture.sampler_name) , *map_texture.texture);
      }
    }
  }

  void setUniforms(ShaderProgramPtr program)
  {
    program->setUniform(uniform_prefix + "size_m", size_m);
    program->setUniform(uniform_prefix + "origin_m", origin_m);
    program->setUniform(uniform_prefix + "origin_terrain_grid", origin_terrain_grid);
    program->setUniform(uniform_prefix + "scale", scale);
    program->setUniform(uniform_prefix + "map_texture_offset_px", map_texture_offset_px);

    for (auto& map : m_maps)
    {
      map->setUniforms(program);
    }
  }

  void addMap(std::unique_ptr<AttributeMapTextureBase>&& map, bool is_child = true)
  {
    if (is_child)
      map->setUniformPrefix(name + '.');
    else
      map->setUniformPrefix(name + '_');

    m_maps.push_back(std::move(map));
  }

private:
  std::vector<std::unique_ptr<AttributeMapTextureBase>> m_maps;
  std::unordered_map<std::string, AttributeMapTextureBase*> m_map_index;
};


} // namespace render_util::terrain

#endif

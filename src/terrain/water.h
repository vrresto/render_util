/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TERRAIN_WATER_H
#define RENDER_UTIL_TERRAIN_WATER_H

#include "module.h"
#include "layer.h"
#include <render_util/terrain_base.h>

namespace render_util::terrain
{


class WaterAnimation;
class WaterAnimationInterpolator;


class Water : public ModuleSubClass<TerrainBase::ModuleIndex::WATER>,
              public IMaterialModule
{
  ShaderParameters m_shader_params;
  TexturePtr m_animation_normal_maps;
  TexturePtr m_foam_texture;
  TexturePtr m_shore_wave_texture;
  TexturePtr m_texture;
  TexturePtr m_noise_texture;
  glm::vec3 m_water_color = glm::vec3(0);
  glm::vec4 m_shore_wave_hz = glm::vec4(0.05, 0.07, 0, 0);
  glm::vec4 m_shore_wave_pos = glm::vec4(0);
  std::unique_ptr<WaterAnimation> m_animation;
  std::unique_ptr<WaterAnimationInterpolator> m_animation_interpolator;

public:
  static constexpr int SMALL_WATER_MAP_RESOLUTION_M = TerrainBase::GRID_RESOLUTION_M;

  Water(TerrainBase::BuildParameters&);
  ~Water();

//   const ShaderParameters &getShaderParameters() const override { return m_shader_params; }

  void compute(std::function<void(ShaderProgramPtr)> set_uniforms) override;

  void reloadShaders(Layer& layer,
                     const ShaderSearchPath &shader_search_path,
                     bool enable_base_layer) override;

  void bindTextures(TextureState&) const override;
  void setUniforms(ShaderProgramPtr program) const override;
  void updateAnimation(float frame_delta) override;

  void setShaderParameters(unsigned int material,
                           unsigned int  detail_options,
                           ShaderParameters&) const override;

  void loadLayer(Layer&,
                 TerrainResourcesBase::Layer&,
                 bool is_base_layer/*FIXME HACK*/,
                 const ShaderSearchPath&) override;

  void addShaders(unsigned int material,
                  int lod_options,
                  std::vector<std::string>& vertex_shaders,
                  std::vector<std::string>& fragment_shaders) const override;
};


}

#endif

#pragma once

#include "buffer_updater.h"

#include <render_util/gl_binding/gl_functions.h>


namespace render_util::terrain
{


constexpr auto DEFAULT_BUFFER_USAGE = GL_DYNAMIC_DRAW;
// constexpr auto DEFAULT_BUFFER_USAGE = GL_STREAM_DRAW;

constexpr auto DEFAULT_BUFFER_UPDATE_METHOD = BufferUpdateMethod::COPY;


class InstanceBuffer
{
  GLuint m_buffer = 0;
  int m_buffer_elements = 64;
  size_t m_buffer_element_size {};

public:
  BufferUpdateMethod m_update_method = DEFAULT_BUFFER_UPDATE_METHOD;
  unsigned m_usage = DEFAULT_BUFFER_USAGE;

  InstanceBuffer()
  {
    gl_binding::gl::CreateBuffers(1, &m_buffer);
    assert(m_buffer > 0);
  }

  ~InstanceBuffer()
  {
    gl_binding::gl::DeleteBuffers(1, &m_buffer);
  }

  GLuint getID() { return m_buffer; }

  int getElementCount() const { return m_buffer_elements; }
  size_t getElementSize() const { return m_buffer_element_size; }

  template <class InstanceData, class Handler>
  void prepare(int instance_count, Handler handler)
  {
    if (sizeof(InstanceData) != m_buffer_element_size)
    {
      m_buffer_element_size = sizeof(InstanceData);
      m_buffer_elements = 64;
    }

    while (m_buffer_elements < instance_count)
      m_buffer_elements *= 2;

    switch (m_update_method)
    {
      case BufferUpdateMethod::COPY:
        handler(CopyingBufferUpdater<InstanceData>(m_buffer,
                                                   m_buffer_elements,
                                                   m_usage));
        break;
      case BufferUpdateMethod::MAP:
        handler(MappingBufferUpdater<InstanceData>(m_buffer,
                                                   m_buffer_elements,
                                                   m_usage));
        break;
    }
  }
};


} // namespace render_util::terrain


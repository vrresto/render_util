/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/texture_array.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace render_util
{


TextureArrayBase::TextureArrayBase(int num_textures, glm::ivec2 texture_size, int format,
                                   int internal_format, int type) :
  Texture(GL_TEXTURE_2D_ARRAY),
  m_num_textures(num_textures),
  m_texture_size(texture_size),
  m_format(format),
  m_internal_format(internal_format),
  m_type(type)
{
  size_t max_levels = 0;
  gl::GetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, (int*) &max_levels);
  LOG_TRACE << "max_levels: " << max_levels << std::endl;

  {
    // texture needs to be bound once to establish its target
    TemporaryTextureBinding(*this);
  }

  gl::TextureParameteri(getID(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TextureParameteri(getID(), GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  float size_max = glm::max(m_texture_size.x, m_texture_size.y);
  int levels = 1 + glm::floor(glm::log2(size_max));

  gl::TextureStorage3D(getID(), levels, m_internal_format,
                       m_texture_size.x, m_texture_size.y, m_num_textures);
}


void TextureArrayBase::setTextureImage(int index, const void* data)
{
  assert(index < m_num_textures);

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  gl::TextureSubImage3D(getID(), 0,
          0, 0, index,
          m_texture_size.x, m_texture_size.y, 1,
          m_format, m_type, data);
}


void TextureArrayBase::generateMipMap()
{
  gl::GenerateTextureMipmap(getID());
}


} // namespace render_util

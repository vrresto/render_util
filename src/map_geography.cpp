/**
 *    Rendering utilities
 *    Copyright (C) 2022  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/map_geography.h>


namespace render_util
{


MapGeography::MapGeography(const MapGeography& other)
{
  set(other);
}


MapGeography::MapGeography(glm::dvec2 origin_geodesic,
                           double meters_per_degree_longitude,
                           std::string map_projection)
{
  set(origin_geodesic, meters_per_degree_longitude, map_projection);
}


void MapGeography::set(const MapGeography& other)
{
  set(other.m_origin_geodesic, other.m_meters_per_degree_longitude,
      other.m_map_projection_id);
}


void MapGeography::set(glm::dvec2 origin_geodesic, double meters_per_degree_longitude,
                       std::string map_projection)
{
  m_origin_geodesic = origin_geodesic;
  m_meters_per_degree_longitude = meters_per_degree_longitude;
  m_map_projection_id = map_projection;
  m_map_projection = util::createMapProjection(m_map_projection_id);
}


glm::dvec2 MapGeography::getOriginGeodesic() const
{
  return m_origin_geodesic;
}


void MapGeography::setOriginGeodesic(const glm::dvec2& origin)
{
  m_origin_geodesic = origin;
}


double MapGeography::getMetersPerDegreeLongitude() const
{
    return m_meters_per_degree_longitude;
}


void MapGeography::setMetersPerDegreeLongitude(double value)
{
  m_meters_per_degree_longitude = value;
}


const std::string& MapGeography::getMapProjection() const
{
  return m_map_projection_id;
}


glm::dvec2 MapGeography::getOriginWorld(const MapGeography& world) const
{
  assert(m_map_projection_id == world.m_map_projection_id);

  return world.geodesicToLocal(m_origin_geodesic);
}


double MapGeography::getScaleWorld(const MapGeography& world) const
{
  assert(m_meters_per_degree_longitude);
  assert(world.m_meters_per_degree_longitude);
  assert(m_map_projection_id == world.m_map_projection_id);

  return world.m_meters_per_degree_longitude  /
    m_meters_per_degree_longitude;
}


void MapGeography::setScaleWorld(double scale_world, const MapGeography& world)
{
  assert(m_meters_per_degree_longitude);
  assert(world.m_meters_per_degree_longitude);
  assert(m_map_projection_id == world.m_map_projection_id);

  m_meters_per_degree_longitude = world.m_meters_per_degree_longitude / scale_world;
}


glm::dvec2 MapGeography::localToWorld(glm::dvec2 local, const MapGeography& world) const
{
  assert(m_meters_per_degree_longitude);
  assert(world.m_meters_per_degree_longitude);
  assert(m_map_projection_id == world.m_map_projection_id);

  return getOriginWorld(world) + getScaleWorld(world) * local;
}


glm::dvec2 MapGeography::geodesicToLocal(glm::dvec2 geodesic) const
{
  auto scale = m_meters_per_degree_longitude /
                m_map_projection->getMetersPerDegreeLongitude();

  auto origin_projected = m_map_projection->project(m_origin_geodesic);

  return scale * (m_map_projection->project(geodesic) - origin_projected);
}


glm::dvec2 MapGeography::localToGeodesic(glm::dvec2 local) const
{
  auto scale = m_meters_per_degree_longitude /
                m_map_projection->getMetersPerDegreeLongitude();
  auto origin_projected = m_map_projection->project(m_origin_geodesic);

  return m_map_projection->unproject((local / scale) + origin_projected);
}


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/texture.h>
// #include <render_util/texture_util.h>
// #include <render_util/texunits.h>
// #include <render_util/image_loader.h>
#include <render_util/debug.h>
#include <log.h>

#include <iostream>
#include <vector>
#include <cstdio>
#include <cassert>
#include <glm/gtc/type_ptr.hpp>
#include <GL/gl.h>

#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;
using std::vector;
using std::endl;


namespace render_util
{


void applyTextureParameter(unsigned int name, int value, unsigned int target)
{
  gl::TexParameteri(target, name, value);
}


void applyTextureParameter(unsigned int name, const glm::vec4 &value, unsigned int target)
{
  gl::TexParameterfv(target, name, glm::value_ptr(value));
}


Texture::Texture(unsigned int target) : m_target(target)
{
  gl::CreateTextures(target, 1, &m_id);
}


Texture::~Texture()
{
  if (m_id)
    gl::DeleteTextures(1, &m_id);
}


std::shared_ptr<Texture> Texture::create(unsigned int target)
{
  return std::make_shared<Texture>(target);
}


template <>
void Texture::setParameter<int>(unsigned name, const int& value)
{
  gl::TextureParameteri(m_id, name, value);
}


template <>
void Texture::setParameter<glm::vec4>(unsigned name, const glm::vec4& value)
{
  gl::TextureParameterfv(m_id, name, glm::value_ptr(value));
}


void TemporaryTextureBinding::create()
{
  int active_texunit {};
  gl::GetIntegerv(GL_ACTIVE_TEXTURE, &active_texunit);
  assert(active_texunit ==  GL_TEXTURE0);

  if (m_texunit >= 0)
    gl::ActiveTexture(GL_TEXTURE0 + m_texunit);

  switch (m_texture.getTarget())
  {
    case GL_TEXTURE_1D:
      gl::GetIntegerv(GL_TEXTURE_BINDING_1D, (GLint*) &m_previous_binding);
      break;
    case GL_TEXTURE_2D:
      gl::GetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*) &m_previous_binding);
      break;
    case GL_TEXTURE_2D_ARRAY:
      gl::GetIntegerv(GL_TEXTURE_BINDING_2D_ARRAY, (GLint*) &m_previous_binding);
      break;
    case GL_TEXTURE_3D:
      gl::GetIntegerv(GL_TEXTURE_BINDING_3D, (GLint*) &m_previous_binding);
      break;
    default:
      LOG_INFO<<std::hex<<m_texture.getTarget()<<endl;
      assert(0);
  }

  gl::BindTexture(m_texture.getTarget(), m_texture.getID());

  if (m_texunit >= 0)
    gl::ActiveTexture(GL_TEXTURE0);
}


TemporaryTextureBinding::TemporaryTextureBinding(Texture& texture, int texunit) :
  m_texture(texture),
  m_texunit(texunit)
{
  create();
}


TemporaryTextureBinding::TemporaryTextureBinding(TexturePtr texture, int texunit) :
  m_texture(*texture),
  m_texunit(texunit)
{
  create();
}


TemporaryTextureBinding::~TemporaryTextureBinding()
{
  if (m_texunit >= 0)
    gl::ActiveTexture(GL_TEXTURE0 + m_texunit);

  gl::BindTexture(m_texture.getTarget(), m_previous_binding);

  gl::ActiveTexture(GL_TEXTURE0);
}


} // namespace render_util

#include <render_util/terrain_resources.h>
#include <render_util/terrain_base.h>


namespace render_util
{


TerrainResourcesBase::Layer& TerrainResourcesBase::getDetailLayer()
{
  return getLayer(TerrainBase::LAYER_DETAIL);
}


TerrainResourcesBase::Layer& TerrainResourcesBase::getBaseLayer()
{
  return getLayer(TerrainBase::LAYER_BASE);
}


} //namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/debug.h>
#include <render_util/debug_config.h>
#include <util.h>

namespace render_util
{


std::string getDebugDir(std::string sub_dir_name)
{
  std::string base_dir(DEBUG_DIR);
  assert(!base_dir.empty());

  auto dir = base_dir + "/";
  if (!sub_dir_name.empty())
    dir += sub_dir_name + "/";

  util::mkdir(dir);

  return dir;
}


} // namespace render_util

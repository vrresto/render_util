/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/shader_util.h>
#include <render_util/texunits.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <set>

#include <render_util/gl_binding/gl_functions.h>
#include <log.h>

using namespace std;


namespace
{


struct DefinitionFile
{
  ifstream in;
  std::string path;
};


class Arguments
{
  std::vector<std::string> m_args;

public:
  Arguments(const std::string& line)
  {
    m_args = util::tokenize(line);
  }

  bool empty() { return m_args.empty(); }

  const std::string& at(size_t index)
  {
    if (index < m_args.size())
    {
      return m_args.at(index);
    }
    else
    {
      throw std::runtime_error("Argument requested: " + std::to_string(index)
                               + " - Arguments: "
                               + std::to_string(m_args.size()));
    }
  }
};


DefinitionFile openDefinition(const std::string &definition,
                              const render_util::ShaderSearchPath& search_path)
{
  for (auto &dir : search_path)
  {
    DefinitionFile def_file;

    def_file.path = dir + '/' + definition + ".program";
    def_file.in.open(def_file.path);

    if (!def_file.in.good())
    {
      LOG_TRACE << "Failed to open " << def_file.path << endl;
    }
    else
    {
      return def_file;
    }
  }

  throw std::runtime_error("Failed to open " + definition);
}


void readDefinition(ifstream &in,
                    set<string> &vertex_shaders,
                    set<string> &fragment_shaders,
                    set<string> &geometry_shaders,
                    set<string> &compute_shaders,
                    set<string> &texunits,
                    render_util::ShaderParameters &params,
                    set<string> &includes)
{
  for (int line_nr = 1; in.good(); line_nr++)
  {
    string line;
    getline(in, line);
    if (line.empty())
      continue;
    if (line[0] == '#')
      continue;

    try
    {
      Arguments args(line);

      if (args.empty())
        continue;

      auto type = args.at(0);
      auto name = args.at(1);

      if (type == "vert")
        vertex_shaders.insert(name);
      else if (type == "frag")
        fragment_shaders.insert(name);
      else if (type == "geom")
        geometry_shaders.insert(name);
      else if (type == "compute")
        compute_shaders.insert(name);
      else if (type == "texunit")
        texunits.insert(name);
      else if (type == "include")
        includes.insert(name);
      else if(type == "parameter")
      {
        if (!params.contains(name))
        {
          auto arg = args.at(2);
          params.set(name, std::stoi(arg));
        }
      }
      else
      {
        throw std::runtime_error("Unknown directive: " + type);
      }
    }
    catch (std::exception& e)
    {
      throw std::runtime_error("Error at line " + std::to_string(line_nr)
                               + ": " + e.what());
    }
  }
}


} // namespace


namespace render_util
{


ShaderProgramPtr createShaderProgram(const std::string &definition,
  const ShaderSearchPath &search_path)
{
  ShaderParameters params;
  return createShaderProgram(definition, search_path, params);
}

ShaderProgramPtr createShaderProgram(const std::string &definition,
                                     const ShaderSearchPath &search_path,
                                     const ShaderParameters &params,
                                     const std::map<unsigned int, std::string> &attribute_locations,
                                     const std::vector<std::string> vertex_shaders,
                                     const std::vector<std::string> fragment_shaders)
{
  return createShaderProgram(vector<string>({definition}), search_path, params,
                             attribute_locations, vertex_shaders, fragment_shaders);
}


ShaderProgramPtr createShaderProgram(const std::vector<std::string> &definitions,
                                     const ShaderSearchPath &search_path,
                                     const ShaderParameters &params_,
                                     const std::map<unsigned int, std::string> &attribute_locations,
                                     const std::vector<std::string> vertex_shaders_,
                                     const std::vector<std::string> fragment_shaders_)
{
//   LOG_TRACE<<"creating shader program: "<<definition<<endl;

  assert(!definitions.empty());
  auto name = definitions.front();

  vector<string> vertex_shaders;
  vector<string> fragment_shaders;
  vector<string> geometry_shaders;
  vector<string> compute_shaders;
  vector<string> texunits;

  auto params = params_;

  try
  {
    set<string> vertex_shaders_set;
    set<string> fragment_shaders_set;
    set<string> geometry_shaders_set;
    set<string> compute_shaders_set;
    set<string> texunits_set;

    vertex_shaders_set.insert(vertex_shaders_.begin(), vertex_shaders_.end());
    fragment_shaders_set.insert(fragment_shaders_.begin(), fragment_shaders_.end());

    set<string> processed_definitions;
    set<string> new_definitions;

    new_definitions.insert(definitions.begin(), definitions.end());

    constexpr int max_iterations = 10;
    int iteration = 0;
    while (!new_definitions.empty())
    {
      assert(iteration < max_iterations);

      set<string> includes;
      for (auto &def : new_definitions)
      {
        auto def_file = openDefinition(def, search_path);
        try
        {
          readDefinition(def_file.in, vertex_shaders_set, fragment_shaders_set,
                        geometry_shaders_set, compute_shaders_set, texunits_set,
                        params, includes);
          processed_definitions.insert(def);
        }
        catch (std::exception& e)
        {
          throw std::runtime_error("Failed to process " + def_file.path + ": " + e.what());
        }
      }

      new_definitions.clear();

      for (auto &include : includes)
      {
        if (processed_definitions.find(include) == processed_definitions.end())
        {
          new_definitions.insert(include);
        }
      }

      iteration++;
    }

    vertex_shaders.assign(vertex_shaders_set.begin(), vertex_shaders_set.end());
    fragment_shaders.assign(fragment_shaders_set.begin(), fragment_shaders_set.end());
    geometry_shaders.assign(geometry_shaders_set.begin(), geometry_shaders_set.end());
    compute_shaders.assign(compute_shaders_set.begin(), compute_shaders_set.end());
    texunits.assign(texunits_set.begin(), texunits_set.end());
  }
  catch (std::exception& e)
  {
    throw ShaderCreationError(name, e.what());
  }

  ShaderProgramPtr program = make_shared<ShaderProgram>(name,
                                                        vertex_shaders,
                                                        fragment_shaders,
                                                        geometry_shaders,
                                                        compute_shaders,
                                                        search_path, true, attribute_locations, params);

  return program;
}


} // namespace

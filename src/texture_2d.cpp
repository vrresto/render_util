/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/texture_2d.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace render_util
{


Texture2DBase::Texture2DBase(int num_lods, glm::ivec2 texture_size, int format,
                             int internal_format, int type) :
  Texture(GL_TEXTURE_2D),
  m_texture_size(texture_size),
  m_format(format),
  m_internal_format(internal_format),
  m_type(type),
  m_lods(num_lods)
{

  {
    // texture needs to be bound once to establish its target
    TemporaryTextureBinding(*this);
  }

  gl::TextureParameteri(getID(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl::TextureParameteri(getID(), GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  if (m_lods == 0)
  {
    float size_max = glm::max(m_texture_size.x, m_texture_size.y);
    m_lods = 1 + glm::floor(glm::log2(size_max));
  }

  gl::TextureStorage2D(getID(), m_lods, m_internal_format,
                       m_texture_size.x, m_texture_size.y);
}


void Texture2DBase::setTextureImage(int lod, glm::ivec2 size, const void* data)
{
  assert(lod < m_lods);

  StateModifier state;
  state.setPixelStoreUnpackAlignment(1);

  gl::TextureSubImage2D(getID(), lod,
          0, 0,
          size.x, size.y,
          m_format, m_type, data);
}


void Texture2DBase::generateMipMap()
{
  gl::GenerateTextureMipmap(getID());
}


} // namespace render_util

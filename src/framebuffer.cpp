/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/framebuffer.h>
#include <render_util/gl_binding/gl_functions.h>

static_assert((GL_COLOR_ATTACHMENT0 + 1) == GL_COLOR_ATTACHMENT1);
static_assert((GL_COLOR_ATTACHMENT0 + 15) == GL_COLOR_ATTACHMENT15);


using render_util::TexturePtr;
using namespace render_util::gl_binding;


namespace render_util
{


FrameBufferBase::FrameBufferBase(glm::ivec2 size, size_t num_draw_buffers) :
  m_size(size)
{
  m_depth_texture = render_util::Texture::create(GL_TEXTURE_2D);

  updateDepthTextureSize();

  create();

  gl::NamedFramebufferTexture(m_id, GL_DEPTH_ATTACHMENT, m_depth_texture->getID(), 0);

  std::vector<GLuint> draw_buffers;

  for (int i = 0; i < num_draw_buffers; i++)
  {
    draw_buffers.push_back(GL_COLOR_ATTACHMENT0 + i);
  }

  gl::NamedFramebufferDrawBuffers(m_id, draw_buffers.size(), draw_buffers.data());
}


FrameBufferBase::~FrameBufferBase()
{
  assert(m_id);
  assert(gl::IsFramebuffer(m_id));

  gl::DeleteFramebuffers(1, &m_id);

  m_id = 0;
}


void FrameBufferBase::create()
{
  FORCE_CHECK_GL_ERROR();

  gl::GenFramebuffers(1, &m_id);
  FORCE_CHECK_GL_ERROR();

  gl::BindFramebuffer(GL_FRAMEBUFFER, m_id);
  gl::BindFramebuffer(GL_FRAMEBUFFER, 0);
  FORCE_CHECK_GL_ERROR();

  assert(gl::IsFramebuffer(m_id));
}


void FrameBufferBase::updateDepthTextureSize()
{
  assert(m_size != glm::ivec2(0));

  render_util::TemporaryTextureBinding binding(m_depth_texture);
  gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  gl::TexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_size.x, m_size.y,
                0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
}


FrameBuffer::FrameBuffer(glm::ivec2 size, size_t num_draw_buffers) :
  FrameBufferBase(size, num_draw_buffers),
  m_color_textures(num_draw_buffers)
{
  updateColorTexturesSize();

  // gl::NamedFramebufferTexture(m_id, GL_DEPTH_ATTACHMENT, m_depth_texture->getID(), 0);

  // gl::NamedFramebufferReadBuffer(m_id, GL_COLOR_ATTACHMENT0);

  // std::vector<GLuint> draw_buffers;

  for (int i = 0; i < m_color_textures.size(); i++)
  {
    gl::NamedFramebufferTexture(m_id, GL_COLOR_ATTACHMENT0 + i,
                                m_color_textures.at(i)->getID(), 0);

    // draw_buffers.push_back(GL_COLOR_ATTACHMENT0 + i);
  }

  // gl::NamedFramebufferDrawBuffers(m_id, draw_buffers.size(), draw_buffers.data());

  // FORCE_CHECK_GL_ERROR();
}



void FrameBuffer::updateColorTexturesSize()
{
  assert(m_size != glm::ivec2(0));

  for (int i = 0; i < m_color_textures.size(); i++)
  {
    auto &texture = m_color_textures[i];

    if (!texture)
      texture = render_util::Texture::create(GL_TEXTURE_2D);
    render_util::TemporaryTextureBinding binding(texture);
    gl::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl::TexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_size.x, m_size.y,
                  0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
  }
}


void FrameBuffer::setSize(glm::ivec2 size)
{
  if (size == m_size)
    return;

  m_size = size;

  updateDepthTextureSize();
  updateColorTexturesSize();
}


TemporaryFrameBufferBinding::TemporaryFrameBufferBinding(FrameBufferBase& fb, bool set_viewport) :
  m_set_viewport(set_viewport)
{
  gl::GetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &m_previous_binding);

  gl::BindFramebuffer(GL_DRAW_FRAMEBUFFER, fb.getID());

  if (m_set_viewport)
  {
    gl::GetIntegerv(GL_VIEWPORT, m_prev_viewport);
    gl::Viewport(0, 0, fb.getSize().x, fb.getSize().y);
  }
}


TemporaryFrameBufferBinding::~TemporaryFrameBufferBinding()
{
  if (m_set_viewport)
  {
    gl::Viewport(m_prev_viewport[0], m_prev_viewport[1],
                 m_prev_viewport[2], m_prev_viewport[3]);
  }
  gl::BindFramebuffer(GL_DRAW_FRAMEBUFFER, m_previous_binding);
}


} // namespace render_util

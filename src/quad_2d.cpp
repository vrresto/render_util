/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <render_util/quad_2d.h>
#include <render_util/shader_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>

#include <glm/glm.hpp>


using namespace render_util::gl_binding;
using namespace glm;


namespace render_util
{


Quad2D::Quad2D(const ShaderSearchPath &shader_search_path,
               std::string shader_program)
{
  if (shader_program.empty())
    shader_program = "quad_2d";

  shader = createShaderProgram(shader_program, shader_search_path);
  assert(shader->isValid());
  setColor(vec4(0,0,0,1));
}


Quad2D::Quad2D(ShaderProgramPtr shader_program) : shader(shader_program)
{
  assert(shader->isValid());
  setColor(vec4(0,0,0,1));
}


void Quad2D::setColor(glm::vec4 color)
{
  shader->setUniform("color", color);
}


void Quad2D::draw(int x, int y, int width, int height)
{
  draw(ivec2(x,y), ivec2(width, height));
}


void Quad2D::draw(vec2 pos, vec2 size)
{
  draw(pos, size, glm::vec2(0), glm::vec2(1));
}


void Quad2D::draw(glm::vec2 pos, glm::vec2 size,
                  glm::vec2 texcoord_origin,
                  glm::vec2 texcoord_extent)
{
  StateModifier state;
  state.setProgram(shader->getID());

  int viewport[4];
  gl::GetIntegerv(GL_VIEWPORT, viewport);

  vec2 viewport_size(viewport[2], viewport[3]);

  vec2 pos_ndc = pos / viewport_size;
  pos_ndc = (2.f * pos_ndc) - vec2(1);

  vec2 size_ndc = 2.f * (size / viewport_size);

  gl::Begin(GL_QUADS);

  gl::TexCoord2f(texcoord_origin.x, texcoord_origin.y);
  gl::Vertex3f(pos_ndc.x, pos_ndc.y, 0);

  gl::TexCoord2f(texcoord_origin.x + texcoord_extent.x, texcoord_origin.y);
  gl::Vertex3f(pos_ndc.x + size_ndc.x, pos_ndc.y, 0);

  gl::TexCoord2f(texcoord_origin.x + texcoord_extent.x, texcoord_origin.y + texcoord_extent.y);
  gl::Vertex3f(pos_ndc.x + size_ndc.x, pos_ndc.y + size_ndc.y, 0);

  gl::TexCoord2f(texcoord_origin.x, texcoord_origin.y + texcoord_extent.y);
  gl::Vertex3f(pos_ndc.x, pos_ndc.y + size_ndc.y, 0);

  gl::End();
}


void drawFullScreenQuad()
{
  gl::Begin(GL_QUADS);
  gl::Vertex3f(-1, -1, 0);
  gl::Vertex3f(-1, +1, 0);
  gl::Vertex3f(+1, +1, 0);
  gl::Vertex3f(+1, -1, 0);
  gl::End();
}


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/texture_state.h>
#include <render_util/context.h>
#include <render_util/texunits.h>
#include <render_util/texture_util.h>
#include <render_util/image_loader.h>
#include <render_util/debug.h>
#include <render_util/gl_binding/gl_functions.h>
#include <log.h>

using namespace render_util;
using namespace render_util::gl_binding;
using std::endl;

namespace
{


thread_local TextureState* g_active = nullptr;


#if 0
Texunit::Target targetFromGL(unsigned unit)
{
  switch (unit)
  {
    case GL_TEXTURE_2D:
      return Texunit::TEXTURE_2D;
    case GL_TEXTURE_3D:
      return Texunit::TEXTURE_3D;
    case GL_TEXTURE_2D_ARRAY:
      return Texunit::TEXTURE_2D_ARRAY;
    default:
      abort();
  }
}


void dumpTexture2D(unsigned texunit, unsigned id)
{
  auto name = getDebugDir("texunit_dump") + "/" + getTexUnitName(texunit) + "_2d.tga";

  auto image = getTextureImage<uint8_t, 3>(id);

  render_util::saveImageToFile(name, image.get(), render_util::ImageType::TGA);
}

#endif

} // namespace

namespace render_util
{


TextureState::TextureState()
{
  gl::GetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
                  reinterpret_cast<int*>(&m_max_texunits));
}


TextureState::~TextureState()
{
  assert(!m_is_bound);
}


void TextureState::unbind()
{
  assert(m_is_bound);
  assert(g_active == this);

  for (unsigned i = 0; i < m_texunits.size(); i++)
  {
    gl::BindTextureUnit(i, 0);
  }

  g_active = nullptr;
  m_is_bound = false;
}


void TextureState::bind()
{
  assert(!m_is_bound);
  assert(!g_active);

  g_active = this;
  m_is_bound = true;

  for (unsigned i = 0; i < m_texunits.size(); i++)
    gl::BindTextureUnit(i, m_texunits[i].texture);
}


TextureState::TexUnit& TextureState::getTexUnitForUniform(const std::string uniform)
{
  auto it = m_texunit_for_uniform.find(uniform);
  if (it != m_texunit_for_uniform.end())
  {
    return m_texunits.at(it->second);
  }
  else
  {
    assert(m_texunits.size() < m_max_texunits);
    m_texunits.emplace_back();
    m_texunits.back().uniform = uniform;
    return m_texunits.back();
  }
}


void TextureState::bind(const std::string& uniform, Texture& texture)
{
  assert(!m_is_bound);
  getTexUnitForUniform(uniform).texture = texture.getID();
}


void TextureState::setSamplerUniforms(ShaderProgram& program) const
{
  for (unsigned i = 0; i < m_texunits.size(); i++)
    program.setUniformi(m_texunits[i].uniform, i);
}


void TextureState::dump() const
{
  if (m_texunits.empty())
  {
    LOG_WARNING << "Empty texture state." << std::endl;
    return;
  }

  LOG_INFO << "Texture state:" << std::endl;
  for (auto& unit : m_texunits)
  {
    LOG_INFO << unit.uniform << ": " << unit.texture << std::endl;
    // dumpTexture2D(it.first, it.second.target_bindings.at(Texunit::TEXTURE_2D));
  }
}


TextureStateBinding::TextureStateBinding(TextureState& state) :
  m_state(state)
{
  m_state.bind();
}


TextureStateBinding::~TextureStateBinding()
{
  m_state.unbind();
}


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util.h>
#include <render_util/render_util.h>
#include <render_util/camera_3d.h>
#include <render_util/physics.h>
#include <render_util/earth_curvature_map.h>

#include <iostream>
#include <filesystem>
#include <iomanip>
#include <ctime>
#include <sstream>


bool util::fileExists(std::string path)
{
  return std::filesystem::is_regular_file(path);
}


void util::mkdir(const std::string &path_, bool recursive)
{
  if (recursive)
    std::filesystem::create_directories(path_);
  else
    std::filesystem::create_directory(path_);
}


std::string util::makeTimeStampString()
{
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);

  std::ostringstream oss;
  oss << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");

  return oss.str();
}


void render_util::updateUniforms(render_util::ShaderProgram& program,
                                 const render_util::Camera &camera)
{
  using Vec3 = render_util::Camera::Vec3;

  glm::mat4 mvp(camera.getProjectionMatrixFarD() * camera.getWorld2ViewMatrixD());

  program.setUniform("camera_pos", camera.getPos());
  program.setUniform("projection", camera.getProjectionMatrixFar());
  program.setUniform("world_to_view", camera.getWorld2ViewMatrix());
  program.setUniform("view_to_world", camera.getView2WorldMatrix());
  program.setUniform("world_to_view_rotation", camera.getWorldToViewRotation());

  auto camera_3d = dynamic_cast<const Camera3D*>(&camera);
  if (camera_3d)
    program.setUniform("ndc_to_view", camera_3d->getNDCToView());

  auto earth_center =
    glm::vec3(camera.getPos().x, camera.getPos().y, -physics::EARTH_RADIUS);
  program.setUniform("earth_center", earth_center);

  program.setUniform("sun_size", glm::vec2(tan(physics::SUN_ANGULAR_RADIUS),
                                            cos(physics::SUN_ANGULAR_RADIUS )));

  program.setUniform("curvature_map_max_distance", earth_curvature_map::curvature_map_max_distance);
}

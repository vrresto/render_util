/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "grid_2d.h"
#include <render_util/shader_util.h>
#include <render_util/render_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util;
using namespace render_util::gl_binding;


namespace render_util::viewer
{


Grid2D::Grid2D(const ShaderSearchPath &shader_search_path)
{
  m_program = createShaderProgram("line_2d", shader_search_path);
  m_program->setUniform("color", glm::vec4(0,0,0,1));
}


void Grid2D::render(const Camera2D &camera, TextRenderer &text_renderer)
{
  auto &program = m_program;
  render_util::updateUniforms(*program, camera);

  auto viewport_size = camera.getViewportSize();

  auto viewport_origin_world = camera.getViewportOriginWorld();
  auto area_size = camera.getAreaSize();

  int grid_res_px_min = 50;

  double grid_res = m_resolution;

  for (int i = 0; i < 10; i++)
  {
    if ((camera.getWorldToViewportScale() * grid_res) >= grid_res_px_min)
      break;
    grid_res *= 10;
  }

  auto viewport_origin_in_grid_space = (viewport_origin_world - m_origin) / grid_res;
  auto grid_start = floor(viewport_origin_in_grid_space);

  glm::ivec2 grid_size = glm::ivec2(glm::ceil(area_size / grid_res)) + glm::ivec2(1);

  StateModifier state;
  state.setDefaults();
  state.enableDepthTest(false);
  state.enableBlend(true);
  state.setProgram(program->getID());

  for (int x = 0; x < grid_size.x; x++)
  {
    auto x_grid = grid_start.x + x;
    auto x_world = m_origin.x + (x_grid * grid_res);

    auto y_0_world = viewport_origin_world.y;
    auto y_1_world = viewport_origin_world.y + area_size.y;

    auto km = x_world / 1000;

    char label[10];
    if (glm::fract(km) != 0)
      snprintf(label, sizeof(label), "%.1f", km);
    else
      snprintf(label, sizeof(label), "%.0f", km);

    // FIXME: don't use world coordinates
    gl::Begin(GL_LINES);
    gl::Vertex2f(x_world, y_0_world);
    gl::Vertex2f(x_world, y_1_world);
    gl::End();

    text_renderer.SetColor(1,1,1);

    auto viewport_coord = camera.worldToViewport(Camera2D::Vec2(x_world, y_0_world));
    text_renderer.DrawText(label, viewport_coord.x, viewport_coord.y);

    viewport_coord = camera.worldToViewport(Camera2D::Vec2(x_world, y_1_world));
    text_renderer.DrawText(label, viewport_coord.x, viewport_coord.y - text_renderer.getCharHeight());
  }

  for (int y = 0; y < grid_size.y; y++)
  {
    auto y_grid = grid_start.y + y;
    auto y_world = m_origin.y + (y_grid * grid_res);

    auto x_0_world = viewport_origin_world.x;
    auto x_1_world = viewport_origin_world.x + area_size.x;

    auto km = y_world / 1000;

    char label[10];
    if (glm::fract(km) != 0)
      snprintf(label, sizeof(label), "%.1f", km);
    else
      snprintf(label, sizeof(label), "%.0f", km);

    int label_width = text_renderer.getCharWidth() * strlen(label);

    // FIXME: don't use world coordinates
    gl::Begin(GL_LINES);
    gl::Vertex2f(x_0_world, y_world);
    gl::Vertex2f(x_1_world, y_world);
    gl::End();

    text_renderer.SetColor(1,1,1);

    auto viewport_coord = camera.worldToViewport(Camera2D::Vec2(x_0_world, y_world));
    text_renderer.DrawText(label,
                           viewport_coord.x,
                           viewport_size.y - viewport_coord.y);

    viewport_coord = camera.worldToViewport(Camera2D::Vec2(x_1_world, y_world));
    text_renderer.DrawText(label,
                           viewport_coord.x - label_width,
                           viewport_size.y - viewport_coord.y);
  }
}


} // namespace render_util::viewer

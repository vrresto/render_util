/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_GRID_2D
#define RENDER_UTIL_VIEWER_GRID_2D

#include "camera_2d.h"
#include <render_util/shader.h>
#include <text_renderer/text_renderer.h>

namespace render_util::viewer
{


class Grid2D
{
  ShaderProgramPtr m_program;

public:
  Camera2D::Unit m_resolution = 100.0;
  Camera2D::Vec2 m_origin {};

  Grid2D(const ShaderSearchPath &shader_search_path);
  void render(const Camera2D&, TextRenderer&);
};


} // namespace render_util::viewer

#endif

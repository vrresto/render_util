/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_TERRAIN_VIEWER_SCENE_BASE_H
#define RENDER_UTIL_VIEWER_TERRAIN_VIEWER_SCENE_BASE_H

#include <render_util/viewer/scene.h>
#include <render_util/viewer/parameterizable.h>
#include <render_util/viewer/bit_selector.h>
#include <render_util/terrain_base.h>
#include <util/integer_sequence.h>

namespace render_util::viewer
{


class TerrainViewerSceneBase : public Scene
{
  struct ShaderOption
  {
    std::string display_name;
    std::string name;
    bool value {};
  };

  int m_anisotropy = 1;
  bool m_enable_curvature = false;
  bool m_enable_detail_layer = true;
  glm::vec2 m_detail_map_clip_max = glm::vec2(1);
  glm::vec2 m_detail_map_clip_min = glm::vec2(0);

  std::vector<ShaderOption> m_shader_options  =
  {
    { "Far texture", "terrain_enable_far_texture", true },
    { "Near texture", "terrain_enable_near_texture", true},
    { "Base layer type map", "terrain_enable_base_layer_type_map", false },
    { "Altitudinal zone textures", "terrain_enable_altitudinal_zone_texture", false },
    { "Debug LOD", "terrain_enable_lod_debug", false },
    { "Enable terrain layer blending", "enable_terrain_layer_blend", false },
    { "Enable detail map clip", "enable_detail_map_clip", false },
    { "Use pass_normal", "terrain_use_pass_normal", false },
    { "TexelFetch height map", "terrain_texelfetch_heightmap", false },
    { "Land texture offset", "terrain_enable_land_texture_offset", false },
    { "Forest layers", "terrain_enable_forest_pass", true },
    { "Forest alpha map workaround", "terrain_forest_enable_alpha_map_precision_workaround", true },
    { "Forest layer separate alpha", "terrain_forest_layers_separate_alpha", true },
    { "Water map pass", "terrain_enable_water_map_pass", false },
    { "Wave parallax mapping", "terrain_water_enable_parallax", true },
    { "Water map smooth sampling", "terrain_water_map_smooth_sampling", false },
    { "Shore waves", "terrain_enable_shore_waves", false },
    { "Waves", "terrain_water_enable_animation", false },
  };

protected:
  Parameters m_rendering_parameters;
  bool m_enable_atmosphere = false;

  TerrainViewerSceneBase() : m_rendering_parameters("Rendering")
  {
    addAction("reload_shaders", "Reload shaders", [this] { reloadShaders(); });
    // addAction("dump_textures", "Dump textures", [this] { getTextureManager().dump(); }); FIXME
  }

  virtual TerrainBase& getTerrain() = 0;

  virtual render_util::ShaderParameters makeShaderParameters()
  {
    render_util::ShaderParameters params;

    for (auto& it : m_shader_options)
      params.set<bool>(it.name, it.value);

    params.set("enable_atmosphere", m_enable_atmosphere);
    params.set("enable_curvature", m_enable_curvature);
    params.set("terrain_enable_detail_layer", m_enable_detail_layer);

    return params;
  }

  void updateUniforms(ShaderProgram& program,
                      const render_util::Camera &camera) override
  {
    Scene::updateUniforms(program, camera);

    program.setUniform("detail_map_clip_min", m_detail_map_clip_min);
    program.setUniform("detail_map_clip_max", m_detail_map_clip_max);
  }

  bool hasError() override
  {
    return getTerrain().hasError();
  }

  std::vector<std::string> getErrors() override
  {
    return getTerrain().getErrors();
  }

  std::vector<ShaderCreationError> getShaderErrors() override
  {
    return getTerrain().getShaderErrors();
  }

  bool isCurvatureEnabled() { return m_enable_curvature; }

  void updateShaderParameters()
  {
    getTerrain().setShaderParameters(makeShaderParameters());
  }

  virtual void reloadShaders()
  {
    updateShaderParameters();
    getTerrain().reloadShaders();
  }

  void addParameters()
  {
    auto addShaderOption = [this] (std::string name, bool &option)
    {
      auto get = [&option] ()
      {
        return option;
      };
      auto set = [this, &option] (bool value)
      {
        if (value != option)
        {
          option = value;
          reloadShaders();
        }
      };
      m_rendering_parameters.addBool(name, get, set);
    };

    Scene::addParameters();

    {
      auto get = [this] ()
      {
        return getTerrain().getLodDistanceScale();
      };
      auto set = [this] (auto value)
      {
        getTerrain().setLodDistanceScale(value);
      };
      m_rendering_parameters.add<float>("LOD distance scale", get, set, 0.001);
    }

    {
      auto get = [this] ()
      {
        return m_anisotropy;
      };
      auto set = [this] (int value)
      {
        if (value != m_anisotropy)
        {
          m_anisotropy = value;
          getTerrain().setAnisotropy(m_anisotropy);
        }
      };
      m_rendering_parameters.add("Anisotropy", get, set, 1.0,
                                 NumericParameter::HINT_PREFER_SLIDER, 1, 16);
    }

    m_rendering_parameters.addVector("detail map clip min", m_detail_map_clip_min, 0.001);
    m_rendering_parameters.addVector("detail map clip max", m_detail_map_clip_max, 0.001);

    addShaderOption("Detail layer", m_enable_detail_layer);
//     addShaderOption("Detail layer textures", "terrain_enable_detail_layer_textures",
//                     m_enable_detail_layer_textures);

    {
      auto get = [this] ()
      {
        return getTerrain().isBaseLayerEnabled();
      };
      auto set = [this] (bool value)
      {
        getTerrain().enableBaseLayer(value);
      };
      m_rendering_parameters.addBool("Base layer", get, set);
    }

    for (auto& option : m_shader_options)
      addShaderOption(option.display_name, option.value);

    addShaderOption("Atmosphere", m_enable_atmosphere);
    addShaderOption("Curvature", m_enable_curvature);

    {
      using Group = BitSelector<decltype(TerrainBase::ModuleMask::ALL),
                                TerrainBase::ModuleMask::ALL>;

      Group::Wrapper wrapper =
      {
        .get = [this] () { return getTerrain().getEnabledModules(); },
        .set = [this] (auto value) { getTerrain().setEnabledModules(value); },
      };

      auto group = std::make_unique<Group>("Modules", wrapper);


      util::visit(TerrainBase::ModuleIndex::AllValues(),
                  [&] (auto index)
                  {
                    group->add(TerrainBase::getModuleName(index),
                               TerrainBase::getModuleMask(index));
                  });

      m_rendering_parameters.viewer::ParametersBase::add(std::move(group));
    }


    addUniform("sea_roughness", 0.5f, 0.01);
    addVectorUniform("terrain_land_texture_offset", glm::vec2(0), 0.01);

    {
      auto get = [this] ()
      {
        return getTerrain().getMapTextureOffset(0);
      };
      auto set = [this] (auto value)
      {
        getTerrain().setMapTextureOffset(0, value);
      };
      m_world_parameters.addVector<glm::vec2>("Map texture offset", get, set, 0.1);
    }

    for (int i = 0; i < getTerrain().getRenderGroups(); i++)
    {
      auto get = [this, i] () -> bool
      {
        return getTerrain().isRenderGroupEnabled(i);
      };
      auto set = [this, i] (bool enable)
      {
        getTerrain().enableRenderGroup(i, enable);
      };
      m_rendering_parameters.addBool("Render group " + std::to_string(i), get, set);
    }
  }

  void addTerrainParameters()
  {
    auto iface = dynamic_cast<IParameterizable*>(&getTerrain());
    if (iface)
      iface->addParameters(m_rendering_parameters);
  }
};


} // namespace render_util

#endif

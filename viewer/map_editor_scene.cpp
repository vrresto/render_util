/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define GLM_ENABLE_EXPERIMENTAL

#include "terrain_viewer_scene_base.h"
#include "terrain_parameters.h"
#include <render_util/viewer/configuration.h>
#include <render_util/viewer/viewer.h>
#include <render_util/viewer/map_loader.h>
#include <render_util/viewer/editor_backend.h>
#include <render_util/viewer/editor.h>
#include <render_util/viewer/scene.h>
#include <render_util/stats.h>
#include <render_util/terrain_util.h>
#include <render_util/image_loader.h>
#include <render_util/image_resample.h>
#include <render_util/image_util.h>
#include <render_util/shader_util.h>
#include <render_util/physics.h>
#include <render_util/state.h>
#include <render_util/texunits.h>
#include <render_util/gl_binding/gl_functions.h>
#include <util/child_process.h>
#include <util/map_projection.h>
#include <util/output_terminal.h>
#include <log.h>

#include <glm/gtx/vec_swizzle.hpp>

#include <queue>
#include <memory>
#include <list>
#include <set>


using namespace render_util;
using namespace render_util::gl_binding;
using namespace render_util::physics;
using namespace render_util::viewer;


namespace
{


class MapEditorScene;
struct GenerateBaseMapParameters;


struct WaterMapTextures
{
//   glm::vec3 water_color = glm::vec3(0);
  bool y_flip = false;
  glm::vec2 offset_m = glm::vec2(0);
  glm::ivec2 table_size = glm::ivec2(0);
  ShaderParameters shader_params;
  float chunk_size_m = 0;
  float chunk_sample_offset = 0;
  float chunk_sample_scale = 1;
  float chunks_num_cols = 0;
  float chunks_num_rows = 0;

  TexturePtr chunks_texture;
  TexturePtr table_texture;

  void setShaderParameters(ShaderParameters &params)
  {
    params.set<bool>("water_map_y_flip", y_flip);
  }

  void setUniforms(ShaderProgramPtr program)
  {
//     program->setUniform("water_color", water_color);
    program->setUniform("water_map_offset_m", offset_m);
    program->setUniform("water_map_table_size", table_size);
    program->setUniform("water_map_chunk_size_m", chunk_size_m);
    program->setUniform("water_map_chunk_sample_offset", chunk_sample_offset);
    program->setUniform("water_map_chunk_sample_scale", chunk_sample_scale);

    program->setUniformi("water_map_chunks_num_cols", chunks_num_cols);
    program->setUniformi("water_map_chunks_num_rows", chunks_num_rows);

    program->setUniformi("sampler_water_map_chunks", TEXUNIT_WATER_MAP_CHUNKS);
    program->setUniformi("sampler_water_map_table", TEXUNIT_WATER_MAP_TABLE);
  }

  // FIXME
  // void bindTextures(TextureManager &txmgr)
  // {
  //   txmgr.bind(TEXUNIT_WATER_MAP_CHUNKS, chunks_texture);
  //   txmgr.bind(TEXUNIT_WATER_MAP_TABLE, table_texture);
  // }
};


struct JobBatch : public Job
{
  std::queue<std::unique_ptr<Job>> m_waiting_jobs;
  std::queue<std::unique_ptr<Job>> m_finished_jobs;
  std::unique_ptr<Job> m_current_job;
  std::string m_name;
  int m_failed_jobs = 0;
  bool m_canceled = false;

  void start() override
  {
    assert(!m_current_job);
    assert(m_finished_jobs.empty());
    assert(!m_waiting_jobs.empty());

    m_current_job = std::move(m_waiting_jobs.front());
    m_waiting_jobs.pop();
    m_current_job->start();
  }

  std::string getName() override
  {
    return m_name;
  }

  std::string getVerboseName() override
  {
    auto total_jobs = m_waiting_jobs.size() + m_finished_jobs.size();
    if (m_current_job)
      total_jobs++;

    return m_name + " (" + std::to_string(m_finished_jobs.size()) + " of "
        + std::to_string(total_jobs) + ")";
  }

  float getProgressPercent() override
  {
    return 0;
  }

  void step(util::OutputTerminal* terminal = nullptr) override
  {
    assert(m_current_job);
    m_current_job->step(terminal);

    if (m_current_job->isFinished())
    {
      if (!m_current_job->wasSuccessful())
        m_failed_jobs++;

      m_finished_jobs.push(std::move(m_current_job));

      if (!m_waiting_jobs.empty() && !m_canceled)
      {
        m_current_job = std::move(m_waiting_jobs.front());
        m_waiting_jobs.pop();
        m_current_job->start();
      }
    }
  }

  bool isFinished() override
  {
    return !m_current_job && (m_waiting_jobs.empty() || m_canceled);
  }

  bool wasSuccessful() override
  {
    assert(isFinished());
    return (m_failed_jobs == 0) && !m_canceled;
  }

  void cancel() override
  {
    m_canceled = true;
    if (m_current_job)
      m_current_job->cancel();
  }

  bool wasCanceled()
  {
    return m_canceled;
  }
};


struct ChildProcessJob : public Job
{
  std::unique_ptr<util::ChildProcess> m_process;
  std::vector<std::string> m_args;
  bool m_canceled = false;

  void start(const std::vector<std::string> &args)
  {
    m_process = std::make_unique<util::ChildProcess>(args);
    m_process->start();
  }

  float getProgressPercent() override { return 0; }

  bool isFinished() override { return m_process->isFinished(); }

  bool wasSuccessful() override { return m_process->success(); }

  void step(util::OutputTerminal* terminal) override
  {
    assert(m_process);
    m_process->updateStatus();

    if (terminal)
    {
      char buffer[1000];

      while (true)
      {
        auto length = m_process->readOutput(buffer, sizeof(buffer));
        if (length <= 0)
          break;
        terminal->write(buffer, length);
      }
    }
  }

  void cancel() override
  {
    if (!m_canceled)
    {
      m_process->kill();
      m_canceled = true;
    }
  }

  bool wasCanceled() override
  {
    return m_canceled;
  }
};


template <typename T>
inline std::string to_string(T value)
{
  std::ostringstream s;
  s.precision(std::numeric_limits<T>::digits10);
  s << value;
  return s.str();
}


struct RasterizeJob : public ChildProcessJob
{
  std::string m_output_file;
  double m_origin_latitude = 0;
  double m_origin_longitude = 0;
  double m_meters_per_degree_longitude = EARTH_CIRCUMFERENCE / 360.0;
  std::string m_projection;
  std::string m_dataset_path;
  std::string m_layer_name;
  std::string m_field_name;
  std::vector<std::string> m_field_values;
  std::string m_name;

  std::string getName() override { return m_name; }

  void start() override
  {
    std::vector<std::string> args =
    {
      "./rasterize-vector-data",
      "--latitude", to_string(m_origin_latitude),
      "--longitude", to_string(m_origin_longitude),
      "--meters-per-degree-longitude", to_string(m_meters_per_degree_longitude),
      "--projection", m_projection,
      "--dataset-path", m_dataset_path,
      "--output-path", m_output_file,
      "--layer-name", m_layer_name,
    };

    if (!m_field_name.empty())
    {
      args.push_back("--field-name");
      args.push_back(m_field_name);
    }

    for (auto& value : m_field_values)
      args.push_back(value);

    ChildProcessJob::start(args);
  }
};


struct GenerateHeightMapJob : public ChildProcessJob
{
  std::string m_output_file;
  std::string m_srtm_dir;
  double m_origin_latitude = 0;
  double m_origin_longitude = 0;
  double m_meters_per_degree_longitude = EARTH_CIRCUMFERENCE / 360;
  std::string m_projection;

  std::string getName() override { return "Generating height map"; }

  void start() override
  {
    std::vector<std::string> args =
    {
      "./map-maker",
      m_srtm_dir,
      to_string(m_origin_latitude),
      to_string(m_origin_longitude),
      to_string(m_meters_per_degree_longitude),
      m_projection,
      m_output_file,
    };

    ChildProcessJob::start(args);
  }
};


class GenerateBaseMapJob : public JobBatch
{
  MapEditorScene& m_scene;

public:
  GenerateBaseMapJob(MapEditorScene& scene);

  void step(util::OutputTerminal* terminal = nullptr) override;
};


class WorldMap
{
  TexturePtr m_texture;
  ShaderProgramPtr m_program;
  glm::ivec2 m_texture_size {};

public:
  WorldMap(const render_util::TextureManager &tex_mgr,
           ShaderSearchPath &search_path,
           std::string texture)
  {
    auto image = loadImageFromFile<ImageRGB>(texture);
    assert(image);

    m_texture_size = image->size();

    image::flipYInPlace(image);

    m_texture = createTexture(image);
    assert(m_texture);

    m_program = createShaderProgram("simple", tex_mgr, search_path);
    assert(m_program);
    m_program->setUniformi("sampler_0", 0);
  }

  glm::ivec2 getTextureSize() { return m_texture_size; }

  void draw(glm::vec2 origin, glm::vec2 size, const render_util::Camera &camera)
  {
    assert(m_texture);
    assert(m_program);

    StateModifier state;
    state.setDefaults();
    state.enableCullFace(false);
    state.enableDepthTest(false);
    state.setFrontFace(GL_CW);
    state.setProgram(m_program->getID());

    render_util::updateUniforms(m_program, camera);

    TemporaryTextureBinding texture_binding(m_texture, 0);

    auto min = origin;
    auto max = min + size;

    gl::Begin(GL_QUADS);

    gl::TexCoord2f(0,0);
    gl::Vertex2f(min.x, min.y);

    gl::TexCoord2f(1,0);
    gl::Vertex2f(max.x, min.y);

    gl::TexCoord2f(1,1);
    gl::Vertex2f(max.x, max.y);

    gl::TexCoord2f(0,1);
    gl::Vertex2f(min.x, max.y);

    gl::End();
  }
};


enum class CoordinateSystemID : int
{
  BASE_MAP = 0,
  DETAIL_MAP,
  CUSTOM
};


class MapEditorTerrainParameters : public TerrainParameters
{
public:
  std::unique_ptr<MapGeography> m_custom_geography;
  CoordinateSystemID m_world_coordinate_system = CoordinateSystemID::DETAIL_MAP;

  MapEditorTerrainParameters()
  {
    m_custom_geography =
      std::make_unique<MapGeography>(glm::dvec2(0),
                                     physics::EARTH_CIRCUMFERENCE / 360.0,
                                     "mercator-wgs84"); //FIXME: don_t hardcode
  }

  const MapGeography& getWorldGeography() override
  {

    switch (m_world_coordinate_system)
    {
      case CoordinateSystemID::DETAIL_MAP:
        return getLayer(TerrainBase::LAYER_DETAIL);
      case CoordinateSystemID::BASE_MAP:
        return getLayer(TerrainBase::LAYER_BASE);
      case CoordinateSystemID::CUSTOM:
        return *m_custom_geography;
      default:
        abort();
    }
  }
};


class TextureSelectorParameter : public SimpleParameter<int, SimpleValueWrapper<int>>,
                                 public ITextureSelector
{
  const std::vector<TexturePtr>& m_textures;

public:
  TextureSelectorParameter(std::string name,
                           int& value,
                           const std::vector<TexturePtr>& textures) :
    SimpleParameter<int, SimpleValueWrapper<int>>(name, 1, value),
    m_textures(textures)
  {
  }

  bool isTextureSelector() override { return true; }
  ITextureSelector* toTextureSelector() override { return this; }

  int getNumTextures() override { return m_textures.size(); }

  unsigned getTexture(int index) override
  {
    unsigned id = 0;

    if (index >= 0 && index < m_textures.size())
    {
      if (m_textures.at(index))
        id = m_textures.at(index)->getID();
    }

    return id;
  }

  unsigned getSelectedTexture() override
  {
    return getTexture(m_value.get());
  }

  int getSelectedIndex() override { return m_value.get(); }

  void setSelectedIndex(int index) override
  {
    m_value.set(index);
  }
};


class LandCoverTypeMappingParameters : public Parameters
{
  std::vector<int> m_mapping;

public:
  LandCoverTypeMappingParameters(const std::vector<std::string>& types,
                                 const std::map<std::string, int>& mapping,
                                 const std::vector<TexturePtr>& textures) :
    Parameters("Base layer types"),
    m_mapping(types.size())
  {
    for (auto& it : mapping)
    {
      LOG_INFO << it.first << " = " << it.second << std::endl;
    }

    int i = 0;
    for (auto& type : types)
    {
      if (!mapping.empty())
        m_mapping.at(i) = mapping.at(type);

      auto param = std::make_unique<TextureSelectorParameter>(types.at(i),
                                                              m_mapping.at(i),
                                                              textures);
      add(std::move(param));
      i++;
    }
  }

  const std::vector<int>& getMapping() { return m_mapping; }
};


class AltitudinalZoneParameter : public ParameterGroup
{
public:
  AltitudinalZone m_zone;
  SimpleParameter<float, SimpleValueWrapper<float>> m_transition_start_height;
  SimpleParameter<float, SimpleValueWrapper<float>> m_transition_end_height;
  std::unique_ptr<VecNParameter<glm::vec4>> m_color;
  std::unique_ptr<TextureSelectorParameter> m_selector;

  AltitudinalZoneParameter(const std::vector<TexturePtr>& textures) : ParameterGroup(""),
    m_transition_start_height("transition_start_height", 1.f,
                              m_zone.transition_start_height),
    m_transition_end_height("transition_end_height", 1.f,
                            m_zone.transition_end_height)
  {
    m_zone.name = "Unnamed";

    ValueWrapper<glm::vec4> wrapper =
    {
      .get = [this] () { return glm::vec4(m_zone.color, 1); },
      .set = [this] (glm::vec4 color) { m_zone.color = glm::vec3(color); }
    };
    m_color = std::make_unique<VecNParameter<glm::vec4>>("color", wrapper, 0.01f,
                                                         NumericParameter::HINT_IS_COLOR);

    m_selector = std::make_unique<TextureSelectorParameter>("texture", m_zone.type_index, textures);
  }

  bool supportsRename() { return true; }
  void rename(std::string name) { m_zone.name = name; }
  std::string getName() { return m_zone.name; }

  bool isModifiable() override { return false; }
  int size() const override { return 4; }

  Parameter& get(int index) override
  {
    switch (index)
    {
      case 0:
        return m_transition_start_height;
      case 1:
        return m_transition_end_height;
      case 2:
        return *m_color;
      case 3:
        return *m_selector;
      default:
        abort();
    }
  }
};


class AltitudinalZoneList : public ModifiableParameterList
{
public:
  const std::vector<TexturePtr>& m_textures;
  std::vector<std::unique_ptr<AltitudinalZoneParameter>> m_zones;

  AltitudinalZoneList(const std::vector<TexturePtr>& textures) :
    ModifiableParameterList("Altitudinal zones"),
    m_textures(textures)
  {
  }

  int size() const override { return m_zones.size(); }
  Parameter& get(int index) override { return *m_zones.at(index); }
  void remove(int index) override { m_zones.erase(std::next(m_zones.begin(), index)); }

  void addNew() override
  {
    m_zones.push_back(std::make_unique<AltitudinalZoneParameter>(m_textures));
  }

  void moveForward(int index) override
  {
    std::swap(m_zones.at(index), m_zones.at(index+1));
  }

  void moveBackward(int index) override
  {
    std::swap(m_zones.at(index), m_zones.at(index-1));
  }
};


class TerrainResources final : public render_util::TerrainResourcesBase
{
  std::array<std::unique_ptr<Layer>, 2> m_layers;

public:
  struct Layer : public TerrainResourcesBase::Layer
  {

    glm::vec3 m_origin_m {};
    float m_scale = 1;
    unsigned int m_resolution_m = 0;
    ElevationMap::ConstPtr m_heightmap;
    MaterialMap::Ptr m_material_map;

    glm::ivec2 getSizePx() { return m_heightmap->size(); }
    glm::vec3 getOriginM() { return m_origin_m; }
    float getScale() { return m_scale; }
    unsigned int getResolutionM() override
    {
      assert (m_resolution_m != 0);
      return m_resolution_m;
    }
    glm::vec2 getMapTextureOffset() override { return {}; }
    ElevationMap::ConstPtr getHeightMap() { return m_heightmap; }
    TypeMap::ConstPtr getTypeMap() { abort(); }
    ImageGreyScale::ConstPtr getForestMap() { abort(); }
//         void loadWaterMap(ImageGreyScale::Ptr &chunks,
//                                   Image<unsigned int>::Ptr &table) { abort(); }
    const WaterMap &getWaterMap() { abort(); }

    MaterialMap::ConstPtr getMaterialMap() override
    {
      auto material_map =
        std::make_shared<MaterialMap>(getSizePx());
      image::fill(material_map, 0);

      return material_map;
    }

    glm::dvec2 getSizeM()
    {
      return glm::dvec2(getResolutionM()) * glm::dvec2(getSizePx());
    }

    ImageGreyScale::ConstPtr getSmallWaterMap() override { return nullptr; }
  };

  void createLayer(int layer_number, EditorBackend& backend)
  {
    auto layer = std::make_unique<Layer>();
    layer->m_heightmap = backend.getLayer(layer_number).createElevationMap();
    layer->m_resolution_m = backend.getLayer(layer_number).getResolutionM();
    m_layers.at(layer_number) = std::move(layer);
  }

#if 0
  struct BaseLayer : public Layer
  {
    BaseLayer(ElevationMapLoader &impl, LayerAttributes &layer) :
      Layer(impl, layer) {}

    glm::ivec2 getSizePx() { abort(); }
    glm::vec3 getOriginM() { abort(); }
    float getScale() { abort(); }
    unsigned int getResolutionM() { abort(); }
    ElevationMap::ConstPtr getHeightMap() { return m_impl.createBaseElevationMap(); }
    TerrainBase::TypeMap::ConstPtr getTypeMap() { abort(); }
    TerrainBase::MaterialMap::ConstPtr getMaterialMap() { abort(); }
    ImageGreyScale::ConstPtr getForestMap() { abort(); }
//         void loadWaterMap(ImageGreyScale::Ptr &chunks,
//                                   Image<unsigned int>::Ptr &table) { abort(); }
    const TerrainBase::WaterMap &getWaterMap() { abort(); }

    double getMetersPerDegreeLongitude() const  { abort(); }
    glm::dvec2 getOriginGeodesic() { abort(); }
  };
#endif

  bool m_has_base_layer = false;

  bool hasBaseLayer() override { return m_has_base_layer; }

  TerrainResourcesBase::Layer& getLayer(int number) override
  {
    assert(m_layers.at(number));
    return *m_layers.at(number);
  }

  const ImageResourceList& getLandTextures() { abort(); }
  const ImageResourceList& getLandTexturesNM() { abort(); }
  const std::vector<float>& getLandTexturesScale() { abort(); }

  std::vector<::ImageRGBA::Ptr> getForestLayers() { abort(); }
  ImageRGBA::Ptr getForestFarTexture() { abort(); }

  const ImageResourceList& getWaterAnimationHeightMaps() { abort(); }
  const ImageResourceList& getWaterAnimationNormalMaps() { abort(); }
  const ImageResourceList& getWaterAnimationFoamMasks() { abort(); }
  ImageGreyScale::Ptr getWaterFoamMask() { abort(); }
  std::shared_ptr<GenericImage> getWaterFoamTexture() { abort(); }

  glm::vec3 getWaterColor() { abort(); }

  ImageGreyScale::Ptr getNoiseTexture() { return nullptr; }
  bool hasTypeMap() override { return false; }

  std::vector<AltitudinalZone> getAltitudinalZones() override;
};


std::vector<AltitudinalZone> TerrainResources::getAltitudinalZones()
{
  return {};
}


class MapEditorScene final : public TerrainViewerSceneBase, public IEditor
{
  std::shared_ptr<render_util::TerrainBase> m_terrain;
  std::shared_ptr<render_util::viewer::EditorBackend> m_backend;
  render_util::ShaderSearchPath m_shader_search_path;
  std::string m_srtm_dir;
  MapEditorTerrainParameters m_terrain_parameters;
  TexturePtr m_curvature_map;
  std::string m_land_polygons_dataset_path;
  std::string m_map_projection_name;
  std::unique_ptr<WorldMap> m_world_map;
  std::unique_ptr<MapGeography> m_world_map_geography;
  std::unique_ptr<AltitudinalZoneList> m_altitudinal_zones;
  std::unique_ptr<LandCoverTypeMappingParameters> m_land_cover_type_mapping;
  bool m_draw_world_map = true;
  std::string m_land_polygons_layer_name;

//   WaterMapTextures m_water_map_textures;
//   TexturePtr m_base_water_map_texture;;

//   glm::dvec2 m_base_map_size_m = glm::vec2(0);
  glm::dvec2 m_base_map_origin_geodesic {};
  glm::dvec2 m_map_size_m = glm::vec2(0);
  glm::vec4 m_base_map_tint_color {};
  bool m_blend_water_depth = true;
  bool m_update_terrain = true;

  std::vector<TexturePtr> m_land_textures;

  util::unique_ptr_vector<IGeometricProperty> m_geometric_properties;

  std::unique_ptr<TerrainResources> m_terrain_resources;

  TerrainBase& getTerrain() override
  {
    assert(m_terrain);
    return *m_terrain;
  }

public:
  bool m_needs_reload = false;

  void getStats(Stats& out) override;
  render_util::ShaderParameters makeShaderParameters() override;

  glm::dvec2 getWorldMapOrigin();
  glm::dvec2 getWorldMapSize();

  void updateBaseMapParameters();
  void loadWorldMap();
  void drawWorldMap();
  void addParameters();
  void loadMap(bool no_update_detail_map = false);
  void drawTerrain(const render_util::Camera &camera);
  void createTerrain();
  void createWaterMapTextures(TerrainResourcesBase::WaterMap water_map);

  render_util::viewer::EditorBackend& getBackend() { return *m_backend; }

  std::vector<AltitudinalZone> getAltitudinalZones()
  {
    std::vector<AltitudinalZone> list;
    for (auto& zone : m_altitudinal_zones->m_zones)
      list.push_back(zone->m_zone);
    return list;
  }

  TerrainResourcesBase& getTerrainResources()
  {
    if (m_backend->getMapLoader())
    {
      assert(!m_terrain_resources);
      return m_backend->getMapLoader()->getTerrainResources();
    }
    else
    {
      assert(m_terrain_resources);
      return *m_terrain_resources;
    }
  }

public:
  MapEditorScene(render_util::viewer::CreateEditorBackendFunc&,
                 std::string srtm_dir,
                 std::string land_polygons_dataset_path,
                 std::string land_polygons_layer_name);
  ~MapEditorScene() override {}

  void update(float frame_delta) override;
  void render(const render_util::Camera &camera) override;
  void setup() override;
  void updateUniforms(render_util::ShaderProgramPtr,
                      const render_util::Camera &camera) override;
  void save() override;
  bool hasUnsavedChanges() override { return true; }

  glm::vec2 getMapSizeM() override;
  void reloadMap() override;
  std::unique_ptr<Job> generateBaseMap(const GenerateBaseMapParameters& options);

  bool isEditor() override { return true; }
  bool needsReload() override { return m_needs_reload; }

  // IEditor
  const util::unique_ptr_vector<IGeometricProperty>& getGeometricProperties() override
  {
    return m_geometric_properties;
  }
};


struct GenerateBaseMapParameters : public Parameters
{
  class LandCoverParameters : public Parameters, public IBulkSelection
  {
    GenerateBaseMapParameters& m_parent;

  public:
    LandCoverParameters(GenerateBaseMapParameters& parent) :
      Parameters("Land cover"),
      m_parent(parent)
    {
      for (auto& name : m_parent.m_all_land_cover_types)
      {
        auto get = [this, name] ()
        {
          return m_parent.isLandCoverSelected(name);
        };
        auto set = [this, name] (bool enable)
        {
          if (enable)
            m_parent.m_selected_land_cover_types.insert(name);
          else
            m_parent.m_selected_land_cover_types.erase(name);
        };
        addBool(name, get, set);
      }

      columns = 2;
    }

    void selectAll() override
    {
      m_parent.m_selected_land_cover_types = m_parent.m_all_land_cover_types;
    }

    void selectNone() override
    {
      m_parent.m_selected_land_cover_types.clear();
    }
  };

  bool auto_center = false;
  bool height_map = false;
  bool water_map = false;
  std::set<std::string> m_selected_land_cover_types;
  std::set<std::string> m_all_land_cover_types;

  GenerateBaseMapParameters(MapEditorScene& scene) : Parameters("Generate base map")
  {
    for (auto& type : scene.getBackend().getLandCoverTypes())
      m_all_land_cover_types.insert(type.name);

    auto general = std::make_unique<Parameters>("General");
    general->addBool("Auto center", auto_center);
    general->addBool("Height map", height_map);
    general->addBool("Water map", water_map);

    auto land_cover_parameters = std::make_unique<LandCoverParameters>(*this);

    add(std::move(general));
    add(std::move(land_cover_parameters));
  }

  bool isAnySelected() const
  {
    return !m_selected_land_cover_types.empty() || height_map || water_map;
  }

  bool isLandCoverSelected(const std::string& name) const
  {
    return m_selected_land_cover_types.find(name) != m_selected_land_cover_types.end();
  }
};


struct GenerateBaseMapJobStarter : public JobStarter
{
  MapEditorScene& m_scene;
  GenerateBaseMapParameters m_parameters;

  GenerateBaseMapJobStarter(MapEditorScene& scene) :
    m_scene(scene),
    m_parameters(scene)
  {
  }

  ParameterGroup& getParameters() override { return m_parameters; }

  std::unique_ptr<Job> createJob() override
  {
    return m_scene.generateBaseMap(m_parameters);
  }

  bool isReady() override { return m_parameters.isAnySelected(); }
};


GenerateBaseMapJob::GenerateBaseMapJob(MapEditorScene& scene) : m_scene(scene)
{
}

void GenerateBaseMapJob::step(util::OutputTerminal* terminal)
{
  JobBatch::step(terminal);
  if (isFinished() && wasSuccessful())
  {
    m_scene.m_needs_reload = true;
  }
}


class LayerRectangleProperty : public IRectangularPropertyFixedAspect
{
  struct State : public IGeometricPropertyState
  {
    MapGeography geography;

    State(const MapGeography& geo) : geography(geo) {}
  };

  std::string m_name;
  const int m_layer = 0;
  TerrainParameters& m_parameters;

  const std::string& getName() override
  {
    return m_name;
  }

  RectD getRect() const override
  {
    auto origin = m_parameters.getLayerOriginWorld(m_layer);
    auto extent = m_parameters.getLayerExtentWorld(m_layer);
    return RectD(origin, extent);
  }

  void setOrigin(glm::dvec2 origin) override
  {
    m_parameters.setLayerOriginWorld(m_layer, origin);
  }

  void setExtentX(double extent) override
  {
    m_parameters.setLayerExtentWorldX(m_layer, extent);
  }

  void setExtentY(double extent) override
  {
    m_parameters.setLayerExtentWorldY(m_layer, extent);
  }

  std::unique_ptr<IGeometricPropertyState> getState() override
  {
    return std::make_unique<State>(m_parameters.getLayer(m_layer));
  }


  void restoreState(IGeometricPropertyState& istate) override
  {
    auto state = dynamic_cast<State*>(&istate);
    assert(state);
    m_parameters.setLayerGeography(m_layer, state->geography);
  }

public:
  LayerRectangleProperty(std::string name, int layer, TerrainParameters& p) :
    m_name(name), m_layer(layer), m_parameters(p) {}
};


MapEditorScene::MapEditorScene(render_util::viewer::CreateEditorBackendFunc &create_backend,
                               std::string srtm_dir,
                               std::string land_polygons_dataset_path,
                               std::string land_polygons_layer_name) :
  m_terrain_parameters(),
  m_srtm_dir(srtm_dir),
  m_land_polygons_dataset_path(land_polygons_dataset_path),
  m_land_polygons_layer_name(land_polygons_layer_name)
{
  m_backend = create_backend();
  m_map_projection_name = m_backend->getMapProjection();

  m_world_map_geography = std::make_unique<MapGeography>(glm::dvec2(0),
                                                          physics::EARTH_CIRCUMFERENCE / 360.0,
                                                          m_backend->getMapProjection());

  auto generate_base_map = [this] ()
  {
    return std::make_unique<GenerateBaseMapJobStarter>(*this);
  };
  addJobAction("generate_base_map", "(Re)generate base map", generate_base_map);


  auto reload_map = [this] ()
  {
    m_needs_reload = true;
  };
  addAction("reload_map", "Reload map", reload_map);

  {
    auto p = std::make_unique<LayerRectangleProperty>("Map rectangle", 0, m_terrain_parameters);
    m_geometric_properties.push_back(std::move(p));
  }
}


void MapEditorScene::createTerrain()
{
  auto shader_params = makeShaderParameters();

  m_terrain = render_util::createTerrain(getTextureManager(), true, m_shader_search_path);

  m_terrain->setProgramSuffix("_editor");

  render_util::TerrainBase::BuildParameters params =
  {
    .resources = getTerrainResources(),
    .shader_parameters = shader_params,
    .enabled_subsystems = getTerrainResources().getSupportedSubsystems(),
    .use_material_map = false,
  };

  m_terrain->build(params);

  updateBaseMapParameters();
}


TexturePtr createBaseWaterMapTexture(ImageGreyScale::Ptr image)
{
  return createTexture(image, true);
}


#if 0
void MapEditorScene::createWaterMapTextures(TerrainBase::WaterMap water_map)
{
  m_water_map_textures = {};

  m_water_map_textures.y_flip = water_map.y_flip;

  m_water_map_textures.chunk_size_m = water_map.chunk_size_m;

  float chunk_size_px = water_map.chunk_size_px;

  assert(chunk_size_px > 0);
  assert(water_map.chunks->h() % water_map.chunk_size_px == 0);
  assert(water_map.chunks->w() % water_map.chunk_size_px == 0);

  float chunk_scale = (chunk_size_px - (2 * water_map.chunk_border_px)) / chunk_size_px;

  m_water_map_textures.chunk_sample_offset = water_map.chunk_border_px / chunk_size_px;
  m_water_map_textures.chunk_sample_scale = chunk_scale;


  m_water_map_textures.chunks_num_rows = water_map.chunks->h() / water_map.chunk_size_px;
  m_water_map_textures.chunks_num_cols = water_map.chunks->w() / water_map.chunk_size_px;

  m_water_map_textures.offset_m = glm::vec2(water_map.offset_terrain_units) *
                                            glm::vec2(TerrainBase::GRID_RESOLUTION_M);
  m_water_map_textures.table_size = water_map.table->size();

  auto table_float = image::convert<float>(water_map.table);

  m_water_map_textures.chunks_texture = createTexture(water_map.chunks, false);

  TextureParameters<int> chunks_params;
  chunks_params.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  chunks_params.set(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  chunks_params.set(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  chunks_params.set(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  chunks_params.apply(m_water_map_textures.chunks_texture);



  m_water_map_textures.table_texture = createFloatTexture(
                                reinterpret_cast<const float*>(table_float->data()),
                                table_float->w(),
                                table_float->h(),
                                1,
                                false);

  TextureParameters<int> table_params;
  table_params.set(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  table_params.set(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  table_params.set(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  table_params.set(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  table_params.apply(m_water_map_textures.table_texture);
}
#endif


void MapEditorScene::drawTerrain(const render_util::Camera &camera)
{
  StateModifier state;
  state.setDefaults();

  state.enableDepthTest(true);
  state.enableBlend(true);
  state.enableCullFace(true);
  state.setFrontFace(GL_CCW);
  state.setDepthMask(true);

  viewer::TerrainClient client(*this, camera);

  if (m_update_terrain)
  {
    m_terrain->setDrawDistance(0);
    m_terrain->update(camera, false);
  }

  if ((isWireframeEnabled()))
    gl::PolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  m_terrain->draw(camera, &client);

  gl::PolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void MapEditorScene::loadWorldMap()
{
  auto texture = "world_map_" + m_map_projection_name + ".tga";

  m_world_map = std::make_unique<WorldMap>(getTextureManager(),
                                           m_shader_search_path,
                                           texture);
}


void MapEditorScene::setup()
{
  m_shader_search_path.push_back(std::string(RENDER_UTIL_SHADER_DIR) + "/atmosphere_none");
  m_shader_search_path.push_back(RENDER_UTIL_SHADER_DIR);
  getCelestialBody(viewer::CelestialBody::SUN).elevation = 30.0;

  m_curvature_map = render_util::createCurvatureTexture(getTextureManager(), RENDER_UTIL_CACHE_DIR);

  m_altitudinal_zones = std::make_unique<AltitudinalZoneList>(m_land_textures);
  for (auto& zone : m_backend->getAltitudinalZones())
  {
     m_altitudinal_zones->addNew();
     m_altitudinal_zones->m_zones.back()->m_zone = zone;
  }

  loadWorldMap();

  LOG_INFO << "Loading map ..." << std::endl;
  loadMap();
  LOG_INFO << "Loading map ... done." << std::endl;

  addParameters();
}


void MapEditorScene::loadMap(bool no_update_detail_map)
{
  m_terrain_parameters.m_has_base_map = false;
  m_terrain_parameters.m_world_coordinate_system = CoordinateSystemID::DETAIL_MAP;
  m_parameter_groups.clear();
  m_world_parameters.clear();
  m_rendering_parameters.clear();
  m_terrain.reset();
  m_terrain_resources.reset();
  m_land_textures.clear();

  if (!m_backend->getMapLoader())
  {
    m_terrain_resources = std::make_unique<TerrainResources>();
    m_terrain_resources->createLayer(0, *m_backend);
  }
  else
  {
    auto& loader = getTerrainResources();

    m_land_textures.resize(loader.getLandTextures().size());

    for (int i = 0; i < m_land_textures.size(); i++)
    {
      auto res = loader.getLandTextures().at(i);

      if (res)
      {
        const glm::ivec2 preview_size(256);

        LOG_INFO << "Creating texture preview: " << res->getName() << std::endl;

        auto cache_dir = std::string(RENDER_UTIL_CACHE_DIR) + "/texture_previews";
        util::mkdir(cache_dir);

        auto cache_name = res->getUniqueID();
        assert(!cache_name.empty());

        auto cache_path = cache_dir + "/" + cache_name;

        if (!util::fileExists(cache_path))
        {
          auto image = res->load(0);

          auto resampled = std::make_shared<GenericImage>(preview_size, image->numComponents());
          resampleImage(*image, *resampled);

          saveImageToFile(cache_path, resampled.get());

#if DEBUG_TEXTURE_PREVIEW
          saveImageToFile("il2ge_map_viewer_dump/texture" + std::to_string(i)
                          + ".tga", image.get());

          saveImageToFile("il2ge_map_viewer_dump/texture_preview" + std::to_string(i)
                          + ".tga", resampled.get());
#endif
        }
        assert(util::fileExists(cache_path));

        auto preview = loadImageFromFile<ImageRGB>(cache_path);

        m_land_textures.at(i) = createTexture(preview);
      }
      else
      {
        m_land_textures.at(i) = nullptr;
      }
    }
  }

  if (!no_update_detail_map)
  {
    auto &detail_layer = m_backend->getLayer(0);

    auto size_m = glm::ivec2(detail_layer.getResolutionM()) * detail_layer.getSizePx();
    auto &geo = detail_layer.getMapGeography();

    m_terrain_parameters.setLayer(TerrainBase::LAYER_DETAIL, geo, size_m);
  }

  {
    auto& loader = getTerrainResources();
    int layer = loader.hasBaseLayer() ? 1 : 0;
    m_map_size_m = glm::vec2(loader.getLayer(layer).getSizePx())
                   * glm::vec2(loader.getLayer(layer).getResolutionM());
  }

  auto map_center = getMapSizeM() / 2.f;

  if (m_backend->hasBaseLayer())
  {
    auto &base_layer = m_backend->getLayer(1);

    m_terrain_parameters.m_has_base_map = true;

    auto size_m = glm::ivec2(base_layer.getResolutionM()) * base_layer.getSizePx();
    auto &geo = base_layer.getMapGeography();

    m_terrain_parameters.setLayer(TerrainBase::LAYER_BASE, geo, size_m);

    m_terrain_parameters.m_world_coordinate_system = CoordinateSystemID::BASE_MAP;

    m_base_map_origin_geodesic = m_terrain_parameters.getBaseLayer().getOriginGeodesic();

    if (m_terrain_resources)
    {
      m_terrain_resources->createLayer(1, *m_backend);
      m_terrain_resources->m_has_base_layer = true;
    }
  }

//   createWaterMapTextures(m_backend->loadWaterMap());

//   auto base_water_map = m_backend->loadBaseWaterMap();
//   if (base_water_map)
//     m_base_water_map_texture = createTexture(base_water_map);
//   else
//     m_base_water_map_texture.reset();

  createTerrain();

  m_initial_camera_pos.x = map_center.x;
  m_initial_camera_pos.y = map_center.y;
  m_initial_camera_pos.z = 10000;
}


render_util::ShaderParameters MapEditorScene::makeShaderParameters()
{

  auto params = TerrainViewerSceneBase::makeShaderParameters();

  params.set("enable_terrain_layer_scale", true);
  params.set("terrain_use_detail_layer_coordinate_system",
             m_terrain_parameters.m_world_coordinate_system == CoordinateSystemID::DETAIL_MAP);
//   m_water_map_textures.setShaderParameters(params);

  return params;
}


void MapEditorScene::addParameters()
{
  TerrainViewerSceneBase::addParameters();

  m_rendering_parameters.addBool("Update terrain", m_update_terrain);

  m_rendering_parameters.addVector("Base map tint", m_base_map_tint_color, 0.1,
                         NumericParameter::HINT_IS_COLOR);

//   {
//     auto get = [this] ()
//     {
//       return m_terrain_parameters.getDetailLayer().getOriginGeodesic();
//     };
//     auto set = [this] (auto value)
//     {
//       m_terrain_parameters.getDetailLayer().setOriginGeodesic(value);
//     };
//     m_world_parameters.addVector<glm::dvec2>("map_origin", get, set, 0.01);
//   }

//   {
//     auto get = [this] ()
//     {
//       return (EARTH_CIRCUMFERENCE / 360.0)
//         / m_terrain_parameters.getDetailLayer().getMetersPerDegreeLongitude();
//     };
//     auto set = [this] (auto scale)
//     {
//       auto meters_per_deg = (EARTH_CIRCUMFERENCE / 360.0) / scale;
//       m_terrain_parameters.getDetailLayer().setMetersPerDegreeLongitude(meters_per_deg);
//     };
//     m_world_parameters.add<double>("Map scale", get, set, 0.01);
//   }

  m_world_parameters.addVector("Base map origin", m_base_map_origin_geodesic, 0.01);
  m_rendering_parameters.addBool("Blend water depth", m_blend_water_depth);
  m_rendering_parameters.addBool("World map", m_draw_world_map);

  {
    struct ChoiceName
    {
      static std::string get(CoordinateSystemID value)
      {
        switch (value)
        {
          case CoordinateSystemID::BASE_MAP:
            return "base map";
          case CoordinateSystemID::DETAIL_MAP:
            return "detail map";
          case CoordinateSystemID::CUSTOM:
            return "custom";
          default:
            abort();
        }
      }
    };

    using Param = viewer::MultipleChoiceParameter<CoordinateSystemID, ChoiceName>;

    auto apply = [this] (CoordinateSystemID value)
    {
      assert(value == CoordinateSystemID::BASE_MAP ? m_terrain_parameters.m_has_base_map : true);

      if (m_terrain_parameters.m_world_coordinate_system != value)
      {
        m_terrain_parameters.m_world_coordinate_system = value;
        reloadShaders();
      }
    };

    std::vector<CoordinateSystemID> values;
    if (m_terrain_parameters.m_has_base_map)
      values.push_back(CoordinateSystemID::BASE_MAP);
    values.push_back(CoordinateSystemID::DETAIL_MAP);
    values.push_back(CoordinateSystemID::CUSTOM);
    auto param = std::make_unique<Param>("coordinate system", apply, values);
    m_rendering_parameters.add(std::move(param));
  }

  {
    using Param = viewer::MultipleChoiceParameter<std::string>;

    auto apply = [this] (std::string value)
    {
      m_map_projection_name = value;
//       m_terrain_parameters.m_map_projection = util::createMapProjection(value);
      loadWorldMap();
    };

    auto values = util::getMapProjectionNames();
    int current_index = 0;
    for (int i = 0; i < values.size(); i++)
    {
      if (m_map_projection_name == values[i])
        current_index = i;
    }

    auto param = std::make_unique<Param>("Map projection", apply, values, current_index);

    m_world_parameters.add(std::move(param));
  }

//   m_parameters.add("base_map_height", m_base_map_height, 100.f);

  m_parameter_groups.push_back(m_world_parameters);
  m_parameter_groups.push_back(m_rendering_parameters);
  m_parameter_groups.push_back(*m_altitudinal_zones);

  m_land_cover_type_mapping =
    std::make_unique<LandCoverTypeMappingParameters>(m_backend->getLandCoverTypeNames(),
                                                     m_backend->getLandCoverTypeMapping(),
                                                     m_land_textures);
  m_parameter_groups.push_back(*m_land_cover_type_mapping);
}


void MapEditorScene::updateBaseMapParameters()
{
  assert(m_terrain);
  m_terrain_parameters.apply(*m_terrain);
}


void MapEditorScene::update(float frame_delta)
{
  m_terrain->updateAnimation(frame_delta);
}


void MapEditorScene::render(const render_util::Camera &camera)
{
  updateBaseMapParameters();

  m_terrain->setAltitudinalZones(getAltitudinalZones());
  m_terrain->setBaseLayerTypeMapping(m_land_cover_type_mapping->getMapping());

//   m_water_map_textures.bindTextures(getTextureManager());

//   if (m_base_water_map_texture)
//     getTextureManager().bind(TEXUNIT_WATER_MAP_BASE, m_base_water_map_texture);

  if (!isCurvatureEnabled() && m_draw_world_map)
    m_world_map->draw(getWorldMapOrigin(), glm::dvec2(getWorldMapSize()), camera);

  drawTerrain(camera);
}


void MapEditorScene::updateUniforms(render_util::ShaderProgramPtr program,
                                       const render_util::Camera &camera)
{
  TerrainViewerSceneBase::updateUniforms(program, camera);

//   m_water_map_textures.setUniforms(program);
  program->setUniformi("sampler_water_map_base", TEXUNIT_WATER_MAP_BASE);

//   program->setUniform("terrain_base_map_height", m_base_map_height);
  program->setUniform("terrain_height_offset", 0.f);


  program->setUniform("base_map_tint", m_base_map_tint_color);

  program->setUniform("blend_water_depth", m_blend_water_depth);
}


void MapEditorScene::save()
{
  std::vector<AltitudinalZone> altitudinal_zones;
  for (int i = 0; i < m_altitudinal_zones->size(); i++)
  {
    altitudinal_zones.push_back(m_altitudinal_zones->m_zones.at(i)->m_zone);
  }

  std::map<std::string, int> land_cover_type_mapping;
  for (int i = 0; i < m_backend->getLandCoverTypeNames().size(); i++)
  {
    land_cover_type_mapping[m_backend->getLandCoverTypeNames().at(i)] =
      m_land_cover_type_mapping->getMapping().at(i);
  }

  m_backend->saveMapAttributes(m_terrain_parameters.getDetailLayer().getOriginGeodesic(),
      m_terrain_parameters.getDetailLayer().getMetersPerDegreeLongitude(),
      m_map_projection_name,
      altitudinal_zones,
      land_cover_type_mapping);
}


std::unique_ptr<Job> MapEditorScene::generateBaseMap(const GenerateBaseMapParameters& options)
{
  glm::ivec2 base_map_size_px(4096); //FIXME
  int base_map_resolution_m = 200; //FIXME

  glm::dvec2 base_map_origin_geodesic {};

  if (options.auto_center)
  {
    auto& layer = getTerrainResources().getLayer(0);
    glm::dvec2 base_map_size_m = base_map_size_px * base_map_resolution_m;
    auto base_map_center_world =
      glm::dvec2(layer.getSizePx() * int(layer.getResolutionM())) / 2.0;
    auto base_map_origin_world = base_map_center_world - (base_map_size_m / 2.0);
    base_map_origin_geodesic =
      m_terrain_parameters.getDetailLayer().localToGeodesic(base_map_origin_world);
  }
  else
  {
    base_map_origin_geodesic = m_terrain_parameters.getBaseLayer().getOriginGeodesic();
  }

  assert(!m_srtm_dir.empty());

  auto batch = std::make_unique<GenerateBaseMapJob>(*this);
  batch->m_name = "Generating base map";

  if (options.height_map)
  {
    auto job = std::make_unique<GenerateHeightMapJob>();
    job->m_origin_latitude = base_map_origin_geodesic.y;
    job->m_origin_longitude = base_map_origin_geodesic.x;
    job->m_meters_per_degree_longitude =
      m_terrain_parameters.getDetailLayer().getMetersPerDegreeLongitude();
    job->m_output_file = m_backend->getBaseMapPath();
    job->m_srtm_dir = m_srtm_dir;
    job->m_projection = m_map_projection_name;
    batch->m_waiting_jobs.push(std::move(job));
  }

  struct DataSource
  {
    std::string dataset_path;
    std::string layer;
  };

  DataSource land_use =
  {
    .dataset_path = "vector_data/landuse_EPSG4326.gpkg",
    .layer = "landuse_EPSG4326_multipolygon",
  };

  DataSource natural =
  {
    .dataset_path = "vector_data/natural_EPSG4326.gpkg",
    .layer = "natural_EPSG4326_multipolygon",
  };

  DataSource land_polygons =
  {
    .dataset_path = m_land_polygons_dataset_path,
    .layer = m_land_polygons_layer_name,
  };

  std::map<std::string, DataSource*> data_sources;
  data_sources["natural"] = &natural;
  data_sources["landuse"] = &land_use;

  auto add_rasterize_job = [&] (std::string name,
                                std::string field,
                                std::vector<std::string> values,
                                DataSource &source,
                                std::string output_file = {})
  {
    auto job = std::make_unique<RasterizeJob>();
    job->m_origin_latitude = base_map_origin_geodesic.y;
    job->m_origin_longitude = base_map_origin_geodesic.x;
    job->m_meters_per_degree_longitude =
      m_terrain_parameters.getDetailLayer().getMetersPerDegreeLongitude();

    if (!output_file.empty())
    {
      job->m_output_file = output_file;
    }
    else
    {
      assert(!name.empty());
      job->m_output_file = m_backend->getBaseMapPath() + "." + name + ".tga";
    }

    job->m_projection = m_map_projection_name;
    job->m_dataset_path = source.dataset_path;
    job->m_layer_name = source.layer;
    job->m_field_name = field;
    job->m_field_values = values;

    batch->m_waiting_jobs.push(std::move(job));
  };

  if (options.water_map)
    add_rasterize_job("", "", {}, land_polygons, m_backend->getBaseWaterMapPath());

  for (auto& type : m_backend->getLandCoverTypes())
  {
    if (options.isLandCoverSelected(type.name))
    {
      add_rasterize_job(type.name, type.osm_key, type.osm_values,
                        *data_sources.at(type.osm_key));
    }
  }

  assert(!batch->m_waiting_jobs.empty());

  return batch;
}


void MapEditorScene::reloadMap()
{
  LOG_ERROR << "reloadMap() is not implemented" << std::endl;

//   m_terrain.reset();
//   m_terrain_parameters.m_has_base_map = false;
//   m_loader.m_has_base_layer = false;
//   m_terrain_parameters.m_world_coordinate_system = CoordinateSystemID::DETAIL_MAP;
//   m_parameters.clear();
//   abort();
// //   m_backend->reloadBaseMap();
//
//   loadMap(true);
//   addParameters();

  m_needs_reload = false;
}


glm::vec2 MapEditorScene::getMapSizeM()
{
  return m_map_size_m;
}


void MapEditorScene::getStats(Stats& stats)
{
  if (m_terrain_parameters.hasBaseMap())
  {
    auto base_map_origin_geodesic = m_terrain_parameters.getBaseLayer().getOriginGeodesic();
    stats.add("base map origin geodesic", base_map_origin_geodesic);

    auto base_map_origin_world =
      m_terrain_parameters.getLayerOriginWorld(TerrainBase::LAYER_BASE);
    stats.add("base map origin world", base_map_origin_world);
  }

  auto& terrain_stats = stats.addChild("terrain");

  m_terrain->getStats(terrain_stats);
}


glm::dvec2 MapEditorScene::getWorldMapOrigin()
{
  auto aspect = double(m_world_map->getTextureSize().x) /
                double(m_world_map->getTextureSize().y);

  auto size_in_map_space = glm::dvec2(EARTH_CIRCUMFERENCE) * glm::dvec2(1.0, 1.0 / aspect);
  auto origin_in_map_space = -(size_in_map_space / 2.0);

  auto origin_in_world_space =
    m_world_map_geography->localToWorld(origin_in_map_space,
                                        m_terrain_parameters.getWorldGeography());

  return origin_in_world_space;
}


glm::dvec2 MapEditorScene::getWorldMapSize()
{
  auto aspect = double(m_world_map->getTextureSize().x) /
                double(m_world_map->getTextureSize().y);

  return glm::dvec2(EARTH_CIRCUMFERENCE) * glm::dvec2(1.0, 1.0 / aspect) *
    m_world_map_geography->getScaleWorld(m_terrain_parameters.getWorldGeography());
}


} // namespace


void render_util::viewer::runMapEditor(CreateEditorBackendFunc &create_backend,
                                       std::string app_name)
{
  Configuration config(app_name + ".ini");

  auto getConfigValue = [&] (std::string key)
  {
    auto value = config.get(key, "");
    if (value.empty())
    {
      throw std::runtime_error("'" + key + "' is not set in configuration");
    }
    return value;
  };

  auto srtm_dir = getConfigValue("srtm_dir");
  auto land_polygons_dataset_path = getConfigValue("land_polygons_dataset_path");
  auto land_polygons_layer_name = getConfigValue("land_polygons_layer_name");

  auto create_func = [&] ()
  {
    auto scene = std::make_shared<MapEditorScene>(create_backend,
                                                  srtm_dir,
                                                  land_polygons_dataset_path,
                                                  land_polygons_layer_name);
    return scene;
  };

  runViewer(create_func, app_name, config);

  LOG_INFO<<"exiting..."<<std::endl;
}

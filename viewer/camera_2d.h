/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_CAMERA_2D_H
#define RENDER_UTIL_VIEWER_CAMERA_2D_H

#include <render_util/camera.h>

#include <glm/gtc/matrix_transform.hpp>


namespace render_util::viewer
{


class Camera2D : public render_util::Camera
{
  glm::ivec2 m_viewport_size = Vec2(1);
  Vec2 m_max_area_size = Vec2(1);
  Unit m_magnification = 1.0;
  Vec3 m_pos {};

  Mat4 m_world_to_view {};
  Mat4 m_view_to_world {};
  Mat4 m_world_to_view_rotation {};
  Mat4 m_projection {};

  void refreshTransform();
  void refreshProjection();

public:
  Camera2D();

  Vec2 getPos2D() const { return Vec2(m_pos); }
  Unit getMagnification() const { return m_magnification; }
  Unit getPixelSizeM() const { return getViewportToWorldScale(); }
  Unit getViewportToWorldScale() const;
  Unit getWorldToViewportScale() const;
  Vec2 viewportToWorld(Vec2 viewport_coords) const;
  Vec2 worldToViewport(Vec2 world_coords) const;
  Vec2 getAreaSize() const;
  Vec2 getViewportOriginWorld() const;

  void setPos(const Vec2 &pos);
  void setMagnification(Unit);
  void setMaxAreaSize(Vec2 size);
  void setViewportOriginWorld(Vec2 world_coords);

  bool cull(const Box &box) const override;
  void setViewportSize(int width, int height) override;
  bool is2D() const override { return true; }
  const Mat4 &getWorldToViewRotationD() const override { return m_world_to_view_rotation; }
  const Mat4 &getView2WorldMatrixD() const override { return m_view_to_world; }
  const Mat4 &getWorld2ViewMatrixD() const override { return m_world_to_view; }
  const Mat4 &getProjectionMatrixFarD() const override { return m_projection; }
  const Vec3 &getPosD() const override { return m_pos; }
  const glm::ivec2 &getViewportSize() const override { return m_viewport_size; }
};


} // namespace render_util::viewer


#endif

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "frontends.h"
#include <render_util/viewer/camera.h>
#include <render_util/viewer/scene.h>
#include <render_util/viewer/viewer.h>
#include <render_util/stats.h>
#include <render_util/state.h>
#include <render_util/text_display.h>
#include <render_util/image_loader.h>
#include <render_util/context.h>
#include <render_util/image_util.h>
#include <render_util/gl_binding/gl_functions.h>
#include <text_renderer/text_renderer.h>
#include <util.h>
#include <log.h>

#include <GLFW/glfw3.h>

#include <chrono>

using namespace glm;
using namespace std;
using namespace render_util::viewer;
using namespace render_util;
using namespace render_util::gl_binding;
using render_util::ShaderProgram;
using Clock = std::chrono::steady_clock;


namespace
{
  enum Axis
  {
    PITCH,
    YAW,
    ROLL
  };


  enum class InputState
  {
    FREE_MOVEMENT,
    SHOWING_MENU,
    EDITING_VALUE
  };


  struct StateHandler
  {
    virtual void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) = 0;
  };


  struct FreeMovement : public StateHandler
  {
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) override;;
  };


  struct ShowingMenu : public StateHandler
  {
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) override;;
  };


  struct EditingValue : public StateHandler
  {
    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) override;;
  };


  const float camera_move_speed_default = 8000.0;
  const float camera_move_speed_min = 100.0;
  const float camera_move_speed_max = 10000.0;
  const double mouse_rotation_speed = 0.2;

  std::shared_ptr<Scene> g_scene;
  viewer::Camera g_camera;
  FreeMovement g_state_handler_free_movement;
  ShowingMenu g_state_handler_showing_menu;
  EditingValue g_state_handler_editing_value;

  float camera_move_speed = camera_move_speed_default;
  dvec2 last_cursor_pos(-1, -1);
  bool g_shift_active = false;
//   bool g_show_menu = false;
//   bool g_editing_value = false;
  InputState g_state = InputState::FREE_MOVEMENT;
  CelestialBody::ID g_current_celestial_body = CelestialBody::SUN;
  int g_current_group_index = 0;
  int g_current_parameter_index = 0;


  bool hasActiveGroup()
  {
    assert(g_current_group_index >= 0);
    return g_current_group_index  < g_scene->getParameterGroups().size();
  }


  int getActiveGroupIndex()
  {
    return g_current_group_index;
  }


  ParameterGroup& getActiveGroup()
  {
    assert(hasActiveGroup());
    return g_scene->getParameterGroups().at(g_current_group_index);
  }


  void setActiveGroup(int index)
  {
    auto old_index = g_current_group_index;
    g_current_group_index = glm::clamp(index, 0, int(g_scene->getParameterGroups().size())-1);
    if (old_index != g_current_group_index)
      g_current_parameter_index = 0;
  }


  bool hasActiveParameter()
  {
    assert(g_current_parameter_index >= 0);
    return hasActiveGroup() && (getActiveGroup().size() > g_current_parameter_index);
  }


  Parameter& getActiveParameter()
  {
    assert(hasActiveParameter());
    return getActiveGroup().at(g_current_parameter_index);
  }


  int getActiveParameterIndex()
  {
    return g_current_parameter_index;
  }


  void setActiveParameter(int index)
  {
    auto& group = getActiveGroup();
    g_current_parameter_index = glm::clamp(index, 0, group.size()-1);
  }


  StateHandler &getStateHandler()
  {
    switch (g_state)
    {
      case InputState::FREE_MOVEMENT:
        return g_state_handler_free_movement;
      case InputState::SHOWING_MENU:
        return g_state_handler_showing_menu;
      case InputState::EDITING_VALUE:
        return g_state_handler_editing_value;
      default:
        abort();
    }
  }


  float getCameraRotationSpeed(const viewer::Camera &camera, Axis axis)
  {
    switch (axis)
    {
      case PITCH:
        return camera.getFov() / 4;
      case YAW:
        return 2 * camera.getFov() / 4;
      case ROLL:
      default:
        return 30;
    }
  }


  std::string getCurrentCelestialBodyName()
  {
    return CelestialBody::getName(g_current_celestial_body);
  }


  CelestialBody& getCurrentCelestialBody()
  {
    return g_scene->getCelestialBody(g_current_celestial_body);
  }


  void printDistance (float distance, const char *suffix, ostream &out)
  {
    out.width(8);
    out << distance << suffix;
  }


  void printStats(int frame_delta_ms, ostream &out, const viewer::Camera &g_camera)
  {
    out.precision(3);
    out.setf(ios_base::fixed);
    out.setf(ios_base::right);

    float fps = 1000.0 / frame_delta_ms;

    out << "fps: ";
    out.width(8);
    out << fps << "   |   ";

    out<<"pos: ";
    printDistance(g_camera.x/1000, " ", out);
    printDistance(g_camera.y/1000, " ", out);
    printDistance(g_camera.z/1000, " ", out);

    out<<"    |    speed: ";
    printDistance(camera_move_speed / 1000, " ", out);

    out<<"   |  yaw: ";
    printDistance(g_camera.yaw, " ", out);

    out<<"   |    " << getCurrentCelestialBodyName() << " elevation: ";
    printDistance(getCurrentCelestialBody().elevation, " ", out);

    out<<"   |    fov: "<<g_camera.getFov();
  }


  void updateMenu(TextDisplay& menu_display)
  {
    menu_display.clear();

    if (g_state == InputState::SHOWING_MENU)
    {
      menu_display.addLine("Options:", glm::vec3(0.6));

      if (hasActiveGroup())
      {
        ParameterGroup& group = getActiveGroup();

        menu_display.addLine("Group: " + group.name, glm::vec3(0.6));

        for (int i = 0; i < group.size(); i++)
        {
          auto &parameter = group.at(i);
          auto parameter_text = parameter.name + ": " + parameter.getValueString();
          auto color = (i == g_current_parameter_index) ? glm::vec3(1) : glm::vec3(0.6);
          menu_display.addLine(parameter_text, color);
        }
      }
    }
    else if (g_state == InputState::EDITING_VALUE && hasActiveParameter())
    {
      auto &parameter = getActiveParameter();
      auto parameter_text = parameter.name + ": " + parameter.getValueString();
      menu_display.addLine(parameter_text, glm::vec3(1));
    }
  }


  void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
  {
    if (action == GLFW_PRESS)
    {
      if (mods == GLFW_MOD_SHIFT)
        g_shift_active = true;
      else
        g_shift_active = false;
    }
    else if (action == GLFW_RELEASE)
    {
      g_shift_active = false;
    }

    if (button == 1)
    {
      if (action == GLFW_PRESS) {
        glfwGetCursorPos(window, &last_cursor_pos.x, &last_cursor_pos.y);
      }
      else if (action == GLFW_RELEASE) {
        last_cursor_pos = dvec2(-1, -1);
      }
    }
  }


  void FreeMovement::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {

    }
    else if (key == GLFW_KEY_M && action == GLFW_PRESS) {
      g_state = InputState::SHOWING_MENU;
    }
    else if (key == GLFW_KEY_P && action == GLFW_PRESS) {
      g_scene->save();
    }
    else if (key == GLFW_KEY_F2 && action == GLFW_PRESS) {
      g_camera.increaseFov();
    }
    else if (key == GLFW_KEY_F3 && action == GLFW_PRESS) {
      g_camera.decreaseFov();
    }
    else if (key == GLFW_KEY_F9 && action == GLFW_PRESS)
    {
      auto &out = cout;

//       out << endl;
//       for (auto &p : g_scene->getParameters())
//       {
//         out << p->name << ": " << p->getValueString() << endl;
//       }
//       out << endl;
    }
    else if (key == GLFW_KEY_R && action == GLFW_PRESS &&
        mods & GLFW_MOD_ALT)
    {
      camera_move_speed = camera_move_speed_default;
    }
    else if (key == GLFW_KEY_F12 && action == GLFW_PRESS) {
      constexpr int num_channels = 3;

      int width, height;
      glfwGetFramebufferSize(window, &width, &height);

      vector<unsigned char> data(width * height * num_channels);
      gl::ReadnPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data.size(), data.data());

      auto image = make_shared<ImageRGB>(ivec2(width, height), move(data));
      image = image::flipY(image);

      util::mkdir("il2ge_map_viewer");
      string path = "il2ge_map_viewer/screenshot_" + util::makeTimeStampString() + ".png";
      saveImageToFile(path, image.get(), ImageType::PNG);
    }
    else if (key == GLFW_KEY_KP_ADD && action == GLFW_PRESS) {
      camera_move_speed *= 2;
    }
    else if (key == GLFW_KEY_KP_SUBTRACT && action == GLFW_PRESS) {
      camera_move_speed /= 2;
    }
    else if (key == GLFW_KEY_KP_8 && action == GLFW_PRESS) {
      if (mods & GLFW_MOD_CONTROL)
        getCurrentCelestialBody().elevation += 0.1;
      else if (mods & GLFW_MOD_SHIFT)
        getCurrentCelestialBody().elevation += 1;
      else
        getCurrentCelestialBody().elevation += 10;
    }
    else if (key == GLFW_KEY_KP_2 && action == GLFW_PRESS) {
      if (mods & GLFW_MOD_CONTROL)
        getCurrentCelestialBody().elevation -= 0.1;
      else if (mods & GLFW_MOD_SHIFT)
        getCurrentCelestialBody().elevation -= 1;
      else
        getCurrentCelestialBody().elevation -= 10;
    }
    else if (key == GLFW_KEY_KP_4 && action == GLFW_PRESS)
    {
      getCurrentCelestialBody().azimuth += 10;
    }
    else if (key == GLFW_KEY_KP_6 && action == GLFW_PRESS)
    {
      getCurrentCelestialBody().azimuth -= 10;
    }
  }


  void ShowingMenu::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
      g_state = InputState::FREE_MOVEMENT;
    }
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
    {
      setActiveGroup(getActiveGroupIndex()-1);
    }
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
    {
      setActiveGroup(getActiveGroupIndex()+1);
    }

    else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS &&
             hasActiveParameter())
    {
      auto& p = getActiveParameter();
      if (p.isBoolean())
        p.toBoolean()->set(!p.toBoolean()->get());
      else
        g_state = InputState::EDITING_VALUE;
    }
    else if (key == GLFW_KEY_R && action == GLFW_PRESS &&
             hasActiveParameter())
    {
      getActiveParameter().reset();
    }
    else if (key == GLFW_KEY_F4 && action == GLFW_PRESS) {
      setActiveParameter(0);
    }
    else if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
      setActiveParameter(1);
    }
    else if (key == GLFW_KEY_F6 && action == GLFW_PRESS) {
      setActiveParameter(2);
    }
    else if (key == GLFW_KEY_F7 && action == GLFW_PRESS) {
      setActiveParameter(3);
    }
    else if (key == GLFW_KEY_F8 && action == GLFW_PRESS) {
      setActiveParameter(4);
    }
    else if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
      setActiveParameter(getActiveParameterIndex()-1);
    }
    else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
      setActiveParameter(getActiveParameterIndex()+1);
    }
  }


  void EditingValue::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    if ((key == GLFW_KEY_ESCAPE || key == GLFW_KEY_ENTER) && action == GLFW_PRESS)
    {
      g_state = InputState::SHOWING_MENU;
    }
    else if (key == GLFW_KEY_R && action == GLFW_PRESS &&
        hasActiveParameter())
    {
      getActiveParameter().reset();
    }

    if ((action == GLFW_PRESS || action == GLFW_REPEAT) && hasActiveParameter())
    {
      float step_factor = mods & GLFW_MOD_SHIFT ? 10 : 1;
      if (mods & GLFW_MOD_CONTROL)
        step_factor = 0.1;

      auto &p = getActiveParameter();

      switch (key)
      {
        case GLFW_KEY_PAGE_UP :
          setActiveParameter(getActiveParameterIndex() + 1);
          break;
        case GLFW_KEY_PAGE_DOWN:
          setActiveParameter(getActiveParameterIndex() - 1);
          break;
        case GLFW_KEY_R:
          p.reset();
          break;
        case GLFW_KEY_LEFT:
          p.decrease(step_factor);
          break;
        case GLFW_KEY_RIGHT:
          p.increase(step_factor);
          break;
        case GLFW_KEY_UP:
          p.increase(step_factor, Parameter::Dimension::Y);
          break;
        case GLFW_KEY_DOWN:
          p.decrease(step_factor, Parameter::Dimension::Y);
          break;
      }
    }
    else if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS &&
        hasActiveParameter())
    {
      auto step_size = mods & GLFW_MOD_ALT ? 1.0 :
          mods & GLFW_MOD_SHIFT ? 0.01 :
            mods & GLFW_MOD_CONTROL ? 10.0 : 0.1;
      getActiveParameter().increase(step_size);
    }
    else if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS &&
        hasActiveParameter())
    {
      auto step_size = mods & GLFW_MOD_ALT ? 1.0 :
          mods & GLFW_MOD_SHIFT ? 0.01 :
            mods & GLFW_MOD_CONTROL ? 10.0 : 0.1;
      getActiveParameter().decrease(step_size);
    }
    else if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT) &&
        hasActiveParameter())
    {
      auto step_size = 0.01;
      getActiveParameter().decrease(step_size);
    }
    else if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT) &&
        hasActiveParameter())
    {
      auto step_size = 0.01;
      getActiveParameter().increase(step_size);
    }
  }


  void processInput(GLFWwindow *window, float frame_delta)
  {
    if (glfwGetMouseButton(window, 1) == GLFW_PRESS)
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);

      if (last_cursor_pos != dvec2(-1, -1)) {
        dvec2 cursor_delta = last_cursor_pos - cursor_pos;
        last_cursor_pos = cursor_pos;

        dvec2 rotation = cursor_delta * mouse_rotation_speed;

        g_camera.yaw += rotation.x;
        g_camera.pitch += rotation.y;
      }
    }
    else if (glfwGetMouseButton(window, 0) == GLFW_PRESS)
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);

      g_scene->cursorPos(cursor_pos);
      if (g_shift_active)
        g_scene->unmark();
      else
        g_scene->mark();
    }
    else
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);

      assert(g_scene);
      g_scene->cursorPos(cursor_pos);
    }

    float move_speed = camera_move_speed;

    if (g_state == InputState::FREE_MOVEMENT)
    {
      if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        g_camera.moveForward(frame_delta * move_speed);
      }
      if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        g_camera.moveForward(frame_delta * -move_speed);
      }

      if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        g_camera.pitch -= getCameraRotationSpeed(g_camera, PITCH) * frame_delta;
      }
      if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        g_camera.pitch += getCameraRotationSpeed(g_camera, PITCH) * frame_delta;
      }
      if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        g_camera.yaw += getCameraRotationSpeed(g_camera, YAW) * frame_delta;
      }
      if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        g_camera.yaw -= getCameraRotationSpeed(g_camera, YAW) * frame_delta;
      }

  //     vec4 direction = g_camera.getDirection();
      if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
        g_camera.roll += getCameraRotationSpeed(g_camera, ROLL) * frame_delta;
      }
      if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
        g_camera.roll -= getCameraRotationSpeed(g_camera, ROLL) * frame_delta;
      }
    }
//     cout<<direction.x<<" "<<direction.y<<" "<<direction.z<<endl;

    g_camera.updateTransform();
  }


  void *getGLProcAddress(const char *name)
  {
    return (void*) glfwGetProcAddress(name);
  }


  void errorCallback(int error, const char* description)
  {
    fprintf(stderr, "Error: %s\n", description);
    exit(1);
  }


  void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    return getStateHandler().keyCallback(window, key, scancode, action, mods);
  }


} // namespace


void render_util::viewer::runFrontendSimple(util::Factory<Scene> f_create_scene,
                                            string app_name)
{
  initLog(app_name);

  glfwSetErrorCallback(errorCallback);

  if (!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
//   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
//   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

  glfwWindowHint(GLFW_VISIBLE, 0);

  GLFWwindow* window = glfwCreateWindow(1024, 768, "Test", NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);

  glfwSetKeyCallback(window, keyCallback);
  glfwSetMouseButtonCallback(window, mouseButtonCallback);

  auto gl_interface = std::make_unique<gl_binding::GL_Interface>(&getGLProcAddress);
  gl_binding::GL_Interface::setCurrent(gl_interface.get());

  auto context = std::make_unique<Context>();
  Context::setCurrent(context.get());

  StateModifier::init();

  g_scene = f_create_scene();
  assert(g_scene);

  g_scene->setup();

  g_camera.setPos(g_scene->getInitialCameraPos());

  CHECK_GL_ERROR();

  glfwShowWindow(window);

#if GLFW_VERSION_MINOR >= 2
  glfwMaximizeWindow(window);
#endif

  auto text_renderer = make_unique<TextRenderer>();

  ShaderSearchPath shader_search_path;
  shader_search_path.push_back(RENDER_UTIL_SHADER_DIR);

  auto menu_display = make_unique<TextDisplay>(shader_search_path);

  Clock::time_point last_frame_time = Clock::now();

  while (!glfwWindowShouldClose(window))
  {
//     gl::Finish();

    CHECK_GL_ERROR();

    Clock::time_point current_frame_time = Clock::now();
    std::chrono::milliseconds frame_delta =
      std::chrono::duration_cast<std::chrono::milliseconds>(current_frame_time - last_frame_time);
    last_frame_time = current_frame_time;

    glfwPollEvents();
    processInput(window, (float)frame_delta.count() / 1000.0);

    if (glfwWindowShouldClose(window))
    {
      CHECK_GL_ERROR();
      break;
    }

    updateMenu(*menu_display);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    StateModifier state;
    state.setDefaults();

    gl::Viewport(0, 0, width, height);

    gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
              GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    g_camera.setViewportSize(width, height);

    g_scene->update(frame_delta.count() / 1000.f);
    g_scene->render(g_camera);

    state.enableBlend(true);

    ostringstream stats;
    printStats(frame_delta.count(), stats, g_camera);

    const int font_height = 30;
    int offset = 0;

    auto draw_text = [&] (const std::string& text, glm::vec3 c, int x, int y)
    {
      text_renderer->SetColor(0,0,0);
      text_renderer->DrawText(text, x+1, y+1);
      text_renderer->DrawText(text, x+2, y+2);

      text_renderer->SetColor(c.x, c.y, c.z);
      text_renderer->DrawText(text, x, y);
    };

    draw_text(stats.str(), glm::vec3(1), 0, 0);
    offset += font_height;

    stats.str("");

#if 0
    Stats scene_stats;
    g_scene->printStats(scene_stats);
    if (!scene_stats.getLines().empty())
    {
      for (int i = 0; i < scene_stats.getLines().size(); i++)
      {
        auto line = scene_stats.getLines().at(i);
        draw_text(line, glm::vec3(1), 0, offset);
        offset += font_height;
      }
    }
#endif

    menu_display->draw(*text_renderer, 0, offset);

    glfwSwapBuffers(window);
  }

  cout<<endl;

  CHECK_GL_ERROR();

  text_renderer.reset();

  g_scene.reset();
  gl::Finish();

  CHECK_GL_ERROR();

  Context::setCurrent(nullptr);
  context.reset();

  gl_binding::GL_Interface::setCurrent(nullptr);
  gl_interface.reset();

  glfwMakeContextCurrent(0);

  glfwDestroyWindow(window);
  glfwTerminate();
}

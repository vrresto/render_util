/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_SCENE_VIEWER_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_SCENE_VIEWER_H

#include "properties.h"
#include <render_util/viewer/parameter.h>
#include <render_util/viewer/scene.h>

#include <ostream>


class TextRenderer;


namespace render_util::viewer
{
  struct CameraState;
}


namespace render_util::viewer::frontend_imgui
{


class SceneViewer
{
protected:
  Scene &m_scene;
  Parameters m_parameters;

  virtual void applyProperties() = 0;

public:
  enum Type
  {
    VIEWER_2D,
    VIEWER_3D
  };

  SceneViewer(Scene &scene) : m_scene(scene), m_parameters("Viewer") {}
  virtual ~SceneViewer() {}

  ParameterGroup& getParameters()
  {
    return m_parameters;
  }

  void setViewportSize(const glm::ivec2 &size)
  {
    getCamera().setViewportSize(size.x, size.y);
  }

  virtual void render()
  {
    m_scene.render(getCamera());
  }

  void showProperties()
  {
    frontend_imgui::showProperties(getParameters());
    applyProperties();
  }

  virtual render_util::Camera &getCamera() = 0;
  virtual void saveState(CameraState&) {};
  virtual void loadState(const CameraState&) {};

  virtual void handleInput(float frame_delta) = 0;
  virtual bool handleMouseInput(float frame_delta, const glm::vec2 &mouse_pos_window,
                                bool is_mouse_hovering,
                                bool is_focused) = 0;
  virtual void printStats(int frame_delta_ms, std::ostream &out) = 0;
};


std::unique_ptr<SceneViewer> createSceneViewer(SceneViewer::Type,
                                               Scene&,
                                               TextRenderer&,
                                               const ShaderSearchPath&);


} // namespace render_util::viewer::frontend_imgui


#endif

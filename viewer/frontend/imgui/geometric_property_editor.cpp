/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "geometric_property_editor.h"
#include "geometry_util.h"
#include "primitive_renderer.h"
#include "camera_2d.h"
#include <render_util/camera_3d.h>
#include <log.h>

#include <imgui.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>


using namespace render_util;
using namespace render_util::viewer;
using namespace render_util::viewer::frontend_imgui;

using UndoAction = GeometricPropertyEditor::UndoAction;


namespace render_util::viewer::frontend_imgui
{


GeometricPropertyEditor::GeometricPropertyEditor()
{
}


GeometricPropertyEditor::~GeometricPropertyEditor()
{
}


void GeometricPropertyEditor::showProperties()
{
  ImGui::BeginDisabled(m_undo_actions.empty());
  std::string undo_label = "Undo (" + std::to_string(m_undo_actions.size()) + ")";
  if (ImGui::Button(undo_label.c_str()))
    undo();
  ImGui::EndDisabled();

  ImGui::SameLine();

  ImGui::BeginDisabled(m_redo_actions.empty());
  std::string redo_label = "Redo (" + std::to_string(m_redo_actions.size()) + ")";
  if (ImGui::Button(redo_label.c_str()))
    redo();
  ImGui::EndDisabled();

  if (m_current_handler)
    m_current_handler->showProperties();
}


void GeometricPropertyEditor::draw(PrimitiveRenderer& renderer,
                                   glm::vec2 mouse_pos_window,
                                   const Camera& camera)
{
  if (m_current_handler)
    m_current_handler->draw(renderer, mouse_pos_window, camera);
}


void GeometricPropertyEditor::handleInput(glm::vec2 mouse_pos_window,
                                          const Camera& camera)
{
  if (m_current_handler)
    m_current_handler->handleInput(mouse_pos_window, camera);
}


void GeometricPropertyEditor::stopDrag()
{
    if (m_current_handler)
      m_current_handler->stopDrag();
}


void GeometricPropertyEditor::setProperty(IGeometricProperty* property)
{
  if (property)
  {
    if (m_current_handler && (m_current_handler->getProperty() != property))
      m_current_handler.reset();

    if (!m_current_handler)
    {
      switch (property->getType())
      {
        case IGeometricProperty::RECTANGLE:
          m_current_handler = createRectangularPropertyHandler(*this, property);
          break;
      }
    }
  }
  else
  {
    m_current_handler.reset();
  }
}


void GeometricPropertyEditor::addUndoAction(std::unique_ptr<UndoAction> action)
{
  LOG_DEBUG << "Adding undo action: " << action->getName() << std::endl;
  m_redo_actions = {};
  m_undo_actions.push(std::move(action));
}


void GeometricPropertyEditor::undo()
{
  assert(!m_undo_actions.empty());

  LOG_INFO << "Undoing: " << m_undo_actions.top()->getName() << std::endl;
  m_undo_actions.top()->undo();
  m_redo_actions.push(std::move(m_undo_actions.top()));
  m_undo_actions.pop();
}


void GeometricPropertyEditor::redo()
{
  assert(!m_redo_actions.empty());

  LOG_INFO << "Redoing: " << m_redo_actions.top()->getName() << std::endl;
  m_redo_actions.top()->redo();
  m_undo_actions.push(std::move(m_redo_actions.top()));
  m_redo_actions.pop();
}


} // namespace render_util::viewer::frontend_imgui

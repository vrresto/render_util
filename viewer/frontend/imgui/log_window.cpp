/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "log_window.h"
#include "utils.h"

#include <log.h>
#include <log/message_only_formatter.h>
#include <util/mutex.h>

#include <plog/Appenders/IAppender.h>
#include <imgui.h>

#include <array>
#include <list>
#include <string>
#include <utility>
#include <iostream>


namespace
{


constexpr auto MAX_LINES = 10000;
constexpr auto MAX_DISPLAY_LINES = 1000;


using Formatter = util::log::MessageOnlyFormatter<false>;


struct Line
{
  util::log::Verbosity verbosity {};
  std::string str;
};


struct SubLogger
{
  std::list<Line> m_lines;

  void write(const plog::util::nstring& str, util::log::Verbosity verbosity)
  {
    if (m_lines.empty())
    {
      m_lines.push_back({ verbosity, "" });
    }
    else if (verbosity != m_lines.back().verbosity)
    {
      if (m_lines.back().str.empty())
        m_lines.back().verbosity = verbosity;
      else
        m_lines.push_back({ verbosity, "" });
    }

    for (auto &c : str)
    {
      if (c == '\n')
        m_lines.push_back({ verbosity, "" });
      else
        m_lines.back().str.push_back(c);
    }

    while (m_lines.size() > MAX_LINES)
      m_lines.pop_front();
  }
};


class Logger : public plog::IAppender
{
  util::Mutex m_mutex;
  bool m_new_warnings = false;
  std::array<SubLogger, util::log::Verbosity_COUNT> m_sub_loggers;

public:
  template <class T>
  void visitLines(int verbosity, T&& func)
  {
    util::MutexLock lock(m_mutex);
    func(m_sub_loggers.at(verbosity).m_lines);
  }

  void clear()
  {
    util::MutexLock lock(m_mutex);
    for (auto& logger : m_sub_loggers)
      logger.m_lines.clear();
    m_new_warnings = false;
  }

  bool hasNewWarnings()
  {
    util::MutexLock lock(m_mutex);
    return std::exchange(m_new_warnings, false);
  }

  virtual void write(const plog::Record& record)
  {
    plog::util::nstring str = Formatter::format(record);
    auto verbosity = util::log::fromPlogSeverity(record.getSeverity());

    util::MutexLock lock(m_mutex);

    if (verbosity <= util::log::WARNING)
      m_new_warnings = true;

    for (int i = 0; i < util::log::Verbosity_COUNT; i++)
    {
      if (verbosity <= i)
        m_sub_loggers.at(i).write(str, verbosity);
    }
  }
};


Logger& getLogger()
{
  static Logger logger;
  return logger;
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


void initLog()
{
  plog::get<util::log::LOGGER_DEFAULT>()->addAppender(&getLogger());
}


void showLog(int& verbosity, ImFont *console_font)
{
  if (ImGui::BeginCombo("Verbosity", util::log::verbosityToString(verbosity)))
  {
    for (int s = 0; s < util::log::Verbosity_COUNT; s++)
    {
      if (ImGui::Selectable(util::log::verbosityToString(s), s == verbosity))
        verbosity = s;
    }
    ImGui::EndCombo();
  }

  ImGui::SameLine();

  if (ImGui::Button("Clear"))
  {
    getLogger().clear();
  }

  ImGui::Separator();

  ImGui::BeginChild("ScrollingRegion", ImVec2(0, 0), false,
                    ImGuiWindowFlags_HorizontalScrollbar);

  ImGui::PushFont(console_font);

  static std::vector<Line> display_lines;
  display_lines.clear();
  display_lines.reserve(MAX_DISPLAY_LINES);

  auto visit_func = [&] (auto& lines)
  {
    auto it = lines.rbegin();

    while (it != lines.rend() && display_lines.size() <= MAX_DISPLAY_LINES)
    {
      if (it->verbosity <= verbosity)
      {
        display_lines.push_back(*it);
      }
      it++;
    }
  };

  getLogger().visitLines(verbosity, visit_func);

  for (auto it = display_lines.rbegin(); it != display_lines.rend(); it++)
  {
    auto& line = *it;

    if (isSeparator(line.str))
    {
      ImGui::Separator();
    }
    else
    {
      if (line.verbosity <= util::log::WARNING)
      {
        ImVec4 color = (line.verbosity == util::log::WARNING) ?
          ImVec4(1,1,0,1) : ImVec4(1,0,0,1);
        ImGui::TextColored(color, line.str.c_str());
      }
      else
        ImGui::TextUnformatted(line.str.c_str());
    }
  }

  display_lines.clear();

  if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
      ImGui::SetScrollHereY(1.0f);

  ImGui::PopFont();

  ImGui::EndChild();
}


bool logHasNewWarnings()
{
  return getLogger().hasNewWarnings();
}


void clearLog()
{
  getLogger().clear();
}


} // namespace render_util::viewer::frontend_imgui

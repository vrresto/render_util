#ifndef IMGUI_USER_CONFIG_H
#define IMGUI_USER_CONFIG_H

#include "config.h"

#include <glm/glm.hpp>

#if ENABLE_VIEWER_IMGUI_FREETYPE
  #define IMGUI_ENABLE_FREETYPE
#endif

#define IM_VEC2_CLASS_EXTRA \
        ImVec2(const glm::ivec2& f) { x = f.x; y = f.y; } \
        operator glm::ivec2() const { return glm::ivec2(x,y); } \
        ImVec2(const glm::vec2& f) { x = f.x; y = f.y; } \
        operator glm::vec2() const { return glm::vec2(x,y); } \
        ImVec2(const glm::dvec2& f) { x = f.x; y = f.y; } \
        operator glm::dvec2() const { return glm::dvec2(x,y); }

#define IM_VEC4_CLASS_EXTRA \
        ImVec4(const glm::vec4& f) { x = f.x; y = f.y; z = f.z; w = f.w; } \
        operator glm::vec4() const { return glm::vec4(x,y,z,w); }

#endif

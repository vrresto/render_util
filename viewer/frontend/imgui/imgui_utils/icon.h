/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_ICON_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_ICON_H

#include <memory>


struct ImFont;


namespace render_util::viewer
{
  class Configuration;
}


namespace render_util::viewer::frontend_imgui
{


enum class IconID
{
//   NONE,
  UNDO,
  REDO,
  RESTORE,
  ARROW_UP,
  ARROW_DOWN,
  EDIT,
  SAVE,
  CLEAR,
  DELETE,
  DISCARD,
  CANCEL,
  CONTENT_COPY,
};


struct IconFont
{
  ImFont* font = nullptr;

  virtual const char* getFileName() = 0;
  virtual short unsigned int getMinGlyph() = 0;
  virtual short unsigned int getMaxGlyph() = 0;
  virtual const char* getIconString(IconID id) = 0;

  virtual const char* getName()
  {
    return getFileName();
  }
};


void createIconFont(Configuration* config = nullptr);
IconFont* getIconFont();
std::unique_ptr<IconFont> createMaterialDesignFont();


inline bool isIconFontLoaded()
{
  return getIconFont() != nullptr;
}


inline const char* getIconString(IconID id)
{
  return getIconFont()->getIconString(id);
}


} // namespace render_util::viewer::frontend_imgui

#endif

/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_POPUP_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_POPUP_H

#include <imgui.h>

#include <string>
#include <vector>


namespace render_util::viewer::frontend_imgui
{


class Popup
{
  std::string m_name;
  bool m_is_open = false;
  ImGuiWindowFlags m_flags = 0;

protected:
  virtual void showContents(bool& close) = 0;

public:
  Popup(std::string name, ImGuiWindowFlags flags = 0);
  virtual ~Popup();

  void show();
  bool isOpen() { return m_is_open; }

  virtual glm::ivec2 getInitialSize() { return glm::ivec2(-1); }
  virtual std::string getDisplayTitle() { return m_name; }
};


class PopupWithButtons : public Popup
{
  std::vector<std::string> m_buttons;
  int m_active_button = 0;
  std::string m_text;
  int m_pressed_button = NO_BUTTON;

  void showContents(bool& close) override;
  virtual void onClose() {}

public:
  enum
  {
    NOT_SHOWN = -2,
    NO_BUTTON = -1
  };

  PopupWithButtons(std::string title,
                   const std::vector<std::string> &buttons,
                   int default_button,
                   std::string text = {}, ImGuiWindowFlags flags = 0) :
    Popup(title, flags),
    m_buttons(buttons),
    m_active_button(default_button),
    m_text(text) {}



  int getPressedButton() { return m_pressed_button; }
};


class YesNoCancelBox : public PopupWithButtons
{
public:
  enum
  {
    BUTTON_YES = 0,
    BUTTON_NO,
    BUTTON_CANCEL
  };

  YesNoCancelBox(std::string title, std::string text = {}) :
    PopupWithButtons(title, { "Yes", "No", "Cancel"}, BUTTON_CANCEL, text,
                     ImGuiWindowFlags_NoNav | ImGuiWindowFlags_AlwaysAutoResize) {}
};


} // namespace render_util::viewer::frontend_imgui


#endif

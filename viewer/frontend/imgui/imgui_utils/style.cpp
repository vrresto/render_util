/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "style.h"
#include "widgets.h"

#include <imgui.h>

#include <string>
#include <functional>
#include <stdexcept>

namespace
{


struct Style
{
  std::string name;
  std::string url;
  std::string base_style;
  std::function<void()> apply;
};


void applyStyle(std::string name);
void applyStyle(Style& style);


std::vector<Style> g_styles =
{
  {
    "Dark", "", "", [] () { ImGui::StyleColorsDark(); }
  },
  {
    "Classic", "", "", [] () { ImGui::StyleColorsClassic(); }
  },
};


ImGuiStyle g_default_style;
int g_current_style_index = -1;


int getStyleIndex(std::string name)
{
  for (int i = 0; i < g_styles.size(); i++)
  {
    if (g_styles.at(i).name == name)
      return i;
  }
  throw std::runtime_error("No style with name " + name);
}


Style& getStyle(std::string name)
{
  return g_styles.at(getStyleIndex(name));
}


void applyStyle(std::string name)
{
  applyStyle(getStyle(name));
}


void applyStyle(Style& style)
{
  if (!style.base_style.empty())
    applyStyle(style.base_style);
  else
    ImGui::GetStyle() = g_default_style;

  style.apply();
}


void applyCurrentStyle()
{
  applyStyle(g_styles.at(g_current_style_index));
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


void initStyle()
{
  ImGui::StyleColorsDark();
  g_default_style = ImGui::GetStyle();
  g_current_style_index = 0;
  applyCurrentStyle();
}


void setStyle(std::string name)
{
  g_current_style_index = getStyleIndex(name);
  applyCurrentStyle();
}


std::string getCurrentStyleName()
{
  if (g_current_style_index != -1)
    return g_styles.at(g_current_style_index).name;
  else
    return {};
}


bool showStyleSelector()
{
  bool changed = false;

  std::string selected_label;
  std::string selected_url;
  if (g_current_style_index != -1)
  {
    selected_label = g_styles.at(g_current_style_index).name;
    selected_url = g_styles.at(g_current_style_index).url;
  }

  if (ImGui::BeginCombo("Colors", selected_label.c_str()))
  {
    for (int i = 0; i < g_styles.size(); i++)
    {
      auto label = g_styles.at(i).name;
      if (ImGui::Selectable(label.c_str(), i == g_current_style_index))
      {
        changed = true;
        g_current_style_index = i;
      }
    }

    ImGui::EndCombo();
  }

  if (!selected_url.empty())
  {
    ImGui::SameLine();
    ImGui::Button("(?)");
    ImGui::OpenPopupOnItemClick("style_info", ImGuiPopupFlags_MouseButtonLeft);
  }

  if (ImGui::BeginPopup("style_info"))
  {
    ImGui::AlignTextToFramePadding();
    ImGui::Text("Url:");
    ImGui::SameLine();
    showCopyableText(selected_url);
    ImGui::EndPopup();
  }

  if (changed)
  {
    applyCurrentStyle();
  }

  return changed;
}


} // namespace render_util::viewer::frontend_imgui

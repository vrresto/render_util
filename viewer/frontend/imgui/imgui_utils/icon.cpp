/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "icon.h"
#include <render_util/viewer/viewer.h>
#include <util/configuration.h>

#include <imgui.h>

#include <vector>
#include <string>


using namespace render_util::viewer::frontend_imgui;


namespace
{


IconFont* g_icon_font = nullptr;


std::vector<std::unique_ptr<IconFont>> createIconFonts()
{
  std::vector<std::unique_ptr<IconFont>> fonts;

  fonts.push_back(createMaterialDesignFont());

  return fonts;
}


const std::vector<std::unique_ptr<IconFont>>& getIconFonts()
{
  static std::vector<std::unique_ptr<IconFont>> fonts = createIconFonts();
  return fonts;
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


void createIconFont(Configuration* config)
{
  std::string font_name;
  float font_size {};

  if (config)
  {
    //FIXME
//     font_name = config->getValue("", "IconFont");
//     util::configuration::parseValue(config->getValue("", "IconFontSize"), font_size);
  }

  if (font_size < 1)
    font_size = 20;

  IconFont* font = nullptr;

  if (!font_name.empty())
  {
    for (auto& f : getIconFonts())
    {
      if (f->getName() == font_name)
      {
        font = f.get();
        break;
      }
    }

    if (!font)
    {
      LOG_ERROR << "No such icon font: " << font_name << std::endl;
      LOG_ERROR << "Valid names are:" << std::endl;
      for (auto& f : getIconFonts())
        LOG_ERROR << f->getName() << std::endl;
    }
  }

  if (!font && !getIconFonts().empty())
  {
    font = getIconFonts().front().get();
  }

  if (font)
  {
    if (!util::fileExists(font->getFileName()))
    {
      LOG_ERROR << "Icon font file doesn't exist: " << font->getFileName() << std::endl;
    }
    else
    {
      ImFontConfig config;
      static const ImWchar icon_ranges[] = { font->getMinGlyph(), font->getMaxGlyph(), 0 };
      auto res = ImGui::GetIO().Fonts->AddFontFromFileTTF(font->getFileName(),
                                              font_size, &config, icon_ranges);

      if (res)
      {
        font->font = res;
        g_icon_font = font;
      }
    }
  }

}


IconFont* getIconFont()
{
  return g_icon_font;
}


} // namespace render_util::viewer::frontend_imgui

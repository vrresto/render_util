/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "popup.h"

#include <imgui.h>
#include <GLFW/glfw3.h>


namespace render_util::viewer::frontend_imgui
{


Popup::Popup(std::string name, ImGuiWindowFlags flags) : m_name(name), m_flags(flags)
{
  assert(!ImGui::IsPopupOpen(nullptr, ImGuiPopupFlags_AnyPopupId
                                      | ImGuiPopupFlags_AnyPopupLevel));
  auto id = "###" + m_name;
  ImGui::OpenPopup(id.c_str());
  m_is_open = true;
}


Popup::~Popup()
{
  assert(!m_is_open);
}


void Popup::show()
{
  auto initial_size = getInitialSize();

  ImVec2 center = ImGui::GetMainViewport()->GetCenter();
  ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

  if (initial_size != glm::ivec2(-1))
    ImGui::SetNextWindowSize(initial_size, ImGuiCond_Appearing);

  auto id = getDisplayTitle() + "###" + m_name;
  if (ImGui::BeginPopupModal(id.c_str(), nullptr, m_flags))
  {
    bool close = false;

    showContents(close);

    if (close)
    {
      ImGui::CloseCurrentPopup();
      m_is_open = false;
    }

    ImGui::EndPopup();
  }
}


void PopupWithButtons::showContents(bool& close)
{
  int ret = NOT_SHOWN;

  {
    ret = NO_BUTTON;

    if (ImGui::IsKeyPressed(ImGuiKey_LeftArrow))
      m_active_button = std::max(m_active_button - 1, 0);

    if (ImGui::IsKeyPressed(ImGuiKey_RightArrow))
      m_active_button = std::min(m_active_button + 1, int(m_buttons.size()) - 1);

    if (!m_text.empty())
      ImGui::Text("%s", m_text.c_str());

    for (int i = 0; i < m_buttons.size(); i++)
    {
      if (i == m_active_button)
      {
        glm::vec4 color = ImGui::GetStyle().Colors[ImGuiCol_Button];
        color *= 1.5;
        ImGui::PushStyleColor(ImGuiCol_Button, color);
      }

      if (ImGui::Button(m_buttons[i].c_str(), ImVec2(120, 0)))
      {
        ret = i;
      }

      if (i == m_active_button)
        ImGui::PopStyleColor();

      ImGui::SameLine();
    }

    if (ImGui::IsKeyPressed(ImGuiKey_Enter))
      ret = m_active_button;

    if (ret >= 0)
    {
      m_pressed_button = ret;
      onClose();
      close = true;
    }
  }
}


} // namespace render_util::viewer::frontend_imgui

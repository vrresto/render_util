/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "icon.h"

#include <imgui.h>

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_WIDGETS_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_WIDGETS_H

namespace render_util::viewer::frontend_imgui
{


inline bool iconButton(IconID id, const std::string& label,
                       const ImVec2& size = ImVec2(0, 0))
{
  if (isIconFontLoaded())
  {
    ImGui::PushFont(getIconFont()->font);
    auto res = ImGui::Button(getIconString(id), size);
    ImGui::PopFont();
    return res;
  }
  else
  {
    return ImGui::Button(label.c_str(), size);
  }
}


inline bool arrowButton(const std::string& label, ImGuiDir dir)
{
  if (isIconFontLoaded())
  {
    switch (dir)
    {
      case ImGuiDir_Up:
        return iconButton(IconID::ARROW_UP, label);
      case ImGuiDir_Down:
        return iconButton(IconID::ARROW_DOWN, label);
      default:
        abort();
    }
  }
  else
  {
    return ImGui::ArrowButton(label.c_str(), dir);
  }
}


inline void showCopyableText(const std::string& text)
{
  ImGui::AlignTextToFramePadding();
  ImGui::Text("%s", text.c_str());
  ImGui::SameLine();
  if (iconButton(IconID::CONTENT_COPY, "Copy to clipboard"))
    ImGui::SetClipboardText(text.c_str());
}


} // namespace render_util::viewer::frontend_imgui

#endif

/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "icon.h"

#include <IconsMaterialDesign.h>

#include <cstdlib>


using namespace render_util::viewer::frontend_imgui;


namespace
{


struct Font : public IconFont
{
  const char* getIconString(IconID id) override
  {
    switch (id)
    {
      case IconID::UNDO:
        return ICON_MD_UNDO;
      case IconID::REDO:
        return ICON_MD_REDO;
      case IconID::CLEAR:
        return ICON_MD_CLEAR;
      case IconID::DELETE:
      case IconID::DISCARD:
        return ICON_MD_DELETE_FOREVER;
//         return ICON_MD_DELETE;
  //       return ICON_MD_CLEAR;
  //       return ICON_MD_DELETE_OUTLINE;
//         return ICON_MD_REMOVE_CIRCLE_OUTLINE;
      case IconID::ARROW_UP:
        return ICON_MD_ARROW_UPWARD;
      case IconID::ARROW_DOWN:
        return ICON_MD_ARROW_DOWNWARD;
      case IconID::RESTORE:
        return ICON_MD_RESTORE;
      case IconID::EDIT:
        return ICON_MD_EDIT;
      case IconID::SAVE:
        return ICON_MD_SAVE;
      case IconID::CANCEL:
        return ICON_MD_CANCEL;
      case IconID::CONTENT_COPY:
        return ICON_MD_CONTENT_COPY;
    };
    abort();
  }

  const char* getFileName() override
  {
    return FONT_ICON_FILE_NAME_MD;
  }

  short unsigned int getMinGlyph() override
  {
    return ICON_MIN_MD;
  }

  short unsigned int getMaxGlyph() override
  {
    return ICON_MAX_16_MD;
  }
};


} // namespace


namespace render_util::viewer::frontend_imgui
{
  std::unique_ptr<IconFont> createMaterialDesignFont()
  {
    return std::make_unique<Font>();
  }
}

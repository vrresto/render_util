/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fonts.h"
#include <render_util/viewer/configuration.h>
#include <util.h>

#include <imgui.h>

#include <array>
#include <map>
#include <set>
#include <vector>
#include <filesystem>


using namespace render_util::viewer;
using namespace render_util::viewer::frontend_imgui;


namespace
{


struct Font
{
  std::string name;
  std::string file_name;
  std::map<int, ImFont*> sizes;

  std::string getLabel()
  {
    if (!name.empty())
      return name;
    else if (!file_name.empty())
      return file_name;
    else
      return "<unknown>";
  }
};


struct FontParameters
{
  int size = 20;
  int index = 0;

  Font& getFont();
  ImFont& getImFont();
  std::string getFileName();
};


const std::array<std::string, FontRole_COUNT> g_font_role_names =
{
  "Default", "Console"
};

std::set<int> g_font_sizes = { 16, 17, 18, 19, 20 };
std::vector<Font> g_fonts;
std::array<FontParameters, FontRole_COUNT> g_parameters;


int getFontIndex(const std::string& file_name)
{
  int i = 0;
  for (auto& font : g_fonts)
  {
    if (font.file_name == file_name)
    {
      return i;
    }
    i++;
  }
  throw std::runtime_error("Font file not loaded: " + file_name);
}


void addBuiltInFont()
{
  Font font;
  font.name = "<built-in>";
  auto imfont = ImGui::GetIO().Fonts->AddFontDefault();
  for (auto& size : g_font_sizes)
    font.sizes[size] = imfont;
  g_fonts.push_back(font);
}


void addFont(std::string file_name)
{
  if (util::fileExists(file_name))
  {
    LOG_INFO << "Adding font: " << file_name << std::endl;

    ImFontConfig font_config;
    Font font;
    font.file_name = file_name;

    for (auto size : g_font_sizes)
    {
      font.sizes[size] = ImGui::GetIO().Fonts->AddFontFromFileTTF(file_name.c_str(),
                                                                  size, &font_config);
    }

    g_fonts.push_back(font);
  }
  else
  {
    LOG_ERROR << "Font file " << file_name << " not found" << std::endl;
  }
}


void addFontsInWorkingDir()
{
  std::filesystem::directory_iterator it(".");
  for (auto& entry : it)
  {
    if (!entry.is_regular_file())
      continue;

    auto& path = entry.path();

    if (util::makeLowercase(path.extension().string()) == ".ttf")
      addFont(path.generic_string());
  }
}


void updateDefaultFont()
{
  ImGui::GetIO().FontDefault = &getFont(DEFAULT_FONT);
}


Font& FontParameters::getFont()
{
  return g_fonts.at(index);
}


bool showFontSelector(FontRole role)
{
  bool changed = false;

  auto& params = g_parameters.at(role);

  auto name = g_font_role_names.at(role) + " font";
  if (ImGui::BeginCombo(name.c_str(), params.getFont().getLabel().c_str()))
  {
    int i = 0;
    for (auto& font : g_fonts)
    {
      ImGui::PushID(reinterpret_cast<intptr_t>(&font));

      if (ImGui::Selectable(font.getLabel().c_str(), i == params.index))
      {
        params.index = i;
        changed = true;
      }

      ImGui::PopID();
      i++;
    }
    ImGui::EndCombo();
  }

  name = g_font_role_names.at(role) + " font size";
  if (ImGui::BeginCombo(name.c_str(), std::to_string(params.size).c_str()))
  {
    for (auto& size : g_font_sizes)
    {
      ImGui::PushID(reinterpret_cast<intptr_t>(&size));

      if (ImGui::Selectable(std::to_string(size).c_str(), size == params.size))
      {
        params.size = size;
        changed = true;
      }

      ImGui::PopID();
    }
    ImGui::EndCombo();
  }

  if (changed && role == DEFAULT_FONT)
    updateDefaultFont();

  return changed;
}


void loadFontParameters(int role, const Configuration& config)
{
  auto& params = g_parameters.at(role);

  auto file_name = config.get("Font" + g_font_role_names.at(role), "");
  if (!file_name.empty())
    params.index = getFontIndex(file_name);

  params.size = config.get("Font" + g_font_role_names.at(role) + "Size",
                                params.size);
}


void saveFontParameters(int role, Configuration& config)
{
  auto& params = g_parameters.at(role);
  if (!params.getFileName().empty())
    config.set("Font" + g_font_role_names.at(role), params.getFileName());
  config.set("Font" + g_font_role_names.at(role) + "Size", std::to_string(params.size));
}


ImFont& FontParameters::getImFont()
{
  return *getFont().sizes.at(size);
}


std::string FontParameters::getFileName()
{
  return g_fonts.at(index).file_name;
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


ImFont& getFont(FontRole role)
{
  return g_parameters.at(role).getImFont();
}


void createFonts()
{
  addBuiltInFont();
  addFontsInWorkingDir();

  updateDefaultFont();
}


bool showFontSelector()
{
  bool changed = false;

  changed = changed || ::showFontSelector(DEFAULT_FONT);
  changed = changed || ::showFontSelector(CONSOLE_FONT);

  return changed;
}


void loadFontParameters(const Configuration& config)
{
  for (int i = 0; i < FontRole_COUNT; i++)
    ::loadFontParameters(i, config);

  updateDefaultFont();
}


void saveFontParameters(Configuration& config)
{
  for (int i = 0; i < FontRole_COUNT; i++)
    ::saveFontParameters(i, config);
}


} // namespace render_util::viewer::frontend_imgui



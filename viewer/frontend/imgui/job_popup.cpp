/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "job_popup.h"
#include "properties.h"
#include "utils.h"
#include <util/output_terminal.h>
#include <log.h>

using namespace render_util::viewer;
using namespace render_util::viewer::frontend_imgui;


namespace
{


void showJobOutput(Job &job, ImFont& console_font, util::OutputTerminal& job_output)
{
  for (auto &line : job_output.getLines())
  {
    if (isSeparator(line))
      ImGui::Separator();
    else
      ImGui::TextUnformatted(line.c_str());
  }

  if (job.isFinished())
  {
    ImGui::Separator();
    if (job.wasSuccessful())
      ImGui::TextColored(ImVec4(0,1,0,1), "Finished");
    else if (job.wasCanceled())
      ImGui::TextColored(ImVec4(1,1,0,1), "Canceled");
    else
      ImGui::TextColored(ImVec4(1,0,0,1), "Error");
  }
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


void JobProgressPopup::showContents(bool& close)
{
  if (!m_job->isFinished())
  {
    m_job->step(&m_terminal);

    if (m_job->isFinished())
    {
      if (m_job->wasSuccessful())
      {
        LOG_INFO << "Job \"" << m_job->getName() << "\" finished successfully" << std::endl;
      }
      else if (m_job->wasCanceled())
      {
        LOG_WARNING << "Job \"" << m_job->getName() << "\" was canceled" << std::endl;
      }
      else
      {
        LOG_ERROR << "Job \"" << m_job->getName() << "\" failed" << std::endl;
      }
    }
  }

  {
    const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y +
                                          ImGui::GetFrameHeightWithSpacing();

    ImGui::PushFont(&m_console_font);
    ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), false,
                      ImGuiWindowFlags_HorizontalScrollbar);
    showJobOutput(*m_job, m_console_font, m_terminal);
    if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
        ImGui::SetScrollHereY(1.0f);
    ImGui::PopFont();
    ImGui::EndChild();

    if (m_job->isFinished())
    {
      if (ImGui::Button("Close"))
        close = true;
    }
    else
    {
      if (ImGui::Button("Cancel"))
        m_job->cancel();
    }
  }
}


std::string JobProgressPopup::getDisplayTitle()
{
  auto title = m_job->getVerboseName();
  if (m_job->isFinished())
  {
    if (m_job->wasSuccessful())
      title += " - Finished";
    else if (m_job->wasCanceled())
      title += " - Canceled";
    else
      title += " - Error";
  }
  return title;
}


glm::ivec2 JobProgressPopup::getInitialSize()
{
  auto size = glm::vec2(ImGui::GetMainViewport()->Size);
  size.x *= 0.4;
  size.y *= 0.7;
  return size;
}


JobProgressPopup::JobProgressPopup(std::unique_ptr<Job> job,
                  util::OutputTerminal& terminal,
                  ImFont& console_font) :
  Popup("###" + job->getName()),
  m_terminal(terminal),
  m_console_font(console_font),
  m_job(std::move(job))
{
  m_job->start();
}



void JobStarterPopup::showContents(bool& close)
{
  assert(!m_job);

  showProperties(m_starter->getParameters(), false);

  if (m_starter->isReady())
  {
    if (ImGui::Button("Start"))
    {
      close = true;
      m_job = m_starter->createJob();
    }
    ImGui::SameLine();
  }

  if (ImGui::Button("Cancel"))
  {
    close = true;
  }
}


std::string JobStarterPopup::getDisplayTitle()
{
  return m_title;
}


JobStarterPopup::JobStarterPopup(const Action& action, std::unique_ptr<Job>& job) :
  Popup(action.name, ImGuiWindowFlags_AlwaysAutoResize),
  m_job(job),
  m_title(action.long_name)
{
  assert(!m_job);
  m_starter = action.createJobStarter();
}


} // namespace render_util::viewer::frontend_imgui

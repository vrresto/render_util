/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "geometric_property_editor.h"
#include "geometry_util.h"
#include "primitive_renderer.h"
#include "camera_2d.h"
#include <render_util/viewer/parameter.h>
#include <render_util/camera_3d.h>
#include <log.h>

#include <imgui.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>


using namespace render_util;
using namespace render_util::viewer;
using namespace render_util::viewer::frontend_imgui;

using Edge = RectD::Edge;
using Corner = RectD::Corner;


namespace
{


class Modifier;
class Mode;
class RectangularPropertyHandler;


const glm::vec4 normal_color(1,0,0,1);
const glm::vec4 dragging_color(1,1,1,1);
const glm::vec4 anchor_color(1, 0.5, 0, 1);
const glm::vec4 mouse_hover_color(1,1,0,1);


enum Dimension { X, Y };


bool isOrigin(Edge edge)
{
  switch (edge)
  {
    case Edge::LEFT:
    case Edge::BOTTOM:
      return true;
    case Edge::RIGHT:
    case Edge::TOP:
      return false;
    default:
      abort();
  }
}


int getDimension(Edge edge)
{
  switch (edge)
  {
    case Edge::LEFT:
    case Edge::RIGHT:
      return Dimension::X;
    case Edge::BOTTOM:
    case Edge::TOP:
      return Dimension::Y;
    default:
      abort();
  }
}


bool getGroundHitPoint(glm::vec2 mouse_pos_viewport,
                       const Camera& camera,
                       glm::dvec3& ground_hit_point)
{
  bool ground_hit = false;

  if (camera.is2D())
  {
    auto cam2d = dynamic_cast<const Camera2D*>(&camera);
    assert(cam2d);
    ground_hit_point = glm::dvec3(cam2d->viewportToWorld(mouse_pos_viewport), 0);
    ground_hit = true;
  }
  else
  {
    auto cam3d = dynamic_cast<const Camera3D*>(&camera);
    assert(cam3d);
    auto ray = cam3d->castRayThroughViewportCoords(mouse_pos_viewport);
    float dist = 0;
    glm::intersectRayPlane(ray.origin, ray.direction, glm::vec3(0), glm::vec3(0,0,1), dist);
    if (dist > 0)
    {
      ground_hit_point = ray.origin + ray.direction * dist;
      ground_hit = true;
    }
  }

  return ground_hit;
}


Corner showCornerSelector(const Corner currently_selected, const char* label)
{
  std::array<Corner, 4> corners =
  {
    Corner::TOP_LEFT,
    Corner::TOP_RIGHT,
    Corner::BOTTOM_LEFT,
    Corner::BOTTOM_RIGHT,
  };

  auto new_selected = currently_selected;

  if (ImGui::BeginTable("select_corner_row", 2, 0))
  {
    ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthFixed);
    ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthStretch);

    ImGui::TableNextColumn();

    ImGui::PushStyleVar(ImGuiStyleVar_CellPadding, glm::vec2(4));
    if (ImGui::BeginTable("select_corner", 2, ImGuiTableFlags_BordersOuter |
                                              ImGuiTableFlags_NoHostExtendX |
                                              ImGuiTableFlags_SizingFixedFit))
    {
      for (auto& corner : corners)
      {
        ImGui::TableNextColumn();

        bool active = currently_selected == corner;

        if (!active)
        {
          glm::vec4 color = ImGui::GetStyle().Colors[ImGuiCol_WindowBg];
          ImGui::PushStyleColor(ImGuiCol_Button, color);
        }

        // HACK fix padding - workaround for ImGui bug
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() - 1);

        if (ImGui::Button(("##" + std::to_string(corner)).c_str(), glm::vec2(20)))
        {
          new_selected = corner;
        }

        if (!active)
          ImGui::PopStyleColor();
      }

      ImGui::EndTable();
    }
    ImGui::PopStyleVar();

    ImGui::TableNextColumn();

    auto size = ImGui::GetItemRectSize();
    auto pos_y = size.y / 2.f - ImGui::GetTextLineHeight() / 2.0;
    ImGui::SetCursorPosY(ImGui::GetCursorPosY() + pos_y);

    ImGui::Text(label);

    ImGui::EndTable();
  }

  return new_selected;
}


class Modifier
{
  std::unique_ptr<IGeometricPropertyState> m_previous_state;

protected:
  IRectangularProperty& m_property;
  IRectangularPropertyFixedAspect* m_fixed_aspect = nullptr;

  void anchor(Corner corner, glm::dvec2 anchor_pos);

public:
  const std::string name;

  Modifier(std::string name, IRectangularProperty& property) :
    m_previous_state(property.getState()),
    m_property(property),
    name(name)
  {
    m_fixed_aspect = dynamic_cast<IRectangularPropertyFixedAspect*>(&m_property);
  }

  virtual bool hasChanges() { return true; }
  virtual bool isDragModifier() { return false; }

  std::unique_ptr<IGeometricPropertyState> takePreviousState()
  {
    assert(m_previous_state);
    return std::move(m_previous_state);
  }

  void setOrigin(glm::dvec2 origin)
  {
    m_property.setOrigin(origin);
  }

  void setExtent(int dim, double extent, Corner anchor_corner);

  void setExtentX(double extent, Corner anchor_corner)
  {
    setExtent(X, extent, anchor_corner);
  }

  void setExtentY(double extent, Corner anchor_corner)
  {
    setExtent(Y, extent, anchor_corner);
  }
};


class DragModifier : public Modifier
{
  glm::dvec2 m_initial_cursor_pos_world {};
  glm::dvec2 m_last_cursor_pos_world {};
  glm::dvec2 m_delta {};
  bool m_has_changes = false;

public:
  DragModifier(std::string name,
               glm::dvec2 initial_cursor_pos_world,
               IRectangularProperty& property) :
    Modifier(name, property),
    m_initial_cursor_pos_world(initial_cursor_pos_world),
    m_last_cursor_pos_world(initial_cursor_pos_world) {}

  virtual void update(glm::dvec2 current_cursor_pos_world);

  bool isDragModifier() override { return true; }
  bool hasChanges() override { return m_has_changes; }

  glm::dvec2 getDelta() { return m_delta; }
};


class DragOriginModifier : public DragModifier
{
public:
  DragOriginModifier(glm::dvec2 initial_cursor_pos_world, IRectangularProperty& property) :
    DragModifier("Drag origin", initial_cursor_pos_world, property) {}

  void update(glm::dvec2 current_cursor_pos_world) override;
};


class DragEdgeModifier : public DragModifier
{
  Edge m_edge {};
  Corner m_anchor {};

public:
  DragEdgeModifier(Edge edge, Corner anchor, glm::dvec2 initial_cursor_pos_world,
                   IRectangularProperty& property);

  void update(glm::dvec2 current_cursor_pos_world) override;

  Edge getEdge() const;
};


class Mode
{
protected:
  RectangularPropertyHandler& m_parent;

public:
  Mode(RectangularPropertyHandler& parent) : m_parent(parent) {}

  enum ID { EDGES, POSITION };

  virtual ~Mode() {}

  virtual ID getID() = 0;
  virtual void showProperties(const RectD& rect) = 0;
  virtual void draw(const RectD& rect,
                     PrimitiveRenderer& renderer,
                     glm::vec2 mouse_pos_window,
                     const Camera& camera) = 0;
  virtual std::unique_ptr<Modifier> handleMouseDown(IRectangularProperty&,
                                                  glm::dvec2 cursor_pos_world) = 0;
};


class PositionMode : public Mode
{
public:
  PositionMode(RectangularPropertyHandler& parent) : Mode(parent) {}

  ID getID() override { return POSITION; }
  void showProperties(const RectD& rect) override;
  void draw(const RectD& rect,
                     PrimitiveRenderer& renderer,
                     glm::vec2 mouse_pos_window,
                     const Camera& camera) override;
  std::unique_ptr<Modifier> handleMouseDown(IRectangularProperty&,
                                          glm::dvec2 cursor_pos_world)  override;
};


class EdgesMode : public Mode
{
  Corner m_anchor = RectD::BOTTOM_LEFT;

  bool isEdgeAnchored(Edge edge);

public:
  EdgesMode(RectangularPropertyHandler& parent) : Mode(parent) {}

  ID getID() override { return EDGES; }
  void showProperties(const RectD& rect) override;
  void draw(const RectD& rect,
                     PrimitiveRenderer& renderer,
                     glm::vec2 mouse_pos_window,
                     const Camera& camera) override;
  std::unique_ptr<Modifier> handleMouseDown(IRectangularProperty&,
                                          glm::dvec2 cursor_pos_world)  override;
};


class RectangularPropertyHandler : public GeometricPropertyEditor::SubTypeHandler
{
  GeometricPropertyEditor& m_parent;
  IRectangularProperty& m_property;
  std::unique_ptr<Modifier> m_current_modifier;

  std::unique_ptr<Mode> m_mode;

  void showProperties() override;
  void draw(PrimitiveRenderer&, glm::vec2 mouse_pos_window, const Camera&) override;
  void handleInput(glm::vec2 mouse_pos_window, const Camera&) override;
  void stopDrag() override;
  IGeometricProperty* getProperty() override { return &m_property; }
  void endCurrentModifier();

  void setMode(Mode::ID id);

public:
  RectangularPropertyHandler(GeometricPropertyEditor& parent, IRectangularProperty& property) :
    m_parent(parent), m_property(property) {}

  ~RectangularPropertyHandler() {}


  bool isDragging();
  void editingFinished();
  void finishDrag();
  Modifier& getModifier();
  const Modifier* getCurrentModifier() { return m_current_modifier.get(); }
};


void Modifier::anchor(Corner corner, glm::dvec2 anchor_pos)
{
  auto extent = m_property.getRect().getExtent();

  switch (corner)
  {
    case Corner::BOTTOM_LEFT:
      break;
    case Corner::TOP_LEFT:
      setOrigin(anchor_pos - extent * glm::dvec2(0, 1));
      break;
    case Corner::TOP_RIGHT:
      setOrigin(anchor_pos - extent);
      break;
    case Corner::BOTTOM_RIGHT:
      setOrigin(anchor_pos - extent * glm::dvec2(1, 0));
      break;
  }
}


void Modifier::setExtent(int dim, double extent, Corner anchor_corner)
{
  assert(m_fixed_aspect);
  auto anchor_pos = m_fixed_aspect->getRect().getCorner(anchor_corner);
  switch (dim)
  {
    case Dimension::X:
      m_fixed_aspect->setExtentX(extent);
      break;
    case Dimension::Y:
      m_fixed_aspect->setExtentY(extent);
      break;
  }
  anchor(anchor_corner, anchor_pos);
}


void DragModifier::update(glm::dvec2 current_cursor_pos_world)
{
  m_delta = current_cursor_pos_world - m_last_cursor_pos_world;
  m_last_cursor_pos_world = current_cursor_pos_world;

  if (m_delta != glm::dvec2(0))
    m_has_changes = true;
}


void DragOriginModifier::update(glm::dvec2 current_cursor_pos_world)
{
  DragModifier::update(current_cursor_pos_world);

  if (hasChanges())
  {
    auto rect = m_property.getRect();
    m_property.setOrigin(rect.getOrigin() + getDelta());
  }
}


DragEdgeModifier::DragEdgeModifier(Edge edge,
                                   Corner anchor,
                                   glm::dvec2 initial_cursor_pos_world,
                                   IRectangularProperty& property) :
  DragModifier("Drag edge", initial_cursor_pos_world, property),
  m_edge(edge),
  m_anchor(anchor)
{
  assert(m_edge != Edge::NONE);
}


void DragEdgeModifier::update(glm::dvec2 current_cursor_pos_world)
{
  DragModifier::update(current_cursor_pos_world);

  if (!hasChanges())
    return;

  auto rect = m_property.getRect();

  auto dim = getDimension(getEdge());

  auto min_pos = rect.getOrigin()[dim];
  auto max_pos = min_pos + rect.getExtent()[dim];
  auto delta = getDelta()[dim];

  bool is_origin = isOrigin(getEdge());

  double extent_new = 0;

  if (is_origin)
  {
    auto min_pos_new = min_pos + delta;
    extent_new = max_pos - min_pos_new;
  }
  else
  {
    auto max_pos_new = max_pos + delta;
    extent_new = max_pos_new - min_pos;
  }

  extent_new = glm::max(1.0, extent_new);

  setExtent(dim, extent_new, m_anchor);
}


Edge DragEdgeModifier::getEdge() const
{
  assert(m_edge != Edge::NONE);
  return m_edge;
}


void PositionMode::showProperties(const RectD& rect)
{
  // FIXME don't convert to single precision
  glm::vec2 origin = rect.getOrigin();
  float origin_values[2] =
  {
    float(origin.x),
    float(origin.y)
  };
  bool origin_changed = ImGui::DragFloat2("Origin", origin_values, 1000);
  bool is_deactivated = ImGui::IsItemDeactivated();

  if (origin_changed)
  {
    assert(!m_parent.isDragging());
    glm::dvec2 origin_new(origin_values[0], origin_values[1]);
    m_parent.getModifier().setOrigin(origin_new);
  }

  if (is_deactivated)
  {
    m_parent.editingFinished();
  }
}


void PositionMode::draw(const RectD& rect,
                        PrimitiveRenderer& renderer,
                        glm::vec2 mouse_pos_window,
                        const Camera& camera)
{
  if (m_parent.isDragging())
  {
    renderer.drawRect(rect, dragging_color, camera);
  }
}


std::unique_ptr<Modifier> PositionMode::handleMouseDown(IRectangularProperty& property,
                                                        glm::dvec2 cursor_pos_world)
{
  return std::make_unique<DragOriginModifier>(cursor_pos_world, property);
}


void EdgesMode::showProperties(const RectD& rect)
{
  m_anchor = showCornerSelector(m_anchor, "Anchor");

  bool anchor_bottom = m_anchor == RectD::BOTTOM_LEFT || m_anchor == RectD::BOTTOM_RIGHT;
  bool anchor_left = m_anchor == RectD::BOTTOM_LEFT || m_anchor == RectD::TOP_LEFT;

  bool any_active = false;

  // FIXME don't convert to single precision
  glm::vec2 min_pos = rect.getOrigin();

  ImGui::BeginDisabled(anchor_left);
  bool min_pos_x_changed = ImGui::DragFloat("Left", &min_pos.x, 1000.f);
  any_active = any_active || ImGui::IsItemActive();
  ImGui::EndDisabled();

  ImGui::BeginDisabled(anchor_bottom);
  bool min_pos_y_changed = ImGui::DragFloat("Bottom", &min_pos.y, 1000.f);
  any_active = any_active || ImGui::IsItemActive();
  ImGui::EndDisabled();

  // FIXME don't convert to single precision
  glm::vec2 max_pos = rect.getOrigin() +  rect.getExtent();

  ImGui::BeginDisabled(!anchor_left);
  bool max_pos_x_changed = ImGui::DragFloat("Right", &max_pos.x, 1000.f);
  any_active = any_active || ImGui::IsItemActive();
  ImGui::EndDisabled();

  ImGui::BeginDisabled(!anchor_bottom);
  bool max_pos_y_changed = ImGui::DragFloat("Top", &max_pos.y, 1000.f);
  any_active = any_active || ImGui::IsItemActive();
  ImGui::EndDisabled();

  bool any_changed = min_pos_x_changed || min_pos_y_changed ||
                     max_pos_x_changed || max_pos_y_changed;

//   ImGui::Text("Any active = %d", any_active);

  if (any_changed)
  {
    assert(!m_parent.isDragging());

    auto& modifier = m_parent.getModifier();

    if (min_pos_x_changed || max_pos_x_changed)
    {
      auto extent_new = max_pos.x - min_pos.x;
      extent_new = glm::max(1.f, extent_new);
      modifier.setExtentX(extent_new, m_anchor);
    }
    else if (min_pos_y_changed || max_pos_y_changed)
    {
      auto extent_new = max_pos.y - min_pos.y;
      extent_new = glm::max(1.f, extent_new);
      modifier.setExtentY(extent_new, m_anchor);
    }
  }

    ///FIXME use ImGui::IsItemDeactivatedAfterEdit() ?
  if (!any_active)
  {
    m_parent.editingFinished();
  }
}


void EdgesMode::draw(const RectD& rect,
                     PrimitiveRenderer& renderer,
                     glm::vec2 mouse_pos_window,
                     const Camera& camera)
{
  auto mouse_pos_viewport = glm::vec2(mouse_pos_window.x,
                                      camera.getViewportSize().y - mouse_pos_window.y);

  glm::dvec3 ground_hit_point {};
  bool ground_hit = getGroundHitPoint(mouse_pos_viewport, camera, ground_hit_point);

  bool is_dragging_edge = false;

  if (m_parent.isDragging())
  {
    auto drag_edge = dynamic_cast<const DragEdgeModifier*>(m_parent.getCurrentModifier());
    if (drag_edge)
    {
      is_dragging_edge = true;

      auto edge_line = rect.getEdge(drag_edge->getEdge());
      renderer.drawLine(edge_line.at(0), edge_line.at(1), 3, dragging_color, camera);
    }
  }

  auto anchor_pos = rect.getCorner(m_anchor);
  renderer.drawFilledCircle(anchor_pos, 5, anchor_color, camera);

  if (ground_hit)
  {
    if (!is_dragging_edge)
    {
      auto edge = getNearestEdge(glm::dvec2(ground_hit_point), rect);

      if (edge != Edge::NONE && !isEdgeAnchored(edge))
      {
        auto edge_line = rect.getEdge(edge);
        renderer.drawLine(edge_line.at(0), edge_line.at(1), 3, mouse_hover_color, camera);
      }
    }
  }
}


std::unique_ptr<Modifier> EdgesMode::handleMouseDown(IRectangularProperty& property,
                                                    glm::dvec2 cursor_pos_world)
{
  auto edge = getNearestEdge(glm::dvec2(cursor_pos_world), property.getRect());

  if (edge != Edge::NONE && !isEdgeAnchored(edge))
  {
    return std::make_unique<DragEdgeModifier>(edge, m_anchor, cursor_pos_world, property);
  }

  return nullptr;
}


bool EdgesMode::isEdgeAnchored(Edge edge)
{
  switch (edge)
  {
    case Edge::LEFT:
      return m_anchor == Corner::TOP_LEFT || m_anchor ==  Corner::BOTTOM_LEFT;
    case Edge::RIGHT:
      return m_anchor == Corner::TOP_RIGHT || m_anchor ==  Corner::BOTTOM_RIGHT;
    case Edge::TOP:
      return m_anchor == Corner::TOP_LEFT || m_anchor ==  Corner::TOP_RIGHT;
    case Edge::BOTTOM:
      return m_anchor == Corner::BOTTOM_LEFT || m_anchor ==  Corner::BOTTOM_RIGHT;
    default:
      abort();
  }
}


void RectangularPropertyHandler::showProperties()
{
  if (ImGui::BeginTabBar("tabs"))
  {
    if (ImGui::BeginTabItem("Position"))
    {
      setMode(Mode::POSITION);
      m_mode->showProperties(m_property.getRect());
      ImGui::EndTabItem();
    }

    if (ImGui::BeginTabItem("Edges"))
    {
      setMode(Mode::EDGES);
      m_mode->showProperties(m_property.getRect());
      ImGui::EndTabItem();
    }

    ImGui::EndTabBar();
  }
}


void RectangularPropertyHandler::draw(PrimitiveRenderer& renderer,
                                      glm::vec2 mouse_pos_window,
                                      const Camera& camera)
{
  auto rect = m_property.getRect();

  renderer.drawRect(rect, normal_color, camera);

  if (m_mode)
    m_mode->draw(rect, renderer, mouse_pos_window, camera);
}


void RectangularPropertyHandler::handleInput(glm::vec2 mouse_pos_window,
                                             const Camera& camera)
{
  auto mouse_pos_viewport = glm::vec2(mouse_pos_window.x,
                                    camera.getViewportSize().y - mouse_pos_window.y);

  glm::dvec3 ground_hit_point {};
  bool ground_hit = getGroundHitPoint(mouse_pos_viewport, camera, ground_hit_point);

  if (ImGui::IsMouseDown(0))
  {
    if (!m_current_modifier)
    {
      if (ground_hit)
      {
        m_current_modifier = m_mode->handleMouseDown(m_property, ground_hit_point);
      }
    }
  }

  if (ImGui::IsMouseReleased(0))
  {
    finishDrag();
  }

  if (isDragging())
  {
    auto drag_action = dynamic_cast<DragModifier*>(m_current_modifier.get());
    assert(drag_action);

    if (ground_hit)
      drag_action->update(ground_hit_point);
  }
}


void RectangularPropertyHandler::stopDrag()
{
  finishDrag();
}


void RectangularPropertyHandler::setMode(Mode::ID id)
{
  if (m_mode && m_mode->getID() == id)
    return;

  switch (id)
  {
    case Mode::EDGES:
      m_mode = std::make_unique<EdgesMode>(*this);
      break;
    case Mode::POSITION:
      m_mode = std::make_unique<PositionMode>(*this);
      break;
  }

  assert(m_mode);
}


Modifier& RectangularPropertyHandler::getModifier()
{
  if (!m_current_modifier)
    m_current_modifier = std::make_unique<Modifier>("Modify rectangle", m_property);

  return *m_current_modifier;
}


bool RectangularPropertyHandler::isDragging()
{
  return m_current_modifier && m_current_modifier->isDragModifier();
}


void RectangularPropertyHandler::finishDrag()
{
  if (isDragging())
    endCurrentModifier();
}


void RectangularPropertyHandler::editingFinished()
{
  if (!isDragging())
    endCurrentModifier();
}


void RectangularPropertyHandler::endCurrentModifier()
{
  if (m_current_modifier)
  {
    if (m_current_modifier->hasChanges())
    {
      auto undo_name = m_current_modifier->name + " of " + m_property.getName();
      auto undo = std::make_unique<GeometricPropertyEditor::UndoAction>(undo_name,
        m_property,
        m_current_modifier->takePreviousState());

      m_parent.addUndoAction(std::move(undo));
    }

    m_current_modifier.reset();
  }
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


std::unique_ptr<GeometricPropertyEditor::SubTypeHandler>
createRectangularPropertyHandler(GeometricPropertyEditor& parent,
                                 IGeometricProperty* property)
{
  assert(property);

  auto rectangular = dynamic_cast<IRectangularProperty*>(property);
  assert(rectangular);

  return std::make_unique<RectangularPropertyHandler>(parent, *rectangular);
}


} // namespace render_util::viewer::frontend_imgui

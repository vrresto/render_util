/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "properties.h"
#include "utils.h"
#include "widgets.h"
#include <log.h>

#include <imgui.h>
#include <glm/gtc/type_ptr.hpp>

#include <stdexcept>

using namespace render_util::viewer::frontend_imgui;
using namespace render_util::viewer;


namespace
{


bool editButton()
{
  if (isIconFontLoaded())
    return iconButton(IconID::EDIT, "Edit");
  else
    return ImGui::ArrowButton("Edit", ImGuiDir_Right);
}


int getImGuiType(NumericParameter::Type type)
{
  switch (type)
  {
    case NumericParameter::INT32: return ImGuiDataType_S32;
    case NumericParameter::FLOAT: return ImGuiDataType_Float;
    case NumericParameter::DOUBLE: return ImGuiDataType_Double;
    default:
      throw std::runtime_error("Unhandled type: " + std::to_string(type));
  }
}


void showProperty(Parameter& parameter, bool show_reset_button = true, float right_margin = 0)
{
  std::string input_name = "##input";

  if (parameter.isNumeric())
  {
    auto param = parameter.toNumeric();
    auto type = getImGuiType(param->componentType());

    if (param->isTextureSelector())
    {
      auto selector = param->toTextureSelector();
      int selected_index = -1;

      auto texture = selector->getSelectedTexture();
      if (texture)
      {
        if (ImGui::ImageButton(reinterpret_cast<void*>(texture),
                               glm::vec2(25), glm::vec2(0), glm::vec2(1), 1))
        {
          ImGui::OpenPopup("select_texture");
        }
      }
      else
      {
        if (ImGui::Button("No Texture"))
        {
          ImGui::OpenPopup("select_texture");
        }
      }

      if (ImGui::BeginPopup("select_texture"))
      {
        const int cols = 5;
        const int button_size = 200;
        const int selected_border = 8;

        if (ImGui::BeginTable("select_texture_table", cols, ImGuiTableFlags_SizingFixedSame))
        {
          for (int i = 0; i < cols; i++)
            ImGui::TableSetupColumn(nullptr, 0, button_size);

          for (int i = 0; i < selector->getNumTextures(); i++)
          {
            int col = i % cols;
            if (col == 0)
              ImGui::TableNextRow(0, button_size);

            if (ImGui::TableSetColumnIndex(col))
            {
              int border = (selector->getSelectedIndex() == i) ? selected_border : 1;

              auto texture = selector->getTexture(i);
              if (texture)
              {
                if (ImGui::ImageButton(reinterpret_cast<void*>(texture),
                                       glm::vec2(button_size - 2 * border),
                                       glm::vec2(0), glm::vec2(1), border))
                {
                  selected_index = i;
                }
              }
            }
          }
          ImGui::EndTable();
        }
        ImGui::EndPopup();
      }

      ImGui::SameLine();

      if (selected_index != -1)
      {
        selector->setSelectedIndex(selected_index);
      }
    }

    ImGui::SetNextItemWidth(-right_margin);

    if (param->hints() & NumericParameter::HINT_IS_COLOR)
    {
      {
        auto color_param = dynamic_cast<VecNParameter<glm::vec4>*>(param);
        assert(color_param);

        auto color = color_param->get();

        if (ImGui::ColorEdit4(input_name.c_str(), glm::value_ptr(color),
                          ImGuiColorEditFlags_NoInputs |
                          ImGuiColorEditFlags_AlphaBar | ImGuiColorEditFlags_Float))
        {
          color_param->set(color);
        }
      }
    }
    else
    {
      std::vector<char> buffer(param->components() * param->componentSize());
      param->get(buffer.data(), buffer.size());

      bool changed = false;

      if (param->hints() && NumericParameter::HINT_PREFER_SLIDER)
      {
        changed = ImGui::SliderScalarN(input_name.c_str(),
                            type,
                            reinterpret_cast<void*>(buffer.data()),
                            param->components(),
                            reinterpret_cast<const void*>(param->minValue()),
                            reinterpret_cast<const void*>(param->maxValue()));
      }
      else if (param->componentType() != NumericParameter::INT32)
      {
        changed = ImGui::DragScalarN(input_name.c_str(),
                          type,
                          reinterpret_cast<void*>(buffer.data()),
                          param->components(),
                          param->stepSize());
      }
      else
      {
        int32_t step = 1;
        changed = ImGui::InputScalarN(input_name.c_str(),
                          type,
                          reinterpret_cast<void*>(buffer.data()),
                          param->components(),
                          &step);

      }

      if (changed)
        param->set(buffer.data(), buffer.size());
    }
  }
  else if (parameter.isMultipleChoice())
  {
    auto param = parameter.toMultipleChoice();
    auto current = param->current();

    if (ImGui::BeginCombo(input_name.c_str(), param->choiceText(current).c_str()))
    {
      for (int i = 0; i < param->choices(); i++)
      {
        if (ImGui::Selectable(param->choiceText(i).c_str(), i == current))
        {
          if (current != i)
            param->set(i);
        }
      }
      ImGui::EndCombo();
    }
  }
  else if (parameter.isBoolean())
  {
    auto param = parameter.toBoolean();
    bool value = param->get();
    if (ImGui::Checkbox(input_name.c_str(), &value))
      param->set(value);
  }
  else if (parameter.isGroup())
  {
    if (ImGui::TreeNode(parameter.name.c_str()))
    {
      showProperties(*parameter.toGroup(), show_reset_button);

      auto selection = dynamic_cast<IBulkSelection*>(&parameter);
      if (selection)
      {
        if (ImGui::Button("Select all"))
          selection->selectAll();

        ImGui::SameLine();

        if (ImGui::Button("Select none"))
          selection->selectNone();
      }

      ImGui::TreePop();
    }
  }
  else
  {
    ImGui::Text("%s", (std::string("Unknown parameter type: ") + parameter.name).c_str());
  }
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


void showProperties(ModifiableParameterList& list)
{
  enum Action
  {
    DELETE, MOVE_UP, MOVE_DOWN, NONE
  };

  Action action = NONE;
  int index = -1;

  for (int i = 0; i < list.size(); i++)
  {
    ImGui::PushID(i);

    auto& property = list.at(i);
    auto name = property.getName();

    ImGui::Spacing();

    if (ImGui::BeginTable("title_bar", 5, 0))
    {
      ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthFixed);
      ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthStretch);
      ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthFixed);
      ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthFixed);
      ImGui::TableSetupColumn(nullptr, ImGuiTableColumnFlags_WidthFixed);

      ImGui::TableNextColumn();
      if (editButton())
      {
        ImGui::OpenPopup("edit_name");
      }

      if (ImGui::BeginPopup("edit_name"))
      {
        std::array<char, 50> buffer {};
        assert(buffer.size() >= name.size()+1);
        strncpy(buffer.data(), name.c_str(), buffer.size()-1);

        ImGui::Text("Edit name");

        if (ImGui::IsWindowAppearing())
          ImGui::SetKeyboardFocusHere();

        if (ImGui::InputText("##input", buffer.data(), buffer.size()))
        {
          if (property.supportsRename())
            property.rename(buffer.data());
        }

        if (ImGui::IsItemDeactivated())
          ImGui::CloseCurrentPopup();

        ImGui::EndPopup();
      }

      ImGui::TableNextColumn();
      ImGui::Text("%s", name.c_str());

      ImGui::TableNextColumn();
      ImGui::BeginDisabled(i <= 0);
      if (arrowButton("up", ImGuiDir_Up))
      {
        index = i;
        action = MOVE_UP;
      }
      ImGui::EndDisabled();

      ImGui::TableNextColumn();
      ImGui::BeginDisabled(i >= list.size()-1);
      if (arrowButton("down", ImGuiDir_Down))
      {
        index = i;
        action = MOVE_DOWN;
      }
      ImGui::EndDisabled();

      ImGui::TableNextColumn();
      if (iconButton(IconID::DELETE, "Remove"))
      {
        index = i;
        action = DELETE;
      }

      ImGui::EndTable();
    }

    showProperty(list.get(i), false);

    ImGui::Spacing();
    ImGui::Separator();

    ImGui::PopID();
  }

  if (index != -1)
  {
    switch (action)
    {
      case DELETE:
        list.remove(index);
        break;
      case MOVE_UP:
        list.moveBackward(index);
        break;
      case MOVE_DOWN:
        list.moveForward(index);
        break;
      case NONE:
        break;
    }
  }

  if (ImGui::Button("New"))
    list.addNew();
}


void showProperties(ParameterGroup& parameters, bool show_reset_button)
{
  if (parameters.isModifiable())
  {
    auto list = dynamic_cast<ModifiableParameterList*>(&parameters);
    assert(list);
    if (list)
    {
      showProperties(*list);
      return;
    }
  }

  auto columns = parameters.columns;

  float max_label_width = 0;
  for (int i = 0; i < parameters.size(); i++)
  {
    auto &parameter = parameters.at(i);

    if (!parameter.isBoolean() && !parameter.isGroup())
    {
      max_label_width = std::max(max_label_width,
                                 ImGui::CalcTextSize(parameter.name.c_str()).x);
    }
  }

  auto table_name = parameters.name + "_table";

  auto right_margin = max_label_width + ImGui::GetStyle().ItemSpacing.x;

  float right_padding = 0;
  if (columns > 1)
    right_padding = ImGui::GetStyle().WindowPadding.x;

  right_margin += right_padding;

  if (ImGui::BeginTable(table_name.c_str(), columns, ImGuiTableFlags_SizingStretchSame))
  {
    for (int i = 0; i < columns; i++)
      ImGui::TableSetupColumn(nullptr, 0);


    for (int i = 0; i < parameters.size(); i++)
    {
      ImGui::TableNextColumn(); // FIXME: returns visibility

      ImGui::PushID(i);

      auto &parameter = parameters.at(i);

      if (!parameter.isGroup() && show_reset_button)
      {
        if (iconButton(IconID::UNDO, "Reset"))
        {
          parameter.reset();
        }
        ImGui::SameLine();
      }

      showProperty(parameter, show_reset_button,
                   parameter.isGroup() ? 0 : right_margin);

      if (!parameter.isGroup())
      {
        ImGui::SameLine();
        ImGui::AlignTextToFramePadding();
        ImGui::Text("%s", parameter.name.c_str());

        if (right_padding > 0)
        {
          ImGui::SameLine();
          ImGui::Dummy(glm::vec2(right_padding, 0));
        }
      }

      ImGui::PopID();
    }

    ImGui::EndTable();
  }
}


} // namespace render_util::viewer::frontend_imgui

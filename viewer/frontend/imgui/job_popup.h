/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_JOB_POPUP_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_JOB_POPUP_H


#include "popup.h"
#include <render_util/viewer/scene.h>


namespace render_util::viewer::frontend_imgui
{


class JobStarterPopup : public Popup
{
  std::unique_ptr<JobStarter> m_starter;
  std::unique_ptr<Job>& m_job;
  std::string m_title;

  void showContents(bool& close) override;
  std::string getDisplayTitle() override;

public:
  JobStarterPopup(const Action& action, std::unique_ptr<Job>& job);
};


class JobProgressPopup : public Popup
{
  util::OutputTerminal& m_terminal;
  ImFont& m_console_font;
  std::unique_ptr<Job> m_job;

  void showContents(bool& close) override;
  std::string getDisplayTitle() override;
  glm::ivec2 getInitialSize() override;

public:
  JobProgressPopup(std::unique_ptr<Job> job,
                   util::OutputTerminal& terminal,
                   ImFont& console_font);
};


} // namespace render_util::viewer::frontend_imgui

#endif

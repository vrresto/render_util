/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_GEOMETRIC_PROPERTY_EDITOR_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_GEOMETRIC_PROPERTY_EDITOR_H

#include <render_util/viewer/parameter.h>

#include <glm/glm.hpp>
#include <unordered_map>
#include <stack>


namespace render_util
{
  class Camera;
}


namespace render_util::viewer::frontend_imgui
{


class PrimitiveRenderer;


class GeometricPropertyEditor
{
public:
  struct SubTypeHandler;
  struct UndoAction;

private:
  std::stack<std::unique_ptr<UndoAction>> m_undo_actions;
  std::stack<std::unique_ptr<UndoAction>> m_redo_actions;
  std::unique_ptr<SubTypeHandler> m_current_handler;

  SubTypeHandler* getHandler(IGeometricProperty&);
  void updateCurrentAction();
  void undo();
  void redo();

public:
  GeometricPropertyEditor();
  ~GeometricPropertyEditor();

  void stopDrag();
  void setProperty(IGeometricProperty*);
  void showProperties();
  void addUndoAction(std::unique_ptr<UndoAction>);

  void draw(PrimitiveRenderer& renderer,
            glm::vec2 mouse_pos_window,
            const Camera& camera);

  void handleInput(glm::vec2 mouse_pos_window,
                   const Camera& camera);
};


struct GeometricPropertyEditor::SubTypeHandler
{
  virtual void draw(PrimitiveRenderer& renderer,
                    glm::vec2 mouse_pos_window,
                    const Camera& camera) = 0;

  virtual void showProperties() = 0;

  virtual void handleInput(glm::vec2 mouse_pos_window,
                           const Camera& camera) = 0;

  virtual void stopDrag() = 0;

  virtual IGeometricProperty* getProperty() = 0;
};


class GeometricPropertyEditor::UndoAction
{
  const std::string m_name;
  IGeometricProperty& m_property;
  std::unique_ptr<IGeometricPropertyState> m_previous_state;
  std::unique_ptr<IGeometricPropertyState> m_new_state;

public:
  UndoAction(std::string name, IGeometricProperty& property,
             std::unique_ptr<IGeometricPropertyState> previous_state) :
    m_name(name),
    m_property(property),
    m_previous_state(std::move(previous_state)),
    m_new_state(property.getState())
  {
    assert(m_previous_state);
  }

  const std::string& getName() { return m_name; }

  void undo()
  {
    assert(m_previous_state);
    m_property.restoreState(*m_previous_state);
  }

  void redo()
  {
    assert(m_new_state);
    m_property.restoreState(*m_new_state);
  }
};


std::unique_ptr<GeometricPropertyEditor::SubTypeHandler>
createRectangularPropertyHandler(GeometricPropertyEditor& parent,
                                 IGeometricProperty* property);


} // namespace render_util::viewer::frontend_imgui

#endif

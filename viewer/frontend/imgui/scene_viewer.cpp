/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "scene_viewer.h"
#include "utils.h"
#include <grid_2d.h>
#include <camera_2d.h>
#include <render_util/viewer/camera.h>
#include <render_util/viewer/camera_state.h>
#include <log.h>

#include <imgui.h>
#include <GLFW/glfw3.h>


using namespace render_util::viewer;
using namespace render_util;


namespace
{


static constexpr float camera_move_speed_default = 8000.0;
static constexpr float camera_move_speed_min = 100.0;
static constexpr float camera_move_speed_max = 10000.0;
static constexpr double mouse_rotation_speed = 0.2;


enum Axis
{
  PITCH,
  YAW,
  ROLL
};


void printDistance (float distance, const char *suffix, std::ostream &out)
{
  out.width(8);
  out << distance << suffix;
}


} // namespace


namespace render_util::viewer::frontend_imgui
{


class SceneViewer2D : public SceneViewer
{
  TextRenderer &m_text_renderer;
  viewer::Camera2D m_camera;
  viewer::Camera2D::Vec2 m_camera_pos {};
  viewer::Camera2D::Unit m_magnification {};
  std::unique_ptr<Grid2D> m_grid;

  void updateProperties()
  {
    m_camera_pos = m_camera.getPos2D();
    m_magnification = m_camera.getMagnification();
  }

  render_util::Camera &getCamera() override { return m_camera; }

protected:
  void applyProperties() override
  {
    m_camera.setMaxAreaSize(m_scene.getMapSizeM());
    m_camera.setPos(m_camera_pos);
    m_camera.setMagnification(m_magnification);
  }

public:

  SceneViewer2D(Scene&, TextRenderer&, const ShaderSearchPath&);

  void render() override;
  void handleInput(float frame_delta) override;
  bool handleMouseInput(float frame_delta, const glm::vec2 &mouse_pos_window,
                        bool is_mouse_hovering,
                        bool is_focused) override;
  void printStats(int frame_delta_ms, std::ostream &out) override;

};


class SceneViewer3D : public SceneViewer
{
  viewer::Camera m_camera;
  float m_camera_move_speed = 0;

  float getCameraRotationSpeed(Axis axis);

  render_util::Camera &getCamera() override { return m_camera; }

protected:
  void applyProperties() override
  {
    m_camera.updateTransform();
  }

public:

  SceneViewer3D(Scene &scene);

  void handleInput(float frame_delta) override;
  bool handleMouseInput(float frame_delta, const glm::vec2 &mouse_pos_window,
                        bool is_mouse_hovering,
                        bool is_focused) override;
  void printStats(int frame_delta_ms, std::ostream &out) override;

  void saveState(CameraState&) override;
  void loadState(const CameraState&) override;
};


SceneViewer2D::SceneViewer2D(Scene &scene,
                             TextRenderer &text_renderer,
                             const ShaderSearchPath &shader_search_path) :
  SceneViewer(scene),
  m_text_renderer(text_renderer)
{
  m_grid = std::make_unique<Grid2D>(shader_search_path);

  m_camera.setMaxAreaSize(m_scene.getMapSizeM());
  m_camera.setPos(m_scene.getMapSizeM() / 2.f);

  updateProperties();

  m_parameters.add("Magnification", m_magnification, 0.01);
  m_parameters.addVector("View center", m_camera_pos, 100.0);
  m_parameters.addVector("Grid origin", m_grid->m_origin, 100.0);
  m_parameters.add("Grid resolution", m_grid->m_resolution, 0.05);
}


void SceneViewer3D::saveState(CameraState& state)
{
  m_camera.saveState(state);
  state.state_3d.move_speed = m_camera_move_speed;
}


void SceneViewer3D::loadState(const CameraState& state)
{
  m_camera.loadState(state);
  m_camera_move_speed = state.state_3d.move_speed;
}


void SceneViewer2D::render()
{
  SceneViewer::render();
  m_grid->render(m_camera, m_text_renderer);
}


bool SceneViewer2D::handleMouseInput(float frame_delta, const glm::vec2 &mouse_pos_window,
                                     bool is_mouse_hovering,
                                     bool is_focused)
{
  auto &io = ImGui::GetIO();

  bool handled = true;

  if (is_focused && io.MouseDown[0] && io.MouseDownOwned[0])
  {
    glm::vec2 mouse_delta = io.MouseDelta;
    auto delta_world = Camera2D::Vec2(mouse_delta) * m_camera.getViewportToWorldScale();
    delta_world *= Camera2D::Vec2(-1, +1);
    m_camera.setPos(m_camera.getPos2D() + delta_world);
  }
  else if (io.MouseWheel && is_mouse_hovering)
  {
    auto zoom_speed = 0.1;

    auto mouse_pos_viewport = mouse_pos_window;
    mouse_pos_viewport.y = m_camera.getViewportSize().y - mouse_pos_viewport.y;
    mouse_pos_viewport += glm::vec2(0.5);

    auto world_pos = m_camera.viewportToWorld(mouse_pos_viewport);

    auto magnification = m_camera.getMagnification();

    auto zoom_increment = magnification * zoom_speed * io.MouseWheel;

    magnification += zoom_increment;

    m_camera.setMagnification(magnification);

    auto pos_relative_to_origin_world_new =
      Camera2D::Vec2(mouse_pos_viewport) * m_camera.getViewportToWorldScale();

    auto viewport_origin_world_new = world_pos - pos_relative_to_origin_world_new;

    m_camera.setViewportOriginWorld(viewport_origin_world_new);
  }
  else
  {
    handled = false;
  }

  if (handled)
    updateProperties();

  return handled;
}


void SceneViewer2D::handleInput(float frame_delta)
{
  constexpr auto DEFAULT_SCROLL_SPEED = 0.2;
  constexpr auto DEFAULT_ZOOM_SPEED = 0.5;

  auto &io = ImGui::GetIO();

  {
    auto view_width_m =
      m_camera.getViewportSize().x * m_camera.getViewportToWorldScale();

    auto scroll_speed = DEFAULT_SCROLL_SPEED;
    if (io.KeyShift)
      scroll_speed *= 2;
    else if (io.KeyCtrl)
      scroll_speed /= 2;

    auto scroll_distance = view_width_m * scroll_speed * frame_delta;

    auto pos = m_camera.getPos2D();

    if (ImGui::IsKeyDown(ImGuiKey_LeftArrow))
      pos.x -= scroll_distance;
    if (ImGui::IsKeyDown(ImGuiKey_RightArrow))
      pos.x += scroll_distance;
    if (ImGui::IsKeyDown(ImGuiKey_DownArrow))
      pos.y -= scroll_distance;
    if (ImGui::IsKeyDown(ImGuiKey_UpArrow))
      pos.y += scroll_distance;

    auto zoom_speed = DEFAULT_ZOOM_SPEED;
    if (io.KeyShift)
      zoom_speed *= 2;

    auto magnification = m_camera.getMagnification();
    auto zoom_increment = magnification * zoom_speed * frame_delta;

    if (ImGui::IsKeyDown(ImGuiKey_KeypadSubtract))
      magnification -= zoom_increment;
    if (ImGui::IsKeyDown(ImGuiKey_KeypadAdd))
      magnification += zoom_increment;

    m_camera.setPos(pos);
    m_camera.setMagnification(magnification);
  }

  updateProperties();
}


void SceneViewer2D::printStats(int frame_delta_ms, std::ostream &out)
{
}


SceneViewer3D::SceneViewer3D(Scene &scene) :
  SceneViewer(scene),
  m_camera_move_speed(camera_move_speed_default)
{
  m_camera.setPos(m_scene.getInitialCameraPos());

  m_parameters.add("Camera moving speed", m_camera_move_speed, 100.0);

  m_parameters.add("Camera position x", m_camera.x, 100.0);
  m_parameters.add("Camera position y", m_camera.y, 100.0);
  m_parameters.add("Camera position z", m_camera.z, 100.0);

  m_parameters.add("Camera pitch", m_camera.pitch, 1.0,
                    NumericParameter::HINT_PREFER_SLIDER,
                    -90.0, +90.0);
  m_parameters.add("Camera yaw", m_camera.yaw, 1.0,
                    NumericParameter::HINT_PREFER_SLIDER,
                    0.0, 360.0);
  m_parameters.add("Camera roll", m_camera.roll, 1.0,
                    NumericParameter::HINT_PREFER_SLIDER,
                    -180.0, +180.0);

  {
    auto get = [this] () { return m_camera.getFov(); };
    auto set = [this] (float fov) { m_camera.setFov(fov); };
    m_parameters.add<float>("FOV", get, set, 0.1,
                            NumericParameter::HINT_PREFER_SLIDER, 0.01, 120.0);
  }
}


float SceneViewer3D::getCameraRotationSpeed(Axis axis)
{
  switch (axis)
  {
    case PITCH:
      return m_camera.getFov() / 4;
    case YAW:
      return 2 * m_camera.getFov() / 4;
    case ROLL:
    default:
      return 30;
  }
}


void SceneViewer3D::printStats(int frame_delta_ms, std::ostream &out)
{
  out.precision(3);
  out.setf(std::ios_base::fixed);
  out.setf(std::ios_base::right);

  float fps = 1000.0 / frame_delta_ms;

  out << "fps: ";
  out.width(8);
  out << fps << "   |   ";

  out<<"pos: ";
  printDistance(m_camera.x/1000, " ", out);
  printDistance(m_camera.y/1000, " ", out);
  printDistance(m_camera.z/1000, " ", out);

  out<<"    |    speed: ";
  printDistance(m_camera_move_speed / 1000, " ", out);

  out<<"   |  yaw: ";
  printDistance(m_camera.yaw, " ", out);

  out<<"   |    fov: "<<m_camera.getFov();
}


bool SceneViewer3D::handleMouseInput(float frame_delta, const glm::vec2 &mouse_pos_window,
                                     bool is_mouse_hovering,
                                     bool is_focused)
{
  const float mouse_rotation_speed = 0.2;

  auto &io = ImGui::GetIO();

  if (is_focused &&
      ((io.MouseDown[1] && io.MouseDownOwned[1]) ||
       (io.MouseDown[0] && io.MouseDownOwned[0]) ||
        glfwGetInputMode(getApplicationWindow(), GLFW_CURSOR) == GLFW_CURSOR_DISABLED))
  {
    auto rotation = -glm::vec2(io.MouseDelta) * mouse_rotation_speed;
    rotation *= (m_camera.getFov() / 100);

    m_camera.yaw += rotation.x;
    m_camera.pitch += rotation.y;
  }

  if (io.MouseWheel && is_mouse_hovering)
  {
    if (!io.KeyCtrl)
    {
      auto fov = m_camera.getFov();

      auto increment =  1-exp(-(fov / 30.0));

      fov -= increment * io.MouseWheel;

      m_camera.setFov(fov);
    }
    else
    {
      if (io.MouseWheel > 0)
        m_camera_move_speed *= 1.1 * abs(io.MouseWheel);
      else if (io.MouseWheel < 0)
        m_camera_move_speed /= 1.1 * abs(io.MouseWheel);
    }
  }

  return false;
}


void SceneViewer3D::handleInput(float frame_delta)
{
  auto move_speed = m_camera_move_speed;

  if (ImGui::IsKeyDown(ImGuiKey_W))
  {
    m_camera.moveForward(frame_delta * move_speed);
  }
  if (ImGui::IsKeyDown(ImGuiKey_S))
  {
    m_camera.moveForward(frame_delta * -move_speed);
  }
  if (ImGui::IsKeyDown(ImGuiKey_UpArrow))
  {
    m_camera.pitch -= getCameraRotationSpeed(PITCH) * frame_delta;
  }
  if (ImGui::IsKeyDown(ImGuiKey_DownArrow))
  {
    m_camera.pitch += getCameraRotationSpeed(PITCH) * frame_delta;
  }
  if (ImGui::IsKeyDown(ImGuiKey_LeftArrow))
  {
    m_camera.yaw += getCameraRotationSpeed(YAW) * frame_delta;
  }
  if (ImGui::IsKeyDown(ImGuiKey_RightArrow))
  {
    m_camera.yaw -= getCameraRotationSpeed(YAW) * frame_delta;
  }
  if (ImGui::IsKeyDown(ImGuiKey_Q))
  {
    m_camera.roll += getCameraRotationSpeed(ROLL) * frame_delta;
  }
  if (ImGui::IsKeyDown(ImGuiKey_E))
  {
    m_camera.roll -= getCameraRotationSpeed(ROLL) * frame_delta;
  }
  if (ImGui::IsKeyPressed(ImGuiKey_KeypadAdd))
  {
    m_camera_move_speed *= 2;
  }
  if (ImGui::IsKeyPressed(ImGuiKey_KeypadSubtract))
  {
    m_camera_move_speed /= 2;
  }

  m_camera.updateTransform();
}


std::unique_ptr<SceneViewer> createSceneViewer(SceneViewer::Type type,
                                               Scene &scene,
                                               TextRenderer &text_renderer,
                                               const ShaderSearchPath &shader_search_path)
{
  switch (type)
  {
    case SceneViewer::VIEWER_2D:
      return std::make_unique<SceneViewer2D>(scene, text_renderer, shader_search_path);
    case SceneViewer::VIEWER_3D:
      return std::make_unique<SceneViewer3D>(scene);
    default:
      abort();
  }
}


} // namespace render_util::viewer::frontend_imgui

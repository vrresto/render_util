/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_SCENE_WINDOW_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_SCENE_WINDOW_H

#include "geometric_property_editor.h"

#include <render_util/shader.h>
#include <render_util/viewer/camera_state.h>
#include <util.h>

#include <glm/glm.hpp>
#include <memory>
#include <vector>


class TextRenderer;


namespace render_util
{
  class FrameBuffer;
}


namespace render_util::viewer
{
  class Scene;
  class Parameter;
  class IGeometricProperty;
}


namespace render_util::viewer::frontend_imgui
{


class SceneViewer;
class GeometricPropertyEditor;
class PrimitiveRenderer;


class SceneWindow
{
  enum Mode
  {
    MODE_2D,
    MODE_3D
  };

  enum InputMode
  {
    INPUT_MODE_CAMERA,
    INPUT_MODE_EDIT
  };

  static constexpr int MAX_SAVE_SLOTS = 10;

  struct ViewerStateSlot
  {
    bool is_empty = true;
    CameraState camera_state {};
  };

  struct SaveFileData
  {
    ViewerStateSlot slot[MAX_SAVE_SLOTS];
  };

  Scene &m_scene;
  int m_mode = MODE_2D;
  int m_edit_mode_parameter = 0;
  int m_input_mode = INPUT_MODE_CAMERA;
  glm::ivec2 m_viewport_size {};
  std::unique_ptr<SceneViewer> m_viewer_2d;
  std::unique_ptr<SceneViewer> m_viewer_3d;
  std::unique_ptr<FrameBuffer> m_framebuffer;
  std::string m_cfg_file_prefix;
  SaveFileData m_save_file_data;
  GeometricPropertyEditor m_geometric_property_editor;
  std::unique_ptr<PrimitiveRenderer> m_primitive_renderer;
  glm::vec3 m_clear_color {};

  intptr_t getFrameBufferTextureID();
  std::string overlayText();
  void handleInput(float frame_delta, const glm::vec2 &mouse_pos_window);
  void setViewportSize(const glm::ivec2&);
  bool isEditor();
  bool hasEditableProperties();
  const util::unique_ptr_vector<IGeometricProperty>& getEditableProperties();
  IGeometricProperty* getEditableProperty();
  void drawEditingControls(glm::vec2 mouse_pos_window);
  void renderScene(glm::vec2 mouse_pos_window);

public:
  SceneWindow(Scene&, TextRenderer&, const ShaderSearchPath&);
  ~SceneWindow();

  Scene& getScene() { return m_scene; }
  glm::ivec2 getViewportSize() { return m_viewport_size; }

  SceneViewer &getViewer();
  void show(float frame_delta);
  void showMenuItems();
  void showOptions();
  void showCameraOptions();
  void loadState(int slot);
  void saveState(int slot);
  void stopDrag();
};


} // namespace render_util::viewer::frontend_imgui


#endif

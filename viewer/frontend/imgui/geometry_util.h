/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_GEOMETRY_UTIL_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_GEOMETRY_UTIL_H


template <typename T>
bool isInRange(T value, T min_value, T max_value)
{
  return value >= min_value && value <= max_value;
}


template <class T>
auto getNearestEdge(typename T::Coordinate p, const T& rect)
{
  using Coordinate = typename T::Coordinate;
  using Corner = typename T::Corner;
  using Edge = typename T::Edge;

  auto min_pos = rect.getOrigin();
  auto max_pos = rect.getOrigin() + rect.getExtent();

  bool is_inside = isInRange(p.y, min_pos.y, max_pos.y) && isInRange(p.x, min_pos.x, max_pos.x);

  if (is_inside)
  {
    auto left_dist = glm::distance(p.x, min_pos.x);
    auto right_dist = glm::distance(p.x, max_pos.x);
    auto bottom_dist = glm::distance(p.y, min_pos.y);
    auto top_dist = glm::distance(p.y, max_pos.y);

    if (left_dist < right_dist && left_dist < bottom_dist && left_dist < top_dist)
      return Edge::LEFT;
    else if (right_dist < left_dist && right_dist < bottom_dist && right_dist < top_dist)
      return Edge::RIGHT;
    else if (bottom_dist < left_dist && bottom_dist < right_dist && bottom_dist < top_dist)
      return Edge::BOTTOM;
    else
      return Edge::TOP;
  }
  else if (isInRange(p.y, min_pos.y, max_pos.y))
  {
    return glm::distance(p.x, min_pos.x) < glm::distance(p.x, max_pos.x) ?
      Edge::LEFT : Edge::RIGHT;
  }
  else if (isInRange(p.x, min_pos.x, max_pos.x))
  {
    return glm::distance(p.y, min_pos.y) < glm::distance(p.y, max_pos.y) ?
      Edge::BOTTOM : Edge::TOP;
  }

  return Edge::NONE;
}

#endif

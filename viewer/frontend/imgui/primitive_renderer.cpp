/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "primitive_renderer.h"

#include <render_util/shader_util.h>
#include <render_util/render_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace render_util::viewer::frontend_imgui
{


PrimitiveRenderer::PrimitiveRenderer(const ShaderSearchPath& shader_search_path)
{
  m_lines_program = createShaderProgram("line_2d", shader_search_path);
}


void PrimitiveRenderer::drawFilledCircle(glm::dvec2 p, float size, glm::vec4 color,
                      const Camera& camera)
{
  //FIXME this actually draws a quad

  auto& program = m_lines_program;

  updateUniforms(*program, camera);
  program->setUniform("color", color);

  StateModifier state;
  state.setProgram(program->getID());

  gl::PointSize(size);
  gl::Begin(GL_POINTS);
  gl::Vertex2f(p.x, p.y);
  gl::End();
  gl::PointSize(1);
}


void PrimitiveRenderer::drawLine(glm::dvec2 begin, glm::dvec2 end, float width, glm::vec4 color,
              const Camera& camera)
{
  auto& program = m_lines_program;

  updateUniforms(*program, camera);
  program->setUniform("color", color);

  StateModifier state;
  state.setProgram(program->getID());

  gl::Begin(GL_LINE_STRIP);
  gl::Vertex2f(begin.x, begin.y);
  gl::Vertex2f(end.x, end.y);
  gl::End();
}


void PrimitiveRenderer::drawRect(const RectD& rect, glm::vec4 color, const Camera& camera)
{
  std::array<glm::dvec2, 5> points;
  points.at(0) = rect.getOrigin() + glm::dvec2(0,0) * rect.getExtent();
  points.at(1) = rect.getOrigin() + glm::dvec2(0,1) * rect.getExtent();
  points.at(2) = rect.getOrigin() + glm::dvec2(1,1) * rect.getExtent();
  points.at(3) = rect.getOrigin() + glm::dvec2(1,0) * rect.getExtent();
  points.at(4) = points.at(0);

  auto& program = m_lines_program;

  updateUniforms(*program, camera);
  program->setUniform("color", color);

  StateModifier state;
  state.setProgram(program->getID());

  gl::Begin(GL_LINE_STRIP);
  for (auto& v : points)
  {
    gl::Vertex2f(v.x, v.y);
  }
  gl::End();
}


} // namespace render_util::viewer::frontend_imgui

/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "icon.h"
#include "fonts.h"
#include "utils.h"
#include "frontends.h"
#include "job_popup.h"
#include "popup.h"
#include "properties.h"
#include "scene_window.h"
#include "log_window.h"
#include "utils.h"
#include "style.h"
#include <render_util/viewer/configuration.h>
#include <render_util/viewer/viewer.h>
#include <render_util/stats.h>
#include <render_util/state.h>
#include <render_util/context.h>
#include <render_util/gl_binding/gl_functions.h>
#include <text_renderer/text_renderer.h>
#include <util/output_terminal.h>
#include <log.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <GLFW/glfw3.h>
#if ENABLE_VIEWER_DBUS_INTERFACE
#include <sdbus-c++/sdbus-c++.h>
#endif

#include <chrono>
#include <queue>
#include <set>


using namespace glm;
using namespace std;
using namespace render_util::viewer::frontend_imgui;
using namespace render_util::viewer;
using namespace render_util;
using namespace render_util::gl_binding;
using render_util::ShaderProgram;
using Clock = std::chrono::steady_clock;


namespace
{


enum class MenuItem
{
  NONE,
  SAVE,
  QUIT,
};


GLFWwindow* g_main_window = nullptr;


void *getGLProcAddress(const char *name)
{
  return (void*) glfwGetProcAddress(name);
}


void errorCallback(int error, const char* description)
{
  fprintf(stderr, "Error: %s\n", description);
  exit(1);
}


void showShaderErrors(Scene& scene)
{
  for (auto& error : scene.getShaderErrors())
  {
    if (ImGui::TreeNode(error.getProgramName().c_str()))
    {
      ImGui::TextWrapped(error.getError().c_str());
      ImGui::Text("%s", "");
      ImGui::TextWrapped(error.getLog().c_str());
      ImGui::TreePop();
    }
  }
}


void showStats(Stats& stats)
{
  for (auto& child : stats.children)
  {
    if (ImGui::TreeNode(child.name.c_str()))
    {
      showStats(child);
      ImGui::TreePop();
    }
  }

  for (auto& entry : stats.entries)
  {
    ImGui::Text("%s: %s", entry.name.c_str(), entry.value.c_str());
  }
}


void showSceneInfo(Scene &scene)
{
  Stats stats;
  scene.getStats(stats);
  showStats(stats);
}


void loadSettings(std::string file_path)
{
  LOG_INFO << "Loading setting from " << file_path << std::endl;

  Configuration cfg(file_path);

  loadFontParameters(cfg);

  auto style = cfg.get("Style", "");
  if (!style.empty())
    setStyle(style);
}


void saveSettings(std::string file_path)
{
  Configuration cfg;

  auto style = getCurrentStyleName();
  if (!style.empty())
    cfg.set("Style", style);

  saveFontParameters(cfg);

  cfg.save(file_path);
}


class QuitApplicationPopup : public YesNoCancelBox
{
  Scene& m_scene;
  bool& m_run_mainloop;

  void onClose() override
  {
    switch (getPressedButton())
    {
      case YesNoCancelBox::BUTTON_YES:
        m_scene.save();
        m_run_mainloop = false;
        break;
      case YesNoCancelBox::BUTTON_NO:
        m_run_mainloop = false;
        break;
      case YesNoCancelBox::BUTTON_CANCEL:
        break;
      default:
        abort();
    }
  }

public:
  QuitApplicationPopup(Scene& scene, bool& run_mainloop) :
    YesNoCancelBox("Save changes?"),
    m_scene(scene),
    m_run_mainloop(run_mainloop)
  {
  }
};


class ReloadMapPopup : public Popup
{
  SceneWindow& m_scene_window;
  int m_frame = 0;

  void showContents(bool& close) override
  {
    ImGui::Text("Reloading map");
    m_frame++;
    if (m_frame > 5) // UGLY HACK - it takes a few frames to actually display the popup
    {
      m_scene_window.getScene().reloadMap();
      close = true;
    }
  }

public:
  ReloadMapPopup(SceneWindow& scene_window) :
    Popup("Reloading map",
          ImGuiWindowFlags_NoDecoration
            | ImGuiWindowFlags_NoResize
            | ImGuiWindowFlags_NoNav
            | ImGuiWindowFlags_AlwaysAutoResize),
    m_scene_window(scene_window) {}
};


} // namespace


GLFWwindow* render_util::viewer::frontend_imgui::getApplicationWindow()
{
  return g_main_window;
}


void render_util::viewer::runFrontendImGui(util::Factory<Scene> f_create_scene,
                                           string app_name,
                                           const Configuration& config)
{
  const auto ui_settings_file_path = app_name + "_ui_settings";

  initLog(app_name);
  frontend_imgui::initLog();

#if ENABLE_VIEWER_DBUS_INTERFACE
  // auto dbus_app_name = app_name;
  std::string dbus_app_name = "il2ge_viewer";

  auto object_path = [&] (auto object_name)
  {
    return "/org/" + dbus_app_name + "/" + object_name;
  };

  auto interface_name = [&] (auto name)
  {
    return "org." + dbus_app_name + "." + name;
  };

  auto service_name = "org." + dbus_app_name;
  auto connection = sdbus::createSessionBusConnection(service_name);
  std::vector<std::unique_ptr<sdbus::IObject>> dbus_objects;
#endif

  glfwSetErrorCallback(errorCallback);

  if (!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

  glfwWindowHint(GLFW_VISIBLE, 0);

  GLFWwindow* window = glfwCreateWindow(1024, 768, app_name.c_str(), NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  g_main_window = window;

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);

  const char* glsl_version = "#version 150";

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking

  // Setup Dear ImGui style
  initStyle();

  // Setup Platform/Renderer backends
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  createFonts();
  createIconFont();

  loadSettings(ui_settings_file_path);

  auto gl_interface = std::make_unique<gl_binding::GL_Interface>(&getGLProcAddress);
  gl_binding::GL_Interface::setCurrent(gl_interface.get());

  auto context = std::make_unique<Context>();
  Context::setCurrent(context.get());

  StateModifier::init();

  render_util::ShaderSearchPath shader_search_path;
  shader_search_path.push_back(RENDER_UTIL_SHADER_DIR);

  auto scene = f_create_scene();
  assert(scene);

  scene->setup();

  auto text_renderer = std::make_unique<TextRenderer>();

  auto scene_window = std::make_unique<SceneWindow>(*scene, *text_renderer, shader_search_path);

#if ENABLE_VIEWER_DBUS_INTERFACE
  {
    auto object = sdbus::createObject(*connection, object_path("scene"));

    for (auto& action : scene->getActions())
    {
      if (action.starts_job) // not supported yet
        continue;

      auto method = [&action] (sdbus::MethodCall call)
      {
        LOG_INFO << "Method called: " << action.name << std::endl;
        action.execute();
        auto reply = call.createReply();
        reply.send();
      };

      auto full_name = interface_name("Scene") + "." + action.name;
      LOG_DEBUG << "Registering method " << full_name << std::endl;
      try
      {
        object->registerMethod(interface_name("Scene"), action.name, "", "", method);
      }
      catch (std::exception& e)
      {
        LOG_ERROR << "Failed to register method " << full_name << std::endl;
        LOG_ERROR << "Reason: " << e.what() << std::endl;
        exit(1);
      }
    }

    object->finishRegistration();
    dbus_objects.push_back(std::move(object));
  }

  {
    auto object = sdbus::createObject(*connection, object_path("application"));

    {
      auto method = [window] (sdbus::MethodCall call)
      {
        LOG_INFO << "Setting focus to main window" << std::endl;
        glfwFocusWindow(window);
        auto reply = call.createReply();
        reply.send();
      };
      object->registerMethod(interface_name("Application"),
                             "focus_main_window", "", "", method);
    }

    {
      auto method = [window] (sdbus::MethodCall call)
      {
        LOG_INFO << "Clearing log" << std::endl;
        clearLog();
        auto reply = call.createReply();
        reply.send();
      };
      object->registerMethod(interface_name("Application"),
                             "clear_log", "", "", method);
    }

    object->finishRegistration();
    dbus_objects.push_back(std::move(object));
  }
#endif

  glfwShowWindow(window);

#if GLFW_VERSION_MINOR >= 2
  glfwMaximizeWindow(window);
#endif

  Clock::time_point last_frame_time = Clock::now();

  std::unique_ptr<Popup> current_popup;

  std::unique_ptr<Job> next_job;
  util::OutputTerminal job_output;

  int log_verbosity = util::log::WARNING;
  bool show_demo_window = false;
  bool show_user_guide = false;
  bool show_style_selector = false;
  bool show_log = false;
  bool fullscreen = false;
  bool continuous_rendering = true;
  bool pause_animations = false;
  bool run_mainloop = true;

  while (run_mainloop)
  {
#if ENABLE_VIEWER_DBUS_INTERFACE
    while (connection->processPendingRequest()) {}
#endif

    if (continuous_rendering)
      glfwPollEvents();
    else
      glfwWaitEventsTimeout(1); //HACK EXPERIMETAL

    Clock::time_point current_frame_time = Clock::now();
    std::chrono::milliseconds frame_delta =
      std::chrono::duration_cast<std::chrono::milliseconds>(current_frame_time - last_frame_time);
    last_frame_time = current_frame_time;

    if (!pause_animations)
      scene->update(frame_delta.count() / 1000.f);

    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGuiIO& io = ImGui::GetIO();

    glm::ivec2 framebuffer_size {};
    glfwGetFramebufferSize(window, &framebuffer_size.x, &framebuffer_size.y);

    if (ImGui::IsMouseReleased(0))
    {
      scene_window->stopDrag();
    }

    if (ImGui::IsKeyPressed(ImGuiKey_Escape))
    {
      glfwSetInputMode(getApplicationWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
      io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
      fullscreen = false;
    }
    if (ImGui::IsKeyPressed(ImGuiKey_M))
    {
      io.ConfigFlags |= ImGuiConfigFlags_NoMouse;
      glfwSetInputMode(getApplicationWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    if (ImGui::IsKeyPressed(ImGuiKey_F))
    {
      fullscreen = !fullscreen;
    }

    if (!current_popup)
    {
      if (io.KeyCtrl)
      {
        if (ImGui::IsKeyPressed(ImGuiKey_S))
          scene->save();
        else if (ImGui::IsKeyPressed(ImGuiKey_Q))
          glfwSetWindowShouldClose(window, true);
      }
    }

    ImGuiDockNodeFlags dock_space_flags = 0;
    //dock_space_flags |= ImGuiDockNodeFlags_AutoHideTabBar;
    if (!fullscreen)
      ImGui::DockSpaceOverViewport(nullptr, dock_space_flags);

    auto selected_menuitem = MenuItem::NONE;
    int selected_action = -1;

    if (logHasNewWarnings())
      show_log = true;

    if (!fullscreen)
    {
      if (ImGui::BeginMainMenuBar())
      {
        if (ImGui::BeginMenu("File"))
        {
          if (ImGui::MenuItem("Save", "CTRL+S"))
            selected_menuitem = MenuItem::SAVE;
          if (ImGui::MenuItem("Quit", "CTRL+Q"))
            selected_menuitem = MenuItem::QUIT;

          {
            int i = 0;
            for (auto& action : scene->getActions())
            {
              if (ImGui::MenuItem(action.long_name.c_str(), ""))
                selected_action = i;
              i++;
            }
          }

          ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Viewer"))
        {
          scene_window->showMenuItems();
          ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Options"))
        {
          ImGui::MenuItem("Pause animations", nullptr, &pause_animations);
          ImGui::MenuItem("Continuous rendering", nullptr, &continuous_rendering);
          ImGui::MenuItem("Show log", nullptr, &show_log);

          if (ImGui::MenuItem("Select style"))
            show_style_selector = true;

          ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Help"))
        {
          if (ImGui::MenuItem("Demo window"))
            show_demo_window = true;

          if (ImGui::MenuItem("User guide"))
            show_user_guide = true;

          ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
      }
    }

    if (glfwWindowShouldClose(window))
    {
      glfwSetWindowShouldClose(g_main_window, false);

      if (!current_popup)
      {
        if (scene->hasUnsavedChanges())
          current_popup = std::make_unique<QuitApplicationPopup>(*scene, run_mainloop);
        else
          run_mainloop = false;
      }
    }

    if (current_popup)
    {
      assert(current_popup->isOpen());
      current_popup->show();
      if (!current_popup->isOpen())
        current_popup.reset();
    }
    else if (next_job)
    {
      assert(!current_popup);
      job_output.clear();
//       assert(console_font);
      current_popup = std::make_unique<JobProgressPopup>(std::move(next_job),
                                                         job_output,
                                                         getFont(CONSOLE_FONT));
    }

    if (scene->needsReload())
    {
      if (!current_popup)
        current_popup = std::make_unique<ReloadMapPopup>(*scene_window);
    }

    if (!fullscreen)
    {
      if (show_style_selector)
      {
        if (ImGui::Begin("Style selector",
                         &show_style_selector,
                         ImGuiWindowFlags_AlwaysAutoResize))
        {
          bool style_changed = showStyleSelector();
          bool font_changed = showFontSelector();

          if (style_changed || font_changed)
            saveSettings(ui_settings_file_path);
        }
        ImGui::End();
      }

      if (show_user_guide)
      {
        if (ImGui::Begin("User guide", &show_user_guide))
          ImGui::ShowUserGuide();
        ImGui::End();
      }

      if (show_demo_window)
        ImGui::ShowDemoWindow(&show_demo_window);

      if (show_log)
      {
        if (ImGui::Begin("Log", &show_log, ImGuiWindowFlags_NoFocusOnAppearing))
        {
          showLog(log_verbosity, &getFont(CONSOLE_FONT));
        }
        ImGui::End();
      }

      if (ImGui::Begin("Shader errors"))
      {
        showShaderErrors(*scene);
      }
      ImGui::End();

      if (ImGui::Begin("Info"))
      {
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                    1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        showSceneInfo(*scene);
      }
      ImGui::End();

      if (ImGui::Begin("Actions"))
      {
        const bool equal_button_size = true;

        ImGuiStyle& style = ImGui::GetStyle();

        glm::ivec2 max_text_size {};
        for (auto& action : scene->getActions())
        {
          glm::ivec2 size = ImGui::CalcTextSize(action.long_name.c_str());
          max_text_size = glm::max(max_text_size, size);
        }

        auto max_button_size = max_text_size + glm::ivec2(style.FramePadding) * 2;
        auto button_size = equal_button_size ? max_button_size : glm::ivec2(0);
        int buttons_count = scene->getActions().size();
        float window_visible_x2 = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;

        for (int n = 0; n < buttons_count; n++)
        {
          auto& action = scene->getActions().at(n);

          if (ImGui::Button(action.long_name.c_str(), button_size))
            selected_action = n;

          float last_button_x2 = ImGui::GetItemRectMax().x;
          // Expected position if next button was on same line
          float next_button_x2 = last_button_x2 + style.ItemSpacing.x + max_button_size.x;
          if (n + 1 < buttons_count && next_button_x2 < window_visible_x2)
            ImGui::SameLine();
        }
      }
      ImGui::End();

      if (ImGui::Begin("Properties"))
      {
        if (ImGui::CollapsingHeader("Viewer", ImGuiTreeNodeFlags_DefaultOpen))
        {
          ImGui::PushID("Viewer");
          scene_window->showOptions();
          ImGui::PopID();
        }

        if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_None))
        {
          ImGui::PushID("Camera");
          scene_window->showCameraOptions();
          ImGui::PopID();
        }

        for (auto& group : scene->getParameterGroups())
        {
          if (ImGui::CollapsingHeader(group.get().name.c_str(), ImGuiTreeNodeFlags_None))
          {
            ImGui::PushID(group.get().name.c_str());
            showProperties(group);
            ImGui::PopID();
          }
        }
      }
      ImGui::End();

      if (ImGui::Begin("Scene"))
      {
        scene_window->show(frame_delta.count() / 1000.f);
      }
      ImGui::End();
    }
    else
    {
      ImGui::SetNextWindowPos(glm::vec2(0));
      ImGui::SetNextWindowSize(glm::vec2(framebuffer_size));
      if (ImGui::Begin("Scene - fullscreen", nullptr,
                       ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize))
      {
        scene_window->show(frame_delta.count() / 1000.f);
      }
      ImGui::End();
    }

    switch (selected_menuitem)
    {
      case MenuItem::SAVE:
        scene->save();
        break;
      case MenuItem::QUIT:
        glfwSetWindowShouldClose(window, true);
        break;
      case MenuItem::NONE:
        break;
    };

    if (selected_action != -1)
    {
      auto& action = scene->getActions().at(selected_action);

      if (action.starts_job)
      {
        assert(!current_popup);
        assert(!next_job);
        current_popup = std::make_unique<JobStarterPopup>(action, next_job);
      }
      else
      {
        action.execute();
      }
    }

    {
      StateModifier state;
      state.setDefaults();

      gl::Viewport(0, 0, framebuffer_size.x, framebuffer_size.y);

      gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

      // ImGui Rendering
      ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
      ImGui::Render();
      gl::ClearColor(clear_color.x * clear_color.w,
                     clear_color.y * clear_color.w,
                     clear_color.z * clear_color.w,
                     clear_color.w);
      gl::Clear(GL_COLOR_BUFFER_BIT);
      ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

    glfwSwapBuffers(window);
  }

  saveSettings(ui_settings_file_path);

  scene_window.reset();
  text_renderer.reset();
  scene.reset();

  Context::setCurrent(nullptr);
  context.reset();

  gl::Finish();

  gl_binding::GL_Interface::setCurrent(nullptr);
  gl_interface.reset();

  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwMakeContextCurrent(0);

  glfwDestroyWindow(window);
  glfwTerminate();
}

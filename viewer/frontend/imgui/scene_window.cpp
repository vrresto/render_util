/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "scene_window.h"
#include "primitive_renderer.h"
#include "properties.h"
#include "scene_viewer.h"
#include <camera_2d.h>
#include <render_util/camera_3d.h>
#include <render_util/framebuffer.h>
#include <render_util/shader_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>
#include <util.h>

#include <imgui.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

#include <fstream>
#include <sstream>
#include <set>

using namespace render_util::gl_binding;
using namespace render_util::viewer;
using namespace render_util::viewer::frontend_imgui;


namespace render_util::viewer::frontend_imgui
{


SceneWindow::SceneWindow(Scene &scene, TextRenderer &text_renderer,
                         const ShaderSearchPath &shader_search_path) :
  m_scene(scene),
  m_viewer_2d(createSceneViewer(SceneViewer::VIEWER_2D, scene, text_renderer, shader_search_path)),
  m_viewer_3d(createSceneViewer(SceneViewer::VIEWER_3D, scene, text_renderer, shader_search_path))
{
  glm::ivec2 initial_size(100);

  m_framebuffer = std::make_unique<FrameBuffer>(initial_size, 1);

  setViewportSize(initial_size);

  try
  {
    auto save_file_chars = util::readFile<char>(m_cfg_file_prefix + "_viewer_state");
    if (save_file_chars.size() == sizeof(m_save_file_data))
    {
      std::memcpy(reinterpret_cast<char*>(&m_save_file_data),
                  save_file_chars.data(), sizeof(m_save_file_data));
    }
  }
  catch (...) {}

  m_primitive_renderer = std::make_unique<PrimitiveRenderer>(shader_search_path);
}


SceneWindow::~SceneWindow()
{
}


intptr_t SceneWindow::getFrameBufferTextureID()
{
  return m_framebuffer->getTexture(0)->getID();
}


void SceneWindow::saveState(int slot)
{
  assert(slot < MAX_SAVE_SLOTS);

  m_save_file_data.slot[slot].is_empty = false;
  getViewer().saveState(m_save_file_data.slot[slot].camera_state);
  m_save_file_data.slot[slot].camera_state.mode = (m_mode == MODE_2D) ?
                                      CameraState::MODE_2D : CameraState::MODE_3D;

  std::array<char, sizeof(m_save_file_data)> chars;
  std::memcpy(chars.data(), reinterpret_cast<char*>(&m_save_file_data), chars.size());

  util::writeFile(m_cfg_file_prefix + "_viewer_state", chars.data(), chars.size());
}


void SceneWindow::loadState(int slot)
{
  assert(slot < MAX_SAVE_SLOTS);

  if (!m_save_file_data.slot[slot].is_empty)
  {
    switch (m_save_file_data.slot[slot].camera_state.mode)
    {
      case CameraState::MODE_2D:
        m_mode = MODE_2D;
        break;
      case CameraState::MODE_3D:
        m_mode = MODE_3D;
        break;
      default:
        abort();
    }

    getViewer().loadState(m_save_file_data.slot[slot].camera_state);
  }
}


SceneViewer &SceneWindow::getViewer()
{
  switch (m_mode)
  {
    case MODE_2D: return *m_viewer_2d;
    case MODE_3D: return *m_viewer_3d;
    default: return *m_viewer_2d;
  }
}


bool SceneWindow::isEditor()
{
  return dynamic_cast<IEditor*>(&m_scene);
}


bool SceneWindow::hasEditableProperties()
{
  return isEditor() && !getEditableProperties().empty();
}


const util::unique_ptr_vector<IGeometricProperty>& SceneWindow::getEditableProperties()
{
  auto editor = dynamic_cast<IEditor*>(&m_scene);
  assert(editor);
  return editor->getGeometricProperties();
}


void SceneWindow::handleInput(float frame_delta, const glm::vec2 &mouse_pos_window)
{
  if (ImGui::IsKeyPressed(ImGuiKey_E))
  {
    if (m_input_mode == INPUT_MODE_CAMERA)
      m_input_mode = INPUT_MODE_EDIT;
    else if (m_input_mode == INPUT_MODE_EDIT)
      m_input_mode = INPUT_MODE_CAMERA;
  }

  if (m_input_mode == INPUT_MODE_CAMERA)
  {
    getViewer().handleInput(frame_delta);
  }
  else if (hasEditableProperties())
  {
    auto& props = getEditableProperties();

    if (ImGui::IsKeyPressed(ImGuiKey_Tab))
    {
      if (ImGui::GetIO().KeyShift)
      {
        m_edit_mode_parameter = (props.size() + (m_edit_mode_parameter-1)) % props.size();
      }
      else
      {
        m_edit_mode_parameter = (m_edit_mode_parameter+1) % props.size();
      }

      if (m_edit_mode_parameter < props.size())
      {
        m_geometric_property_editor.setProperty(props.at(m_edit_mode_parameter).get());
      }
    }

    m_geometric_property_editor.handleInput(mouse_pos_window, getViewer().getCamera());
  }
}


std::string SceneWindow::overlayText()
{
  if (m_input_mode == INPUT_MODE_EDIT)
  {
    if (hasEditableProperties())
    {
      auto &p = getEditableProperties().at(m_edit_mode_parameter);
      std::stringstream stream;
      stream << "Edit: " << p->getName();
      return stream.str();
    }
    else
    {
      return "Edit: no editable parameter";
    }
  }
  else if (m_input_mode == INPUT_MODE_CAMERA)
  {
    return "Camera";
  }
  else
  {
    return "";
  }
}


void SceneWindow::showMenuItems()
{
  auto getStateSlotText2D = [] (ViewerStateSlot &slot ) -> std::string
  {
    std::ostringstream s;
    s << "2D ("
      << slot.camera_state.state_2d.x << ","
      << slot.camera_state.state_2d.y << ","
      << slot.camera_state.state_2d.z << ")"
      << " (" << slot.camera_state.state_2d.magnification << ")";
    return s.str();
  };

  auto getStateSlotText3D = [] (ViewerStateSlot &slot ) -> std::string
  {
    std::ostringstream s;
    s << "3D ("
      << slot.camera_state.state_3d.x << ","
      << slot.camera_state.state_3d.y << ","
      << slot.camera_state.state_3d.z << ")"
      << " ("
      << slot.camera_state.state_3d.pitch << ","
      << slot.camera_state.state_3d.yaw << ","
      << slot.camera_state.state_3d.roll << ")"
      << " (" << slot.camera_state.state_3d.fov << ")";
    return s.str();
  };

  auto getStateSlotText = [&] (ViewerStateSlot &slot) -> std::string
  {
    if (slot.is_empty)
      return "(empty)";

    switch (slot.camera_state.mode)
    {
      case CameraState::MODE_2D:
        return getStateSlotText2D(slot);
      case CameraState::MODE_3D:
        return getStateSlotText3D(slot);
      default:
        return "";
    }
  };

  if (ImGui::BeginMenu("Load state"))
  {
    int slot = -1;
    for (int i = 0; i < MAX_SAVE_SLOTS; i++)
    {
      auto name = std::to_string(i) + " " + getStateSlotText(m_save_file_data.slot[i]);
      if (ImGui::MenuItem(name.c_str(), nullptr, false, !m_save_file_data.slot[i].is_empty)
              &&  !m_save_file_data.slot[i].is_empty)
        slot = i;
    }
    ImGui::EndMenu();

    if (slot != -1)
    {
      loadState(slot);
    }
  }

  if (ImGui::BeginMenu("Save state"))
  {
    int slot = -1;
    for (int i = 0; i < MAX_SAVE_SLOTS; i++)
    {
      auto name = std::to_string(i) + " " + getStateSlotText(m_save_file_data.slot[i]);
      if (ImGui::MenuItem(name.c_str(), nullptr, false, ImGui::GetIO().KeyShift))
        slot = i;
    }
    ImGui::EndMenu();

    if (slot != -1)
    {
      saveState(slot);
    }
  }
}


void SceneWindow::showOptions()
{
  ImGui::ColorEdit3("Clear color", glm::value_ptr(m_clear_color),
                    ImGuiColorEditFlags_NoInputs |
                    ImGuiColorEditFlags_Float);

  ImGui::Text("Camera mode");
  if (ImGui::RadioButton("2D", m_mode == MODE_2D))
    m_mode = MODE_2D;
  ImGui::SameLine();
  if (ImGui::RadioButton("3D", m_mode == MODE_3D))
    m_mode = MODE_3D;

  ImGui::Text("Input mode");
  if (ImGui::RadioButton("Camera", m_input_mode == INPUT_MODE_CAMERA))
    m_input_mode = INPUT_MODE_CAMERA;
  ImGui::SameLine();
  if (ImGui::RadioButton("Edit", m_input_mode == INPUT_MODE_EDIT))
    m_input_mode = INPUT_MODE_EDIT;

  if (m_input_mode == INPUT_MODE_EDIT)
  {
    auto editor = dynamic_cast<IEditor*>(&m_scene);

    if (editor && !editor->getGeometricProperties().empty())
    {
      auto& props = editor->getGeometricProperties();

      if (m_edit_mode_parameter < props.size())
      {
        auto& current = props.at(m_edit_mode_parameter);

        if (ImGui::BeginCombo("Edit parameter", current->getName().c_str()))
        {
          for (int i = 0; i < props.size(); i++)
          {
            auto& p = props.at(i);

            if (ImGui::Selectable(p->getName().c_str(), i == m_edit_mode_parameter))
              m_edit_mode_parameter = i;
          }

          ImGui::EndCombo();
        }

        m_geometric_property_editor.setProperty(current.get());
        m_geometric_property_editor.showProperties();
      }
    }
  }
}


void SceneWindow::showCameraOptions()
{
  getViewer().showProperties();
}


void SceneWindow::renderScene(glm::vec2 mouse_pos_window)
{
  assert(m_viewport_size.x > 0 && m_viewport_size.y > 0);

  TemporaryFrameBufferBinding fb(*m_framebuffer, true);

  gl::ClearColor(m_clear_color.r, m_clear_color.g, m_clear_color.b, 1.0);

  StateModifier state;
  state.setDefaults();

  gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
            GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  getViewer().render();
  drawEditingControls(mouse_pos_window);
}


IGeometricProperty* SceneWindow::getEditableProperty()
{
  if (hasEditableProperties())
  {
    auto& props = getEditableProperties();
    if (m_edit_mode_parameter < props.size())
      return props.at(m_edit_mode_parameter).get();
  }

  return nullptr;
}


void SceneWindow::drawEditingControls(glm::vec2 mouse_pos_window)
{
  if (m_input_mode == INPUT_MODE_EDIT)
  {
    m_geometric_property_editor.draw(*m_primitive_renderer, mouse_pos_window,
                                      getViewer().getCamera());
  }
}


void SceneWindow::setViewportSize(const glm::ivec2 &size)
{
  m_viewport_size = size;

  if (size.x < 1 || size.y < 1)
    return;

  m_framebuffer->setSize(size);
  getViewer().setViewportSize(size);
}


void SceneWindow::show(float frame_delta)
{
  ImVec2 canvas_origin = ImGui::GetCursorScreenPos();  // ImDrawList API uses screen coordinates!
  ImVec2 canvas_sz = ImGui::GetContentRegionAvail();   // Resize canvas to what's available

//   auto is_mouse_hovering =
//     ImGui::IsMouseHoveringRect(canvas_origin,
//                                glm::vec2(canvas_origin) + glm::vec2(canvas_sz));

  bool is_mouse_hovering = ImGui::IsWindowHovered();

  ImVec2 canvas_p0 = ImVec2(canvas_origin.x + canvas_sz.x, canvas_origin.y);
  ImVec2 canvas_p1 = ImVec2(canvas_origin.x, canvas_origin.y + canvas_sz.y);

  glm::ivec2 scene_viewport;
  scene_viewport.x = canvas_sz.x;
  scene_viewport.y = canvas_sz.y;

  setViewportSize(scene_viewport);

  if (m_viewport_size.x > 0 && m_viewport_size.y > 0)
  {
    auto mouse_pos_window =
      glm::vec2(ImGui::GetMousePos()) - glm::vec2(ImGui::GetCursorScreenPos());

    if (m_input_mode == INPUT_MODE_CAMERA)
    {
      if (!getViewer().handleMouseInput(frame_delta, mouse_pos_window,
                                        is_mouse_hovering,
                                        ImGui::IsWindowFocused()))
      {
        if (ImGui::IsWindowFocused())
          handleInput(frame_delta, mouse_pos_window);
      }
    }
    else if (m_input_mode == INPUT_MODE_EDIT)
    {
      if (ImGui::IsWindowFocused())
        handleInput(frame_delta, mouse_pos_window);
    }

    renderScene(mouse_pos_window);
  }

  intptr_t texture_id = getFrameBufferTextureID();

  ImDrawList* draw_list = ImGui::GetWindowDrawList();

  draw_list->AddImage(reinterpret_cast<void*>(texture_id), canvas_p1, canvas_p0);

  auto overlay_text = overlayText();

  glm::vec2 text_size = ImGui::CalcTextSize(overlay_text.c_str());
  glm::vec2 text_max = glm::vec2(canvas_origin) + text_size + glm::vec2(2);

  glm::vec4 bg_color(glm::vec3(0.f), 0.5f);
  draw_list->AddRectFilled(canvas_origin, text_max, ImColor(bg_color));

  glm::vec4 fg_color(glm::vec3(1.f), 1.f);
  draw_list->AddText(canvas_origin, ImColor(fg_color), overlay_text.c_str());

  if (m_scene.hasError())
  {
    auto errors = m_scene.getErrors();

    auto line_height = text_size.y + 5;

    for (int i = 0; i < errors.size(); i++)
    {
      auto& overlay_text = errors.at(i);

      auto text_origin = glm::vec2(canvas_origin) + glm::vec2(0, (i+1) * line_height);

      glm::vec2 text_size = ImGui::CalcTextSize(overlay_text.c_str());
      glm::vec2 text_max = glm::vec2(text_origin) + text_size + glm::vec2(2);

      glm::vec4 bg_color(glm::vec3(0.f), 1.0f);
      draw_list->AddRectFilled(text_origin, text_max, ImColor(bg_color));

      glm::vec4 fg_color(glm::vec3(1.f, 0.f, 0.f), 1.f);
      draw_list->AddText(text_origin, ImColor(fg_color), overlay_text.c_str());
    }
  }
}


void SceneWindow::stopDrag()
{
  m_geometric_property_editor.stopDrag();
}


} // namespace render_util::viewer::frontend_imgui

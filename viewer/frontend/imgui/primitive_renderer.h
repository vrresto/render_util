/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTEND_IMGUI_PRIMITIVE_RENDERER_H
#define RENDER_UTIL_VIEWER_FRONTEND_IMGUI_PRIMITIVE_RENDERER_H

#include <render_util/geometry.h>
#include <render_util/shader.h>

#include <glm/glm.hpp>


namespace render_util
{
  class Camera;
  class TextureManager;
}


namespace render_util::viewer::frontend_imgui
{


class PrimitiveRenderer
{
  ShaderProgramPtr m_lines_program;

public:
  PrimitiveRenderer(const ShaderSearchPath&);

  void drawFilledCircle(glm::dvec2, float size, glm::vec4 color, const Camera& camera);
  void drawRect(const RectD&, glm::vec4 color, const Camera& camera);
  void drawLine(glm::dvec2 begin, glm::dvec2 end, float width,
                glm::vec4 color, const Camera& camera);
};


} // namespace render_util::viewer::frontend_imgui

#endif
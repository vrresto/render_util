/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_FRONTENDS
#define RENDER_UTIL_VIEWER_FRONTENDS

#include <factory.h>

#include <memory>

namespace render_util::viewer
{


class Scene;
class Configuration;

void runFrontendSimple(util::Factory<Scene> f_create_scene, std::string app_name);
void runFrontendImGui(util::Factory<Scene> f_create_scene,
                      std::string app_name,
                      const Configuration& config);

}

#endif

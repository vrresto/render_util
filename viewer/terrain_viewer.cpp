/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "terrain_viewer_scene_base.h"
#include <render_util/viewer/application.h>
#include <render_util/viewer/camera.h>
#include <render_util/viewer/viewer.h>
#include <render_util/viewer/scene.h>
#include <render_util/viewer/map_loader.h>
#include <render_util/render_util.h>
#include <render_util/texture_state.h>
#include <render_util/atmosphere.h>
#include <render_util/shader.h>
#include <render_util/shader_util.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <render_util/elevation_map.h>
#include <render_util/camera.h>
#include <render_util/terrain_util.h>
#include <render_util/image_util.h>
#include <render_util/cirrus_clouds.h>
#include <render_util/state.h>
#include <render_util/stats.h>
#include <render_util/gl_binding/gl_binding.h>
#include <log.h>

#include <argparse/argparse.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <memory>
#include <exception>

#include <render_util/gl_binding/gl_interface.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace glm;
using namespace std;
using namespace render_util::gl_binding;
using namespace render_util::viewer;
using namespace render_util;

#include <render_util/skybox.h>


#define ENABLE_CIRRUS 0


namespace
{


constexpr auto g_terrain_use_lod = true;
constexpr auto cache_path = RENDER_UTIL_CACHE_DIR;
constexpr auto shader_path = RENDER_UTIL_SHADER_DIR;

constexpr AtmosphereCreationParameters DEFAULT_ATMOSPHERE_PARAMETERS =
{
  .precomputation =
  {
    .options =
    {
      .use_half_precision = false,
      .use_constant_solar_spectrum = false,
      .use_ozone = true,
      .use_combined_textures = true,
      .do_white_balance = true,
      .scattering_orders = 6,
      .precomputed_luminance = false,
      .single_mie_horizon_hack = false,
    },
    .atmosphere_properties =
    {
      .mie_angstrom_beta_factor = 10.0,
    },
    .allow_slow_precomputation = false,
    .cache_mode = AtmospherePrecomputationParameters::CacheMode::USE_OR_CREATE,
  },
  .max_cirrus_albedo = 0.4,
};

const auto shore_wave_hz = vec4(0.05, 0.07, 0, 0);


struct Terrain
{
  std::shared_ptr<TerrainBase> m_terrain;
  std::shared_ptr<TerrainBase> getTerrain() { return m_terrain; }
};


// glm::dvec3 hitGround(const Beam &beam_)
// {
//   auto beam = beam_;
//
//   beam.direction *= -1;
//
//   const glm::vec3 plane_normal = glm::vec3(0,0,1);
//
//   float distance = -dot(plane_normal, beam.origin) / dot(beam.direction, plane_normal);
//
//   if (distance < 0)
//     throw std::exception();
//   else
//     return beam.origin + beam.direction * distance;
// }


} // namespace


namespace terrain_viewer
{

#if 0
  class Map : public MapBase
  {
    TerrainBase::MaterialMap::ConstPtr m_material_map;
    MapTextures m_map_textures;
    WaterAnimation m_water_animation;
    std::shared_ptr<GenericImage> m_cirrus_texture;

  public:
    Map(const TextureManager &texture_manager) : m_map_textures(texture_manager) {}

    MapTextures &getTextures() override { return m_map_textures; }
    WaterAnimation &getWaterAnimation() override { return m_water_animation; }
    void setMaterialMap(TerrainBase::MaterialMap::ConstPtr map) override
    {
      m_material_map = map;
    }

    void setCirrusTexture(std::shared_ptr<GenericImage> texture) override
    {
      m_cirrus_texture = texture;
    }

    std::shared_ptr<GenericImage> getCirrusTexture() { return m_cirrus_texture; }

    TerrainBase::MaterialMap::ConstPtr getMaterialMap() { return m_material_map; }
  };
#endif
}

class TerrainViewerScene : public TerrainViewerSceneBase
{
//   const bool m_use_base_map = false;
//   const bool m_use_base_water_map = false;

// #if ENABLE_BASE_MAP
//   glm::vec2 base_map_origin = glm::vec2(0);
//   glm::vec2 base_map_size_m = glm::vec2(0);
// #endif

  Atmosphere::Type m_atmosphere_type = Atmosphere::DEFAULT;
  bool m_update_cache = false;
  int m_load_modules = 0;
  int m_enable_modules = 0;
  vec2 map_size = vec2(0);
  ivec2 mark_pixel_coords = ivec2(0);
  render_util::TexturePtr curvature_map;
  render_util::TexturePtr atmosphere_map;

  render_util::ShaderProgramPtr sky_program;
//   render_util::ShaderProgramPtr forest_program;

  ShaderSearchPath m_shader_search_path;

  Terrain m_terrain;
  std::unique_ptr<Atmosphere> m_atmosphere;

  shared_ptr<MapLoaderBase> m_map_loader;

#if ENABLE_CIRRUS
  unique_ptr<CirrusClouds> m_cirrus_clouds;
#endif

// #if ENABLE_VIEWER_BASE_MAP
//   render_util::ImageGreyScale::Ptr m_base_map_land;
//   render_util::TexturePtr m_base_map_land_texture;
//   ElevationMap::Ptr m_elevation_map_base;
// #endif


  void addAtmosphereController(std::string name, Atmosphere::Parameter p)
  {
    if (m_atmosphere->hasParameter(p))
    {
      using Wrapper = ValueWrapper<float>;

      Wrapper wrapper =
      {
        [this,p] { return m_atmosphere->getParameter(p); },
        [this,p] (auto value) { m_atmosphere->setParameter(p, value); },
      };
      m_rendering_parameters.addWrapper<float>(name, wrapper);
    }
  }


  void createControllers()
  {
    addAtmosphereController("exposure", Atmosphere::Parameter::EXPOSURE);
    addAtmosphereController("saturation", Atmosphere::Parameter::SATURATION);
    addAtmosphereController("brightness_curve_exponent", Atmosphere::Parameter::BRIGHTNESS_CURVE_EXPONENT);
    addAtmosphereController("texture_brightness", Atmosphere::Parameter::TEXTURE_BRIGHTNESS);
    addAtmosphereController("texture_brightness_curve_exponent",
                            Atmosphere::Parameter::TEXTURE_BRIGHTNESS_CURVE_EXPONENT);
    addAtmosphereController("texture_saturation", Atmosphere::Parameter::TEXTURE_SATURATION);
    addAtmosphereController("gamma", Atmosphere::Parameter::GAMMA);
    addAtmosphereController("blue saturation", Atmosphere::Parameter::BLUE_SATURATION);

    addAtmosphereController("uncharted2_a", Atmosphere::Parameter::UNCHARTED2_A);
    addAtmosphereController("uncharted2_b", Atmosphere::Parameter::UNCHARTED2_B);
    addAtmosphereController("uncharted2_c", Atmosphere::Parameter::UNCHARTED2_C);
    addAtmosphereController("uncharted2_d", Atmosphere::Parameter::UNCHARTED2_D);
    addAtmosphereController("uncharted2_e", Atmosphere::Parameter::UNCHARTED2_E);
    addAtmosphereController("uncharted2_f", Atmosphere::Parameter::UNCHARTED2_F);
    addAtmosphereController("uncharted2_w", Atmosphere::Parameter::UNCHARTED2_W);
  }

#if 1
  void createTerrain(render_util::TerrainBase::BuildParameters &params,
                     const ShaderSearchPath &shader_search_path,
                     glm::vec2 base_map_origin)
  {
    m_terrain.m_terrain = render_util::createTerrain(shader_search_path);

    auto on_texture_state_create = [this] (TextureState& state)
    {
      m_atmosphere->bindTextures(state);
      state.bind("sampler_curvature_map", *curvature_map);
    };

    getTerrain().setTextureStateCreateObserver(on_texture_state_create);

//     m_terrain.getTerrain()->setAnisotropy(m_anisotropy);
    getTerrain().build(params);
  }
#endif

//   void updateTerrain()
//   {
//     m_terrain.update(camera);
//     m_terrain_cdlod.update(camera);
//   }

  void drawTerrain(const render_util::Camera&);


  void updateUniforms(render_util::ShaderProgram& program,
                      const render_util::Camera &camera) override;
//   void updateBaseWaterMapTexture();
//   void buildBaseMap();

  TerrainBase& getTerrain() override
  {
    assert(m_terrain.getTerrain());
    return *m_terrain.getTerrain();
  }

  render_util::ShaderParameters makeShaderParameters() override
  {
    auto params = TerrainViewerSceneBase::makeShaderParameters();
    params.add(m_atmosphere->getShaderParameters());
    return params;
  }

  void reloadShaders() override
  {
    try
    {
      sky_program = render_util::createShaderProgram("sky",
                                                     m_shader_search_path,
                                                     makeShaderParameters());
    }
    catch (...)
    {
    }

    TerrainViewerSceneBase::reloadShaders();
  }

public:
  TerrainViewerScene(CreateMapLoaderFunc&, Atmosphere::Type,
                     bool update_cache,
                     bool enable_atmosphere,
                     int load_modules,
                     int enable_modules);
  ~TerrainViewerScene() override;

  void update(float frame_delta) override;
  void render(const render_util::Camera &camera) override;

  void setup() override;
  void mark() override;
  void unmark() override;
  void cursorPos(const glm::dvec2&) override;
  glm::vec2 getMapSizeM() override { return map_size; }
//   void rebuild() override { buildBaseMap(); }

  void getStats(Stats& stats) override
  {
    auto& terrain_stats = stats.addChild("terrain");
    m_terrain.getTerrain()->getStats(terrain_stats);
  }

};


void TerrainViewerScene::drawTerrain(const render_util::Camera& camera)
{
  auto update_uniforms = [&] (auto& program)
  {
    updateUniforms(program, camera);
  };

  getTerrain().update(camera, false);
  getTerrain().draw(camera, update_uniforms);
}


TerrainViewerScene::TerrainViewerScene(CreateMapLoaderFunc &create_map_loader,
                                       Atmosphere::Type atmosphere_type,
                                       bool update_cache,
                                       bool enable_atmosphere,
                                       int load_modules,
                                       int enable_modules) :
  m_atmosphere_type(atmosphere_type),
  m_update_cache(update_cache),
  m_load_modules(load_modules),
  m_enable_modules(enable_modules)
{
  m_enable_atmosphere = enable_atmosphere;

  LOG_INFO<<"TerrainViewerScene::TerrainViewerScene"<<endl;

  m_map_loader = create_map_loader();
  assert(m_map_loader);
}

TerrainViewerScene::~TerrainViewerScene()
{
// #if ENABLE_VIEWER_BASE_MAP
//   auto land_map_flipped = image::flipY(m_base_map_land);
//   saveImageToFile(RENDER_UTIL_CACHE_DIR "/base_map_land_editor.tga", land_map_flipped.get());
//   std::ofstream s(RENDER_UTIL_CACHE_DIR "/base_map_origin_editor", ios_base::binary);
//   s<<"BaseMapOriginX="<<base_map_origin.x<<endl;
//   s<<"BaseMapOriginY="<<base_map_origin.y<<endl;
// #endif
}


// void TerrainViewerScene::buildBaseMap()
// {
// #if ENABLE_VIEWER_BASE_MAP
//   m_elevation_map_base = m_map_loader->createBaseElevationMap(m_base_map_land);
//   base_map_size_m =
//     glm::vec2(m_elevation_map_base->getSize() * (int)render_util::HEIGHT_MAP_BASE_METERS_PER_PIXEL);
//
//   updateTerrain(m_elevation_map_base);
//
//   m_map->getTextures().bind(getTextureManager());
//
//   updateBaseWaterMapTexture();
// #endif
// }


void TerrainViewerScene::mark()
{
// #if ENABLE_VIEWER_BASE_MAP
//   m_base_map_land->at(mark_pixel_coords) = 255;
//   updateBaseWaterMapTexture();
// #endif
}


void TerrainViewerScene::unmark()
{
// #if ENABLE_VIEWER_BASE_MAP
//   m_base_map_land->at(mark_pixel_coords) = 0;
//   updateBaseWaterMapTexture();
// #endif
}


void TerrainViewerScene::cursorPos(const glm::dvec2 &pos)
{
// #if ENABLE_VIEWER_BASE_MAP
//   auto beam = camera.createBeamThroughViewportCoord(glm::vec2(pos));
//
//   vec3 ground_plane_pos = vec3(0);
//
//   try
//   {
//     ground_plane_pos = hitGround(beam);
//   }
//   catch(...)
//   {
//     return;
//   }
//
//   auto base_map_pos_relative = (glm::vec2(ground_plane_pos.x, ground_plane_pos.y) - base_map_origin) / base_map_size_m;
//   mark_pixel_coords = base_map_pos_relative * glm::vec2(m_base_map_land->getSize());
//   mark_pixel_coords = clamp(mark_pixel_coords, ivec2(0), m_base_map_land->getSize() - ivec2(1));
// #endif
}


// void TerrainViewerScene::updateBaseWaterMapTexture()
// {
// #if ENABLE_VIEWER_BASE_MAP
//   setTextureImage(m_base_map_land_texture, m_base_map_land);
// #endif
// }


void TerrainViewerScene::setup()
{
  LOG_INFO<<"void TerrainViewerScene::setup()"<<endl;

  {
    auto params = DEFAULT_ATMOSPHERE_PARAMETERS;

    if (m_update_cache)
      params.precomputation.cache_mode =
        AtmospherePrecomputationParameters::CacheMode::UPDATE;
    else
      params.precomputation.cache_mode =
        AtmospherePrecomputationParameters::CacheMode::USE_OR_CREATE;

    m_atmosphere = createAtmosphere(m_atmosphere_type, RENDER_UTIL_SHADER_DIR, params);
  }

  curvature_map = render_util::createCurvatureTexture(cache_path);
  atmosphere_map = render_util::createAmosphereThicknessTexture(cache_path);

  m_shader_search_path.push_back(string(RENDER_UTIL_SHADER_DIR) + "/" +
                                  m_atmosphere->getShaderPath());
  m_shader_search_path.push_back(RENDER_UTIL_SHADER_DIR);

//   forest_program = render_util::createShaderProgram("forest", getTextureManager(), shader_path);
//   forest_program = render_util::createShaderProgram("forest_cdlod", getTextureManager(), shader_path);

#if ENABLE_BASE_MAP
  base_map_origin = m_map_loader->getBaseMapOrigin();
  m_base_map_land = m_map_loader->createBaseLandMap();
  m_elevation_map_base = m_map_loader->createBaseElevationMap(m_base_map_land);
#endif

  render_util::TerrainBase::BuildParameters params =
  {
    .resources = m_map_loader->getTerrainResources(),
    .shader_parameters = makeShaderParameters(),
    .load_modules = m_load_modules,
    .enabled_modules = m_enable_modules,
  };

//   createTerrain(params, shader_search_path, glm::vec2(m_base_map_origin));
  createTerrain(params, m_shader_search_path, glm::vec2(0));

  map_size = glm::vec2(params.resources.getDetailLayer().getSizePx()) *
                       glm::vec2(params.resources.getDetailLayer().getResolutionM());


  assert(map_size != vec2(0));

  LOG_INFO<<"map size: "<<map_size.x<<","<<map_size.y<<endl;

  CHECK_GL_ERROR();

// #if ENABLE_VIEWER_BASE_MAP
//   if (!m_base_map_land)
//     m_base_map_land = image::create<unsigned char>(0, ivec2(128));
//   m_base_map_land_texture =
//       render_util::createTexture<render_util::ImageGreyScale>(m_base_map_land);
//   getTextureManager().bind(TEXUNIT_WATER_MAP_BASE, m_base_map_land_texture);
//   buildBaseMap();
// #endif

#if ENABLE_CIRRUS
  m_cirrus_clouds = make_unique<CirrusClouds>(0.7, getTextureManager(), shader_search_path,
                                              shader_params, 7000, m_map->getCirrusTexture());
#endif

  CHECK_GL_ERROR();
  CHECK_GL_ERROR();

  m_initial_camera_pos.x = map_size.x / 2;
  m_initial_camera_pos.y = map_size.y / 2;
  m_initial_camera_pos.z = 10000;

  addParameters();

  createControllers();

  m_parameter_groups.push_back(m_world_parameters);
  m_parameter_groups.push_back(m_rendering_parameters);

  addTerrainParameters();

  reloadShaders();
}


void TerrainViewerScene::updateUniforms(render_util::ShaderProgram& program,
                                        const render_util::Camera &camera)
{
  CHECK_GL_ERROR();

  TerrainViewerSceneBase::updateUniforms(program, camera);

  m_atmosphere->setUniforms(program);

  program.setUniform("toggle_lod_morph", toggle_lod_morph);

  CHECK_GL_ERROR();


  CHECK_GL_ERROR();

  program.setUniform("terrain_height_offset", 0.f);
  program.setUniform("terrain_base_map_height", 0.f);

  CHECK_GL_ERROR();
}


void TerrainViewerScene::update(float frame_delta)
{
  m_terrain.m_terrain->updateAnimation(frame_delta);
}


void TerrainViewerScene::render(const render_util::Camera& camera)
{
  if (m_enable_atmosphere && sky_program)
  {
    StateModifier state;
    state.setDefaults();
    state.enableDepthTest(false);
    state.enableCullFace(true);
    state.setFrontFace(GL_CW);
    state.setProgram(sky_program->getID());

    updateUniforms(*sky_program, camera);
    sky_program->assertUniformsAreSet();

    render_util::drawSkyBox();
  }

  drawTerrain(camera);

#if ENABLE_CIRRUS
  {
    StateModifier state;

    state.setDefaults();
    state.enableBlend(true);
    state.enableDepthTest(true);
    state.setProgram(m_cirrus_clouds->getProgram()->getID());

    updateUniforms(m_cirrus_clouds->getProgram(), camera);
    m_cirrus_clouds->getProgram()->setUniform("is_far_camera", true);
    m_cirrus_clouds->draw(state, camera);
    m_cirrus_clouds->getProgram()->setUniform("is_far_camera", false);
    m_cirrus_clouds->draw(state, camera);
  }
#endif

  // forest
#if 0
  const int forest_layers = 5;
  const int forest_layer_repetitions = 1;
  const float forest_layer_height = 2.5;

  terrain_renderer.m_terrain->setDrawDistance(5000.f);
  terrain_renderer.m_terrain->update(camera);

  getCurrentGLContext()->setCurrentProgram(forest_program);
  updateUniforms(forest_program);
  forest_program->setUniformi("forest_layer", 0);
  forest_program->setUniform("terrain_height_offset", 0);
  CHECK_GL_ERROR();

  gl::Enable(GL_BLEND);
  gl::BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  gl::Disable(GL_CULL_FACE);

//   terrain_renderer.m_terrain->draw(forest_program);
  CHECK_GL_ERROR();

  for (int i = 1; i < forest_layers; i++)
  {
    forest_program->setUniformi("forest_layer", i);

    for (int y = 0; y < forest_layer_repetitions; y++)
    {
      float height = i * forest_layer_height + y * (forest_layer_height / forest_layer_repetitions);
      forest_program->setUniform("terrain_height_offset", height);
      terrain_renderer.m_terrain->draw(forest_program);
      CHECK_GL_ERROR();
    }
  }

  gl::Enable(GL_CULL_FACE);
  gl::Disable(GL_BLEND);
#endif

}


class TerrainViewerApplication : public viewer::Application
{
  bool m_update_cache = false;
  bool m_atmosphere = false;
  int m_load_modules = 0;
  int m_enable_modules = 0;
  CreateMapLoaderFunc m_create_map_loader;
  GuiType m_gui = GuiType::DEFAULT;

  int getModulesFromString(std::string arg)
  {
    int modules = 0;

    for (auto& name : util::tokenize(arg, ','))
    {
      if (name == "all")
        modules |= TerrainBase::ModuleMask::ALL;
      else
        modules |= TerrainBase::getModuleMask(TerrainBase::getModuleIndex(name));
    }

    return modules;
  }

  std::string getModulesHelp()
  {
    return "comma separated list - possible values: \n"
           + util::joinStrings(TerrainBase::getModuleNames(), ",") + ",all";
  }

public:
  TerrainViewerApplication(std::string name, CreateMapLoaderFunc& create_map_loader) :
    Application(name),
    m_create_map_loader(create_map_loader)
  {
  }

  void addArguments(argparse::ArgumentParser& args) override
  {
    args.add_argument("--update-cache").default_value(false).implicit_value(true);
    args.add_argument("--no-atmosphere").default_value(false).implicit_value(true);
    args.add_argument("--load-modules").default_value(std::string("all"))
      .help(getModulesHelp());;
    args.add_argument("--enable-modules").default_value(std::string("all"))
      .help(getModulesHelp());
    args.add_argument("--gui").default_value(std::string("default"));
  }

  void processArguments(argparse::ArgumentParser& args) override
  {
    m_update_cache = args.get<bool>("--update-cache");
    m_atmosphere = !args.get<bool>("--no-atmosphere");
    m_load_modules = getModulesFromString(args.get<std::string>("--load-modules"));
    m_enable_modules = getModulesFromString(args.get<std::string>("--enable-modules"));

    auto gui = args.get<std::string>("--gui");

    if (gui == "imgui")
      m_gui = GuiType::IMGUI;
    else if (gui == "simple")
      m_gui = GuiType::SIMPLE;
    else if (gui == "default")
      m_gui = GuiType::DEFAULT;
    else
      throw std::runtime_error("invalid gui");
  }

  void run() override
  {
    Atmosphere::Type atmosphere = Atmosphere::DEFAULT;
    if (util::makeLowercase(getConfig().get("atmosphere", "")) == "precomputed")
      atmosphere = Atmosphere::PRECOMPUTED;

    auto create_scene = [this, atmosphere]
    {
      auto scene = make_shared<TerrainViewerScene>(m_create_map_loader,
                                                   atmosphere,
                                                   m_update_cache,
                                                   m_atmosphere,
                                                   m_load_modules,
                                                   m_enable_modules);
      return scene;
    };

    runViewer(create_scene, getName(), getConfig(), m_gui);

    LOG_INFO<<"exiting..."<<endl;
  }
};



namespace render_util::viewer
{


std::unique_ptr<Application>
createTerrainViewer(std::string app_name, CreateMapLoaderFunc create_map_ldr)
{
  return std::make_unique<TerrainViewerApplication>(app_name, create_map_ldr);
}


}

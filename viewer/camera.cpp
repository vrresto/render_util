#include <render_util/viewer/camera.h>
#include <render_util/viewer/camera_state.h>


namespace render_util::viewer
{


void Camera::saveState(CameraState& state)
{
  state.state_3d.x = x;
  state.state_3d.y = y;
  state.state_3d.z = z;
  state.state_3d.pitch = pitch;
  state.state_3d.yaw = yaw;
  state.state_3d.roll = roll;
  state.state_3d.fov = getFov();
}


void Camera::loadState(const CameraState& state)
{
  x = state.state_3d.x;
  y = state.state_3d.y;
  z = state.state_3d.z;
  pitch = state.state_3d.pitch;
  yaw = state.state_3d.yaw;
  roll = state.state_3d.roll;

  updateTransform();
  setFov(state.state_3d.fov);
}


} // namespace render_util::viewer

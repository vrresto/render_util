/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_TERRAIN_PARAMETERS_H
#define RENDER_UTIL_VIEWER_TERRAIN_PARAMETERS_H


#include <render_util/map_geography.h>
#include <render_util/terrain_base.h>
#include <render_util/physics.h>
#include <util/map_projection.h>
#include <log.h>

#include <glm/glm.hpp>

#include <memory>
#include <cassert>
#include <optional>


namespace render_util::viewer
{


class MapProperties : public MapGeography
{
  glm::dvec2 m_size_m_local;

public:
  MapProperties(const MapGeography& geo, glm::dvec2 size_m_local) :
    MapGeography(geo), m_size_m_local(size_m_local) {}

  glm::dvec2 getSizeLocal() { return m_size_m_local; }
};


class TerrainParameters
{
  std::array<std::unique_ptr<MapProperties>, 2> m_layers;

public:
  bool m_has_base_map = false;

  virtual const MapGeography& getWorldGeography() = 0;

  bool hasBaseMap() { return m_has_base_map; }

  MapProperties& getLayer(int layer)
  {
    assert(m_layers.at(layer));
    return *m_layers.at(layer);
  }

  MapProperties& getDetailLayer()
  {
    return getLayer(TerrainBase::LAYER_DETAIL);
  }

  MapProperties& getBaseLayer()
  {
    return getLayer(TerrainBase::LAYER_BASE);
  }

  void setLayer(int layer, const MapGeography& geo, glm::dvec2 size_m_local)
  {
    m_layers.at(layer) = std::make_unique<MapProperties>(geo, size_m_local);
  }

  void setLayerGeography(int layer, const MapGeography& geo)
  {
    m_layers.at(layer)->set(geo);
  }

  double getLayerScaleWorld(int layer)
  {
    return getLayer(layer).getScaleWorld(getWorldGeography());
  }

  void setLayerScaleWorld(int layer, double scale)
  {
    getLayer(layer).setScaleWorld(scale, getWorldGeography());
  }

  glm::dvec2 getLayerOriginWorld(int layer)
  {
    return getLayer(layer).getOriginWorld(getWorldGeography());
  }

  void setLayerOriginWorld(int layer, glm::dvec2 world)
  {
    auto geodesic = getWorldGeography().localToGeodesic(world);
    getLayer(layer).setOriginGeodesic(geodesic);
  }

  glm::dvec2 getLayerExtentWorld(int layer)
  {
    return getLayerScaleWorld(layer) * getLayer(layer).getSizeLocal();
  }

  void setLayerExtentWorldX(int layer, double new_extent)
  {
    setLayerScaleWorld(layer, new_extent / getLayer(layer).getSizeLocal().x);
  }

  void setLayerExtentWorldY(int layer, double new_extent)
  {
    setLayerScaleWorld(layer, new_extent / getLayer(layer).getSizeLocal().y);
  }

  void applyLayerParameters(int layer, TerrainBase &terrain)
  {
    terrain.setLayerOrigin(layer, glm::vec3(getLayerOriginWorld(layer), 0));
    terrain.setLayerScale(layer, getLayerScaleWorld(layer));
  }

  void apply(TerrainBase &terrain)
  {
    applyLayerParameters(TerrainBase::LAYER_DETAIL, terrain);
    if (hasBaseMap())
      applyLayerParameters(TerrainBase::LAYER_BASE, terrain);
  }
};


} // namespace render_util::viewer


#endif

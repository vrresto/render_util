/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "render_util/viewer/configuration.h"
#include <util.h>

#include <fstream>

namespace render_util::viewer
{


Configuration::Configuration() {}


Configuration::Configuration(std::string file_path)
{
  std::ifstream in(file_path);

  while (in.good())
  {
    std::string line;
    std::getline(in, line);
    if (util::isNullOrWhiteSpace(line))
      continue;

    line = util::trim(line);
    auto split_pos = line.find('=');
    if (split_pos != std::string::npos)
    {
      auto key = util::trim(line.substr(0, split_pos));
      auto value = util::trim(line.substr(split_pos + 1));

      if (!value.empty())
      {
        values[key] = value;
      }
    }
  }
}


void Configuration::save(std::string file_path)
{
  std::ofstream out(file_path);

  for (auto& it : values)
  {
    out << it.first << " = " << it.second << std::endl;
  }
}


} // namespace render_util::viewer

/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "frontends.h"
#include "config.h"
#include <render_util/viewer/viewer.h>
#include <render_util/viewer/configuration.h>


namespace
{

constexpr bool USE_IMGUI_DEFAULT = false;

}


namespace render_util::viewer
{


void runViewer(util::Factory<Scene> f_create_scene,
               std::string app_name,
               const Configuration& cfg,
               GuiType gui)
{
#if ENABLE_VIEWER_IMGUI_FRONTEND
  bool use_imgui = cfg.get("UseImGui", USE_IMGUI_DEFAULT);

  switch (gui)
  {
    case GuiType::SIMPLE:
      use_imgui = false;
      break;
    case GuiType::IMGUI:
      use_imgui = true;
      break;
    case GuiType::DEFAULT:
    default:
      break;
  }

  if (use_imgui)
    runFrontendImGui(f_create_scene, app_name, cfg);
  else
#endif
    runFrontendSimple(f_create_scene, app_name);
}


}

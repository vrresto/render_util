/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "camera_2d.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

using namespace glm;
using namespace render_util;

using Mat4 = Camera::Mat4;
using Vec2 = Camera::Vec2;
using Vec3 = Camera::Vec3;
using Unit = Camera::Unit;


namespace render_util::viewer
{


Camera2D::Camera2D()
{
  m_max_area_size = Vec2(1);
  m_viewport_size = ivec2(1);
  m_pos = Vec3(0);

  refreshTransform();
  refreshProjection();
}


Vec2 Camera2D::getAreaSize() const
{
  auto aspect = Unit(m_viewport_size.x) / Unit(m_viewport_size.y);

  Vec2 size;
  size.y = m_max_area_size.y / m_magnification;
  size.x = size.y * aspect;

  return size;
}


Vec2 Camera2D::getViewportOriginWorld() const
{
  return Vec2(m_pos) - (getAreaSize() / 2.0);
}


Unit Camera2D::getViewportToWorldScale() const
{
  return (m_max_area_size.y / m_magnification) / m_viewport_size.y;
}


Unit Camera2D::getWorldToViewportScale() const
{
  return 1.0 / getViewportToWorldScale();
}


Vec2 Camera2D::viewportToWorld(Vec2 viewport_coords) const
{
  return getViewportOriginWorld() + (viewport_coords * getViewportToWorldScale());
}


Vec2 Camera2D::worldToViewport(Vec2 world_coords) const
{
  return (world_coords - getViewportOriginWorld()) / getViewportToWorldScale();
}


void Camera2D::setPos(const Vec2 &pos)
{
  m_pos = Vec3(pos, 0);
  refreshTransform();
}


void Camera2D::setMagnification(Unit magnification)
{
  m_magnification = magnification;
  refreshProjection();
}


void Camera2D::setMaxAreaSize(Vec2 size)
{
  m_max_area_size = size;
  refreshProjection();
}


void Camera2D::setViewportOriginWorld(Vec2 world_coords)
{
  auto area_size = getAreaSize();

  auto new_pos = world_coords + (area_size / 2.0);

  m_pos = Vec3(new_pos, 0);

  refreshTransform();
}


void Camera2D::setViewportSize(int width, int height)
{
  m_viewport_size = ivec2(width, height);
  refreshProjection();
}


void Camera2D::refreshTransform()
{
  Mat4 world_to_y_up(1);

  m_world_to_view = world_to_y_up * translate(Mat4(1), Vec3(-m_pos));

  m_view_to_world = affineInverse(m_world_to_view);

  m_world_to_view_rotation = world_to_y_up;
}


void Camera2D::refreshProjection()
{
  auto area_size = getAreaSize();

  auto left = - (area_size.x / 2);
  auto right = + (area_size.x / 2);
  auto bottom = - (area_size.y / 2);
  auto top = + (area_size.y / 2);

  m_projection = ortho(left, right, bottom, top, -5000.0, 5000.0);
}


bool Camera2D::cull(const Box &box) const
{
  return false;
}


} // namespace render_util::viewer

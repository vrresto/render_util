/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/viewer/viewer.h>
#include <log/file_appender.h>
#include <log/console_appender.h>
#include <log/txt_formatter.h>
#include <log/message_only_formatter.h>
#include <log.h>

#include <argparse/argparse.hpp>


namespace render_util::viewer
{


void initLog(std::string app_name)
{
  static bool initialized = false;
  if (initialized)
    return;

  initialized = true;

#if USE_PLOG
  constexpr bool ADD_NEW_LINE = false;

  using namespace util::log;
  using FileSink = FileAppender<TxtFormatter<ADD_NEW_LINE>>;
  using ConsoleSink = ConsoleAppender<MessageOnlyFormatter<ADD_NEW_LINE>>;

  static FileSink file_sink_warn(app_name + "_warnings.log");
  static FileSink file_sink_info(app_name + "_info.log");
  static FileSink file_sink_debug(app_name + "_debug.log");
  static FileSink file_sink_trace(app_name + "_trace.log");

  static ConsoleSink console_sink;

  auto &logger_default = plog::init(plog::verbose);
  auto &logger_console = plog::init<LOGGER_CONSOLE>(plog::info);

  auto &warn_sink = plog::init<LOGGER_WARNING>(plog::warning, &file_sink_warn);
  auto &info_sink = plog::init<LOGGER_INFO>(plog::info, &file_sink_info);
  auto &debug_sink = plog::init<LOGGER_DEBUG>(plog::debug, &file_sink_debug);
  auto &trace_sink = plog::init<LOGGER_TRACE>(plog::verbose, &file_sink_trace);

  logger_console.addAppender(&console_sink);

  logger_default.addAppender(&logger_console);
  logger_default.addAppender(&warn_sink);
  logger_default.addAppender(&info_sink);
  logger_default.addAppender(&debug_sink);
  logger_default.addAppender(&trace_sink);
#endif
}


void addLogArguments(argparse::ArgumentParser& arguments)
{
  arguments.add_argument("--verbose").nargs(1).default_value(int(plog::info)).scan<'i', int>().required();
}


void processLogArguments(argparse::ArgumentParser& arguments)
{
  int value = arguments.get<int>("--verbose");

  if (value >= 0 && value <= plog::verbose)
  {
    plog::get<util::log::LOGGER_CONSOLE>()->setMaxSeverity(plog::Severity(value));
  }
  else
  {
    throw std::runtime_error("No such verbosity level: " + std::to_string(value));
  }
}


} // namespace render_util::viewer

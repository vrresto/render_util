/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "frontends.h"
#include "config.h"
// #include <render_util/viewer/viewer.h>
#include <render_util/viewer/scene.h>
#include <render_util/viewer/configuration.h>


using namespace render_util::viewer;


std::string APP_NAME = "test_frontend";


class DummyScene : public render_util::viewer::Scene
{
  void setup() override {}
  void update(float frame_delta) override  {}
  void render(const render_util::Camera &camera) override  {}
  glm::vec2 getMapSizeM() override  { return glm::vec2(100); }
};


int main()
{
  auto create_scene = [] ()
  {
    return std::make_shared<DummyScene>();
  };

  Configuration cfg(APP_NAME + ".ini");

#if ENABLE_VIEWER_IMGUI_FRONTEND
  runFrontendImGui(create_scene, APP_NAME, cfg);
#endif
}

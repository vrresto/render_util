#version 130

uniform sampler2D sampler_0;

varying vec2 pass_texcoord;

void main(void)
{
  gl_FragColor = texture(sampler_0, pass_texcoord);
}

#version 130

uniform mat4 projectionMatrixFar;
uniform mat4 world2ViewMatrix;

varying vec2 pass_texcoord;

void main(void)
{
  gl_Position = projectionMatrixFar * world2ViewMatrix * gl_Vertex;
  pass_texcoord = gl_MultiTexCoord0.xy;
}

/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include terrain_params.h.glsl
#include terrain_geometry_util.h.glsl

#define ENABLE_DETAIL_MAP_CLIP @enable_detail_map_clip@
#define ENABLE_TERRAIN_LAYER_BLEND @enable_terrain_layer_blend@
#define ENABLE_TERRAIN_LAYER_SCALE @enable_terrain_layer_scale@

uniform Terrain terrain;

#if @enable_base_map@
  uniform float terrain_base_map_height = 0.0;
#endif

#if ENABLE_DETAIL_MAP_CLIP
  uniform vec2 detail_map_clip_min;
  uniform vec2 detail_map_clip_max;
#endif


vec2 getHeightMapTextureCoords(in TerrainLayer layer, vec2 pos_m)
{
  vec2 coords =
#if ENABLE_TERRAIN_LAYER_SCALE
    (pos_m - layer.origin_m + vec2(0, layer.scale * layer.height_map.resolution_m))
      / (layer.scale * layer.height_map.size_m);
#else
    (pos_m - layer.origin_m + vec2(0, layer.height_map.resolution_m))
      / layer.height_map.size_m;
#endif
  coords.y = 1.0 - coords.y;
  return coords;
}


vec2 getNormalMapCoords(in TerrainLayer layer, vec2 pos_m)
{
  vec2 coords =
#if ENABLE_TERRAIN_LAYER_SCALE
    (pos_m - layer.origin_m + vec2(0, layer.scale * layer.normal_map.resolution_m))
      / (layer.scale * layer.normal_map.size_m);
#else
    (pos_m - layer.origin_m + vec2(0, layer.normal_map.resolution_m))
      / layer.normal_map.size_m;
#endif
  coords.y = 1.0 - coords.y;
  return coords;
}


vec3 sampleTerrainNormalMap(in TerrainLayer layer, vec2 pos_m)
{
  vec2 coords = getNormalMapCoords(layer, pos_m);
  vec3 normal = texture2D(layer.normal_map.sampler, coords).xyz;
  normal.y *= -1;
  return normal;
}


float getTerrainHeight(vec2 pos_m)
{
  vec2 height_map_texture_coords = getHeightMapTextureCoords(terrain.detail_layer, pos_m);
  float detail = texture2D(terrain.detail_layer.height_map.sampler, height_map_texture_coords).x;

#if @enable_base_map@
  vec2 base_height_map_texture_coords = getHeightMapTextureCoords(terrain.base_layer, pos_m);
  float base = texture2D(terrain.base_layer.height_map.sampler, base_height_map_texture_coords).x;
  base += terrain_base_map_height;

  float detail_blend = getDetailMapBlend(pos_m);

//   if (detail < 0.2)
//     return base;
//   else
    return mix(base, detail, detail_blend);
#else
  return detail;
#endif
}


float getTerrainHeightDetail(vec2 pos_m)
{
  vec2 height_map_texture_coords = getHeightMapTextureCoords(terrain.detail_layer, pos_m);
  float detail = texture2D(terrain.detail_layer.height_map.sampler, height_map_texture_coords).x;
  return detail;
}


#if @enable_base_map@
float getTerrainHeightBase(vec2 pos_m)
{
  vec2 base_height_map_texture_coords = getHeightMapTextureCoords(terrain.base_layer, pos_m);
  float base = texture2D(terrain.base_layer.height_map.sampler, base_height_map_texture_coords).x;
  base += terrain_base_map_height;

  return base;
}
#endif


float getDetailMapBlend(vec2 pos)
{
#if ENABLE_TERRAIN_LAYER_SCALE
  vec2 map_size = terrain.detail_layer.size_m * terrain.detail_layer.scale;
#else
  vec2 map_size = terrain.detail_layer.size_m;
#endif
  vec2 min_pos = terrain.detail_layer.origin_m;
  vec2 max_pos = terrain.detail_layer.origin_m + map_size;

  float blend_dist = 8000.0;

#if ENABLE_DETAIL_MAP_CLIP
  vec2 clip_min = min_pos + detail_map_clip_min * map_size;
  vec2 clip_max = min_pos + detail_map_clip_max * map_size;

  if (any(lessThan(pos, clip_min)) || any(greaterThan(pos, clip_max)))
    return 0.0;
#endif


#if !ENABLE_TERRAIN_LAYER_BLEND || ENABLE_DETAIL_MAP_CLIP
  if (all(greaterThan(pos, min_pos)) && all(lessThan(pos, max_pos)))
    return 1.0;
  else
    return 0.0;
#endif

//   float threshold_noise = clamp(genericNoise(pos * 0.0008), -1, 1);
//   float threshold_noise_coarse = clamp(genericNoise(pos * 0.00005), -1, 1);

//   float threshold = 0.5;

//   threshold -= 0.5 * threshold_noise_coarse;
//   threshold += 0.1 * threshold_noise;

//   threshold = clamp(threshold, 0.1, 0.9);

  vec2 detail_blend =
    smoothstep(min_pos, min_pos + blend_dist, pos) -
    smoothstep(max_pos - blend_dist, max_pos, pos);

//   float detail_blend = detail_blend_x * detail_blend_y;

//   detail_blend = smoothstep(threshold, threshold + 0.4, detail_blend);

//   detail_blend *= smoothstep(0.5, 0.6, detail_blend);

  return detail_blend.x * detail_blend.y;
}

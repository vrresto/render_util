#version 130

uniform mat4 projectionMatrixFar;
uniform mat4 world2ViewMatrix;

void main()
{
  gl_Position = projectionMatrixFar * world2ViewMatrix  * vec4(gl_Vertex.x, gl_Vertex.y, 0, 1);
}

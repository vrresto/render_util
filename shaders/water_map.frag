/**
 *    Rendering utilities
 *    Copyright (C) 2020  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#version 130

#define ENABLE_WATER_MAP 1
#define WATER_MAP_Y_FLIP @water_map_y_flip@
#define DEBUG_SMALL_WATER_MAP @debug_small_water_map:0@

#include terrain_params.h.glsl


uniform int max_texture_array_elements;

uniform vec2 water_map_offset_m;
uniform float water_map_chunk_size_m;
uniform ivec2 water_map_table_size;
uniform float water_map_chunk_sample_offset;
uniform float water_map_chunk_sample_scale;
uniform int water_map_chunks_num_cols;
uniform int water_map_chunks_num_rows;

uniform Terrain terrain;

uniform sampler2D sampler_water_map_simple;
uniform sampler2D sampler_water_map_base;

uniform sampler2D sampler_water_map_table;
uniform sampler2D sampler_water_map_chunks;

#if DEBUG_SMALL_WATER_MAP
uniform sampler2D sampler_small_water_map;
#endif


float sampleWaterMapChunk(vec2 table_coords, ivec2 chunk_coords)
{
  chunk_coords = clamp(chunk_coords, ivec2(0), water_map_table_size - ivec2(1));

  int chunk_index = int(texelFetch(sampler_water_map_table, chunk_coords, 0).x);

  int chunk_index_x = chunk_index % water_map_chunks_num_cols;
  int chunk_index_y = chunk_index / water_map_chunks_num_cols;

  vec2 chunk_relative_coords = table_coords - vec2(chunk_coords);

  // clamp to edge / avoid bleeding over of adjacent chunks
  chunk_relative_coords = clamp(chunk_relative_coords, vec2(0), vec2(1));

  chunk_relative_coords *= water_map_chunk_sample_scale;
  chunk_relative_coords += water_map_chunk_sample_offset;

  vec2 coords_offset =
    vec2(chunk_index_x, chunk_index_y) /
    vec2(water_map_chunks_num_cols, water_map_chunks_num_rows);

  vec2 coords =
    coords_offset +
    (chunk_relative_coords / vec2(water_map_chunks_num_cols, water_map_chunks_num_rows));

  return texture(sampler_water_map_chunks, coords).x;
}


float sampleWaterMap(vec2 coords)
{
  float border = water_map_chunk_sample_offset;
//   border = water_map_chunk_sample_offset * 4;

  ivec2 chunk_coords = ivec2(floor(coords));
  vec2 chunk_relative_coords = fract(coords);

  ivec2 adjacent_chunk_offset_sign;
  adjacent_chunk_offset_sign.x = chunk_relative_coords.x < 0.5 ? -1 : 1;
  adjacent_chunk_offset_sign.y = chunk_relative_coords.y < 0.5 ? -1 : 1;

//   vec2 nearest_grid_offset = round(coords) - coords;
//   vec2 nearest_grid_distance = abs(nearest_grid_offset);
//   ivec2 adjacent_chunk_offset_sign = ivec2(nearest_grid_offset / nearest_grid_distance);
  ivec2 adjacent_chunk1_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(1,0);
  ivec2 adjacent_chunk2_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(0,1);
  ivec2 adjacent_chunk3_coords = chunk_coords + adjacent_chunk_offset_sign * ivec2(1,1);

  float value = sampleWaterMapChunk(coords, chunk_coords);
  float value1 = sampleWaterMapChunk(coords, adjacent_chunk1_coords);
  float value2 = sampleWaterMapChunk(coords, adjacent_chunk2_coords);
  float value3 = sampleWaterMapChunk(coords, adjacent_chunk3_coords);

  vec2 adjacent_blend = vec2(1) - (abs(round(coords) - coords) / border);
//   vec2 adjacent_blend = vec2(1) - (nearest_grid_distance / border);
  adjacent_blend = clamp(adjacent_blend, vec2(0), vec2(1));

  vec4 weights = vec4(0);
  weights[3] = 0.25 * adjacent_blend.x * adjacent_blend.y;
  weights[1] = 0.5 * adjacent_blend.x - weights[3];
  weights[2] = 0.5 * adjacent_blend.y - weights[3];
  weights[0] = 1.0 - (weights[1] + weights[2] + weights[3]);

  return value * weights[0] +
         value1 * weights[1] +
         value2 * weights[2] +
         value3 * weights[3];
}


#if DEBUG_SMALL_WATER_MAP
float sampleSmallWaterMap(vec2 pos)
{
  vec2 coords = pos / terrain.detail_layer.size_m;
  return texture(sampler_small_water_map, coords).x;
}
#endif


#if ENABLE_BASE_WATER_MAP
float getWaterDepthBase(vec2 pos)
{
  float depth = 1 - texture2D(sampler_water_map_base, (pos - height_map_base_origin) / height_map_base_size_m).x;

  float threshold_noise = clamp(genericNoise(pos * 0.0004), -1, 1);
  float threshold_noise_coarse = clamp(genericNoise(pos * 0.0001), -1, 1);

  float threshold = 0.5;

  threshold -= 0.5 * threshold_noise_coarse;
  threshold += 0.5 * threshold_noise;

  threshold = clamp(threshold, 0.1, 0.9);

  threshold = 0.8;

  depth = smoothstep(threshold, threshold + 0.2, depth);

  return depth;
}
#endif


#if @enable_base_map@


vec2 getPosInLayer(in TerrainLayer layer, vec2 pos_m)
{
  vec2 coords =
#if @enable_terrain_layer_scale@
    (pos_m - layer.origin_m) / (layer.scale * layer.size_m);
#else
    (pos_m - layer.origin_m) / layer.size_m;
#endif
  coords.y = 1.0 - coords.y;
  return coords;
}


float getWaterDepthBase(vec2 pos)
{
  vec2 posInLayer = getPosInLayer(terrain.base_layer, pos);

  float depth = 1 - texture2D(sampler_water_map_base, posInLayer).x;

  return depth;
}


float getWaterDepth(vec2 pos)
{
#if ENABLE_WATER_MAP
  vec2 layer_size_m = terrain.detail_layer.scale * terrain.detail_layer.size_m;

  vec2 posInLayer = getPosInLayer(terrain.detail_layer, pos + water_map_offset_m);

  vec2 waterMapTablePos = posInLayer * layer_size_m;

  vec2 waterMapTableCoords = waterMapTablePos / (water_map_chunk_size_m * terrain.detail_layer.scale);

  float depth = 1 - sampleWaterMap(waterMapTableCoords);

  return depth;
#else
  return texture2D(sampler_water_map_simple, pos.xy / terrain.detail_layer.size_m).x;
#endif
}


#else


float getWaterDepth(vec2 pos)
{
#if ENABLE_WATER_MAP
  vec2 waterMapTablePos = pos.xy + water_map_offset_m;

#if WATER_MAP_Y_FLIP
  waterMapTablePos.y = terrain.detail_layer.size_m.y - waterMapTablePos.y;
#endif

  vec2 waterMapTableCoords = waterMapTablePos / water_map_chunk_size_m;

  float depth = 1 - sampleWaterMap(waterMapTableCoords);

#if ENABLE_BASE_MAP
  {
    float detail_map_blend = getDetailMapBlend(pos);
    detail_map_blend = smoothstep(0.7, 1.0, detail_map_blend);

    #if ENABLE_BASE_WATER_MAP
      float base_depth = getWaterDepthBase(pos);
      depth = mix(base_depth, depth, detail_map_blend);
    #else
      depth *= detail_map_blend;
    #endif

  }
#endif

  return depth;
#else
  return texture2D(sampler_water_map_simple, pos.xy / terrain.detail_layer.size_m).x;
#endif
}


#endif

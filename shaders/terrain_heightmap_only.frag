/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#version 330

#include lighting_definitions.glsl
#include terrain_params.h.glsl
#include terrain_geometry_util.h.glsl

#define ENABLE_TERRAIN_LAYER_SCALE @enable_terrain_layer_scale@

layout(location = 0) out vec4 out_color0;

uniform vec3 cameraPosWorld;
uniform Terrain terrain;

varying float vertexHorizontalDist;
varying vec3 passObjectPosFlat;


bool isClipped(in vec2 pos, in TerrainLayer layer)
{
#if ENABLE_TERRAIN_LAYER_SCALE
  vec2 layer_size = layer.size_m * layer.scale;
#else
  vec2 layer_size = layer.size_m;
#endif

  if (any(lessThan(pos, layer.origin_m)) ||
      any(greaterThan(pos, layer.origin_m + layer_size)))

  {
    return true;
  }
  else
  {
    return false;
  }
}


void main(void)
{
#if @enable_base_map@
  if (isClipped(passObjectPosFlat.xy, terrain.base_layer) &&
      isClipped(passObjectPosFlat.xy, terrain.detail_layer))
    discard;
#else
  if (isClipped(passObjectPosFlat.xy, terrain.detail_layer))
    discard;
#endif

  float detail_blend = getDetailMapBlend(passObjectPosFlat.xy);

  vec3 normal = sampleTerrainNormalMap(terrain.detail_layer, passObjectPosFlat.xy);

#if @enable_base_map@
  vec3 base_normal = sampleTerrainNormalMap(terrain.base_layer, passObjectPosFlat.xy);
  normal = mix(base_normal, normal, detail_blend);
#endif

  vec3 light_direct;
  vec3 light_ambient;
  calcLight(passObjectPosFlat, normal, light_direct, light_ambient);

  out_color0 = vec4(0.5, 0.5, 0.5, 1.0);

  vec3 color_base = vec3(0.05, 0.3, 0.6) * 0.8;
  vec3 color0 = vec3(0.1, 0.5, 0.2) * 0.9;
  vec3 color1 = vec3(0.3, 0.5, 0);
  vec3 color2 = vec3(0.5, 0.5, 0.0);
  vec3 color3 = vec3(1,1,1);

  float height = getTerrainHeight(passObjectPosFlat.xy);

  out_color0.rgb = mix(color_base, color0, clamp(height / 2, 0, 1));
  out_color0.rgb = mix(out_color0.rgb, color1, clamp(height / 10, 0, 1));
  out_color0.rgb = mix(out_color0.rgb, color2, clamp(height / 300, 0, 1));
  out_color0.rgb = mix(out_color0.rgb, color3, clamp(height / 2000, 0, 1));


  float height_detail = getTerrainHeightDetail(passObjectPosFlat.xy);

  if (height_detail > 0.2)
  {
    out_color0.rgb = mix(out_color0.rgb, out_color0.rgb * vec3(2, 1, 1), detail_blend);
  }

  out_color0.rgb *= (light_direct * 1.5 + light_ambient * 0.5);

#if 0
#if @enable_base_map@
  float height_base = getTerrainHeightBase(passObjectPosFlat.xy);
#endif
  float height_detail = getTerrainHeightDetail(passObjectPosFlat.xy);

  out_color0.rgb = vec3(0.0);
  float height_grid = 150;

//   float height = getTerrainHeight(passObjectPosFlat.xy);

  float dist_to_height_line_detail = distance(height_detail / height_grid,
                                       round(height_detail / height_grid));

  if (passObjectPosFlat.x > 0 && passObjectPosFlat.x < terrain.detail_layer.size_m.x &&
      passObjectPosFlat.y > 0 && passObjectPosFlat.y < terrain.detail_layer.size_m.y)
  {
    int color = int(floor(height_detail / height_grid)) % 2;

//     if (dist_to_height_line_detail < 0.01)
//       out_color0.r = 1;
    out_color0.r = mix(out_color0.r, 1, color);
  }


  float dist_to_height_line_base = distance(height_base / height_grid,
                                       round(height_base / height_grid));

//   if (dist_to_height_line_base < 0.01)
//     out_color0.g = 1;

  int line_base = int(floor(height_base / height_grid)) % 2;
  out_color0.g = mix(out_color0.g, 1, line_base);
#endif
}

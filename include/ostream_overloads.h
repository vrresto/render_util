/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_H
#define RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_H


#include <glm/glm.hpp>
#include <ostream>


namespace util::ostream_overloads_private
{


template <typename T>
std::ostream& putVec(std::ostream& out, const T& vec)
{
  out << '[';

  for (int i = 0; i < vec.length(); i++)
  {
    if (i > 0)
      out << ", ";
    out << vec[i];
  }

  out << ']';

  return out;
}


} // namespace util::ostream_overloads_private


namespace util::ostream_overloads
{


#define RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(type) \
  inline std::ostream& operator<<(std::ostream& out, const glm::type &arg) \
  { \
    return ::util::ostream_overloads_private::putVec(out, arg); \
  }


RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(ivec2)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(ivec3)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(ivec4)

RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(vec2)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(vec3)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(vec4)

RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(dvec2)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(dvec3)
RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC(dvec4)

#undef RENDER_UTIL_UTIL_OSTREAM_OVERLOADS_ADD_VEC

}

#endif

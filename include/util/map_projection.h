/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_MAP_PROJECTION_H
#define UTIL_MAP_PROJECTION_H

#include <glm/glm.hpp>

#include <memory>
#include <string>
#include <vector>


namespace util
{


struct MapProjection
{
  virtual ~MapProjection() {}
  virtual glm::dvec2 project(glm::dvec2) = 0;
  virtual glm::dvec2 unproject(glm::dvec2) = 0;
  virtual double getMetersPerDegreeLongitude() = 0; // FIXME this doesn't really belong here
};


std::unique_ptr<MapProjection> createMapProjection(std::string name);
std::vector<std::string> getMapProjectionNames();


} // namespace util

#endif

#pragma once

#include <util/non_copyable.h>

#include <memory>

#ifndef _WIN32
#include <mutex>
#endif


namespace util
{


#ifdef _WIN32

class Mutex : public util::NonCopyable
{
  friend class MutexLock;
  struct Impl;

  void lock();
  void unlock();

  std::unique_ptr<Impl> m_impl;

public:
  Mutex();
  ~Mutex();
};


class MutexLock : public util::NonCopyable
{
  Mutex& m_mutex;

public:
  MutexLock(Mutex& m) : m_mutex(m)
  {
    m_mutex.lock();
  }

  ~MutexLock()
  {
    m_mutex.unlock();
  }
};

#else

using Mutex = std::mutex;
using MutexLock = std::lock_guard<Mutex>;

#endif


} // namespace util

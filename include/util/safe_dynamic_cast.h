#pragma once

#include <util/demangle.h>

#include <stdexcept>

namespace util
{
  template <class To, class From>
  To& safe_dynamic_cast(From& from)
  {
    auto casted = dynamic_cast<To*>(&from);
    if (!casted)
    {
      throw std::runtime_error(std::string("Invalid cast from ") +
                                demangle(typeid(from).name()) + " to " +
                                demangle(typeid(To).name()));
    }
    return *casted;
  }
}

/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_CHILD_PROCESS_UNIX_H
#define UTIL_CHILD_PROCESS_UNIX_H

#include <vector>
#include <string>
#include <list>


namespace util
{


class ChildProcessUnix
{
  std::vector<std::string> m_args;
  bool m_running = false;
  bool m_exited = false;
  int m_pid = 0;
  int m_exit_status = 1;
  bool m_signaled = false;
  int m_signal = 0;
  int m_pipefd[2] {};

public:
  ChildProcessUnix(const std::vector<std::string> &args);
  ~ChildProcessUnix();

  bool isFinished();
  int exitStatus();
  bool success();
  void updateStatus();
  void start();
  int readOutput(char *buffer, size_t buffer_size);
  void kill();
};


} // namespace util

#endif

/**
 *    Utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_CHILD_OUTPUT_TERMINAL_H
#define UTIL_CHILD_OUTPUT_TERMINAL_H

#include <list>
#include <string>


namespace util
{


class OutputTerminal
{
  std::list<std::string> m_lines;

public:
  OutputTerminal();

  void write(const char* chars, size_t num_chars);
  void newLine();
  void clear();

  const std::list<std::string>& getLines() const;
};


} // namespace util

#endif

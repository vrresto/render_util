#pragma once

#include <util/safe_dynamic_cast.h>


namespace util
{


struct Object
{
  virtual ~Object() {}

  template <class T>
  T& to()
  {
    return safe_dynamic_cast<T>(*this);
  }
};


} // namespace util

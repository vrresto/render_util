#pragma once

#include <string>

namespace util
{
  std::string demangle(const char*);
}

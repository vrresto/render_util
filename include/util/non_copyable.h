#pragma once

namespace util
{


struct NonCopyable
{
  NonCopyable(const NonCopyable&) = delete;
  NonCopyable& operator=(const NonCopyable&) = delete;
  NonCopyable() {}
};



} // namespace util

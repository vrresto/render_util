#pragma once

#include <utility>

namespace util
{


template<typename F, typename T, T... Ints>
void visit(std::integer_sequence<T, Ints...> int_seq, F f)
{
  (( f(Ints) ), ...);
}


template<class Functor, typename T, T... Ints>
void visitWithTemplate(std::integer_sequence<T, Ints...> int_seq, Functor f)
{
  (( f.template operator()<Ints>() ), ...);
}


} // namespace util

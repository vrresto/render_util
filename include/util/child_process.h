#ifndef UTIL_CHILD_PROCESS_H
#define UTIL_CHILD_PROCESS_H

#ifndef _WIN32
#include <util/child_process_unix.h>
namespace util
{
  using ChildProcess = ChildProcessUnix;
}
#else
#error Not implemented
#endif

#endif

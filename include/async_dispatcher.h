/**
 *    Rendering utilities
 *    Copyright (C) 2024 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <functional>
#include <future>
#include <cassert>


class AsyncDispatcher
{
public:
  using WorkFunction = std::function<void(int)>;

  AsyncDispatcher(WorkFunction f) : do_work(f) {}

  void dispatch(int num_items)
  {
    std::vector<std::future<void>> results;

    for (int i = 0; i < num_items; i++)
    {
      results.push_back(std::async(do_work, i));
    }

    for (auto& res : results)
    {
      res.wait();
    }
  }

private:
  WorkFunction do_work;
};

/*
  Based on plog/Appenders/ConsoleAppender.h
  Copyright (c) 2016 Sergey Podobry (sergey.podobry at gmail.com).
  Documentation and sources: https://github.com/SergiusTheBest/plog
  License: MPL 2.0, http://mozilla.org/MPL/2.0/
*/

#pragma once

#include <plog/Appenders/IAppender.h>
#include <plog/Util.h>
#include <util/mutex.h>

#include <iostream>

namespace util::log
{
    class SimpleConsoleAppenderBase : public plog::IAppender
    {
    public:
        SimpleConsoleAppenderBase();

    protected:
        util::Mutex m_mutex;
        const bool  m_isatty;
    };

    template<class Formatter>
    class SimpleConsoleAppender : public SimpleConsoleAppenderBase
    {
    public:
        virtual void write(const plog::Record& record)
        {
            plog::util::nstring str = Formatter::format(record);
            util::MutexLock lock(m_mutex);

            writestr(str);
        }

    protected:
        void writestr(const plog::util::nstring& str)
        {
            std::cout << str << std::flush;
        }
    };
}

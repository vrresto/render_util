/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef THREADED_DISPATCHER_H
#define THREADED_DISPATCHER_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <functional>
#include <cassert>

class ThreadedDispatcher
{
public:
  typedef std::function<void(int)> WorkFunction;

private:
  typedef std::unique_lock<std::mutex> Lock;


  const int m_num_threads = std::thread::hardware_concurrency();
  std::condition_variable start_cond;
  std::mutex start_cond_mutex;
  int num_ready_threads = 0;
  int m_progress = 0;
  WorkFunction do_work;


  void threadMain(int thread_id, int start, int end)
  {
    {
      Lock lock(start_cond_mutex);
      num_ready_threads++;
      start_cond.wait(lock);
      std::cout<<"thread "<<thread_id<<" start."<<std::endl;
    }

    for (int i = start; i < end; i++)
    {
      do_work(i);

      {
        Lock lock(start_cond_mutex);
        m_progress++;
      }
    }

    {
      Lock lock(start_cond_mutex);
      std::cout<<"thread "<<thread_id<<" end."<<std::endl;
    }
  }


public:
  ThreadedDispatcher(WorkFunction f) : do_work(f) {}


  void dispatch(int num_items)
  {
    int batch_size = num_items / m_num_threads;
    if (num_items % m_num_threads != 0)
      batch_size++;

    std::cout<<"batch_size: "<<batch_size<<std::endl;

    std::thread threads[m_num_threads];

    for (int i = 0; i < m_num_threads; i++)
    {
      std::cout<<"creating thread "<<i<<std::endl;

      int start_item = i * batch_size;
      int end_item = std::min(start_item + batch_size, num_items);

      assert(start_item < end_item);
      assert(start_item < num_items);
      assert(end_item <= num_items);

      threads[i] = std::thread(&ThreadedDispatcher::threadMain, this, i, start_item, end_item);
    }

    while (1)
    {
      Lock lock(start_cond_mutex);
      if (num_ready_threads == m_num_threads)
        break;
    }

    start_cond.notify_all();

    int progress = 0;
    float progress_percent = 0;

    std::cout.precision(2);
    std::cout << std::fixed;

    while (progress < num_items)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(300));

      {
        Lock lock(start_cond_mutex);
        progress = m_progress;
      }

      float progress_percent_new = progress * 100 / (float)(num_items);

      if (progress_percent_new != progress_percent)
      {
        progress_percent = progress_percent_new;
        std::cout<<"progress: "<<progress_percent<<" %"<<std::endl;
      }
    }

    for (int i = 0; i < m_num_threads; i++)
    {
      threads[i].join();
    }
  }

};

#endif

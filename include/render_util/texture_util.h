/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TEXTURE_UTIL_H
#define RENDER_UTIL_TEXTURE_UTIL_H

#include <render_util/image.h>
#include <render_util/image_util.h>
#include <render_util/render_util.h>
#include <render_util/elevation_map.h>
#include <render_util/texture.h>

#include <half.hpp>
#include <vector>
#include <type_traits>


namespace render_util::texture_format
{
  struct TextureFormatParametersBase
  {
    static int getFormat(unsigned int num_components);
  };

  template <typename T>
  struct TextureFormatParameters {};

  template <>
  struct TextureFormatParameters<unsigned char> : public TextureFormatParametersBase
  {
    static int getType();
    static int getInternalFormat(unsigned int num_components);
  };

  template <>
  struct TextureFormatParameters<float> : public TextureFormatParametersBase
  {
    static int getType();
    static int getInternalFormat(unsigned int num_components);
  };

  template <>
  struct TextureFormatParameters<half_float::half> : public TextureFormatParametersBase
  {
    static int getType();
    static int getInternalFormat(unsigned int num_components);
  };
}


namespace render_util
{
  bool isTextureSizeValid(int w, int h);


  inline bool isTextureSizeValid(glm::ivec2 size)
  {
    return isTextureSizeValid(size.x, size.y);
  }


  struct ImageResource
  {
    virtual glm::ivec2 getSize() const = 0;
    virtual unsigned int getNumComponents() const = 0;
    virtual std::unique_ptr<GenericImage> load(int scale_exponent, int num_components = 0) const = 0;
    virtual const std::string& getName() const = 0;
    virtual const std::string& getUniqueID() const = 0;

    int w() const { return getSize().x; }
    int h() const { return getSize().y; }
  };


  TexturePtr createTexture(const unsigned char *data, int w, int h, int bytes_per_pixel, bool mipmaps);
  TexturePtr createTextureExt(const unsigned char *data,
                              size_t data_size,
                              int w, int h,
                              int num_components,
                              size_t bytes_per_component,
                              int type,
                              int format,
                              int internal_format,
                              bool mipmaps);
  TexturePtr createFloatTexture1D(const float *data, size_t size, int num_components);
  TexturePtr createFloatTexture(const float *data, int w, int h, int num_components, bool mipmaps = false);
  TexturePtr createUnsignedIntTexture(const unsigned int *data, int w, int h);


  void setTextureImage(TexturePtr texture,
                      const unsigned char *data,
                      int w,
                      int h,
                      int bytes_per_pixel,
                      bool mipmaps);


  template <typename T>
  void setTextureImage(TexturePtr texture, T image, bool mipmaps = true)
  {
    using ImageType = typename image::TypeFromPtr<T>::Type;
    static_assert(std::is_same<typename ImageType::ComponentType, unsigned char>::value);

    setTextureImage(texture,
                    image->getData(),
                    image->w(),
                    image->h(),
                    ImageType::BYTES_PER_PIXEL,
                    mipmaps);
  }


  template <typename T>
  TexturePtr createTexture(T image, bool mipmaps = true)
  {
    using ImageType = typename image::TypeFromPtr<T>::Type;
    static_assert(std::is_same<typename ImageType::ComponentType, unsigned char>::value);

    return createTexture(image->data(),
                         image->w(),
                         image->h(),
                         image->numComponents(),
                         mipmaps);
  }


  template <typename T>
  TexturePtr createFloatTexture(T image, bool mipmaps)
  {
    static_assert(std::is_same<typename T::element_type::ComponentType, float>::value);
    return createFloatTexture(reinterpret_cast<const float*>(image->getData()),
                              image->w(),
                              image->h(),
                              T::element_type::NUM_COMPONENTS,
                              mipmaps);
  }


  template <typename T>
  TexturePtr createTextureExt(T image, bool mipmaps)
  {
    using namespace texture_format;

    using ImageType = typename image::TypeFromPtr<T>::Type;
    using ComponentType = typename ImageType::ComponentType;
    using Parameters = TextureFormatParameters<ComponentType>;

    auto type = Parameters::getType();
    auto format = Parameters::getFormat(ImageType::NUM_COMPONENTS);
    auto internal_format = Parameters::getInternalFormat(ImageType::NUM_COMPONENTS);
    auto num_components = ImageType::NUM_COMPONENTS;

    return createTextureExt(reinterpret_cast<const unsigned char*>(image->getData()),
                         image->getDataSize(),
                         image->w(),
                         image->h(),
                         num_components,
                         sizeof(ComponentType),
                         type,
                         format,
                         internal_format,
                         mipmaps);
  }


  TexturePtr createAmosphereThicknessTexture(std::string resource_path);
  TexturePtr createCurvatureTexture(std::string resource_path);

  ImageRGBA::Ptr createMapFarTexture(
      ImageGreyScale::ConstPtr type_map,
      const std::vector<ImageRGBA::ConstPtr> &textures,
//       ImageGreyScale::ConstPtr forest_map,
//       ImageRGBA::ConstPtr forest_texture,
      int type_map_meters_per_pixel,
      int meters_per_tile);


  ImageGreyScale::Ptr createTerrainLightMap(const ElevationMap&);


  void getTextureImage(unsigned int texture_id,
                       int format,
                       int  type,
                       size_t bytes_per_pixel,
                       std::vector<uint8_t> &data, glm::ivec2 &size);

  template <class T, int N>
  std::unique_ptr<Image<T,N>> getTextureImage(unsigned int texture_id)
  {
    std::vector<uint8_t> data;
    glm::ivec2 size(0);
    getTextureImage(texture_id,
                    texture_format::TextureFormatParameters<T>::getFormat(N),
                    texture_format::TextureFormatParameters<T>::getType(),
                    sizeof(T) * N,
                    data, size);

    return std::make_unique<Image<T,N>>(size, std::move(data));
  }
}

#endif

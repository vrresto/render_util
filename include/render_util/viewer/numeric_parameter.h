#pragma once

#include <render_util/viewer/parameter_base.h>


namespace render_util::viewer
{

struct NumericParameter : public Parameter
{
  enum Type
  {
    INT32,
    FLOAT,
    DOUBLE,
  };

  static unsigned constexpr HINT_PREFER_SLIDER = 1 << 0;
  static unsigned constexpr HINT_IS_COLOR = 1 << 1;

  NumericParameter(std::string name, unsigned hints = 0) :
    Parameter(name),
    m_hints(hints) {}

  bool isNumeric() override { return true; }
  NumericParameter *toNumeric() override  { return this; }
  unsigned hints() { return m_hints; }

  virtual Type componentType() = 0;
  virtual size_t componentSize() = 0;
  virtual int components() = 0;
  virtual void set(const void *buffer, size_t buffer_size) = 0;
  virtual void get(void *buffer, size_t buffer_size) = 0;
  virtual const void* minValue() = 0;
  virtual const void* maxValue() = 0;

private:
  unsigned m_hints = 0;
};


template <typename T>
NumericParameter::Type getType();

static_assert(sizeof(int) == 4);
template <>
inline NumericParameter::Type getType<int>() { return NumericParameter::INT32; }

template <>
inline NumericParameter::Type getType<float>() { return NumericParameter::FLOAT; }

template <>
inline NumericParameter::Type getType<double>() { return NumericParameter::DOUBLE; }


} // namespace render_util::viewer

#pragma once

namespace render_util::viewer
{


using CreateEditorBackendFunc =
  std::function<std::shared_ptr<EditorBackend>()>;


void runMapEditor(CreateEditorBackendFunc&,
                  std::string app_name);


} // namespace render_util::viewer

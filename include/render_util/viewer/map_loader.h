#pragma once


#include <render_util/image.h>

#include <memory>


namespace render_util
{
  class TerrainResourcesBase;
}


namespace render_util::viewer
{


class MapLoaderBase
{
public:
  virtual ~MapLoaderBase() {}

  virtual TerrainResourcesBase &getTerrainResources() = 0;
  virtual const std::shared_ptr<GenericImage> getCirrusTexture() const = 0;
//     virtual std::string getMapProjection() = 0;
//     virtual std::string getBaseMapPath() = 0;
//     virtual std::string getBaseWaterMapPath() = 0;
};


} // namespace render_util::viewer

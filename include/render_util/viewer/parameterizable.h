#pragma once

namespace render_util::viewer
{


struct ParametersBase;


struct IParameterizable
{
  virtual void addParameters(ParametersBase&) = 0;
};


} // namespace render_util::viewer

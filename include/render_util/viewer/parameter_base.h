#pragma once

#include <string>
#include <cassert>


namespace render_util::viewer
{


struct MultipleChoiceParameterBase;
struct NumericParameter;
struct ParameterGroup;
struct ITextureSelector;
struct IBooleanParameter;


struct Parameter
{
  struct Dimension
  {
    enum
    {
      X, Y, Z
    };
  };

  const std::string name; // FIXME make non-const protected

  Parameter(std::string name) : name(name) {}
  virtual ~Parameter() {}

  virtual void reset() = 0;
  virtual void decrease(float step_factor = 1, size_t dim = Dimension::X) = 0;
  virtual void increase(float step_factor = 1, size_t dim = Dimension::X) = 0;
  virtual std::string getValueString() = 0;
  virtual int dimensions() { return 1; }
  virtual float stepSize() { return 1.0; }
  virtual bool isNumeric() { return false; }
  virtual NumericParameter *toNumeric() { return nullptr; }
  virtual bool isMultipleChoice() { return false; }
  virtual MultipleChoiceParameterBase *toMultipleChoice() { return nullptr; }
  virtual bool isBoolean() { return false; }
  virtual IBooleanParameter *toBoolean() { return nullptr; }
  virtual bool isGroup() { return false; }
  virtual ParameterGroup* toGroup() { return nullptr; }
  virtual bool isTextureSelector() { return false; }
  virtual ITextureSelector* toTextureSelector() { return nullptr; }
  virtual bool supportsRename() { return false; }
  virtual void rename(std::string) { assert(0); }
  virtual std::string getName() { return name; }
};


struct ParameterGroup : public Parameter
{
  // TODO should be layout-agnostic
  // this requires the GUI to have a smart layout algorithm
  int columns = 1;

  ParameterGroup(std::string name) : Parameter(name) {}

  bool isGroup() { return true; }
  ParameterGroup* toGroup() { return this; }
  void reset() override {};
  void decrease(float step_factor = 1, size_t dim = Dimension::X) override {};
  void increase(float step_factor = 1, size_t dim = Dimension::X) override {};
  std::string getValueString() override { return "NO VALUE"; }

  Parameter& at(int index) { return get(index); }

  virtual int size() const = 0;
  virtual Parameter& get(int index) = 0;
  virtual bool isModifiable() = 0;
};


struct ModifiableParameterList : public ParameterGroup
{
  ModifiableParameterList(std::string name) : ParameterGroup(name) {}

  bool isModifiable() override { return true; }

  virtual void addNew() = 0;
  virtual void remove(int index) = 0;
  virtual void moveForward(int index) = 0;
  virtual void moveBackward(int index) = 0;
};


struct MultipleChoiceParameterBase : public Parameter
{
  MultipleChoiceParameterBase(std::string name) : Parameter(name) {}

  bool isMultipleChoice() { return true; }
  MultipleChoiceParameterBase *toMultipleChoice() { return this; }

  virtual int current() = 0;
  virtual size_t choices() = 0;
  virtual std::string choiceText(size_t choice) = 0;
  virtual void set(size_t choice) = 0;
};


template <typename T>
inline std::string getParameterValueString(T value)
{
  return std::to_string(value);
}


template <>
inline std::string getParameterValueString<std::string>(std::string value)
{
  return value;
}


template <>
inline std::string getParameterValueString<float>(float value)
{
  char buf[100];
  snprintf(buf, sizeof(buf), "%.2f", value);
  return buf;
}


template <>
inline std::string getParameterValueString<bool>(bool value)
{
  return value ? "on" : "off";
}


} // namespace render_util::viewer

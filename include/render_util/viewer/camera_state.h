
/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_CAMERA_STATE_H
#define RENDER_UTIL_VIEWER_CAMERA_STATE_H

namespace render_util::viewer
{


struct CameraState2D
{
  double x, y, z;
  double magnification;
};


struct CameraState3D
{
  float fov;
  double x, y, z;
  float pitch, yaw, roll;
  float move_speed;
};


struct CameraState
{
  enum { MODE_2D, MODE_3D };

  int mode;

  union
  {
    CameraState2D state_2d;
    CameraState3D state_3d;
  };
};


} // render_util::viewer


#endif

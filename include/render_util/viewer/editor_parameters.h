#pragma once

#include <render_util/geometry.h>

#include <string>
#include <memory>


namespace render_util::viewer
{


struct IGeometricPropertyState
{
  virtual ~IGeometricPropertyState() {}
};


struct IGeometricProperty
{
  enum Type { RECTANGLE };

  virtual ~IGeometricProperty() {}
  virtual Type getType() = 0;
  virtual const std::string& getName() = 0;
  virtual std::unique_ptr<IGeometricPropertyState> getState() = 0;
  virtual void restoreState(IGeometricPropertyState&) = 0;

};


struct IRectangularProperty : public IGeometricProperty
{
  Type getType() override { return RECTANGLE; }

  virtual RectD getRect() const = 0;
  virtual void setOrigin(glm::dvec2) = 0;
  virtual bool isAspectFixed() const = 0;
};


struct IRectangularPropertyFixedAspect : public IRectangularProperty
{
  bool isAspectFixed() const override { return true; }
  virtual void setExtentX(double) = 0;
  virtual void setExtentY(double) = 0;
};


class ITextureSelector
{
public:
  virtual int getNumTextures() = 0;
  virtual int getSelectedIndex() = 0;
  virtual void setSelectedIndex(int index) = 0;
  virtual unsigned getTexture(int index) = 0;
  virtual unsigned getSelectedTexture() = 0;
};


struct IBulkSelection
{
  virtual void selectAll() = 0;
  virtual void selectNone() = 0;
};


} // namespace render_util::viewer

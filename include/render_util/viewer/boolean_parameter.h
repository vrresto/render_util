#pragma once

#include <render_util/viewer/parameter_base.h>


namespace render_util::viewer
{


struct IBooleanParameter
{
  virtual bool get() = 0;
  virtual void set(bool value) = 0;
};


template <class Wrapper>
struct BooleanParameter : public Parameter, public IBooleanParameter
{
  const bool m_default_value = false;
//   bool m_value = false;
//   std::function<void(bool)> m_apply;
  Wrapper m_value;

  BooleanParameter(std::string name, Wrapper wrapper, bool default_value) :
    Parameter(name),
    m_default_value(default_value),
    m_value(wrapper)
  {
  }

  void reset() override
  {
    m_value.set(m_default_value);
  }

  void decrease(float step_factor = 1, size_t dim = Dimension::X) override
  {
//     m_value = !m_value;
//     m_apply(m_value);
  }

  void increase(float step_factor = 1, size_t dim = Dimension::X) override
  {
//     m_value = !m_value;
//     m_apply(m_value);
  }

  std::string getValueString() override
  {
    return getParameterValueString(m_value.get());
  }

  bool get() override
  {
    return m_value.get();
  }

  void set(bool value) override
  {
    m_value.set(value);
  }

  bool isBoolean() override { return true; }
  IBooleanParameter *toBoolean() override { return this; }
};


} // namespace render_util::viewer

#pragma once

#include <render_util/viewer/parameter_base.h>

#include <functional>


namespace render_util::viewer
{


template <typename T>
struct ValueAsString
{
  static std::string get(T value)
  {
    return getParameterValueString<T>(value);
  }
};


template <typename T, class ChoiceName = ValueAsString<T>>
class DEPRECATED_MultipleChoiceParameter : public MultipleChoiceParameterBase
{
  using Applier = std::function<void(T)>;

  const std::vector<T> m_possible_values;
  int m_current_index = 0;
  Applier m_apply;

public:
  DEPRECATED_MultipleChoiceParameter(std::string name,
                          Applier apply_,
                          const std::vector<T> &possible_values,
                          int current_index = 0) :
    MultipleChoiceParameterBase(name),
    m_possible_values(possible_values),
    m_current_index(current_index),
    m_apply(apply_)
  {
    apply();
  }

  int current() override { return m_current_index; }

  size_t choices() override { return m_possible_values.size(); }

  std::string choiceText(size_t choice) override
  {
    return ChoiceName::get(m_possible_values.at(choice));
  }

  void set(size_t choice) override
  {
    assert(choice < m_possible_values.size());
    m_current_index = choice;
    apply();
  }

  T getCurrentValue() { return m_possible_values.at(m_current_index); }

  void apply() { m_apply(getCurrentValue()); }

  void reset() override
  {
    m_current_index = 0;
    apply();
  }

  void increase(float step_factor, size_t) override
  {
    m_current_index = (m_current_index+1) % m_possible_values.size();
    apply();
  }

  void decrease(float step_factor, size_t) override
  {
    m_current_index = (m_current_index-1) % m_possible_values.size();
    apply();
  }

  std::string getValueString() override
  {
    return ChoiceName::get(getCurrentValue());
  }

  bool isMultipleChoice() override { return true; }
};


} // namespace render_util::viewer

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_SCENE_H
#define RENDER_UTIL_VIEWER_SCENE_H


#include <render_util/viewer/parameter.h>
#include <render_util/render_util.h>
#include <util.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <list>
#include <sstream>


namespace util
{
  class OutputTerminal;
}


namespace render_util
{
  class Stats;
}


namespace render_util::viewer
{


struct Job
{
  virtual ~Job() {}
  virtual std::string getVerboseName() { return getName(); }
  virtual void start() = 0;
  virtual std::string getName() = 0;
  virtual float getProgressPercent() = 0;
  virtual void step(util::OutputTerminal* terminal = nullptr) = 0;
  virtual bool isFinished() = 0;
  virtual bool wasSuccessful() = 0;
  virtual void cancel() = 0;
  virtual bool wasCanceled() = 0;
};


struct JobStarter
{
  virtual ParameterGroup& getParameters() = 0;
  virtual std::unique_ptr<Job> createJob() = 0;
  virtual bool isReady() = 0;
};


struct CelestialBody
{
  enum ID
  {
    SUN,
    MOON
  };

  float elevation = 90.0;
  float azimuth = 0.0;

  glm::vec3 getDirection()
  {
    glm::vec4 pitch_axis(1,0,0,0);

    glm::mat4 yaw = glm::rotate(glm::mat4(1), glm::radians(azimuth), glm::vec3(0.0, 0.0, 1.0));

    pitch_axis = yaw * pitch_axis;

    glm::mat4 pitch = glm::rotate(glm::mat4(1), glm::radians(elevation), glm::vec3(pitch_axis));

    auto dir = yaw * glm::vec4(0,1,0,0);
    dir = pitch * dir;

    return glm::vec3(dir);
  }

  static std::string getName(CelestialBody::ID id)
  {
    switch (id)
    {
      case SUN:
        return "sun";
      case MOON:
        return "moon";
      default:
        abort();
    }
  }
};


class UniformParameterBase
{
public:
  virtual ~UniformParameterBase() {}
  virtual void setUniform(ShaderProgram &program) = 0;
};


template <typename T>
class UniformParameter : public UniformParameterBase
{
public:
  const std::string m_name;
  T m_value;

  UniformParameter(std::string name, T value) : m_name(name), m_value(value) {}

  void setUniform(ShaderProgram &program) override
  {
    program.setUniform<T>(m_name, m_value);
  }
};


struct Action
{
  const std::string name;
  const std::string long_name;
  const bool starts_job = false;
  const std::function<void()> execute;
  const std::function<std::unique_ptr<JobStarter>()> createJobStarter;
};


class Scene
{
  std::array<CelestialBody, 2> m_celestial_bodies;
  std::vector<std::unique_ptr<UniformParameterBase>> m_uniforms;
  std::vector<Action> m_actions;

  void addCelestialBodyParameters(CelestialBody::ID id)
  {
    auto name = CelestialBody::getName(id);
    auto &body = m_celestial_bodies.at(id);
    {
      auto get = [&body] () { return body.elevation; };
      auto set = [&body] (float elevation) { body.elevation = elevation; };
      m_world_parameters.add<float>(name + " elevation", get, set, 0.1,
                              NumericParameter::HINT_PREFER_SLIDER,
                              -10, +90);
    }
    {
      auto get = [&body] () { return body.azimuth; };
      auto set = [&body] (float azimuth) { body.azimuth = azimuth; };
      m_world_parameters.add<float>(name + " azimuth", get, set, 0.1,
                              NumericParameter::HINT_PREFER_SLIDER,
                              0, 360);
    }
  }

protected:
  glm::vec3 m_initial_camera_pos {};
  std::vector<std::reference_wrapper<ParameterGroup>> m_parameter_groups;
  Parameters m_world_parameters;

  void addAction(std::string name, std::string long_name, std::function<void()> execute)
  {
    m_actions.push_back({ name, long_name, false, execute, {} });
  }

  void addJobAction(std::string name, std::string long_name,
                    std::function<std::unique_ptr<JobStarter>()> start_job)
  {
    m_actions.push_back({ name, long_name, true, {}, start_job });
  }

public:
  Scene() : m_world_parameters("World")
  {
  }

  const std::vector<Action>& getActions() { return m_actions; }

  void addParameters()
  {
    addCelestialBodyParameters(CelestialBody::SUN);
    addCelestialBodyParameters(CelestialBody::MOON);
  }

  virtual ~Scene() {}

#if 1
  template <typename T>
  void addUniform(std::string name, T initial_vlaue,
                  float step = 1.0, unsigned hints = 0,
                  T min_value = 0, T max_value = 1)
  {
    auto u = std::make_unique<UniformParameter<T>>(name, initial_vlaue);
    m_world_parameters.add(name, u->m_value, step, hints, min_value, max_value);
    m_uniforms.push_back(std::move(u));
  }
#endif

  template <typename T>
  void addVectorUniform(std::string name, T initial_vlaue,
                  float step = 1.0, unsigned hints = 0,
                  typename T::value_type min_value = 0, typename T::value_type max_value = 1)
  {
    auto u = std::make_unique<UniformParameter<T>>(name, initial_vlaue);
    m_world_parameters.addVector(name, u->m_value, step, hints, min_value, max_value);
    m_uniforms.push_back(std::move(u));
  }

  bool toggle_lod_morph = false;
  bool pause_animations = false;

  CelestialBody &getCelestialBody(CelestialBody::ID id)
  {
    return m_celestial_bodies.at(id);
  }

  const std::vector<std::reference_wrapper<ParameterGroup>>& getParameterGroups()
  {
    return m_parameter_groups;
  }

  glm::vec3 getSunDir()
  {
    return getCelestialBody(CelestialBody::SUN).getDirection();
  }

  virtual void updateUniforms(render_util::ShaderProgram& program,
                              const render_util::Camera &camera)
  {
    render_util::updateUniforms(program, camera);
    program.setUniform("sun_dir", getSunDir());

    for (auto &u : m_uniforms)
      u->setUniform(program);
  }

  glm::vec3 getInitialCameraPos() { return m_initial_camera_pos; }

  virtual void mark() {}
  virtual void unmark() {}
  virtual void cursorPos(const glm::dvec2&) {}
  virtual void rebuild() {}
  virtual void save() {}
  virtual bool hasUnsavedChanges() { return false; }
  virtual void getStats(Stats&) {}
  virtual void reloadMap() {}
  virtual bool isEditor() { return false; }
  virtual bool hasError() { return false; }
  virtual std::vector<std::string> getErrors() { return {}; }
  virtual bool needsReload() { return false; }

  virtual void setup() = 0;
  virtual void update(float frame_delta) = 0;
  virtual void render(const render_util::Camera &camera) = 0;
  virtual glm::vec2 getMapSizeM() = 0;

  virtual std::vector<ShaderCreationError> getShaderErrors()
  {
    return {};
  }
};


struct IEditor
{
  virtual const util::unique_ptr_vector<IGeometricProperty>& getGeometricProperties() = 0;
};


} // namespace render_util::viewer


#endif

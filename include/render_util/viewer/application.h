/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDE_UTIL_VIEWER_APPLICATION_H
#define RENDE_UTIL_VIEWER_APPLICATION_H

#include <render_util/viewer/configuration.h>


namespace argparse
{
  class ArgumentParser;
}


namespace render_util::viewer
{


class Application
{
  std::string m_name;
  Configuration m_config;

public:
  Application(std::string name) : m_name(name), m_config(m_name + ".ini") {}
  virtual ~Application() {}

  virtual void addArguments(argparse::ArgumentParser&) {}
  virtual void processArguments(argparse::ArgumentParser&) {}

  virtual void run() = 0;

  const std::string& getName() { return m_name; }
  Configuration& getConfig() { return  m_config; }
};


} // namespace render_util::viewer

#endif

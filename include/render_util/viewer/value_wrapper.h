#pragma once

#include <functional>

namespace render_util::viewer
{


template <typename T>
struct ValueWrapper
{
  using GetFunc = std::function<T()>;
  using SetFunc = std::function<void(T)>;

  const GetFunc get;
  const SetFunc set;
};


} // namespace render_util::viewer


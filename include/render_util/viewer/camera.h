/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_CAMERA_H
#define RENDER_UTIL_VIEWER_CAMERA_H

#include <render_util/camera_3d.h>

#include <glm/gtc/matrix_transform.hpp>


namespace render_util::viewer
{


struct CameraState;


struct Camera : public render_util::Camera3D
{
  render_util::Camera::Unit x = 0;
  render_util::Camera::Unit y = 0;
  render_util::Camera::Unit z = 0;
  render_util::Camera::Unit yaw = 0;
  render_util::Camera::Unit pitch = 0;
  render_util::Camera::Unit roll = 0;

  Camera()
  {
    updateTransform();
  }

  float getFovIncement()
  {
    auto fov = getFov();

    if (fov < 1)
      return 0.1;
    else if (fov < 5)
      return 1;
    else if (fov < 20)
      return 5;
    else
      return 10;
  }

  float getFovDecrement()
  {
    auto fov = getFov();

    if (fov <= 0.1)
      return 0;
    else if (fov <= 1)
      return 0.1;
    else if (fov <= 5)
      return 1;
    else if (fov <= 20)
      return 5;
    else
      return 10;
  }

  void setPos(const glm::vec3 &pos)
  {
    x = pos.x;
    y = pos.y;
    z = pos.z;
    updateTransform();
  }

  void increaseFov()
  {
    setFov(getFov() + getFovIncement());
  }

  void decreaseFov()
  {
    setFov(getFov() - getFovDecrement());
  }

  glm::dvec4 getDirection()
  {
    auto rot_yaw = glm::rotate(glm::dmat4(1), glm::radians(yaw), glm::dvec3(0,0,1));
    glm::dvec4 pitch_axis(0,1,0,0);
    pitch_axis = rot_yaw * pitch_axis;
    auto rot_pitch = glm::rotate(glm::dmat4(1), glm::radians(-pitch),
                                 glm::dvec3(pitch_axis.x, pitch_axis.y, pitch_axis.z));

    glm::dvec4 direction(1,0,0,0);
    direction = rot_pitch * rot_yaw * direction;

    return direction;
  }

  void moveForward(double dist)
  {
    glm::dvec4 pos(x,y,z,1);

    pos += getDirection() * dist;

    x = pos.x;
    y = pos.y;
    z = pos.z;

    updateTransform();
  }

  void updateTransform()
  {
    setTransform(x, y, z, yaw, pitch, roll);
  }

  void loadState(const CameraState&);
  void saveState(CameraState&);
};


} // namespace render_util::viewer


#endif

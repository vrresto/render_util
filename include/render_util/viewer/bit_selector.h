#pragma once

namespace render_util::viewer
{


template <typename BitMask, BitMask ALL_BITS>
class BitSelector : public viewer::ParametersBase, public viewer::IBulkSelection
{
public:
  using Param = viewer::BooleanParameter<viewer::ValueWrapper<bool>>;
  using Wrapper = viewer::ValueWrapper<BitMask>;

  BitSelector(const std::string& name, viewer::ValueWrapper<BitMask> wrapper) :
    viewer::ParametersBase(name),
    m_value(wrapper)
  {
  }

  void selectAll() override
  {
    m_value.set(ALL_BITS);
  }

  void selectNone() override
  {
    m_value.set(0);
  }

  void add(std::string name, BitMask mask)
  {
    viewer::ValueWrapper<bool> wrapper =
    {
      .get = [this, mask] ()
      {
        return m_value.get() & mask;
      },
      .set = [this, mask] (bool enable)
      {
        if (enable)
          m_value.set(m_value.get() | mask);
        else
          m_value.set(m_value.get() & ~mask);
      },
    };
    viewer::ParametersBase::add(std::make_unique<Param>(name, wrapper,
                                m_value.get() & mask));
  }

private:
  Wrapper m_value;
};


} // namespace render_util::viewer

#pragma once

#include <render_util/viewer/numeric_parameter.h>
#include <render_util/viewer/value_wrapper.h>


namespace render_util::viewer
{


template <typename ValueT>
class VecNParameter : public NumericParameter
{
  using Wrapper = ValueWrapper<ValueT>;
  using ComponentT = typename ValueT::value_type;
  const ValueT m_default_value = ValueT(0);
  const float m_step = 1;
  const ComponentT m_min_value = 0;
  const ComponentT m_max_value = 1;
  Wrapper m_value;

public:
  VecNParameter(std::string name, Wrapper wrapper, float step, unsigned hints = 0,
                ComponentT min_value = 0,
                ComponentT max_value = 1) :
    NumericParameter(name, hints),
    m_default_value(wrapper.get()), m_step(step), m_min_value(min_value), m_max_value(max_value), m_value(wrapper)
  {
  }

  void change(size_t dim, float step_factor)
  {
    assert(dim < m_value.get().length());
    auto new_value = m_value.get();
    new_value[dim] += step_factor * m_step;
    m_value.set(new_value);
  }

  int dimensions() override { return m_value.get().length(); }

  const void* minValue() override { return reinterpret_cast<const void*>(&m_min_value); }
  const void* maxValue() override { return reinterpret_cast<const void*>(&m_max_value); }

  float stepSize() override { return m_step; }

  size_t componentSize() override { return sizeof(ComponentT); }

  int components() override { return m_value.get().length(); }

  Type componentType() override
  {
    return getType<ComponentT>();
  }

  void set(const ValueT &value)
  {
    m_value.set(value);
  }

  ValueT get()
  {
    return m_value.get();
  }

  void set(const void *buffer, size_t buffer_size) override
  {
    assert(buffer_size == sizeof(ComponentT) * m_value.get().length());

    ValueT new_value;

    auto v = reinterpret_cast<const ComponentT*>(buffer);
    for (int i = 0; i < new_value.length(); i++)
      new_value[i] = v[i];

    m_value.set(new_value);
  }

  void get(void *buffer, size_t buffer_size) override
  {
    assert(buffer_size == sizeof(ComponentT) * m_value.get().length());

    auto v = reinterpret_cast<ComponentT*>(buffer);
    for (int i = 0; i < m_value.get().length(); i++)
      v[i] = m_value.get()[i];
  }

  void reset() override
  {
    m_value.set(m_default_value);
  }

  void decrease(float step_factor, size_t dim) override
  {
    assert(step_factor > 0);
    change(dim, -step_factor);
  }

  void increase(float step_factor, size_t dim) override
  {
    assert(step_factor > 0);
    change(dim, +step_factor);
  }

  std::string getValueString() override
  {
    std::string value;

    value += '[';
    for (int i = 0; i < m_value.get().length(); i++)
    {
      char buf[100];
      snprintf(buf, sizeof(buf), "%.2f", m_value.get()[i]);
      if (i > 0)
        value += ", ";
      value += buf;
    }
    value += ']';

    return value;
  }
};


} // namespace render_util::viewer

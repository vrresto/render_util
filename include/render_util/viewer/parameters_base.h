#pragma once

#include <render_util/viewer/parameter_base.h>

#include <vector>
#include <memory>


namespace render_util::viewer
{


using ParameterList = std::vector<std::unique_ptr<Parameter>>;


class ParametersBase : public ParameterGroup
{
protected:
  ParameterList m_parameters;

public:
  ParametersBase(std::string name) : ParameterGroup(name) {}

  int size() const override { return m_parameters.size(); }
  Parameter& get(int index) override { return *m_parameters.at(index); }
  bool isModifiable() override { return false; }

  bool empty() const { return m_parameters.empty(); }

  void add(std::unique_ptr<Parameter> &&p)
  {
    m_parameters.push_back(std::move(p));
  }
};


} // namespace render_util::viewer

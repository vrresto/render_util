#pragma once

#include <render_util/land_cover.h>


namespace render_util::viewer
{


struct EditorBackend
{
  struct Layer
  {
    virtual unsigned getResolutionM() = 0;
    virtual glm::ivec2 getSizePx() = 0;
    virtual ElevationMap::Ptr createElevationMap() = 0;
    virtual const render_util::MapGeography& getMapGeography() = 0;
  };

  virtual ~EditorBackend() {}

//     virtual int getMetersPerPixel() const = 0;
//     virtual glm::dvec2 getMapOrigin() const = 0;

//     virtual ElevationMap::Ptr createBaseElevationMap() const = 0;
//     virtual int getBaseElevationMapMetersPerPixel() const = 0;
//     virtual glm::dvec2 getBaseElevationMapOrigin() const = 0;

//     virtual double getBaseMapMetersPerDegreeLongitude() const = 0;
//     virtual double getDetailMapMetersPerDegreeLongitude() const = 0;

  virtual std::vector<AltitudinalZone> getAltitudinalZones() { return {}; }

  virtual bool hasBaseLayer() = 0;

  virtual Layer& getLayer(int number) = 0;

  virtual void saveMapAttributes(const glm::dvec2 &origin,
                                  double meters_per_degree_longitude,
                                  std::string map_projection,
                                  const std::vector<AltitudinalZone>&,
                                  const std::map<std::string, int>& land_cover_types) = 0;

  virtual void reloadBaseMap() = 0;
  virtual std::string getBaseMapPath() = 0;

  virtual std::string getMapProjection() = 0;

  virtual TerrainResourcesBase::WaterMap loadWaterMap() = 0;
  virtual ImageGreyScale::Ptr loadBaseWaterMap() = 0;
  virtual std::string getBaseWaterMapPath() = 0;

  virtual const std::vector<std::string>& getLandCoverTypeNames() = 0;
  virtual std::map<std::string, int> getLandCoverTypeMapping() = 0;
  virtual const std::vector<LandCoverType>& getLandCoverTypes() = 0;

  virtual MapLoaderBase* getMapLoader() = 0;
};


} // namespace render_util::viewer

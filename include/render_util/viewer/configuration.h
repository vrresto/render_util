/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_CONFIGURATION_H
#define RENDER_UTIL_VIEWER_CONFIGURATION_H

#include <map>
#include <string>
#include <sstream>


namespace render_util::viewer
{


class Configuration
{
  std::map<std::string, std::string> values;

  std::string getString(const std::string& key, const std::string& default_value) const
  {
    auto it = values.find(key);
    if (it != values.end())
      return it->second;
    else
      return default_value;
  }

public:
  Configuration();
  Configuration(std::string file_path);

  void save(std::string file_path);

  std::string get(const std::string& key, const char* default_value) const
  {
    return getString(key, default_value);
  }

  std::string get(const std::string& key, const std::string& default_value) const
  {
    return getString(key, default_value);
  }

  template <typename T>
  T get(const std::string& key, const T& default_value) const
  {
    static_assert(!std::is_same<T, std::string>::value);
    static_assert(!std::is_same<T, char*>::value);

    auto str = getString(key, "");
    if (str.empty())
    {
      return default_value;
    }

    T value;
    std::istringstream s(str);
    s >> value;

    return s.fail() ? default_value : value;
  }

  void set(const std::string& key, const std::string& value)
  {
    values[key] = value;
  }

  template <typename T>
  void set(const std::string& key, const T& value)
  {
    values[key] = std::to_string(value);
  }
};


} // namespace render_util::viewer

#endif

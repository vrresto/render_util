#pragma once

#include <render_util/viewer/parameters_base.h>
#include <render_util/viewer/value_wrapper.h>
#include <render_util/viewer/parameter_base.h>
#include <render_util/viewer/boolean_parameter.h>
#include <render_util/viewer/simple_parameter.h>
#include <render_util/viewer/vector_parameter.h>


namespace render_util::viewer
{


template <typename T>
class SimpleValueWrapper
{
  T &m_value;

public:
  SimpleValueWrapper(T &value) : m_value(value) {}
  T get() { return m_value; }
  void set(T value) { m_value = value; }
};


class Parameters : public ParametersBase
{
  int m_active_parameter_index = 0;

public:
  Parameters(std::string name) : ParametersBase(name) {}

  const ParameterList &getParameters() { return m_parameters; }
  void clear() { m_parameters.clear(); }

  int getActiveIndex() { return m_active_parameter_index; }

  void setActive(int index)
  {
    if (m_parameters.empty())
      return;

    if (index < 0)
      index += m_parameters.size();
    index = index % m_parameters.size();
    m_active_parameter_index = index;
  }

  Parameter &getActive() { return *m_parameters.at(m_active_parameter_index); }

  // template <typename T, class ChoiceName = ValueAsString<T>>
  // void addMultipleChoice(std::string name,
  //                        std::function<void(T)> apply,
  //                        const std::vector<T> &values,
  //                        int current_index = 0)
  // {
  //   auto p = std::make_unique<MultipleChoiceParameter<T, ChoiceName>>
  //            (name, apply, values, current_index);
  //   m_parameters.push_back(std::move(p));
  // }

  void addBool(std::string name,
               typename ValueWrapper<bool>::GetFunc get,
               typename ValueWrapper<bool>::SetFunc set)
  {
    using Wrapper = ValueWrapper<bool>;
    using ParameterType = BooleanParameter<Wrapper>;

    Wrapper wrapper =
    {
      .get = get,
      .set = set,
    };

    auto p = std::make_unique<ParameterType>(name, wrapper, get());
    m_parameters.push_back(std::move(p));
  }

  void addBool(std::string name, bool& value)
  {
    using Wrapper = SimpleValueWrapper<bool>;
    auto p = std::make_unique<BooleanParameter<Wrapper>>(name, value, value);
    m_parameters.push_back(std::move(p));
  }

  template <typename T>
  void add(std::string name, T &value, float step = 1.0, unsigned hints = 0,
           T min_value = 0, T max_value = 1)
  {
    using Wrapper = SimpleValueWrapper<T>;
    auto p = std::make_unique<SimpleParameter<T,Wrapper>>(name, step, Wrapper(value), hints,
                                                          min_value, max_value);
    m_parameters.push_back(std::move(p));
  }

  template <typename T, class Wrapper>
  void addWrapper(std::string name, Wrapper wrapper, float step = 1.0, unsigned hints = 0,
           T min_value = 0, T max_value = 1)
  {
    auto p = std::make_unique<SimpleParameter<T,Wrapper>>(name, step, wrapper, hints,
                                                          min_value, max_value);
    m_parameters.push_back(std::move(p));
  }

  template <typename T>
  void add(std::string name,
           typename ValueWrapper<T>::GetFunc get,
           typename ValueWrapper<T>::SetFunc set,
           float step = 1.0,
           unsigned hints = 0,
           T min_value = 0, T max_value = 1)
  {
    ValueWrapper<T> wrapper =
    {
      .get = get,
      .set = set,
    };
    auto p = std::make_unique<SimpleParameter<T,ValueWrapper<T>>>(name, step, wrapper, hints,
                                                                  min_value, max_value);
    m_parameters.push_back(std::move(p));
  }

  template <typename T>
  void addVector(std::string name, T &value, float step, unsigned hints = 0,
                 typename T::value_type min = 0, typename T::value_type max = 1)
  {
    ValueWrapper<T> wrapper =
    {
      .get = [&value] () { return value; },
      .set = [&value] (const T& new_value) { value = new_value; }
    };
    auto p = std::make_unique<VecNParameter<T>>(name, wrapper, step, hints, min, max);
    m_parameters.push_back(std::move(p));
  }

  template <typename T>
  void addVector(std::string name,
                 typename ValueWrapper<T>::GetFunc get,
                 typename ValueWrapper<T>::SetFunc set,
                 float step,
                 unsigned hints = 0,
                 typename T::value_type min = 0, typename T::value_type max = 1)
  {
    ValueWrapper<T> wrapper =
    {
      .get = get,
      .set = set
    };
    auto p = std::make_unique<VecNParameter<T>>(name, wrapper, step, hints, min, max);
    m_parameters.push_back(std::move(p));
  }
};


} // namespace render_util::viewer

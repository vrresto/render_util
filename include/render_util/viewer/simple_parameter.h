#pragma once

#include <render_util/viewer/numeric_parameter.h>


namespace render_util::viewer
{


template <typename T, class W>
class SimpleParameter : public NumericParameter
{
  using Wrapper = W;

  const T min_value = 0;
  const T max_value = 1;
  const T default_value {};
  const T step {};

protected:
  Wrapper m_value;

public:
  SimpleParameter(std::string name, float step, Wrapper wrapper,
                  unsigned hints = 0,
                  T min_value = 0,
                  T max_value = 1)
      : NumericParameter(name, hints),
    min_value(min_value), max_value(max_value),
    default_value(wrapper.get()), step(step), m_value(wrapper)
  {
  }

  const void* minValue() override { return reinterpret_cast<const void*>(&min_value); }

  const void* maxValue() override { return reinterpret_cast<const void*>(&max_value); }

  float stepSize() override { return step; }

  Type componentType() override { return getType<T>(); }

  size_t componentSize() override { return sizeof(T); }

  int components() override { return 1; }

  void set(const void *buffer, size_t buffer_size) override
  {
    assert(buffer_size == sizeof(T));
    auto v = reinterpret_cast<const T*>(buffer);
    m_value.set(*v);
  }

  void get(void *buffer, size_t buffer_size) override
  {
    assert(buffer_size == sizeof(T));
    auto v = reinterpret_cast<T*>(buffer);
    *v = m_value.get();
  }

  void reset() override
  {
    m_value.set(default_value);
  }

  void increase(float step_factor, size_t) override
  {
    assert(step_factor > 0);
    m_value.set(m_value.get() + step_factor * step);
  }

  void decrease(float step_factor, size_t) override
  {
    assert(step_factor > 0);
    m_value.set(m_value.get() - step_factor * step);
  }

  std::string getValueString() override
  {
    return getParameterValueString(m_value.get());
  }
};


} // namespace render_util::viewer

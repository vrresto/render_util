#pragma once

#include <render_util/viewer/parameter_base.h>

#include <vector>
#include <functional>
#include <stdexcept>


namespace render_util::viewer
{


template <typename T>
class MultipleChoiceParameter : public MultipleChoiceParameterBase
{
public:
  struct Choice
  {
    std::string name;
    T value;
  };

  using Choices = std::vector<Choice>;
  using Set = std::function<void(T)>;
  using Get = std::function<T()>;

private:
  Get m_get;
  Set m_set;

  Choices m_choices;

  size_t indexFromChoice(T value)
  {
    for (size_t i = 0; i < m_choices.size(); i++)
    {
      if (m_choices[i].value == value)
        return i;
    }
    throw std::runtime_error("Invalid index");
  }

public:
  MultipleChoiceParameter(std::string name, Get get, Set set, Choices choices) :
    MultipleChoiceParameterBase(name),
    m_get(get),
    m_set(set),
    m_choices(choices)
  {
  }

  int current() override
  {
    return indexFromChoice(m_get());
  }

  size_t choices() override { return m_choices.size(); }

  std::string choiceText(size_t choice) override
  {
    return m_choices.at(choice).name;
  }

  void set(size_t index) override
  {
    m_set(m_choices.at(index).value);
  }

  void reset() override
  {
  }

  void increase(float step_factor, size_t) override
  {
    set((current()+1) % m_choices.size());
  }

  void decrease(float step_factor, size_t) override
  {
    set((current()-1) % m_choices.size());
  }

  std::string getValueString() override
  {
    return m_choices.at(current()).name;
  }

  bool isMultipleChoice() override { return true; }
};


} // namespace render_util::viewer

/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_CONTEXT_H
#define RENDER_UTIL_CONTEXT_H

#include <memory>
#include <cassert>


namespace render_util
{


class StateStack;


class Context
{
  std::unique_ptr<StateStack> m_state_stack;

  static thread_local Context *s_current;

public:
  Context();
  ~Context();

  StateStack& getStateStack() { return *m_state_stack; }

  static Context &getCurrent()
  {
    assert(s_current);
    return *s_current;
  }

  static void setCurrent(Context *c) { s_current = c; }
};


} // namespace render_util

#endif

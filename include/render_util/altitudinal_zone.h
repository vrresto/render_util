#pragma once

#include <glm/glm.hpp>
#include <string>

namespace render_util
{


struct AltitudinalZone
{
  std::string name;
  float transition_start_height = 0;
  float transition_end_height = 0;
  glm::vec3 color {};
  int type_index = 0;
};


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_STATS_H
#define RENDER_UTIL_STATS_H

#include <util/non_copyable.h>

#include <ostream_overloads.h>

#include <sstream>


namespace render_util
{


struct Stats : public util::NonCopyable
{
  struct Entry
  {
    std::string name;
    std::string value;
  };

  std::string name;
  std::vector<Entry> entries;
  std::vector<Stats> children;

  Stats() = default;

  Stats(Stats&& other) :
    name(std::move(other.name)),
    entries(std::move(other.entries)),
    children(std::move(other.children))
  {
  }

  void add(const std::string& name, const std::string& value)
  {
    entries.push_back({ name, value});
  }

  template <typename T>
  void add(const std::string& name, const T& value)
  {
    using namespace util::ostream_overloads;

    std::ostringstream stream;
    stream << value;
    add(name, stream.str());
  }

  Stats& addChild(const std::string& name)
  {
    children.emplace_back();
    children.back().name = name;
    return children.back();
  };
};


} // namespace render_util

#endif

/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TEXTURE_2D_H
#define RENDER_UTIL_TEXTURE_2D_H

#include <render_util/texture_util.h>
#include <render_util/image.h>
#include <log.h>


namespace render_util
{


// TODO create common Texture2D TextureArray base class


class Texture2DBase : public Texture
{
  glm::ivec2 m_texture_size {};
  int m_format {};
  int m_internal_format {};
  int m_type {};
  int m_lods {};

protected:
  void setTextureImage(int lod, glm::ivec2 size, const void* data);

public:
  Texture2DBase(int num_lods, glm::ivec2 texture_size, int format,
                int internal_format, int type);

  glm::ivec2 getTextureSize() { return m_texture_size; }

  void generateMipMap();
};


template <typename ComponentType = uint8_t>
class BasicTexture2D : public Texture2DBase
{
  int m_num_components {};

  size_t getTextureDataSize(glm::ivec2 size)
  {
    return size.x * size.y *
          m_num_components * sizeof(ComponentType);
  }

public:
  using FormatParams = typename texture_format::TextureFormatParameters<ComponentType>;

  BasicTexture2D(int num_lods, glm::ivec2 texture_size, int num_components) :
    Texture2DBase(num_lods, texture_size,
                    FormatParams::getFormat(num_components),
                    FormatParams::getInternalFormat(num_components),
                    FormatParams::getType()),
    m_num_components(num_components) {}

  template <class ImageType>
  BasicTexture2D(const ImageType& image, int num_lods = 0, bool generate_mipmap = true) :
    Texture2DBase(num_lods, image.getSize(),
                    FormatParams::getFormat(image.numComponents()),
                    FormatParams::getInternalFormat(image.numComponents()),
                    FormatParams::getType()),
    m_num_components(image.numComponents())
  {
    setTextureImage(0, image);
    if (generate_mipmap)
      generateMipMap();
  }

  template <class ImageType>
  void setTextureImage(int lod, const ImageType& image)
  {
    static_assert(std::is_same<ComponentType, typename ImageType::ComponentType>::value);
//     assert(image.size() == getTextureSize());
    assert(image.numComponents() == m_num_components);
    assert(getTextureDataSize(image.size()) == image.getDataSize());
    Texture2DBase::setTextureImage(lod, image.size(), image.getData());
  }
};


using Texture2D = BasicTexture2D<uint8_t>;


} // namespace render_util

#endif

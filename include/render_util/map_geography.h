/**
 *    Rendering utilities
 *    Copyright (C) 2022  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_MAP_GEOMETRY_H
#define RENDER_UTIL_MAP_GEOMETRY_H

#include <util/map_projection.h>
#include <log.h>

#include <glm/glm.hpp>
#include <memory>


namespace render_util
{


class MapGeography
{
  glm::dvec2 m_origin_geodesic {};
  double m_meters_per_degree_longitude = 0;
  std::string m_map_projection_id;
  std::unique_ptr<util::MapProjection> m_map_projection;

public:
  MapGeography(const MapGeography& other);
  MapGeography(glm::dvec2 origin_geodesic, double meters_per_degree_longitude,
               std::string map_projection);

  glm::dvec2 getOriginGeodesic() const;
  double getMetersPerDegreeLongitude() const;
  const std::string& getMapProjection() const;
  glm::dvec2 getOriginWorld(const MapGeography& world) const;
  double getScaleWorld(const MapGeography& world) const;
  glm::dvec2 localToWorld(glm::dvec2 local, const MapGeography& world) const;
  glm::dvec2 geodesicToLocal(glm::dvec2 geodesic) const;
  glm::dvec2 localToGeodesic(glm::dvec2 local) const;

  void set(const MapGeography& other);
  void set(glm::dvec2 origin_geodesic, double meters_per_degree_longitude,
           std::string map_projection);
  void setOriginGeodesic(const glm::dvec2& origin);
  void setMetersPerDegreeLongitude(double);
  void setScaleWorld(double scale_world, const MapGeography& world);
};


} // namespace render_util


#endif

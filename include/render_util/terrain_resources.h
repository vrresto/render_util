#pragma once


#include <render_util/elevation_map.h>
#include <render_util/altitudinal_zone.h>
#include <util/object.h>


namespace render_util
{


class ImageResource;


struct TerrainResourcesBase : public util::Object
{
  using ImageResourceList = std::vector<std::shared_ptr<ImageResource>>;
  using MaterialMap = Image<unsigned int>;

  struct Layer : public util::Object
  {
    virtual bool isBaseLayer() { return false; }
    virtual glm::ivec2 getSizePx() = 0;
    virtual glm::vec3 getOriginM() = 0;
    virtual float getScale() = 0;
    virtual unsigned int getResolutionM() = 0;
    virtual glm::vec2 getMapTextureOffset() = 0;
    virtual ElevationMap::ConstPtr getHeightMap() = 0;
    virtual MaterialMap::ConstPtr getMaterialMap() = 0;
  };

  struct BaseLayer : public Layer
  {
    bool isBaseLayer() override { return true; }
  };

  Layer& getDetailLayer();
  Layer& getBaseLayer();

  virtual const std::vector<std::string>& getBaseLayerTypes()
  {
    static std::vector<std::string> types = { "default" };
    return types;
  }

  virtual const std::vector<int>& getBaseLayerTypeMapping()
  {
    static std::vector<int> mapping = { 0 };
    return mapping;
  }

  virtual int getSupportedModules() { return 0; }
  virtual std::vector<AltitudinalZone> getAltitudinalZones() { return {}; }

  virtual bool hasBaseLayer() = 0;
  virtual Layer& getLayer(int number) = 0;

  virtual ImageGreyScale::Ptr getNoiseTexture() = 0;
  virtual std::shared_ptr<GenericImage> getFarNoiseTexture() = 0;
  virtual bool farNoiseTextureIsNormalMap() = 0;
};


struct TerrainLandResources
{
  using TypeMap = ImageGreyScale;

  struct Layer
  {
    virtual TypeMap::ConstPtr getTypeMap() = 0;
  };

  virtual bool hasTypeMap() { return true; }

  virtual const TerrainResourcesBase::ImageResourceList& getLandTextures() = 0;
  virtual const TerrainResourcesBase::ImageResourceList& getLandTexturesNM() = 0;
  virtual const std::vector<float>& getLandTexturesScale() = 0;
};


struct TerrainForestResources
{
  struct Layer
  {
    virtual ImageGreyScale::ConstPtr getForestMap() = 0;
  };

  virtual std::vector<ImageRGBA::Ptr> getForestLayers() = 0;
};


struct TerrainWaterResources
{
  struct WaterMap
  {
    std::vector<ImageGreyScale::ConstPtr> chunk_blocks;
    Image<unsigned int>::ConstPtr table;
    int chunks_per_row = 0;
    int chunk_size_px = 0;
    float chunk_size_m = 0;
    float chunk_border_px = 0;
    bool y_flip = false;
  };

  struct Layer
  {
    virtual const WaterMap &getWaterMap() = 0;
    virtual ImageGreyScale::ConstPtr getSmallWaterMap() = 0;
  };

  virtual glm::vec3 getWaterColor() = 0;
  virtual const TerrainResourcesBase::ImageResourceList& getWaterAnimationHeightMaps() = 0;
  virtual const TerrainResourcesBase::ImageResourceList& getWaterAnimationNormalMaps() = 0;
  virtual const TerrainResourcesBase::ImageResourceList& getWaterAnimationFoamMasks() = 0;
  virtual ImageGreyScale::Ptr getWaterFoamMask() = 0;
  virtual std::shared_ptr<GenericImage> getWaterFoamTexture() = 0;
  virtual std::shared_ptr<GenericImage> getWaterTexture() = 0;
  virtual std::shared_ptr<GenericImage> getWaterNoiseTexture() = 0;
};



} // namespace render_util

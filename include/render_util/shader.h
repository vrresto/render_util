/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_SHADER_H
#define RENDER_UTIL_SHADER_H

#include <util.h>

#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstdio>
#include <cassert>
#include <memory>
#include <variant>
#include <glm/glm.hpp>


namespace render_util
{
  using ShaderSearchPath = std::vector<std::string>;


  class ShaderCreationError : public std::exception
  {
    std::string m_program_name;
    std::string m_error;
    std::string m_log;
    std::string m_message;

  public:
    ShaderCreationError(std::string program_name, std::string error_msg,
                        std::string log = {})
    {
      m_program_name = program_name;
      m_log = log;
      m_error = error_msg;
      m_message = "Failed to create shader program " + program_name + ": " + error_msg;
    }
    const char* what() const noexcept override { return m_message.c_str(); }
    const std::string& getProgramName() { return m_program_name; }
    const std::string& getLog() { return m_log; }
    const std::string& getError() { return m_error; }
  };


  class ShaderParameters
  {
    using ShaderParameter = std::variant<size_t, int, bool, float, std::string>;

    std::unordered_map<std::string, ShaderParameter> m_map;

  public:
    void add(const ShaderParameters &other);

    bool contains(const std::string &name)
    {
      return m_map.find(name) != m_map.end();
    }

    template <typename T>
    T get(const std::string& name) const
    {
      return std::get<T>(m_map.at(name));
    }

    template <typename T>
    void set(const std::string &name, const T &value)
    {
      m_map[name] = value;
    }

    void setIfNotDisabled(std::string name, bool value)
    {
      if (contains(name))
      {
        value = get<bool>(name) && value;
      }

      set<bool>(name, value);
    }

    std::string toString(const std::string& name) const
    {
      return util::toString(m_map.at(name));
    }

    void dump(std::ostream&) const;
  };


  class Shader
  {
    unsigned int m_id = 0;
    unsigned int m_type = 0;
    std::string m_preprocessed_source;
    std::string m_name;
    std::string m_filename;
    std::vector<std::string> m_includes;

  public:
    Shader(const std::string &name,
                     const std::vector<std::string> &paths,
                     unsigned int type,
                     const ShaderParameters &params);

    ~Shader();

    void preProcess(const std::vector<char> &in, const ShaderParameters &params,
                        const std::vector<std::string> &paths);
    void compile();
    unsigned int getID() { return m_id; }
    unsigned int getType() { return m_type; }
    const std::string &getName() { return m_name; }
    const std::string &getFileName() { return m_filename; }
  };


  class ShaderProgram
  {
  public:
    unsigned long long frame_nr = 0; // FIXME HACK
    bool is_far_camera = false; // FIXME HACK

    ShaderProgram() {}
    ShaderProgram(const std::string &name,
                  const std::vector<std::string> &vertex_shaders,
                  const std::vector<std::string> &fragment_shaders,
                  const std::vector<std::string> &geometry_shaders,
                  const std::vector<std::string> &compute_shaders,
                  const std::vector<std::string> &paths,
                  bool must_be_valid = true,
                  const std::map<unsigned int, std::string> &attribute_locations = {},
                  const ShaderParameters &parameters = {});

    ~ShaderProgram();

    unsigned int getId();
    unsigned int getID() { return getId(); }

    bool hasUnsetUniforms();
    void assertUniformsAreSet();
    void logErrorsOnce();
    void logErrors();

    bool isValid() { return is_valid; }
    bool isUsable() { return is_valid && !hasUnsetUniforms(); }

    template <typename T>
    void setUniform(int location, const T &value);

    template <typename T>
    void setUniform(const std::string &name, const T &value)
    {
      auto uniform = getUniform(name);
      if (uniform && uniform->location != -1)
      {
        setUniform<T>(uniform->location, value);
        if (!uniform->is_set)
          set_uniforms_count++;
        assert(set_uniforms_count <= uniforms.size());
        uniform->is_set = true;
      }
      else if (error_fail)
      {
        printf("uniform not found: %s - program: %s\n", name.c_str(), this->name.c_str());
        exit(1);
      }
    }

    void setUniformi(const std::string &name, int value)
    {
      return setUniform<int>(name, value);
    }

  private:
    struct Uniform
    {
      std::string name;
      int location = -1;
      bool is_set = false;
    };

    const ShaderParameters m_parameters = {};

    unsigned int id = 0;
    std::string name;
    std::vector<std::string> paths;

    std::vector<std::string> vertex_shaders;
    std::vector<std::string> fragment_shaders;
    std::vector<std::string> geometry_shaders;
    std::vector<std::string> compute_shaders;

    std::vector<std::unique_ptr<Shader>> shaders;

    const std::map<unsigned int, std::string> attribute_locations;
    std::unordered_map<std::string, Uniform> uniforms;
    size_t set_uniforms_count = 0;
    bool is_valid = false;
    bool were_errors_logged = false;
    bool must_be_valid = true;
    bool error_fail = false;

    bool link();
    void create();
    void assertIsValid();
    void getAllUniforms();
    void logUnsetUniforms();
    Uniform* getUniform(const std::string &name);
  };

  typedef std::shared_ptr<ShaderProgram> ShaderProgramPtr;
}

#endif

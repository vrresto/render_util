/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_CAMERA_H
#define RENDER_UTIL_CAMERA_H

#include <render_util/geometry.h>

#include <memory>

namespace render_util
{
  struct Camera
  {
    using Mat4 = glm::dmat4;
    using Vec2 = glm::dvec2;
    using Vec3 = glm::dvec3;
    using Vec4 = glm::dvec4;
    using Unit = double;

    virtual ~Camera() {}

    virtual bool is2D() const { return false; }

    virtual const Mat4 &getWorldToViewRotationD() const = 0;
    virtual const Mat4 &getView2WorldMatrixD() const = 0;
    virtual const Mat4 &getWorld2ViewMatrixD() const = 0;
    virtual const Mat4 &getProjectionMatrixFarD() const = 0;
    virtual const Vec3 &getPosD() const = 0;
    virtual const glm::ivec2 &getViewportSize() const = 0;
    virtual bool cull(const Box &box) const = 0;
    virtual void setViewportSize(int width, int height) = 0;
//     Unit getFov() const;
//     Unit getZNear() const;
//     Unit getZFar() const;


    glm::mat4 getWorldToViewRotation() const
    {
      return glm::mat4(getWorldToViewRotationD());
    }

    Mat4 getVP() const
    {
      return getProjectionMatrixFarD() * getWorld2ViewMatrixD();
    }

    glm::mat4 getVP_s() const
    {
      return glm::mat4(getVP());
    }

    const glm::mat4 getView2WorldMatrix() const
    {
      return glm::mat4(getView2WorldMatrixD());
    }

    const glm::mat4 getWorld2ViewMatrix() const
    {
      return glm::mat4(getWorld2ViewMatrixD());
    }

    const glm::mat4 getProjectionMatrixFar() const
    {
      return glm::mat4(getProjectionMatrixFarD());
    }

    const glm::vec3 getPos() const
    {
      return glm::vec3(getPosD());
    }
  };

}

#endif

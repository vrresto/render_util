/**
 *    Rendering utilities
 *    Copyright (C) 2021  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_FRAMEBUFFER_H
#define RENDER_UTIL_FRAMEBUFFER_H

#include <render_util/texture.h>

#include <vector>
#include <glm/glm.hpp>


namespace render_util
{


class FrameBufferBase
{
protected:
  unsigned int m_id = 0;
  glm::ivec2 m_size = glm::ivec2(0);
  TexturePtr m_depth_texture;

  void create();
  void updateDepthTextureSize();

public:
  FrameBufferBase(glm::ivec2 size, size_t num_draw_buffers);
  ~FrameBufferBase();

  glm::ivec2 getSize() { return m_size; }
  unsigned int getID() { return m_id; }
};


class FrameBuffer : public FrameBufferBase
{
  // unsigned int m_id = 0;
  // glm::ivec2 m_size = glm::ivec2(0);
  std::vector<TexturePtr> m_color_textures;
  // TexturePtr m_depth_texture;

  // void create();

  void updateColorTexturesSize();

public:
  FrameBuffer(glm::ivec2 size, size_t num_draw_buffers);
  void setSize(glm::ivec2 size);
  const TexturePtr &getTexture(size_t i) { return m_color_textures.at(i); }
};


class TemporaryFrameBufferBinding
{
  int m_previous_binding = 0;
  bool m_set_viewport = false;
  int m_prev_viewport[4] {};

public:
  TemporaryFrameBufferBinding(FrameBufferBase&, bool set_viewport);
  ~TemporaryFrameBufferBinding();
};


}

#endif

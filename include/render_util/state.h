/**
 *    Rendering utilities
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <util/non_copyable.h>
#include <render_util/gl_binding/gl_functions.h>

#include <stack>
#include <array>
#include <bitset>
#include <memory>
#include <cstdlib>

#ifndef RENDER_UTIL_STATE_H
#define RENDER_UTIL_STATE_H


namespace render_util
{


struct State
{
  struct CapIndex
  {
    enum Enum : size_t
    {
      CULL_FACE,
      BLEND,
      DEPTH_TEST,
      STENCIL_TEST,
      ALPHA_TEST,
      POLYGON_OFFSET_FILL,
      POLYGON_OFFSET_LINE,
      ENUM_SIZE,
    };
  };

  std::bitset<CapIndex::ENUM_SIZE> caps;
  unsigned program = 0;
  GLenum front_face = GL_CW;
  GLenum cull_face = GL_BACK;
  GLenum depth_func = GL_LEQUAL;
  bool depth_mask = true;
  std::tuple<GLenum, GLenum> blend_func { GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA };
  std::tuple<GLenum, GLclampf> alpha_func { GL_ALWAYS, 0 };
  std::tuple<GLfloat, GLfloat> polygon_offset { 0, 0 };
  GLenum polygon_mode { GL_FILL };
  GLint pixel_store_pack_alignment = 1;
  GLint pixel_store_unpack_alignment = 1;

  static GLenum getCapFromIndex(size_t index)
  {
    return cap_from_index.at(index);
  }

  static int getIndexFromCap(GLenum cap)
  {
    for (int i = 0; i < CapIndex::ENUM_SIZE; i++)
    {
      if (cap_from_index[i] == cap)
        return i;
    }
    return -1;
  }

private:
  constexpr static std::array<unsigned, CapIndex::ENUM_SIZE> cap_from_index =
  {
    GL_CULL_FACE,
    GL_BLEND,
    GL_DEPTH_TEST,
    GL_STENCIL_TEST,
    GL_ALPHA_TEST,
    GL_POLYGON_OFFSET_FILL,
    GL_POLYGON_OFFSET_LINE,
  };
};


struct StateModifier : public util::NonCopyable
{
  using CapIndex = State::CapIndex;

  StateModifier();
  ~StateModifier();

  void setPixelStorePackAlignment(GLint param)
  {
    set<wrapPixelStorePackAlignment>(m_current_state->pixel_store_pack_alignment, param);
  }

  void setPixelStoreUnpackAlignment(GLint param)
  {
    set<wrapPixelStoreUnpackAlignment>(m_current_state->pixel_store_unpack_alignment, param);
  }

  void setPolygonMode(GLenum mode)
  {
    set<wrapPolygonMode>(m_current_state->polygon_mode, mode);
  }

  void setPolygonOffset(GLfloat factor, GLfloat units)
  {
    set<gl_binding::state::PolygonOffset>(m_current_state->polygon_offset, factor, units);
  }

  void setBlendFunc(GLenum sfactor, GLenum dfactor)
  {
    set<gl_binding::state::BlendFunc>(m_current_state->blend_func, sfactor, dfactor);
  }

  void setAlphaFunc(GLenum func, GLclampf ref)
  {
    set<gl_binding::state::AlphaFunc>(m_current_state->alpha_func, func, ref);
  }

  void setProgram(unsigned program)
  {
    set<gl_binding::state::UseProgram>(m_current_state->program, program);
  }

  void setFrontFace(GLenum value)
  {
    set<gl_binding::state::FrontFace>(m_current_state->front_face, value);
  }

  void setCullFace(GLenum value)
  {
    set<gl_binding::state::CullFace>(m_current_state->cull_face, value);
  }

  void setDepthFunc(GLenum value)
  {
    set<gl_binding::state::DepthFunc>(m_current_state->depth_func, value);
  }

  void setDepthMask(bool value)
  {
    set<gl_binding::state::DepthMask>(m_current_state->depth_mask, value);
  }

  void enableCullFace(bool value)
  {
    enable<CapIndex::CULL_FACE>(value);
  }

  void enableBlend(bool value)
  {
    enable<CapIndex::BLEND>(value);
  }

  void enableDepthTest(bool value)
  {
    enable<CapIndex::DEPTH_TEST>(value);
  }

  void enableStencilTest(bool value)
  {
    enable<CapIndex::STENCIL_TEST>(value);
  }

  void enableAlphaTest(bool value)
  {
    enable<CapIndex::ALPHA_TEST>(value);
  }

  void enablePolygonOffsetFill(bool value)
  {
    enable<CapIndex::POLYGON_OFFSET_FILL>(value);
  }

  void enablePolygonOffsetLine(bool value)
  {
    enable<CapIndex::POLYGON_OFFSET_LINE>(value);
  }

  void enable(size_t index, bool value)
  {
    assert(isCurrent());
    auto cap = State::getCapFromIndex(index);
    if (m_current_state->caps.test(index) != value)
    {
      m_current_state->caps.set(index, value);
      if (value)
        gl_binding::state::Enable(cap);
      else
        gl_binding::state::Disable(cap);
    }
  }

  template <State::CapIndex::Enum I>
  void enable(bool value)
  {
    enable(I, value);
  }

  static void init();

  void setDefaults();
  const State& getState() { return *m_current_state; }

private:
  State *m_current_state = nullptr;
  const State *m_prev_state = nullptr;

  static void wrapPolygonMode(GLenum mode)
  {
    gl_binding::state::PolygonMode(GL_FRONT_AND_BACK, mode);
  }

  static void wrapPixelStorePackAlignment(GLenum param)
  {
    gl_binding::state::PixelStorei(GL_PACK_ALIGNMENT, param);
  }

  static void wrapPixelStoreUnpackAlignment(GLenum param)
  {
    gl_binding::state::PixelStorei(GL_UNPACK_ALIGNMENT, param);
  }

  template <auto Setter, typename T>
  void set(T& attr, T value)
  {
    assert(isCurrent());
    if (attr != value)
    {
      attr = value;
      Setter(value);
    }
  }

  template <auto Setter, typename T, typename... Values>
  void set(T& attr, Values... values)
  {
    assert(isCurrent());
    if (std::tie(values...) != attr)
    {
      attr = std::tie(values...);
      Setter(values...);
    }
  }

  bool isCurrent();
};


class StateStack : private std::stack<std::unique_ptr<State>>
{
  friend class StateModifier;

  const State* current()
  {
    assert(!empty());
    return top().get();
  }
};


} // namespace render_util

#endif

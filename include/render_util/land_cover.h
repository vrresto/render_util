#pragma once

#include <string>

namespace render_util
{


struct LandCoverType
{
  std::string name;
  std::string osm_key;
  std::vector<std::string> osm_values;
};


} // namespace render_util

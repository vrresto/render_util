/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TERRAIN_BASE_H
#define RENDER_UTIL_TERRAIN_BASE_H

#include <render_util/shader.h>
#include <render_util/camera.h>
#include <render_util/elevation_map.h>
#include <render_util/texture_util.h>
#include <render_util/terrain_resources.h>
#include <render_util/altitudinal_zone.h>
#include <factory.h>

#include <string>
#include <vector>
#include <utility>
#include <glm/glm.hpp>


namespace render_util
{


class Stats;
class TextureState;


class TerrainBase
{
public:
  enum LayerNumber : int
  {
    LAYER_DETAIL = 0,
    LAYER_BASE = 1
  };

  static constexpr int GRID_RESOLUTION_M = 200;
  static constexpr int TILE_SIZE_GRID = 8;
  static constexpr int TILE_SIZE_M = GRID_RESOLUTION_M * TILE_SIZE_GRID;

#if 0
  struct ModuleIndex
  {
    enum Enum
    {
      NONE, LAND, FOREST, WATER, RENDERER, MATERIAL_MAP_DEBUG
    };
    using AllValues = std::integer_sequence<Enum,
      NONE, LAND, FOREST, WATER, RENDERER, MATERIAL_MAP_DEBUG
    >;
    static constexpr auto ENUM_SIZE = AllValues::size();
  };
#else
  struct ModuleIndex
  {
    enum
    {
      NONE, LAND, FOREST, WATER, RENDERER, MATERIAL_MAP_DEBUG,
      ENUM_SIZE
    };
    using Enum = int;
    using AllValues = std::make_integer_sequence<int, ENUM_SIZE>;
  };
#endif

  template <ModuleIndex::Enum I>
  struct ModuleMaskFromIndex
  {
    static_assert(I > 0);
    static_assert(I < ModuleIndex::ENUM_SIZE);
    static constexpr auto VALUE = 1 << (I-1);
  };

  template <ModuleIndex::Enum I>
  static constexpr auto MODULE_MASK = ModuleMaskFromIndex<I>::VALUE;

  struct ModuleMask
  {
    static constexpr int LAND = MODULE_MASK<ModuleIndex::LAND>;
    static constexpr int FOREST = MODULE_MASK<ModuleIndex::FOREST>;
    static constexpr int WATER = MODULE_MASK<ModuleIndex::WATER>;
    static constexpr int RENDERER = MODULE_MASK<ModuleIndex::RENDERER>;
    static constexpr int MATERIAL_MAP_DEBUG = MODULE_MASK<ModuleIndex::MATERIAL_MAP_DEBUG>;

    static constexpr int ALL = LAND | FOREST | WATER | RENDERER | MATERIAL_MAP_DEBUG;
  };

  struct MaterialID // FIXME rename MaterialOption or MaterialOptions
  {
    static constexpr unsigned int LAND = 1;
    static constexpr unsigned int WATER = 1 << 1;
    static constexpr unsigned int FOREST = 1 << 2;
    static constexpr unsigned int WATER_MAP = 1 << 3;
    static constexpr unsigned int ALL = LAND | WATER | FOREST | WATER_MAP;
  };

  struct BuildParameters
  {
    TerrainResourcesBase& resources;
    const ShaderParameters &shader_parameters;
    int load_modules = 0;
    int enabled_modules = 0;
    bool use_material_map = true;
  };

  using TextureStateObserver = std::function<void(TextureState&)>;
  using UniformUpdater = std::function<void(ShaderProgram&)>;

  static int getModuleMask(ModuleIndex::Enum index);
  static const std::string getModuleName(ModuleIndex::Enum index);
  static ModuleIndex::Enum getModuleIndex(const std::string& name);
  static std::vector<std::string> getModuleNames();

  virtual ~TerrainBase() {}

  virtual void setTextureStateCreateObserver(TextureStateObserver) = 0;

  virtual void build(BuildParameters&) = 0;
  virtual void draw(const Camera&, const UniformUpdater&) = 0;

  virtual void enableBaseLayer(bool) = 0;
  virtual bool isBaseLayerEnabled() = 0;

  virtual void update(const Camera &camera, bool low_detail) = 0;
  virtual void setDrawDistance(float dist) = 0;
  // virtual std::vector<glm::vec3> getNormals() = 0;
  virtual TexturePtr getNormalMapTexture() = 0;
  virtual void setProgramSuffix(std::string) = 0;

  virtual void setBaseLayerTypeMapping(const std::vector<int>&) = 0;
  virtual void setAltitudinalZones(const std::vector<AltitudinalZone>&) = 0;
  virtual void setLayerOrigin(int layer, glm::vec3 origin) = 0;
  virtual void setLayerScale(int layer, float scale) = 0;

  virtual void setMapTextureOffset(int layer, glm::vec2) = 0;
  virtual glm::vec2 getMapTextureOffset(int layer) = 0;

  virtual void reloadShaders() = 0;
  virtual bool hasError() = 0;
  virtual std::vector<std::string> getErrors() = 0;
  virtual void setShaderParameters(const ShaderParameters&) = 0;
  virtual void updateAnimation(float frame_delta) = 0;
  virtual void setAnisotropy(int) = 0;
  virtual void setLodDistanceScale(float) = 0;
  virtual float getLodDistanceScale() = 0;
  virtual void getStats(Stats&) = 0;
  virtual int getEnabledModules() = 0;
  virtual void setEnabledModules(int mask) = 0;
  virtual int getRenderGroups() = 0;
  virtual bool isRenderGroupEnabled(int pass) = 0;
  virtual void enableRenderGroup(int pass, bool enable) = 0;

  virtual std::vector<ShaderCreationError> getShaderErrors() = 0;
};


template <>
struct TerrainBase::ModuleMaskFromIndex<TerrainBase::ModuleIndex::NONE>
{
  static constexpr auto VALUE = 0;
};


} // namespace render_util

#endif

/**
 *    Rendering utilities
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TEXTURE_ARRAY_H
#define RENDER_UTIL_TEXTURE_ARRAY_H

#include <render_util/texture_util.h>
#include <render_util/image.h>
#include <log.h>


namespace render_util
{


class TextureArrayBase : public Texture
{
  int m_num_textures {};
  glm::ivec2 m_texture_size {};
  int m_format {};
  int m_internal_format {};
  int m_type {};

protected:
  void setTextureImage(int index, const void* data);

public:
  TextureArrayBase(int num_textures, glm::ivec2 texture_size, int format,
                  int internal_format, int type);

  glm::ivec2 getTextureSize() { return m_texture_size; }

  void generateMipMap();
};


template <typename ComponentType = uint8_t>
class BasicTextureArray : public TextureArrayBase
{
  int m_num_components {};

  size_t getTextureDataSize()
  {
    return getTextureSize().x * getTextureSize().y *
          m_num_components * sizeof(ComponentType);
  }

public:
  using FormatParams = typename texture_format::TextureFormatParameters<ComponentType>;

  BasicTextureArray(int num_textures, glm::ivec2 texture_size, int num_components) :
    TextureArrayBase(num_textures, texture_size,
                    FormatParams::getFormat(num_components),
                    FormatParams::getInternalFormat(num_components),
                    FormatParams::getType()),
    m_num_components(num_components) {}

  template <class ImageType>
  void setTextureImage(int index, const ImageType& image)
  {
    static_assert(std::is_same<ComponentType, typename ImageType::ComponentType>::value);
    assert(image.size() == getTextureSize());
    assert(image.numComponents() == m_num_components);
    assert(getTextureDataSize() == image.getDataSize());
    TextureArrayBase::setTextureImage(index, image.getData());
  }
};


template <typename T, class ImageResourceList>
std::unique_ptr<BasicTextureArray<T>> createTextureArray(const ImageResourceList& src)
{
  auto& first = src.at(0);
  assert(first);

  auto texture = std::make_unique<BasicTextureArray<T>>(src.size(),
                                                        first->getSize(),
                                                        first->getNumComponents());

  int i = 0;
  for (auto& res : src)
  {
    assert(res);

    auto image = res->load(0, first->getNumComponents());
    assert(image);
    assert(image->getSize() == first->getSize());

    texture->setTextureImage(i, *image);

    i++;
  }

  texture->generateMipMap();

  return texture;
}


using TextureArray = BasicTextureArray<uint8_t>;


} // namespace render_util

#endif

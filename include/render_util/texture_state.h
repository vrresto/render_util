/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_TEXTURE_STATE_H
#define RENDER_UTIL_TEXTURE_STATE_H

#include <util/non_copyable.h>

#include <unordered_map>
#include <vector>
#include <string>


namespace render_util
{


class ShaderProgram;
class Texture;


class TextureState : public util::NonCopyable
{
  friend class TextureStateBinding;

  struct TexUnit
  {
    std::string uniform;
    unsigned texture {};
  };

  unsigned m_max_texunits {};
  std::unordered_map<std::string, unsigned> m_texunit_for_uniform;
  std::vector<TexUnit> m_texunits;
  bool m_is_bound = false;

  void bind();
  void unbind();

  TextureState::TexUnit& getTexUnitForUniform(const std::string uniform);

public:
  TextureState();
  ~TextureState();

  void bind(const std::string& uniform, Texture& texture);
  void setSamplerUniforms(ShaderProgram&) const;
  void dump() const;
};


class TextureStateBinding : public util::NonCopyable
{
  TextureState& m_state;

public:
  TextureStateBinding(TextureState& state);
  ~TextureStateBinding();
};


} // namespace render_util

#endif

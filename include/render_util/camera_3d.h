/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_CAMERA_3D_H
#define RENDER_UTIL_CAMERA_3D_H

#include <render_util/camera.h>

#include <memory>

namespace render_util
{
  class Camera3D : public Camera
  {
    struct Private;
    std::unique_ptr<Private> p;

  public:
//     using Mat4 = glm::dmat4;
//     using Vec3 = glm::dvec3;
//     using Vec4 = glm::dvec4;
//     using Unit = double;


    Camera3D();
    Camera3D(const Camera3D &other);
    Camera3D &operator=(const Camera3D &other);
    ~Camera3D();

    Unit getFov() const;
    Unit getZNear() const;
    Unit getZFar() const;
    const Mat4 &getWorldToViewRotationD() const override;
    const Mat4 &getView2WorldMatrixD() const override;
    const Mat4 &getWorld2ViewMatrixD() const override;
    const Mat4 &getProjectionMatrixFarD() const override;
    const Vec3 &getPosD() const override;
    const glm::ivec2 &getViewportSize() const override;
    bool cull(const Box &box) const override;
    void setViewportSize(int width, int height) override;
    Ray castRayThroughViewportCoords(glm::vec2) const;

    bool cullSphere(Vec3 pos, Unit radius) const;

#if 0
    glm::mat4 getWorldToViewRotation() const
    {
      return glm::mat4(getWorldToViewRotationD());
    }

    Mat4 getVP() const
    {
      return getProjectionMatrixFarD() * getWorld2ViewMatrixD();
    }

    glm::mat4 getVP_s() const
    {
      return glm::mat4(getVP());
    }

    const glm::mat4 getView2WorldMatrix() const
    {
      return glm::mat4(getView2WorldMatrixD());
    }

    const glm::mat4 getWorld2ViewMatrix() const
    {
      return glm::mat4(getWorld2ViewMatrixD());
    }

    const glm::mat4 getProjectionMatrixFar() const
    {
      return glm::mat4(getProjectionMatrixFarD());
    }

    const glm::vec3 getPos() const
    {
      return glm::vec3(getPosD());
    }
#endif

    const glm::vec2 &getNDCToView() const;
    void setTransform(Unit x, Unit y, Unit z, Unit yaw, Unit pitch, Unit roll);
    void setFov(Unit fov);
    void setProjection(Unit fov, Unit z_near, Unit z_far);
  };

}

#endif

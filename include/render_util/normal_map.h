/**
 *    Rendering utilities
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <render_util/geometry.h>

#include <glm/glm.hpp>


namespace render_util
{


enum class GridMeshTriangleArrangement
{
  TOP_LEFT_BOTTOM_RIGHT,
  BOTTOM_LEFT_TOP_RIGHT
};


template <class T_in, class T_out>
void computeNormalMap_2(glm::ivec2 size, const T_in& get_height, T_out set_normal,
                        bool tiled = false)
{
  using namespace glm;

  GridMeshTriangleArrangement triangle_arrangement =
    GridMeshTriangleArrangement::TOP_LEFT_BOTTOM_RIGHT;

  struct Triangle
  {
    std::array<glm::ivec2, 3> vertex_coords;

    Triangle operator+ (ivec2 offset) const
    {
      Triangle t = *this;
      t.vertex_coords[0] += offset;
      t.vertex_coords[1] += offset;
      t.vertex_coords[2] += offset;
      return t;
    }

    Triangle operator- (ivec2 offset) const
    {
      return *this + -offset;
    }
  };

  const Triangle top_left =
  {
    {
      ivec2(0,0),
      ivec2(1,1),
      ivec2(0,1),
    }
  };

  const Triangle bottom_right =
  {
    {
      ivec2(0,0),
      ivec2(1,0),
      ivec2(1,1),
    }
  };

  // adjacent triangles
  std::array<Triangle, 6> triangles;
  if (triangle_arrangement == GridMeshTriangleArrangement::TOP_LEFT_BOTTOM_RIGHT)
  {
    triangles =
    {
      top_left,
      bottom_right,

      bottom_right - ivec2(1,0),

      top_left - ivec2(1),
      bottom_right - ivec2(1),

      top_left - ivec2(0,1),
    };
  }
  else
  {
    abort();
  }

  for (int y = 0; y < size.y; y++)
  {
    for (int x = 0; x < size.x; x++)
    {
      ivec2 vertex_coords(x,y);
      vec3 normal(0);

      // get average of triangle normals
      for (auto &triangle : triangles)
      {
        // get vertices
        vec3 vertices[3];
        for (unsigned int i = 0; i < 3; i++)
        {
          auto coords = vertex_coords + triangle.vertex_coords[i];
          auto& v = vertices[i];

          v.x = coords.x;
          v.y = coords.y;

          if (tiled)
            coords = (size + coords) % size;
          else
            coords = clamp(coords, ivec2(0), size-1);

          v.z = get_height(coords);
        }

        normal += calcNormal(vertices);
      }

      normal = normalize(normal);

      set_normal(x, y, normal);
    }
  }
}


template <typename T>
struct NormalMapCreatorIO
{
};


template <>
struct NormalMapCreatorIO<float>
{
  static float pixelToHeight(float height)
  {
    return height;
  }

  static glm::vec3 normalToPixel(glm::vec3 normal)
  {
    return normal;
  }
};


template <>
struct NormalMapCreatorIO<uint8_t>
{
  static float pixelToHeight(uint8_t height)
  {
    return height / 255.f;
  }

  static glm::uvec3 normalToPixel(glm::vec3 normal)
  {
    return ((normal + 1.f) / 2.f) * 255.f;
  }
};


template <class T_src, class T_dst>
void createNormalMap(const T_src& height_map,
                     T_dst& normal_map_out,
                     float grid_scale,
                     float height_scale,
                     bool tiled = false)
{
  using Input = NormalMapCreatorIO<typename T_src::ComponentType>;
  using Output = NormalMapCreatorIO<typename T_dst::ComponentType>;

  auto get_height = [&] (auto& coords)
  {
    return (Input::pixelToHeight(height_map.get(coords)) * height_scale) / grid_scale;
  };

  auto set_normal = [&normal_map_out] (auto& x, auto& y, auto& normal)
  {
    glm::vec2 coords(x,y);
    auto pixel = Output::normalToPixel(normal);

    normal_map_out.at(coords, 0) = pixel.x;
    normal_map_out.at(coords, 1) = pixel.y;
    normal_map_out.at(coords, 2) = pixel.z;
  };

  computeNormalMap_2(normal_map_out.size(), get_height, set_normal, tiled);
}


template <class T_dst>
void createNormalMapFromFloat(const ElevationMap& height_map,
                              T_dst& normal_map_out,
                              float grid_scale,
                              bool tiled = false)
{
  createNormalMap(height_map, normal_map_out, grid_scale, 1.f, tiled);
}


template <class T_src, class T_dst>
void createNormalMapFromUint8(const T_src& height_map,
                              T_dst& normal_map_out,
                              float max_height_m,
                              float height_map_width_m,
                              bool tiled = false)
{
  static_assert(std::is_same<typename T_src::ComponentType, uint8_t>::value);

  createNormalMap(height_map, normal_map_out,
                  height_map_width_m / height_map.w(),
                  max_height_m, tiled);
}


} // namespace render_util

/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H
#define UTIL_H

#include <log.h>

#include <memory>
#include <string>
#include <vector>
#include <variant>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <cassert>


#define UNIMPLEMENTED util::unimplemented(__PRETTY_FUNCTION__, __FILE__, __LINE__);


namespace util
{


[[ noreturn ]] void unimplemented(const char* function, const char* file, int line);


constexpr auto PI = 3.1415926535897932;


template <typename T>
using unique_ptr_vector = std::vector<std::unique_ptr<T>>;


template <typename T>
T limitDecimalPlaces(T value, int places)
{
  return std::floor(value * std::pow(10, places)) / std::pow(10, places);
}


template <typename T>
typename std::underlying_type<T>::type to_underlying(T value)
{
  return typename std::underlying_type<T>::type(value);
}


inline bool isPrefix(const std::string &prefix, const std::string &s)
{
  return s.compare(0, prefix.size(), prefix) == 0;
}


inline bool isSuffix(const std::string &suffix, const std::string &s)
{
  if (s.size() >= suffix.size())
  {
    return s.compare(s.size() - suffix.size(), suffix.size(), suffix) == 0;
  }
  else
  {
    return false;
  }
}


inline std::string stripSuffix(const std::string& suffix, const std::string str)
{
  if (!isSuffix(suffix, str))
    throw std::runtime_error(suffix + " is not a suffix of " + str);
  return str.substr(0, str.size() - suffix.size());
}


inline bool isNullOrWhiteSpace(const std::string &s)
{
  return s.find_first_not_of(" \t\n\r") == std::string::npos;
}


inline std::string trim(const std::string &in)
{
  size_t start = in.find_first_not_of(" \t\n\r");
  if (start == std::string::npos)
    return {};

  size_t end = in.find_last_not_of(" \t\n\r");
  assert(end != std::string::npos);

  if (start >= end+1)
  {
    LOG_INFO<<"start: "<<start<<", end: "<<end<<std::endl;
  }

  assert(start < end+1);

  size_t len = end+1 - start;
  assert(len);

  return in.substr(start, len);
}


inline std::vector<std::string> tokenize(std::string in, const std::string &separators = " \t\n\r")
{
  std::vector<std::string> out;

  for (size_t token_start = 0, token_len = 0; token_start < in.size(); )
  {
    size_t token_end = token_start + token_len;

    if (token_end >= in.size())
    {
      if (token_len)
      {
        std::string token = in.substr(token_start);
        out.push_back(std::move(token));
      }
      break;
    }
    else if (separators.find(in[token_end]) != std::string::npos)
    {
      if (token_len)
      {
        std::string token = in.substr(token_start, token_len);
        out.push_back(std::move(token));
      }

      token_start = token_end + 1;
      token_len = 0;
    }
    else
    {
      token_len++;
    }
  }

  return out;
}


template <class T>
std::string joinStrings(const T& strings, const std::string& separator)
{
  if (strings.empty())
    return {};

  size_t joined_size = 0;

  for (auto& s : strings)
    joined_size += s.size();

  joined_size += (strings.size() - 1) * separator.size();

  std::string joined;
  joined.reserve(joined_size);

  for (auto it = strings.begin(); it != strings.end(); it++)
  {
    if (it != strings.begin())
      joined += separator;
    joined += *it;
  }

  return joined;
}


inline std::vector<std::string> tokenize(std::string in, char separator)
{
  return tokenize(in, std::string {separator});
}


inline std::string makeLowercase(const std::string &input)
{
  std::string output = input;
  for(size_t i = 0; i < output.size(); i++)
  {
    output[i] = tolower(output[i]);
  }
  return output;
}


inline std::string getDirFromPath(const std::string &path)
{
  std::string dir;
  size_t pos = path.find_last_of("/\\");
  if (pos != std::string::npos)
  {
    if (pos < path.size() && pos > 0)
      dir = path.substr(0, pos);
  }

  return dir;
}


inline std::string getFileExtensionFromPath(const std::string &path)
{
  auto pos = path.find_last_of('.');
  if (pos != std::string::npos)
    return path.substr(pos + 1, std::string::npos);
  else
    return {};
}


inline std::string basename(std::string path, bool remove_extension = false)
{
  size_t pos = path.find_last_of("/\\");
  if (pos != std::string::npos)
  {
    if (pos < path.size())
      path = path.substr(pos + 1);
    else
      path = {};
  }

  if (remove_extension)
    path = path.substr(0, path.find_last_of('.'));

  return path;
}


inline std::string resolveRelativePathComponents(const std::string &path)
{
  auto components = tokenize(path, "/\\");
  assert(!components.empty());

  std::string resolved;

  bool starts_withg_slash = isPrefix("/", path);

  std::vector<std::string> new_components;

  for (auto &component : components)
  {
    if (component == "..")
    {
      assert(!new_components.empty());
      new_components.pop_back();
    }
    else
    {
      new_components.push_back(component);
    }
  }

  assert(!new_components.empty());

  for (auto &component : new_components)
  {
    if (starts_withg_slash || !resolved.empty())
      resolved += '/';
    resolved += component;
  }

  return resolved;
}


template <typename T>
std::string toString(const T& variant)
{
  std::string as_string;

  auto visitor = [&] (auto&& value)
  {
    using Type = std::decay_t<decltype(value)>;
    if constexpr (std::is_same<Type, std::string>::value)
      as_string = value;
    else
      as_string = std::to_string(value);
  };
  std::visit(visitor, variant);

  return as_string;
}


template <typename T>
inline std::vector<T> readFile(const std::string &path)
{
  static_assert(sizeof(T) == 1);
  using namespace std;

  bool success = false;

  std::vector<T> content;

  std::ifstream file(path, ios_base::binary);
  if (file.good())
  {
    file.seekg (0, file.end);
    size_t size = file.tellg();
    file.seekg (0, file.beg);
    content.resize(size);
    file.read(reinterpret_cast<char*>(content.data()), size);

    if (file.good())
      success = true;
  }

  if (!success)
  {
    throw std::runtime_error("Failed to read " + path);
  }

  return content;
}


template <typename T>
inline bool readFile(const std::string &path, T& content, bool quiet = false)
{
  using ElementType = typename std::remove_const<typename T::value_type>::type;
  static_assert(sizeof(ElementType) == 1);

  using namespace std;

  bool success = false;

  std::ifstream file(path, ios_base::binary);
  if (file.good())
  {
    file.seekg (0, file.end);
    size_t size = file.tellg();
    file.seekg (0, file.beg);
    content.resize(size);
    file.read(reinterpret_cast<char*>(content.data()), size);

    if (file.good())
      success = true;
  }

  if (!success)
  {
    if (!quiet)
      LOG_ERROR << "Failed to read " << path << endl;
    else
      LOG_TRACE << "Failed to read " << path << endl;
  }

  return success;
}


inline bool writeFile(const std::string &path, const char *data, size_t data_size)
{
  using namespace std;

  std::ofstream out(path, ios_base::binary | ios_base::trunc);
  if (!out.good()) {
    LOG_ERROR<<"can't open output file "<<path<<endl;
    return false;
  }

  assert(out.tellp() == 0);

  out.write(data, data_size);

  size_t size = out.tellp();
//   LOG_TRACE<<"data_size:"<<data_size<<endl;
//   LOG_TRACE<<"size:"<<size<<endl;
  assert(data_size == size);

  if (!out.good()) {
    LOG_ERROR<<"error during writing to output file "<<path<<endl;
    return false;
  }

  return true;
}


void mkdir(const std::string &path, bool recursive = false);
bool fileExists(std::string path);

std::string makeTimeStampString();


} // namespace util

#endif
